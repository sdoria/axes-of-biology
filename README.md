# **The axes of biology: a novel axes-based network embedding paradigm to decipher the functional mechanisms of the cell**
- Doria-Belenguer S.
- Xenos A.
- Ceddia G. 
- Malod-Dognin N.
- Przulj N.

## **Overview of the project**

Common approaches for deciphering biological networks involve network embedding algorithms. These approaches strictly focus on clustering the genes' embedding vectors and interpreting such clusters to reveal the hidden information of the networks. However, the difficulty in interpreting the genes' clusters and the limitations of the functional annotations’ resources hinder the identification of the currently unknown cell's functioning mechanisms.
We propose a new approach that shifts this functional exploration from the embedding vectors of genes in space to the axes of the space itself. Our methodology better disentangles biological information from the embedding space than the classic gene-centric approach. Moreover, it uncovers new data-driven functional interactions that are unregistered in the functional ontologies, but biologically coherent. Furthermore, we exploit these interactions to define new higher-level annotations that we term Axes-Specific Functional Annotations and validate them through literature curation. Finally, we leverage our methodology to discover evolutionary connections between cellular functions and the evolution of species.

## **Description of the respository**

This repository has been created to present our recent work that innovatively uses the axes of PPI network embedding spaces to uncover the cell's functional organization.

This repository contains all scripts and input data required to replicate the analysis presented in the study.

In order to replicate the results, please use the Species_Complete_Main.py

## **Updates**

Bugs/code upload incompleteness was updated on 15/05/2024.

