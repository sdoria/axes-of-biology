## Terminal Paralelization for Server:

'''
This code take the user argument and run the corresponding function.

usage:
    
    Marenostrum -dimensions -matrix  -species -permutations.
    
    - dimensions   : number of dimensions.
    - matrix       : embedding algorithm.
    - species      : organism.
    - permutations : number of permutations per each.
    - name         : name of the file (no avoid overwritting)
'''

# Packages:

import argparse

# Our Packages:

import Marenostrum_Functions

# Parser Arguments from the command line:

parser = argparse.ArgumentParser()

parser.add_argument("-dimensions", '--dimensions', help="number of dimensions",
                    type=int, required=False)
parser.add_argument("-matrix", '--matrix', help="matrix path",
                    type=str, required=False)
parser.add_argument("-permutations", '--permutations', help="number of permutations",
                    type=int, required=False)
parser.add_argument("-species", '--species', help="specie name",
                    type=str, required=True)
parser.add_argument("-name", '--name', help="name",
                    type=str, required=False)
parser.add_argument("-name2", '--name2', help="name",
                    type=str, required=False)
parser.add_argument("-job", '--job', help="job name",
                    type=str, required=False)
parser.add_argument("-orthonormal", '--orthonormal', help="orthonormality of the axes",
                    type=str, required=False)
parser.add_argument("-annotation", '--annotation', help="set of annotations",
                    type=str, required=False)
parser.add_argument("-axis1", '--axis1', help="from",
                    type=str, required=False)


args = parser.parse_args()

# Run the permutations:

if args.job == "Permutations":       
    Marenostrum_Functions.Run_Permutations(args.species, args.dimensions, args.matrix, args.permutations, args.name, args.orthonormal, args.annotation)
elif args.job == "Sum":
    Marenostrum_Functions.Sum_Permutations(args.species, args.dimensions, args.matrix, args.permutations, args.orthonormal, args.annotation)
elif args.job == "Jobs":
    Marenostrum_Functions.generate_jobs(args.species, args.dimensions, args.matrix, args.permutations, args.orthonormal)
elif args.job == "Sum_Jobs":
    Marenostrum_Functions.generate_jobs_Sum(args.species)
elif args.job == "NMTF":
    Marenostrum_Functions.NMTF(args.species, args.dimensions, args.orthonormal, args.matrix)
elif args.job == "NMTF_jobs":
    Marenostrum_Functions.generate_NMTF_jobs(args.species, args.orthonormal, args.matrix)
elif args.job == "GO_Emb":
    Marenostrum_Functions.Embedd_GO_terms(args.matrix, args.species, args.dimensions, args.annotation, args.orthonormal)
elif args.job == "GO_Emb_jobs":
    Marenostrum_Functions.generate_Embedd_GO_jobs(args.matrix, args.species, args.orthonormal)    
elif args.job == "Intra_Jobs":
    Marenostrum_Functions.generate_Calculate_Intra_Inter_Axes(args.species) 
elif args.job == "Intra":
    Marenostrum_Functions.Calculate_Intra_Inter_Axes(args.species, args.matrix, args.annotation)
elif args.job == "Perm_Distance": 
    Marenostrum_Functions.Permutations_Distances(args.species, args.matrix, args.dimensions, args.annotation, args.permutations, args.name, args.name2)
elif args.job == "Perm_Distance_Jobs": 
     Marenostrum_Functions.Permutations_Distances_Jobs(args.species, args.dimensions, args.matrix, args.permutations)
elif args.job == "Sum_Distances":
    Marenostrum_Functions.Sum_Permutations_Dist(args.species, args.matrix, args.dimensions, args.annotation, args.permutations)
elif args.job == "Sum_Distances_Job":
    Marenostrum_Functions.Sum_Permutations_Dist_Job(args.species, args.matrix, args.dimensions, args.annotation, args.permutations)
elif args.job == "TF_IDF_Jobs": 
     Marenostrum_Functions.TF_IDF_Jobs(args.species, args.dimensions, args.matrix, args.annotation)     
elif args.job == "TF_IDF_Run":  
    Marenostrum_Functions.TF_IDF_Abstract(args.species, args.dimensions, args.matrix, args.annotation, args.name, args.name2, args.axis1) 
elif args.job == "NCBI_TF_IDF_Jobs": 
     Marenostrum_Functions.NCBI_TF_IDF_Jobs(args.species, args.dimensions, args.matrix, args.annotation)        
elif args.job == "NCBI_TF_IDF_Run":  
    Marenostrum_Functions.TF_IDF_NCBI(args.species, args.dimensions, args.matrix, args.annotation, args.name, args.name2, args.axis1)  










    

