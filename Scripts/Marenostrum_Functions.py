import os
import json
import random
import numpy as np
import pandas as pd
from scipy.linalg import svd
from sklearn.metrics import pairwise_distances

def Run_Permutations(organism, optimal_dimension, matrix, times, name, orthonormal, annotations):
    
    # Paths:
    
    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/"    
    Annotation_path    = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/"
    GO_embeddings_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    network_path       = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
       
    # Get the specie information:
      
    # Check if a folder exist:
    
    if orthonormal == "True":
        label = ""
    else:
        label = "_No_Orth"
        
    path_it = f"{permutation_path}Permutations_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}/"       
    os.makedirs(path_it, exist_ok=True)
    
    # Load Genes embeddings:
    
    if matrix == "PPMI":
        if orthonormal == "True":
            path = f'{network_path}G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)
        else:
            path = f'{network_path}No_Orth_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)            
    elif matrix == "Adj":
        if orthonormal == "True":
            path = f'{network_path}G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)
        else:
            path = f'{network_path}No_Orth_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)                        
    elif matrix == "node2vec":
        path = f'{network_path}node2vec_array_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True)
    elif matrix == "LINE":
        path = f'{network_path}Line_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
    elif matrix == "Deepwalk":
        path = f'{network_path}Deepwalk_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
        
    # Load the annotations:
    
    if annotations == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)  

    # Load the genes and reorder the annotations accordingly:        
          
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]  
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
    
    # Load the GO embeddings (Orthonormal or not for NMTF):
    
    if orthonormal == "True":
    
        GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{organism}_{optimal_dimension}_{matrix}.csv', 
                                index_col = 0)
        GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
        
    elif orthonormal == "False":
        GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{organism}_{optimal_dimension}_No_Orth_{matrix}.csv', 
                                index_col = 0)
        GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
        
    # Start the permutations
                                
    Count_db = pd.DataFrame(0, columns=GO_embeddings.columns, index = GO_embeddings.index)

    # Keep the names:
    
    index_orig   = GO_Matrix_filt.index
    columns_orig = GO_Matrix_filt.columns
    
    # Init the permutation test for N times:
       
    for rep in range(times):
        
        # Randomly suffle the Gene/GO matrix:
            
        Gene_GO_matrix_it = GO_Matrix_filt.sample(frac=1).reset_index(drop=True)
        Gene_GO_matrix_it.index = index_orig
        Gene_GO_matrix_it.columns = columns_orig
            
        # Do the Embeddings using the suffled Gene/GO matrix (direct calculation):
        
        gene_embeddings_db_inverse = pd.DataFrame(np.linalg.pinv(G_sp) , columns = index_orig)
        GO_emebddings_it = gene_embeddings_db_inverse.dot(Gene_GO_matrix_it)

        # Compare the new scores vs the original scores:
            
        comparison = GO_emebddings_it >= GO_embeddings
            
        # Update the counts:
            
        Count_db = Count_db + comparison
        
    # Finished:
    
    # Save the matrix in a folder with the names:
    
    Count_db.to_csv(f'{path_it}/P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{name}.csv', 
                          header = True, index=True)
    
def Sum_Permutations(organism, optimal_dimension, matrix, times, orthonormal, annotations):
    
    # Generic path:
    
    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/"
    
    if orthonormal == "True":
        label = ""
    else:
        label = "_No_Orth"
    
    # Count the number of files in the directory:
    
    directory = f'{permutation_path}Permutations_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}/'   
    number    = len(os.listdir(directory))
    
    # Load all the files in the folder:
    
    Final = pd.DataFrame()

    for i in range(number):
        
        if i == 0: 
            permutations_it   = pd.read_csv(f'{directory}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{int(i)}.csv',index_col = 0, dtype={0: str})
            Final = permutations_it

        else:
            permutations_it   = pd.read_csv(f'{directory}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{int(i)}.csv',index_col = 0, dtype={0: str})
            Final = Final + permutations_it
            
    # Sum all the permutations in the list:
    
    Total_counts = times * number
    
    # Calculate the p-values:
    
    Final = (Final + 1)/(Total_counts + 1)
        
    # Save the final total permutations outside of the folder:
     
    Final.to_csv(f'{permutation_path}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{Total_counts}.csv', 
                          header = True, index=True)   
                
def generate_jobs(organism, optimal_dimension, matrix, times, orthonormal):

    # cd /gpfs/projects/bsc79/bsc79321/Axes_Species/
    # Generic path:

    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/" 
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/" 
    
    if orthonormal == "True":
        label = ""
    else:
        label = "_No_Orth"
    
    # Split tasks:
    
    permutations = int(times/800) 
    tasks_n      = int(times/permutations)
    
    # Start to write the resources file:    
    
    with open(f'{permutation_path}Resources_{organism}_{optimal_dimension}_{matrix}{label}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=50\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n") # Normal queue
        #the_file.write("#SBATCH --qos=debug\n") # Debug queue
                       
        the_file.write("#SBATCH --cpus-per-task=16\n") # max 16 in nord3 / max 48 in MN4
                               
        # Time limit (the ones with 1000 takes more time):
        
        if optimal_dimension == 1000:
            the_file.write("#SBATCH --time=20:00:00\n") # 10
        else:
            the_file.write("#SBATCH --time=20:00:00\n")
                                   
        the_file.write("\n")
        the_file.write("\n")
                       
        #the_file.write("module purge && module load intel impi greasy mkl python/3.7.4\n")   # Marenostrum
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n")   # Nord3        
        
        the_file.write(f'FILE={permutation_path}Tasks_{organism}_{optimal_dimension}_{matrix}{label}.sh\n') 
        
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                
        #the_file.write(f'/apps/GREASY/2.2/INTEL/IMPI/bin/greasy $FILE\n') # Marenostrum
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
        
        
    the_file.close()
    
    # Start to write the task file:
    
    with open(f'{permutation_path}Tasks_{organism}_{optimal_dimension}_{matrix}{label}.txt', 'a') as the_file:
        
        for i in range(tasks_n):           
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix {matrix} -species {organism} -permutations {str(permutations)} -job Permutations -name {str(i)} -orthonormal {orthonormal} -annotation BP_Back\n')
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix {matrix} -species {organism} -permutations {str(permutations)} -job Permutations -name {str(i)} -orthonormal {orthonormal} -annotation BP_Leaf\n')

    the_file.close()
    
    # Change the name of the file:

    old_name = f'{permutation_path}Tasks_{organism}_{optimal_dimension}_{matrix}{label}.txt'
    new_name = f'{permutation_path}Tasks_{organism}_{optimal_dimension}_{matrix}{label}.sh'

    # Renaming the file
    os.rename(old_name, new_name)
    

def generate_jobs_Sum(organism):

    # Generic path:

    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/" 
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/" 
        
    # Start to write the resources file:    
    
    with open(f'{permutation_path}Sum_Resources_{organism}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=50\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n")                     
        the_file.write("#SBATCH --cpus-per-task=16\n")
        
        # Time limit:

        the_file.write("#SBATCH --time=20:00:00\n")

        the_file.write("\n")
        the_file.write("\n")      
                
        #the_file.write("module purge && module load intel impi greasy mkl python/3.7.4\n")  # Marenostrum
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n") # Nord3      
         
        the_file.write(f'FILE={permutation_path}Sum_Tasks_{organism}.sh\n')         
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                       
        #the_file.write(f'/apps/GREASY/2.2/INTEL/IMPI/bin/greasy $FILE\n')  # Marenostrum
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
        
        
    the_file.close()
    
    if organism == "Human":
        tt = 125
    else:
        tt = 75
    
    # Start to write the task file (in this case I prepare the code for human if not change the 125 to 75):
    
    with open(f'{permutation_path}Sum_Tasks_{organism}.txt', 'a') as the_file:
        for optimal_dimension in [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]:        
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix PPMI -species {organism} -permutations {tt} -orthonormal True -annotation BP_Back -job Sum \n')          
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix PPMI -species {organism} -permutations {tt} -orthonormal True -annotation BP_Leaf -job Sum \n')  

            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix PPMI -species {organism} -permutations {tt} -orthonormal False -annotation BP_Back -job Sum \n')          
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix PPMI -species {organism} -permutations {tt} -orthonormal False -annotation BP_Leaf -job Sum \n')  

            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Adj -species {organism} -permutations {tt} -orthonormal True -annotation BP_Back -job Sum \n')          
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Adj -species {organism} -permutations {tt} -orthonormal True -annotation BP_Leaf -job Sum \n')  

            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Adj -species {organism} -permutations {tt} -orthonormal False -annotation BP_Back -job Sum \n')          
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Adj -species {organism} -permutations {tt} -orthonormal False -annotation BP_Leaf -job Sum \n')  

            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix LINE -species {organism} -permutations {tt} -orthonormal True -annotation BP_Back -job Sum \n') 
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix LINE -species {organism} -permutations {tt} -orthonormal True -annotation BP_Leaf -job Sum \n')
            
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Deepwalk -species {organism} -permutations {tt} -orthonormal True -annotation BP_Back -job Sum \n') 
            the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix Deepwalk -species {organism} -permutations {tt} -orthonormal True -annotation BP_Leaf -job Sum \n')                 
    the_file.close()
    
    # Change the name of the file:
    
    old_name = f'{permutation_path}Sum_Tasks_{organism}.txt'
    new_name = f'{permutation_path}Sum_Tasks_{organism}.sh'

    # Renaming the file
    os.rename(old_name, new_name)


class SNMTF(object):
    """Compute Partial Orthonormal Non-negative Matrix Tri-Factorization (NMTF) Embeddings"""
    def __init__(self, max_iter=1000, verbose = 10):

        super(SNMTF,self).__init__()
        self.max_iter = max_iter
        self.verbose = verbose


    def Score(self, R1, P, U, G, norm_R1):
        GT = np.transpose(G)

        ErR1 = R1 - np.matmul(P, np.matmul(U, GT))
        norm_erR1 = np.linalg.norm(ErR1, ord='fro')

        rel_erR1 = norm_erR1 / norm_R1

        return norm_erR1, rel_erR1


    def Get_Clusters(self, M, nodes):
        n,k = np.shape(M)
        Clusters = [[] for i in range(k)]
        for i in range(n):
            idx = np.argmax(M[i])
            Clusters[idx].append(nodes[i])
        return Clusters
    
    
    def SVD_Matrix(self, R1, matrix,file_name_array_path):
        
        print("Generating the SVD Matrix")
        
        J,K,L = svd(R1)
        
        file_name_array_path_1 = file_name_array_path + "_J_Matrix_" + str(matrix) + "_Human"
        file_name_array_path_2 = file_name_array_path + "_K_Matrix_" + str(matrix) + "_Human"
        file_name_array_path_3 = file_name_array_path + "_L_Matrix_" + str(matrix) + "_Human"
        
        np.save(file_name_array_path_1, J)
        np.save(file_name_array_path_2, K)
        np.save(file_name_array_path_3, L)
        

    def Solve_MUR_Hum(self, specie, R1, k1, matrix, orthonormality, network = "PPI", init= "SVD"):
        
        '''
        This Script is similar to the one used for Yeast. The only change is that the SVD is done one
        time (SVD_Matrix function), and this function recuperates it from the folder. In that way, we
        avoid repeating it for a lot of times and only two, one per each, is needed.
        '''
        
        # Paths:
        
        file_name_array_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
         
        print("Starting NMTF")
        n,n=np.shape(R1)
        norm_R1 = np.linalg.norm(R1, ord='fro')

        if init == "rand":
            print(" * Using random initialization")
            P = np.random.rand(n,k1) + 1e-5
            U = np.random.rand(k1,k1) + 1e-5
            G = np.random.rand(n,k1) + 1e-5
        elif init == 'SVD':
            
            # Check the species to init the NMTF:
            
            if specie == "Human":
                
                # We load the SVD that is already computed:
                
                J = np.load(file_name_array_path + "_J_Matrix_" + str(matrix) + "_Human.npy")
                K = np.load(file_name_array_path + "_K_Matrix_" + str(matrix) + "_Human.npy")
                L = np.load(file_name_array_path + "_L_Matrix_" + str(matrix) + "_Human.npy")
                
            else:
                
                # For the species we compute the SVD directly:
                
                J,K,L = svd(R1)
            
            P = np.zeros((n,k1)) + 1e-5
            G = np.zeros((n,k1)) + 1e-5
            U = np.zeros((k1,k1)) + 1e-5

            for i in range(n):
                for j in range(k1):
                    if J[i][j] > 0.:
                        P[i][j] = J[i][j]

            for i in range(k1):
                U[i][i] = abs(K[i])

            for i in range(n):
                for j in range(k1):
                    if L[i][j] > 0.:
                        G[i][j] = L[i][j]
            
        else :
            print("Unknown initializer: %s"%(init))
            exit(0)
        OBJ, REL2 = self.Score(R1,P, U, G, norm_R1)
        print(" - Init:\t OBJ:%.4f\t REL2:%.4f"%(OBJ, REL2))            
            
        # Begining M.U.R. (depending on the orthonormality constrain these update rules change):
        
        # If orthonormal update rules are needed:
        
        if orthonormality == "True":
            
            for it in range(1,self.max_iter+1):
    
                R1T = np.transpose(R1)
                UT = np.transpose(U)
    
                PT = np.transpose(P)
                PT_P = np.matmul(PT,P)
    
                GT = np.transpose(G)
                GT_G = np.matmul(GT,G)
    
                #update rule for G
    
                #R1 matrix
    
                #update rule for P
    
                R1_G_UT = np.matmul(np.matmul(R1, G), UT)
                U_GT_G_UT = np.matmul(np.matmul(U,GT_G),UT)
    
                P_U_GT_G_UT = np.matmul(P,U_GT_G_UT)
    
                P_mult = np.sqrt( np.divide(R1_G_UT,P_U_GT_G_UT + 1e-7) )
    
                #update rule for U
    
                PT_R1_G = np.matmul(PT, np.matmul(R1, G))
                PT_P_U_GT_G = np.matmul(PT_P, np.matmul(U, GT_G))
    
                U_mult = np.sqrt( np.divide(PT_R1_G,PT_P_U_GT_G + 1e-7))
    
                #update rule for G
    
                R1T_P_U = np.matmul(np.matmul(R1T, P), U)
                G_GT_R1T_P_U = np.matmul(G, np.matmul(GT, R1T_P_U))
    
                G_mult = np.sqrt( np.divide(R1T_P_U, G_GT_R1T_P_U + 1e-7) )
    
                # Applying M.U.R.
                P = np.multiply(P, P_mult) + 1e-7
                U = np.multiply(U, U_mult) + 1e-7
                G = np.multiply(G, G_mult) + 1e-7
    
    
                if (it%self.verbose == 0) or (it==1):
                    OBJ, REL2 = self.Score(R1, P, U, G, norm_R1)
                    print(" - It %i:\t OBJ:%.4f\t REL2:%.4f"%(it, OBJ, REL2))
             
            # Save the matrices:
            
            file_name_array_path_1 = file_name_array_path + "P_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_2 = file_name_array_path + "U_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_3 = file_name_array_path + "G_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            
            np.save(file_name_array_path_1, P)
            np.save(file_name_array_path_2, U)
            np.save(file_name_array_path_3, G)
        
        # If non-orthonormal update rules are needed (M.U.R change):
            
        else:
            
        	# Begining M.U.R. (no orthonormal)
                
            for it in range(1,self.max_iter+1):
        
                R1T = np.transpose(R1)
        
                PT   = np.transpose(P)
                PT_P = np.matmul(PT,P)
        
                GT   = np.transpose(G)
                GT_G = np.matmul(GT,G)
        
                # R1 matrix
        
                # Update rule for S
        
                GTG_inv = np.linalg.inv(GT_G)
                PTP_inv = np.linalg.inv(PT_P)
                PT_R1_G = np.matmul(PT, np.matmul(R1, G))
        
                PTP_inv_PT_R1_G = np.matmul(PTP_inv, PT_R1_G)
                U_mult = np.matmul(PTP_inv_PT_R1_G, GTG_inv)
        
                U = U_mult + 1e-7
        
                # Update rule for P
        
                U1_T = np.transpose(U)
        
                R1_G_U1_T     = np.matmul(np.matmul(R1, G), U1_T)
                R1_G_U1_T_pos = (np.absolute(R1_G_U1_T)+R1_G_U1_T)/2
                R1_G_U1_T_neg = (np.absolute(R1_G_U1_T)-R1_G_U1_T)/2
        
                U1_GT_G          =  np.matmul(U,GT_G)
                U1_GT_G_U1_T     = np.matmul(U1_GT_G,U1_T)
                U1_GT_G_U1_T_pos = (np.absolute(U1_GT_G_U1_T)+U1_GT_G_U1_T)/2
                U1_GT_G_U1_T_neg = (np.absolute(U1_GT_G_U1_T)-U1_GT_G_U1_T)/2
        
                P_U1_GT_G_U1_T_pos = np.matmul(P,U1_GT_G_U1_T_pos)
                P_U1_GT_G_U1_T_neg = np.matmul(P,U1_GT_G_U1_T_neg)
        
                P_nom   = R1_G_U1_T_pos + P_U1_GT_G_U1_T_neg
                P_denom = R1_G_U1_T_neg + P_U1_GT_G_U1_T_pos
        
                P_mult = np.sqrt( np.divide(P_nom,P_denom + 1e-5) )
                P = np.multiply(P, P_mult) + 1e-5
        
                # Update rule for G
        
                P1_T = np.transpose(P)
        
                R1T_P1_U1 = np.matmul(np.matmul(R1, P), U)
                R1T_P1_U1_pos = (np.absolute(R1T_P1_U1)+R1T_P1_U1)/2
                R1T_P1_U1_neg = (np.absolute(R1T_P1_U1)-R1T_P1_U1)/2
        
                U1_T_P1_T = np.matmul(U1_T,P1_T)
        
                U1_T_P1_T_P1_U1     = np.matmul(U1_T_P1_T,np.matmul(P,U))
                U1_T_P1_T_P1_U1_pos = (np.absolute(U1_T_P1_T_P1_U1)+U1_T_P1_T_P1_U1)/2
                U1_T_P1_T_P1_U1_neg = (np.absolute(U1_T_P1_T_P1_U1)-U1_T_P1_T_P1_U1)/2
        
                G_U1_T_P1_T_P1_U1_pos = np.matmul(G,U1_T_P1_T_P1_U1_pos)
                G_U1_T_P1_T_P1_U1_neg = np.matmul(G,U1_T_P1_T_P1_U1_neg)
        
                G_nom   = R1T_P1_U1_pos + G_U1_T_P1_T_P1_U1_neg
                G_denom = R1T_P1_U1_neg + G_U1_T_P1_T_P1_U1_pos
        
                G_mult = np.sqrt(np.divide(G_nom,G_denom + 1e-5) )
                G = np.multiply(G, G_mult) + 1e-5
        
                if (it%self.verbose == 0) or (it==1):
                    OBJ, REL2 = self.Score(R1, P, U, G, norm_R1)
                    print(" - It %i:\t OBJ:%.4f\t REL2:%.4f"%(it, OBJ, REL2))
         
            # Save the matrices (No orthonormal):
            
            file_name_array_path_1 = file_name_array_path + "No_Orth_P_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_2 = file_name_array_path + "No_Orth_U_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_3 = file_name_array_path + "No_Orth_G_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            
            np.save(file_name_array_path_1, P)
            np.save(file_name_array_path_2, U)
            np.save(file_name_array_path_3, G)        
        
def NMTF(organism, dimensions, orthonormality, matrix):
    
    # Load the corresponding matrix to decompose:
    
    file_name_array_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"  
    
    if matrix == "Adj":
        Matrix = np.load(f'{file_name_array_path}{matrix}_{organism}_PPI.npy', allow_pickle= True)
    else:
        Matrix = np.load(f'{file_name_array_path}{matrix}_{organism}.npy', allow_pickle= True)
           
    # Start the solver:
    
    Solver = SNMTF(500, 50)
    
    # Start the NMTF:
    
    Solver.Solve_MUR_Hum(organism, Matrix, dimensions, matrix, orthonormality,f'PPI_{organism}', "SVD" )  
                         
                 
def generate_NMTF_jobs(organism, orthonormality, matrix):

    # Generic path:

    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/" 
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/" 
    
    # To do the orthonormal and non-orthonormal in different tasks:
    
    if orthonormality == "True":
        tag = ""
    else:
        tag = "_No_Orth"
        
    # Start to write the resources file:    
    
    with open(f'{permutation_path}NMTF_Resources_{organism}_{matrix}{tag}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=21\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n")                     
        the_file.write("#SBATCH --cpus-per-task=16\n")
        
        # Time limit:

        the_file.write("#SBATCH --time=10:00:00\n")

        the_file.write("\n")
        the_file.write("\n")      
                
        #the_file.write("module purge && module load intel impi greasy mkl python/3.7.4\n")  # Marenostrum
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n") # Nord3      
         
        the_file.write(f'FILE={permutation_path}NMTF_Tasks_{organism}_{matrix}{tag}.sh\n')         
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                       
        #the_file.write(f'/apps/GREASY/2.2/INTEL/IMPI/bin/greasy $FILE\n')  # Marenostrum
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
                
    the_file.close()
    
    # Start to write the task file
    
    with open(f'{permutation_path}NMTF_Tasks_{organism}_{matrix}{tag}.txt', 'a') as the_file:
        for optimal_dimension in [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]:        
            the_file.write(f'python3 {scrip_path}Marenostrum.py -species {organism} -dimensions {optimal_dimension} -matrix {matrix} -orthonormal {orthonormality}  -job NMTF\n')          
        
    the_file.close()
    
    # Change the name of the file:
    
    old_name = f'{permutation_path}NMTF_Tasks_{organism}_{matrix}{tag}.txt'
    new_name = f'{permutation_path}NMTF_Tasks_{organism}_{matrix}{tag}.sh'

    # Renaming the file
    os.rename(old_name, new_name)    


def GO_Embeddings_Human(Matrix_Gene_GO, gene_embeddings, save_path, GO, network, matrix, orthonormal):
    
    '''
    Imputs:
            
        - Matrix_Gene_GO  : DataFrame with genes and GO terms (0 : Not annotated, 1 : Annotated)
        - gene_embeddings : Array with the embeddings of the genes (the bases, matrix G).
        - save_path       : string, path to save the output.
        - GO              : string, annotations that are used.
        - network         : string, network (e.g., PPI for protein-protein interaction network).
        - matrix          : string, network matrix representation (e.g., Adj for adjancency matrix).
                          
    '''
    
    save              = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Matrix with gene embeddings (if the previous function was used to create the gene/GO the order of the genes is the same):
    
    gene_embeddings_db = pd.DataFrame(gene_embeddings)
    gene_embeddings_db.index = Matrix_Gene_GO.index
    
    # Calculate the GO embeddings:

    gene_embeddings_db_inverse = pd.DataFrame(np.linalg.pinv(gene_embeddings_db.values), gene_embeddings_db.columns, gene_embeddings_db.index)
    GO_embeddings = gene_embeddings_db_inverse.dot(Matrix_Gene_GO)
    
    # Save the output:
    
    if orthonormal == "True":        
        save_path_1 = save + "_GO_Embeddings_" + str(GO) + "_" + str(network) + "_"+ str(len(gene_embeddings_db.columns))+ "_" + str(matrix) + ".csv"
        GO_embeddings.to_csv(save_path_1)
    else:
        save_path_1 = save + "_GO_Embeddings_" + str(GO) + "_" + str(network) + "_"+ str(len(gene_embeddings_db.columns))+ "_No_Orth_" + str(matrix) + ".csv"
        GO_embeddings.to_csv(save_path_1)        


def Annotate_Gene_Space(organism, dimensions, matrix, annotation, orthonormal):
    
    # Networks path:
    
    Annotation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/" 
    Bases_path        = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
      
    # Get the specie information:
    
    if annotation == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)        
         
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]
        
    # Delete the annotations without genes (>0):
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
        
    # Start the embeddings:
                 
    # Fix the gene vectorial representation:
    
    if orthonormal == "True":           
        G_sp  = np.load(f'{Bases_path}G_Matrix_{dimensions}_PPI_{organism}_{matrix}.npy', allow_pickle=True)
    else:
        G_sp  = np.load(f'{Bases_path}No_Orth_G_Matrix_{dimensions}_PPI_{organism}_{matrix}.npy', allow_pickle=True)
                    
    # Do the embeddings:
            
    GO_Embeddings_Human(GO_Matrix_filt,  G_sp, Annotation_path,  GO = annotation, network = f'PPI_{organism}', matrix = matrix, orthonormal=str(orthonormal))
            
def Annotate_LINE_space(organism, dimensions, annotation):
    
    # Networks path:
    
    Annotation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/" 
    Bases_path        = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
    save              = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Get the specie information:
        
    if annotation == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)   
        
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]
        
    # Delete the annotations without genes (>0):
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
                
    G_sp       = np.load(f'{Bases_path}Line_Bases_{organism}_{dimensions}.npy', allow_pickle=True).T
    G_sp       = pd.DataFrame(G_sp)
    G_sp.index = GO_Matrix_filt.index
    G_sp_inver = pd.DataFrame(np.linalg.pinv(G_sp.values), G_sp.columns, G_sp.index)
    GO_embedd  = G_sp_inver.dot(GO_Matrix_filt) 
            
    # # Save file:
               
    GO_embeddings_norm_db         = pd.DataFrame(GO_embedd)
    GO_embeddings_norm_db.columns = GO_embedd.columns
    GO_embeddings_norm_db.to_csv(f'{save}_GO_Embeddings_{annotation}_PPI_{organism}_{dimensions}_LINE.csv')

            
def Annotate_Deepwalk_space(organism, dimensions, annotation):
    
    # Networks path:
    
    Annotation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/" 
    Bases_path        = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
    save              = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Get the specie information:
        
    if annotation == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)   
        
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]
        
    # Delete the annotations without genes (>0):
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
    
    # Load the bases of Deepwalk:
                   
    G_sp       = np.load(f'{Bases_path}Deepwalk_Bases_{organism}_{dimensions}.npy', allow_pickle=True).T
    G_sp       = pd.DataFrame(G_sp)
    G_sp.index = GO_Matrix_filt.index
    G_sp_inver = pd.DataFrame(np.linalg.pinv(G_sp.values), G_sp.columns, G_sp.index)
    GO_embedd  = G_sp_inver.dot(GO_Matrix_filt) 
            
    # Save file:
               
    GO_embeddings_norm_db         = pd.DataFrame(GO_embedd)
    GO_embeddings_norm_db.columns = GO_embedd.columns       
    GO_embeddings_norm_db.to_csv(f'{save}_GO_Embeddings_{annotation}_PPI_{organism}_{dimensions}_Deepwalk.csv')


def Embedd_GO_terms(matrix, organism, dimensions, annotation = "BP_Back", orthonormal = "True"):
    
    if matrix == "PPMI":
        Annotate_Gene_Space(organism, dimensions,  matrix, annotation, orthonormal)
    elif matrix == "Adj":
        Annotate_Gene_Space(organism, dimensions,  matrix, annotation, orthonormal)
    elif matrix == "LINE":
        Annotate_LINE_space(organism, dimensions, annotation)
    elif matrix == "Deepwalk":
        Annotate_Deepwalk_space(organism, dimensions, annotation)
    else:
        print("Error")
                 
def generate_Embedd_GO_jobs(matrix, organism, orthonormality):

    # Generic path:

    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/" 
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/" 
    
    # To do the orthonormal and non-orthonormal in different tasks:
    
    if orthonormality == "True":
        tag = ""
    else:
        tag = "_No_Orth"
        
    # Start to write the resources file:    
    
    with open(f'{permutation_path}Embedd_GO_Resources_{organism}_{matrix}{tag}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=21\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n")                     
        the_file.write("#SBATCH --cpus-per-task=16\n")
        
        # Time limit:

        the_file.write("#SBATCH --time=10:00:00\n")

        the_file.write("\n")
        the_file.write("\n")      
                
        #the_file.write("module purge && module load intel impi greasy mkl python/3.7.4\n")  # Marenostrum
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n") # Nord3      
         
        the_file.write(f'FILE={permutation_path}Embedd_GO_Tasks_{organism}_{matrix}{tag}.sh\n')         
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                       
        #the_file.write(f'/apps/GREASY/2.2/INTEL/IMPI/bin/greasy $FILE\n')  # Marenostrum
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
                
    the_file.close()
    
    # Start to write the task file
    
    with open(f'{permutation_path}Embedd_GO_Tasks_{organism}_{matrix}{tag}.txt', 'a') as the_file:
        for optimal_dimension in [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]:        
            the_file.write(f'python3 {scrip_path}Marenostrum.py -matrix {matrix} -species {organism} -dimensions {optimal_dimension} -annotation BP_Back -orthonormal {orthonormality}  -job GO_Emb\n') 
            the_file.write(f'python3 {scrip_path}Marenostrum.py -matrix {matrix} -species {organism} -dimensions {optimal_dimension} -annotation BP_Leaf -orthonormal {orthonormality}  -job GO_Emb\n')           
    the_file.close()
    
    # Change the name of the file:
    
    old_name = f'{permutation_path}Embedd_GO_Tasks_{organism}_{matrix}{tag}.txt'
    new_name = f'{permutation_path}Embedd_GO_Tasks_{organism}_{matrix}{tag}.sh'

    # Renaming the file
    os.rename(old_name, new_name)
    

def generate_Calculate_Intra_Inter_Axes(org):

    # Generic path:

    scrip_path         = "/gpfs/projects/bsc79/bsc79321/"
    Path_Clust         = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Clustering_GOGO/"
       
    # Start to write the resources file:    
    
    with open(f'{Path_Clust}Resources_Intra_Inter_{org}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=14\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n")                     
        the_file.write("#SBATCH --cpus-per-task=16\n")
        
        # Time limit:

        the_file.write("#SBATCH --time=20:00:00\n")

        the_file.write("\n")
        the_file.write("\n")      
                
        #the_file.write("module purge && module load intel impi greasy mkl python/3.7.4\n")  # Marenostrum
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n") # Nord3      
         
        the_file.write(f'FILE={Path_Clust}Intra_Inter_Tasks_{org}.sh\n')         
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                       
        #the_file.write(f'/apps/GREASY/2.2/INTEL/IMPI/bin/greasy $FILE\n')  # Marenostrum
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
                
    the_file.close()
    
    # Start to write the task file
    
    with open(f'{Path_Clust}Intra_Inter_Tasks_{org}.txt', 'a') as the_file:  
        for matrix in ["PPMI", "Adj", "No_Orth_PPMI", "No_Orth_Adj", "LINE", "Deepwalk"]:
            the_file.write(f'python3 {scrip_path}Marenostrum.py  -species {org} -matrix {matrix} -annotation BP_Back  -job Intra\n') 
            the_file.write(f'python3 {scrip_path}Marenostrum.py  -species {org} -matrix {matrix} -annotation BP_Leaf  -job Intra\n')          
    the_file.close()
     
    # Change the name of the file:
    
    old_name = f'{Path_Clust}Intra_Inter_Tasks_{org}.txt'
    new_name = f'{Path_Clust}Intra_Inter_Tasks_{org}.sh'

    # Renaming the file
    os.rename(old_name, new_name)

            
def Calculate_Intra_Inter_Axes(org, matrix, annotations): 
    
    # Matrix list: ["PPMI", "Adj", "No_Orth_PPMI", "No_Orth_Adj", "LINE", "Deepwalk"]
    
    dimensions = [50, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]
    
    # Paths:

    Path_Semantic   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Semantic_Similarity/"
    Path_Clust      = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Clustering_GOGO/"
    Axes_Annot_Path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Annotations/"
           
    # Load the Semantic Similarity:
            
    Semantic = pd.read_csv(f'{Path_Semantic}Semantic_{org}.csv', index_col = 0) 
            
     # Set the dictionary:
            
    keys = []
            
    for dm in dimensions:
                
        keys.append(f'{dm}_Inter')
        keys.append(f'{dm}_Intra')
        keys.append(f'{dm}_Inter_Random')
        keys.append(f'{dm}_Intra_Random')
            
        Result = dict(([(key, []) for key in keys]))
            
        # Iterate by dimensions:
            
        for dim in dimensions:
                
            # Load the corresponding associacions
                
            Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}.json')) 
                
            # Delete empty axes:
             
            Axes     = {k: v for k, v in Axes.items() if v}
            average  = round(np.mean([len(Axes[i]) for i in Axes.keys()]))
                
            # Analyze the intra/inter cluster distance:                    
                    
            intra_distance = []
            inter_distance = []
                
            intra_random   = []
            inter_random   = []
                    
            # Intra cluster semantic similarity:
                    
            for cluster in list(set(Axes.keys())):
                        
                # Get the intra cluster distance:
                        
                GO_cluster_1 = Axes[cluster]
                        
                if len(GO_cluster_1) > 1:
                            
                    GO_cluster_1        = list(set(GO_cluster_1).intersection(set(Semantic.index))) 
                    GO_cluster_1_Random = random.sample(list(Semantic.index),average )
                            
                    Semantic_1   = Semantic.loc[GO_cluster_1, GO_cluster_1]
                    Semantic_1   = np.array(Semantic_1)[np.triu_indices(np.array(Semantic_1).shape[0], k = 1)]
                            
                    Semantic_1_random   = Semantic.loc[GO_cluster_1_Random, GO_cluster_1_Random]
                    Semantic_1_random   = np.array(Semantic_1_random)[np.triu_indices(np.array(Semantic_1_random).shape[0], k = 1)]
                                                                                   
                    intra_distance.append(round(np.mean(Semantic_1),3))
                    intra_random.append(round(np.mean(Semantic_1_random),3))
                    
            # Inter cluster semantic similarity:
                    
            lista = list(set(Axes.keys()))
                    
            for i in range(len(lista)):                       
                cluster_1    = lista[i]
                GO_cluster_1 = Axes[cluster_1]
                            
                for j in range(i+1, len(lista)):

                    cluster_2           = lista[j]
                    GO_cluster_2        = Axes[cluster_2]
                        
                    GO_cluster_2_Random = random.sample(list(Semantic.index),average)
                    GO_cluster_1_Random = random.sample(list(Semantic.index),average)
                            
                    # Take the semantic similarity:
                        
                    GO_cluster_1 = list(set(GO_cluster_1).intersection(set(Semantic.index)))
                    GO_cluster_2 = list(set(GO_cluster_2).intersection(set(Semantic.index)))
                            
                    Semantic_1 = Semantic.loc[GO_cluster_1, GO_cluster_2]
                    inter_distance.append(round(Semantic_1.mean().mean(),3))

                    Semantic_1_Random = Semantic.loc[GO_cluster_1_Random, GO_cluster_2_Random]
                    inter_random.append(round(Semantic_1_Random.mean().mean(),3))
                
            # Add the info to the list:
                    
            Result[f'{dim}_Inter'] = inter_distance
            Result[f'{dim}_Intra'] = intra_distance
                
            Result[f'{dim}_Inter_Random'] = inter_random
            Result[f'{dim}_Intra_Random'] = intra_random                                
                    
        sa = json.dumps(Result)
        f = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes_{annotations}.json' ,"w")
        f.write(sa)
        f.close()   
        
def shuffle(df):     
    df_random = df.copy()
    for i in df_random.index:
        random           = df_random.loc[i].sample(frac=1)
        random.index     = df.columns
        df_random.loc[i] = random
    return(df_random)    

def shuffle2(df):     
    df_random = df.copy()
    for i in df_random.index:
        random           = df_random.loc[i].sample(frac=1)
        random.index     = df.columns
        df_random.loc[i] = random
    for i in df_random.columns:
        random           = df_random[i].sample(frac=1)
        random.index     = df.index
        df_random[i]     = random
        
    return(df_random)    
    
def Permutations_Distances(org, matrix, dim, annotations, times, name, name_2):
    
    # Paths:
    
    Meaning_path       = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Meaning/"
    Axes_path          = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Annotations/"
    GO_embeddings_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Generate a folder to save the counts:
    
    path_it = f"{Meaning_path}Permutations_{org}_{dim}_{matrix}_{annotations}_2/"       
    os.makedirs(path_it, exist_ok=True)
    
    # Load the Axes:
    
    Axes = json.load(open(f'{Axes_path}Associations_{org}_{matrix}_{annotations}_{dim}.json')) 
    Axes = {k: v for k, v in Axes.items() if v}
    
    # Load GO embeddings:
    
    embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{org}_{dim}_{matrix}.csv', index_col = 0).T 
    embeddings[embeddings < 0] = 0
    
    embeddings_index  = embeddings.index
    embeddings_array  = np.array(embeddings) 
    
    distance_emb = pairwise_distances(embeddings_array, metric="cosine")
    distance_emb = pd.DataFrame(distance_emb, embeddings_index, embeddings_index)    
     
    # Load the real distance:
    
    Result_dist    = pd.read_csv(f'{Meaning_path}Cosine_Distances_Embeddings_{org}_{matrix}_{dim}_{annotations}.csv',
                          index_col = 0, dtype={0: str})
    Result_dist.index = [str(i) for i in Result_dist.index] 
    
    # Prepare an empty count matrix:
    
    counts = pd.DataFrame(0, columns=list(Axes.keys()), index = list(Axes.keys()))
    
    # Init the premutations:
    
    for rep in range(times):
        
        # Randomize the distance matrix (change the index and columns):
        
        distance_emb_rand = shuffle2(distance_emb)

        # Use the random distances to compute the BMA between the axes:
        
        Result_dist_Random = pd.DataFrame(0, columns=list(Axes.keys()), index = list(Axes.keys()))        
        axes               = list(set(Axes.keys()))
        
        for i in range(len(axes)): 
            axis1    = axes[i]
            GO_axis1 = Axes[axis1]        
            for j in range(i+1, len(axes)): 
                axis2    = axes[j] 
                GO_axis2 = Axes[axis2]
                
                GO_embeddings_subset     = distance_emb_rand.loc[GO_axis1, GO_axis2]            
                best_match               = (sum(GO_embeddings_subset.min()) + sum(GO_embeddings_subset.T.min()))
                best_match               = best_match/(len(GO_embeddings_subset.min())+len(GO_embeddings_subset.T.min()))
                
                Result_dist_Random.loc[axis1,axis2] = best_match
                Result_dist_Random.loc[axis2,axis1] = best_match 
        
        # Compare the Random distance matrix to the real distance one:
        
        # Compare the real and the random:
        
        compare = Result_dist_Random <= Result_dist
        counts  = counts + compare 
    
    # Ones the permutations are finished save the count matrix:
    
    counts.to_csv(f'{path_it}Distance_Count_{org}_{dim}_{matrix}_{annotations}_{times}_{name}_{name_2}.csv', 
                          header = True, index=True)
        
def Permutations_Distances_Jobs(organism, optimal_dimension, matrix, times):
    
    # Paths:
    
    Meaning_path       = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Meaning/"
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/"  

    # Split tasks:
    
    permutations = int(times/1000) 
    tasks_n      = int(times/permutations)
    
    # Split tasks in differnt jobs (divide in 100 since eah job will run 50 in parllel):
    
    jobs_n = int(tasks_n/100)
    
    # Start to write the resources file: 
    
    # Write as many resources files as jobs:
    
    for job in range(jobs_n):
    
        with open(f'{Meaning_path}Resources_{organism}_{optimal_dimension}_{matrix}_{job}.txt', 'a') as the_file:
            
            # Write the first lines:
            
            the_file.write("#!/bin/bash\n")
            the_file.write("#SBATCH --job-name=axes_perm\n")
            the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
            the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
            the_file.write("#SBATCH --ntasks=50\n")
                           
            the_file.write("#SBATCH --qos=bsc_ls\n") # Normal queue
            the_file.write("#SBATCH --cpus-per-task=16\n") # max 16 in nord3 / max 48 in MN4
            the_file.write("#SBATCH --time=20:00:00\n")
    
            the_file.write("\n")
            the_file.write("\n")
                           
            the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n")   # Nord3        
            
            the_file.write(f'FILE={Meaning_path}Tasks_{organism}_{optimal_dimension}_{matrix}_{job}.sh\n') 
            
            the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
            the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
            
            
        the_file.close()
    
        # Start to write the task file:
        
        with open(f'{Meaning_path}Tasks_{organism}_{optimal_dimension}_{matrix}_{job}.txt', 'a') as the_file:
            
            # 100 tasks of 10 repetitions for each job:
            
            for i in range(100):           
                the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {optimal_dimension} -matrix {matrix} -species {organism} -permutations {str(permutations)} -job Perm_Distance -name {str(i)} -name2 {str(job)} -annotation BP_Back\n')
    
    
        the_file.close()
    
        # Change the name of the file:
    
        old_name = f'{Meaning_path}Tasks_{organism}_{optimal_dimension}_{matrix}_{job}.txt'
        new_name = f'{Meaning_path}Tasks_{organism}_{optimal_dimension}_{matrix}_{job}.sh'
    
        # Renaming the file
        os.rename(old_name, new_name)

   
def Sum_Permutations_Dist(org, matrix, dim, annotations, times):
    
    # Generic path:
    
    permutation_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Meaning/Permutations_Human_500_PPMI_BP_Back_3/"
    
    # Set the information to sum the different files:    
    
    permutations = int(times/1000) 
    tasks_n      = int(times/permutations)
    jobs_n       = int(tasks_n/100)
    
    # Sum all the counts:
    
    Final = pd.DataFrame()
    
    for job in range(jobs_n):
        
        for task in range(100):
            
            if (job == 0) & (task == 0):               
                 permutations_it   = pd.read_csv(f'{permutation_path}Distance_Count_{org}_{dim}_{matrix}_{annotations}_{times}_{task}_{job}.csv',index_col = 0, dtype={0: str})
                 Final = permutations_it
            else:
                permutations_it   = pd.read_csv(f'{permutation_path}Distance_Count_{org}_{dim}_{matrix}_{annotations}_{times}_{task}_{job}.csv',index_col = 0, dtype={0: str})
                Final = Final + permutations_it
    
    # Compute the p-value:
    
    Total_counts = permutations * (100 * jobs_n)
    Final        = (Final + 1)/(Total_counts + 1)
    
    # Save the p-values:

    Final.to_csv(f'{permutation_path}P_value_{org}_{matrix}_{dim}_{annotations}.csv', 
                          header = True, index=True)                
    
    
def Sum_Permutations_Dist_Job(org, matrix, dim, annotations, times):
     
    # Paths:
    
    Meaning_path       = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Axes_Meaning/"
    scrip_path         = "/gpfs/projects/bsc79/bsc79321/"  
    
    with open(f'{Meaning_path}Sum_Resources_{org}_{dim}.txt', 'a') as the_file:
        
        # Write the first lines:
        
        the_file.write("#!/bin/bash\n")
        the_file.write("#SBATCH --job-name=axes_perm\n")
        the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
        the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
        the_file.write("#SBATCH --ntasks=2\n")
                       
        the_file.write("#SBATCH --qos=bsc_ls\n")                     
        the_file.write("#SBATCH --cpus-per-task=16\n")
        
        # Time limit:

        the_file.write("#SBATCH --time=5:00:00\n")

        the_file.write("\n")
        the_file.write("\n")      
                
     
        the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n") # Nord3      
         
        the_file.write(f'FILE={Meaning_path}Sum_Tasks_{org}_{dim}.sh\n')         
        the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
                                       
        the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
        
        
    the_file.close()
    

    # Start to write the task file (in this case I prepare the code for human if not change the 125 to 75):
    
    with open(f'{Meaning_path}Sum_Tasks_{org}_{dim}.txt', 'a') as the_file:
    
        the_file.write(f'python3 {scrip_path}Marenostrum.py -dimensions {dim} -matrix {matrix} -species {org} -permutations {str(times)} -job Sum_Distances -annotation BP_Back\n')
    
    the_file.close()
    
    # Change the name of the file:
    
    old_name = f'{Meaning_path}Sum_Tasks_{org}_{dim}.txt'
    new_name = f'{Meaning_path}Sum_Tasks_{org}_{dim}.sh'

    # Renaming the file
    os.rename(old_name, new_name)  

def tfidf(word, sentence, corpus):
    tf = sentence.count(word)/len(sentence)
    idf = np.log10(len(corpus)/ sum([1 for doc in corpus if word in doc]))
    return round(tf*idf,4)    

def TF_IDF_Abstract(org, dim, matrix, annotations, name, name2, axis1):
    
    # Path:
    
    empty_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/TF_IDF/"
    
    # Load the axes:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{org}_{matrix}_{annotations}_{dim}_Hard.json',"r"))
    
    # Load the files:
    
    f1 = open(f'{empty_path}Corpus_{org}_{dim}')
    f2 = open(f'{empty_path}Vocabulary_{org}_{dim}')
    
    Corpus     = json.load(f1)
    Vocabulary = json.load(f2)
    
    f1.close()
    f2.close()
    
    # Init the TF-IDF:
    
    Final = pd.DataFrame(0, columns=list(axes_dic.keys()), index = list(Vocabulary))
    
    sentence = Corpus[int(axis1)]
    
    if len(sentence) > 0:
        for word in sentence:
            Final.loc[word, axis1] = tfidf(word, sentence, Corpus)
    
    # Save the TF-IDF:
    
    Final.to_csv(f'{empty_path}TF_IDF_{org}_{dim}_{name}_{name2}.csv')
    
    
def TF_IDF_NCBI(org, dim, matrix, annotations, name, name2, axis1):
    
    # Path:
    
    empty_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/TF_IDF/"
    
    # Load the axes:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{org}_{matrix}_{annotations}_{dim}_Hard.json',"r"))
    
    # Load the files:
    
    f1 = open(f'{empty_path}Corpus_{org}_{dim}_NCBI')
    f2 = open(f'{empty_path}Vocabulary_{org}_{dim}_NCBI')
    
    Corpus     = json.load(f1)
    Vocabulary = json.load(f2)
    
    f1.close()
    f2.close()
    
    # Init the TF-IDF:
    
    Final = pd.DataFrame(0, columns=list(axes_dic.keys()), index = list(Vocabulary))
    
    sentence = Corpus[int(axis1)]
    
    if len(sentence) > 0:
        for word in sentence:
            Final.loc[word, axis1] = tfidf(word, sentence, Corpus)
    
    # Save the TF-IDF:
    
    Final.to_csv(f'{empty_path}TF_IDF_{org}_{dim}_{name}_{name2}_NCBI.csv')
    
def NCBI_TF_IDF_Jobs(org, dim, matrix, annotations): 
        
    # Path:
    
    empty_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/TF_IDF/"
    scrip_path = "/gpfs/projects/bsc79/bsc79321/" 
    
    # Load Axes:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{org}_{matrix}_{annotations}_{dim}_Hard.json',"r"))     
    jobs_n   = int(len(axes_dic.keys())/25)
    
    # We divide the tasks in 10 jobs:

    axis_from = 0
    
    for job in range(jobs_n):
        
        print("Hola")
        
        # For job i write the resources:
              
        with open(f'{empty_path}NCBI_Resources_{org}_{dim}_{matrix}_TF_IDF_{job}.txt', 'a') as the_file:
                
            # Write the first lines:
                
            the_file.write("#!/bin/bash\n")
            the_file.write("#SBATCH --job-name=axes_perm\n")
            the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
            the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
            the_file.write("#SBATCH --ntasks=25\n")
                               
            the_file.write("#SBATCH --qos=bsc_ls\n") # Normal queue
            the_file.write("#SBATCH --cpus-per-task=16\n") # max 16 in nord3 / max 48 in MN4
            the_file.write("#SBATCH --time=48:00:00\n")
        
            the_file.write("\n")
            the_file.write("\n")
                               
            the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n")   # Nord3        
                
            the_file.write(f'FILE={empty_path}Tasks_NCBI_{org}_{dim}_{matrix}_TF_IDF_{job}.sh\n') 
            
            the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
            the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
                            
        the_file.close()  
        
        # For job i write the Tasks:
        
        with open(f'{empty_path}Tasks_NCBI_{org}_{dim}_{matrix}_TF_IDF_{job}.txt', 'a') as the_file:
            
            for i in range(25): 
                
                pp = str(axis_from)
                
                the_file.write(f'python3 {scrip_path}Marenostrum.py -species {org} -dimensions {dim} -matrix {matrix} -annotation BP_Back -name {pp} -name2 {pp} -axis1 {pp} -job NCBI_TF_IDF_Run\n') 
                axis_from = axis_from + 1
                
        # Change the name of the file:
    
        old_name = f'{empty_path}Tasks_NCBI_{org}_{dim}_{matrix}_TF_IDF_{job}.txt'
        new_name = f'{empty_path}Tasks_NCBI_{org}_{dim}_{matrix}_TF_IDF_{job}.sh'

        # Renaming the file
        os.rename(old_name, new_name)     

        
def TF_IDF_Jobs(org, dim, matrix, annotations): 
        
    # Path:
    
    empty_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/TF_IDF/"
    scrip_path = "/gpfs/projects/bsc79/bsc79321/" 
    
    # Load Axes:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{org}_{matrix}_{annotations}_{dim}_Hard.json',"r"))     
    jobs_n   = int(len(axes_dic.keys())/25)
    
    # We divide the tasks in 10 jobs:

    axis_from = 0
    
    for job in range(jobs_n):
        
        print("Hola")
        
        # For job i write the resources:
              
        with open(f'{empty_path}Resources_{org}_{dim}_{matrix}_TF_IDF_{job}.txt', 'a') as the_file:
                
            # Write the first lines:
                
            the_file.write("#!/bin/bash\n")
            the_file.write("#SBATCH --job-name=axes_perm\n")
            the_file.write("#SBATCH --output=embed_kmers-%J.out\n")
            the_file.write("#SBATCH --error=embed_kmers-%J.err\n")
            the_file.write("#SBATCH --ntasks=25\n")
                               
            the_file.write("#SBATCH --qos=bsc_ls\n") # Normal queue
            the_file.write("#SBATCH --cpus-per-task=16\n") # max 16 in nord3 / max 48 in MN4
            the_file.write("#SBATCH --time=48:00:00\n")
        
            the_file.write("\n")
            the_file.write("\n")
                               
            the_file.write("module purge && module load intel impi greasy mkl python/3.7.12\n")   # Nord3        
                
            the_file.write(f'FILE={empty_path}Tasks_{org}_{dim}_{matrix}_TF_IDF_{job}.sh\n') 
            
            the_file.write(f'export GREASY_LOGFILE=embed_kmers.log\n')
            the_file.write(f'/apps/GREASY/2.2.3/INTEL/IMPI/bin/greasy $FILE\n') # Nord3
                            
        the_file.close()  
        
        # For job i write the Tasks:
        
        with open(f'{empty_path}Tasks_{org}_{dim}_{matrix}_TF_IDF_{job}.txt', 'a') as the_file:
            
            for i in range(25): 
                
                pp = str(axis_from)
                
                the_file.write(f'python3 {scrip_path}Marenostrum.py -species {org} -dimensions {dim} -matrix {matrix} -annotation BP_Back -name {pp} -name2 {pp} -axis1 {pp} -job TF_IDF_Run\n') 
                axis_from = axis_from + 1
                
        # Change the name of the file:
    
        old_name = f'{empty_path}Tasks_{org}_{dim}_{matrix}_TF_IDF_{job}.txt'
        new_name = f'{empty_path}Tasks_{org}_{dim}_{matrix}_TF_IDF_{job}.sh'

        # Renaming the file
        os.rename(old_name, new_name) 
        
 
    
