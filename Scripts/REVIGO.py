# Packages:

import os
import sys
import time
import json
import requests
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats 
from selenium import webdriver
from collections import Counter
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from scipy.stats import mannwhitneyu
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import LabelEncoder
from scipy.cluster.hierarchy import dendrogram 
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics.cluster import adjusted_rand_score
from sklearn.metrics.pairwise import euclidean_distances
import scipy.spatial as sp, scipy.cluster.hierarchy as hc
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

# Our packages:

os.chdir("/media/sergio/sershiosdisk/Scripts/")
import Axes_Study_Scripts_Human


# Functions:

def Submit_Whole_Ontology(GO_path, savepath):
    
    # Read enrichments file
    
    enrichments = open(GO_path, 'r').read()
    
    # Submit job to Revigo
    
    payload = {'cutoff':'0.7', 'valueType':'pvalue', 'speciesTaxon':'0', 'measure':'SIMREL', 'goList':enrichments}
    r = requests.post("http://revigo.irb.hr/StartJob.aspx", data=payload)
    
    jobid = r.json()['jobid']
    
    # Check job status
    
    running = 1
    while (running!=0):
        r = requests.post("http://revigo.irb.hr/QueryJobStatus.aspx", data={'jobid':jobid})
        running = r.json()['running']
        time.sleep(1)
    
    # Fetch results
    r = requests.post("http://revigo.irb.hr/ExportJob.aspx", data={'jobid':jobid, 'namespace':'1', 'type':'csvtable'})

    # Write results to a file - if file name is not provided the default is output.csv
    
    savepath_output = savepath + "REVIGO_Complete_Output.csv"
    
    with open(sys.argv[2] if len(sys.argv)>=3 else savepath_output,'w') as f:
        f.write(r.text)
    
    print("Complete")
    
def Submit_Query(GO_list, savepath):
    
    # Set the preferences:

    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.download.folderList", 2)
    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.dir", savepath)
    fp.set_preference('browser.helperApps.neverAsk.saveToDisk', "text/plain, application/vnd.ms-excel, text/csv, text/comma-separated-values, application/octet-stream")

    # Start the browser:

    binary = FirefoxBinary('/usr/bin/firefox')
    driver = webdriver.Firefox(firefox_binary = binary, firefox_profile = fp)
    driver.get("http://revigo.irb.hr/")
    assert "REVIGO" in driver.title
    
    # Accept the terms:
    
    press_button = driver.find_element_by_css_selector(
        "html body div.ui-dialog.ui-corner-all.ui-widget.ui-widget-content.ui-front.no-close.ui-dialog-buttons div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix div.ui-dialog-buttonset"
         )
    press_button.click()
    
    # Fill the GO terms:
    
    for GO in GO_list:
        
        text_area = driver.find_element_by_css_selector("html body form#aspnetForm div div div textarea#ctl00_MasterContent_txtGOInput")
        text_area.send_keys(str(GO))
        text_area.send_keys("\n")
    
    # Submit the job:
    
    press_button_sub = driver.find_element_by_css_selector("html body form#aspnetForm div div div p input#ctl00_MasterContent_btnStart.ui-button.ui-corner-all.ui-widget")
    press_button_sub.click()
    
    # Download the file:
    
    time.sleep(30)
        
    link = driver.find_element_by_link_text('Export results to text table (CSV)')
    link.click()
    
    # Wait to download and quit the browser:
    
    time.sleep(20)
    driver.quit()
    
def Apply_REVIGO_to_PPI(PPI_axes, save_path, Name):
    
    # Create the main directory:
    
    main_folder = save_path + str(Name)
    os.mkdir(main_folder)
    
    # Start the comparisons:
    
    for axe in range(len(PPI_axes)):
        
        GO_list = PPI_axes[axe]
        
        # QUERY REVIGO:
        
        Submit_Query(GO_list, main_folder)
        
        # Rename REVIGO:
        
        os.rename(main_folder + '/Revigo.csv', main_folder +'/Axe_' + str(axe) + ".csv") 

def Give_me_the_Domains(file_path):
    
    # Open the file:
    
    file = pd.read_csv(file_path, sep = " ")
    term_ids = []
    
    for line in range(len(file)):
        file_line = file.loc[line]
        if file_line["Eliminated"] == True:
            temp_representative = file_line["Representative,"]
            term_ids.append(temp_representative)
        else:
            term_ids.append(file_line["TermID,"])
        
    # Count the GO terms
    
    word = "GO"
    real_terms = []
    for line in range(len(term_ids)):
        Term = term_ids[line]
        if word in Term:
            real_terms.append(Term)
        else:
            new_term = real_terms[len(real_terms) - 1]
            real_terms.append(new_term)
    
    # Count them:
    
    pd.DataFrame(real_terms).count(0)
    
    Counter_dic = Counter(real_terms)
    Count_pd    = pd.DataFrame.from_dict(Counter_dic, orient='index')
    Count_pd    = Count_pd.sort_values(0, ascending = False)
    
    # Final result:
    
    GO_filter     = list(Count_pd.index)
    file.index    = file["TermID,"]
    GO_decription = list(file.loc[GO_filter]["Name,"])
    Counter_list  = Count_pd[0]
    
    Final_pandas = pd.DataFrame({"GO": GO_filter, "Description" : GO_decription, "Counter" : Counter_list }).reset_index(drop = True)
    
    return(Final_pandas)
    
def Produce_All_Domains_PPI(data_path, Axes_association, Name):
    
    # Change the Data Path:
    
    file_path = "/media/sergio/sershiosdisk/Human/Axes/Meaning/" + str(Name) + "/Axe_"
    
    for axe in range(len(Axes_association)):
        
        file_path_it = file_path + str(axe) + ".csv"
        
        # Get the domains:
        
        domain = Give_me_the_Domains(file_path_it)
        
        # Save the domain:
        
        save_domain = "/media/sergio/sershiosdisk/Human/Axes/Meaning/" + str(Name)
        
        domain.to_csv(save_domain + "/Domain_" + str(axe) + ".csv")
    
    # Finish
    
    print("Your domains are finished")
    
def Apply_Representative_To_All_Axes(k, Axes_association,data_path,number = 5):
    
    # Start the loop:
    
    for axe in range(len(Axes_association)):
        data_path_it = f'{data_path}Domain_{axe}.csv'
        Give_The_Representative_Domains(k, axe, data_path_it, number)
             
    # Finish the loop:
    
    print("Your plots are prepared")
    
def Give_The_Representative_Domains(k, number_axe, data_path, number = 5):
    
    # Read the file
    
    file = pd.read_csv(data_path, index_col = 0)
    
    # Get the most representative domains:
    
    domains_representative = file.iloc[:number]
    domains_representative = domains_representative.replace(',','', regex=True)
    
    Cluster_pd = Give_clusters(k, number_axe)
    Cluster_pd = Cluster_pd.replace(',','', regex=True)
            
    # Choose only the most representative ones:
            
    Cluster_pd = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative.GO)] 
    
    # Get colors:
    
    dic_colors = Define_Colors_Plot(Cluster_pd, domains_representative)
    
    # Count the total number for the plot:
    
    Total       = sum(domains_representative.Counter)
    percentages = [i*100/Total for i in domains_representative.Counter]
    
    # Another counts:
    
    Real_Total      = sum(file.Counter)
    Real_Percentage = (Total*100)/ Real_Total
    
    # Get the description of the GO terms:
    
    GO_labels = domains_representative.Description
    
    # Plot the pie chart:
    
    plt.figure(figsize=(8,8))
    plt.rcParams.update({'font.size': 22})
    
    final_color = [dic_colors[i] for i in dic_colors.keys()]
    
    title = f"Axe_{number_axe}_Domain"
    plt.pie(percentages, labels=GO_labels, autopct='%1.1f%%', shadow=False, colors= final_color)
    plt.title(title)
    plt.text(-1,-1.5,f'Coverage : {round(Real_Percentage,2)}%')
    
    # Save the plot:
    
    plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Pie_Plots/Domains_Pie_Axe_{number_axe}.png',dpi=300, bbox_inches = "tight")

def Plot_All_Coordinates_to_Axes(k,permutations, Axes_association, number = 5):
    
    # Load the information:
    
    # GO embeddings:
    
    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # P_values:
    
    p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/{permutations}/Adjusted_P_value_PPI_PPMI_{k}.csv',  index_col = 0, dtype={0: str})
    
    # Loop:
    
    dimension_counter = 0
    
    for dimension in p_values.index:
        list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
        if len(list_Assoc) > 0:
            Place_Domains_into_Axes(dimension_counter, dimension, GO_Embeddings, p_values, number)
            dimension_counter+=1
        else:
            continue

def Give_clusters(k, dimension_counter):
    
    # Load the Axes information:
    
    file = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Axe_{dimension_counter}.csv', sep = " ")
    
    # Create the clusters:
    
    term_ids = []
    
    for line in range(len(file)):
        file_line = file.loc[line]
        if file_line["Eliminated"] == True:
            temp_representative = file_line["Representative,"]
            term_ids.append(temp_representative)
        else:
            term_ids.append(file_line["TermID,"])
        
    # Clasfy the terms
    
    clusters = []
    word = "GO"
    for line in range(len(term_ids)):
        Term = term_ids[line]
        if word in Term:
            clusters.append(Term)
        else:
            new_term = clusters[len(clusters) - 1]
            clusters.append(new_term)
            
    # Cluster dataframe:
    
    Cluster_pd = pd.DataFrame({"GO_Term" : file["TermID,"], "Cluster" : clusters})
    
    # Return the information
    
    return(Cluster_pd)
       
            
def Place_Domains_into_Axes(dimension_counter, dimension, GO_Embeddings, p_values, number = 5):
    
    # Load the representative domains:
    
    path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{dimension}/Domain_{dimension_counter}.csv'
    domain                 = pd.read_csv(path_domain, index_col = 0)
    domains_representative = domain.iloc[:number]
    domains_representative = domains_representative.replace(',','', regex=True)
    
    # Create the clusters:
    
    Cluster_pd = Give_clusters(dimension, dimension_counter)
    Cluster_pd = Cluster_pd.replace(',','', regex=True)
    
    # Subset the clusters:
    
    Cluster_pd_sub = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative["GO"])]
    
    # Colors:
    
    dic = Define_Colors_Plot(Cluster_pd_sub, domains_representative)
    
    # Get the distance:
    
    GO_Embeddings_filtered = GO_Embeddings.loc[dimension, Cluster_pd_sub.GO_Term]
    
    # Get the p_value:
    
    p_values_filtered = p_values.loc[dimension, Cluster_pd_sub.GO_Term]
    
    # Descriptions:
    
    descriptions = []
    
    for Cluster in Cluster_pd_sub.Cluster:
        
        descriptions.append(list(domains_representative[domains_representative.GO == Cluster]["Description"]))
    
    descriptions = [item for sublist in descriptions for item in sublist]
    
    # Counts:
    
    counts = []
    
    for Cluster in Cluster_pd_sub.Cluster:
        
        counts.append(list(domains_representative[domains_representative.GO == Cluster]["Counter"]))
    
    counts = [item for sublist in counts for item in sublist]
    
    # Create the data Frame:
    
    Cluster_pd_sub["Coordenate"]  = GO_Embeddings_filtered.values
    Cluster_pd_sub["p_value"]     = p_values_filtered.values
    Cluster_pd_sub["Description"] = descriptions
    Cluster_pd_sub["Counts"]      = counts
    
    # Order the dataframe to fit with the other plot:
    
    Cluster_pd_sub = Cluster_pd_sub.sort_values("Counts", ascending = False)
    
    # Plot the distance:
    
    fig, ax = plt.subplots(figsize=(12,9), facecolor='w')
    plt.rcParams.update({'font.size': 22})

    # Plot the line:
    
    # Adapt the line:
    
    if max(Cluster_pd_sub["Coordenate"]) > 1:
        x2 = int(max(Cluster_pd_sub["Coordenate"])+1)
    else:
        x2 = 1
    
    plt.plot([0,x2], [0,0], lw=4) 
    ax.set_ylim([-1,1])
    
    # Add the ticks directly to the line:
    
    # Get the labels:
    
    line_ticks = np.linspace(0, x2, 6)
    labels     = ['{:.2f}'.format(tt) for tt in line_ticks]
    
    # Draw the things that we need (numbers and ticks):
    
    for label in range(len(labels)):
        ax.text(line_ticks[label],   -0.15, labels[label], ha = "center")
        plt.vlines(line_ticks[label], -0.05, 0.05, colors='#1f77b4', lw=3)

    # Scatter Plot:
    
    # Set the markersize depending on the p_value:
    
    p_values    = Cluster_pd_sub.p_value
    
    normal_size = 8
    size       = ((np.log10(p_values)*-1) + normal_size).reset_index(drop = True)
    size_black = (size + 1).reset_index(drop = True)
    
    # Plot the Scatter:
    
    groups = Cluster_pd_sub.groupby("Description") 
    Cluster_pd_sub = Cluster_pd_sub.reset_index(drop = True)
    plots_dots = []
    
    names_legend = []
    
    for i, (name, group) in enumerate(groups):
        print(name)
        plt.plot(group["Coordenate"], [0] *len(group), marker="o", linestyle="", label=name, color = "black", markersize=size_black[i])
        p2 = plt.plot(group["Coordenate"], [0] *len(group), marker="o", linestyle="", label=name, color = dic[name], markersize=size[i])
        plots_dots.append(p2)
        names_legend.append(name)
    plots_dots = [item for sublist in plots_dots for item in sublist]
    
    # Remove all the axes:
    
    plt.axis('off')
    
    list_description = []
    for description in names_legend:
        list_description.append(dic[description][0])
        
    Order_Stuff = pd.DataFrame({"Value" : list_description, "Description" : names_legend, "Colors" : plots_dots})
    Order_Stuff = Order_Stuff.sort_values("Value")
    
    # Legend:
    
    plt.legend(Order_Stuff["Colors"], Order_Stuff["Description"], prop={'size': 15})
   
    # Save the figure:
    
    plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{dimension}/Scatter_Plots/SCatter_Pie_Axe_{dimension_counter}.png',dpi=300, bbox_inches = "tight")

def Define_Colors_Plot(Cluster_pd_sub, domains_representative, colors_id = 8):
    
    # Prepare the way of generating the colors
    
   descriptions = []
    
   for Cluster in Cluster_pd_sub.Cluster:
        
       descriptions.append(list(domains_representative[domains_representative.GO == Cluster]["Description"]))
    
   descriptions = [item for sublist in descriptions for item in sublist]
    
    # Counts:
    
   counts = []
    
   for Cluster in Cluster_pd_sub.Cluster:
        
       counts.append(list(domains_representative[domains_representative.GO == Cluster]["Counter"]))
    
   counts = [item for sublist in counts for item in sublist]
    
   # Create the data Frame:
    
   Cluster_pd_sub["Description"] = descriptions
   Cluster_pd_sub["Counts"]      = counts
   Cluster_pd_sub = Cluster_pd_sub.sort_values("Counts", ascending = False)
    
   # Colors
   
   cmap     = plt.get_cmap('Spectral')
   colors   = [cmap(i) for i in np.linspace(0, 1, colors_id)]
    
   dic          = {new_list: [] for new_list in list(Cluster_pd_sub.Description)} 
   descriptions = list(Cluster_pd_sub.Description.drop_duplicates())
   color_count = 0
    
   for des in descriptions:
       description_incluster = list(Cluster_pd_sub[Cluster_pd_sub.Description == des]["Description"])
       color_cluster = colors[color_count]
       color_count+=1
       for GO in description_incluster:
           dic[GO] = color_cluster   
            
   return(dic)           
    

def Give_Dimension_GO_Distance_Domain(k, Taxons_Dic, dimension_counter, dimension, GO_Embeddings, p_values, number):
    
    # Load the representative domains:
    
    path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{dimension_counter}.csv'
    domain                 = pd.read_csv(path_domain, index_col = 0)
    domains_representative = domain.iloc[:number]
    domains_representative = domains_representative.replace(',','', regex=True)
    domains_representative = domains_representative[domains_representative.GO.isin(list(Taxons_Dic.keys()))]
    
    # Get the information:
    
    GO_terms_list  = list(domains_representative.GO)
    dimension_list = np.repeat(dimension, len(GO_terms_list))
    distance       = GO_Embeddings.loc[dimension, GO_terms_list]
    
    # Count the number of species:
    
    count_list = [len(Taxons_Dic[GO]) for GO in GO_terms_list]
    
    # Generate the Data to give back:
    
    Result = pd.DataFrame({"GO" : GO_terms_list, "Dimension" : dimension_list, "Distance" : distance, "Evolution" : count_list}).reset_index(drop = True)
    
    return(Result)

def Give_Dimension_GO_Distance_Cluster(k, Taxons_Dic, dimension_counter, dimension, GO_Embeddings, p_values, number = 5, normalized = False): 
    
    # Load the representative domains:
    
    path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{dimension_counter}.csv'
    domain                 = pd.read_csv(path_domain, index_col = 0)
    domains_representative = domain.iloc[:number]
    domains_representative = domains_representative.replace(',','', regex=True)
    
    # Get the clusters:
    
    Cluster_pd = Give_clusters(k, dimension_counter)
    Cluster_pd = Cluster_pd.replace(',','', regex=True)
    
    # Filters:
    
    Cluster_pd = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative.GO)]
    Cluster_pd = Cluster_pd[Cluster_pd.GO_Term.isin(list(Taxons_Dic.keys()))]
    
    # Get the information:
    
    GO_terms_list  = list(Cluster_pd.GO_Term)
    dimension_list = np.repeat(dimension, len(GO_terms_list))
    distance       = GO_Embeddings.loc[dimension, GO_terms_list]
    
    if normalized == True:
        norm = np.LA.norm(GO_Embeddings.loc[dimension].values)
        distance = distance/norm

    # Count the number of species:
    
    count_list = [len(Taxons_Dic[GO]) for GO in GO_terms_list]
    
    # Generate the Data to give back:
    
    Result = pd.DataFrame({"GO" : GO_terms_list, "Dimension" : dimension_list, "Distance" : distance, "Evolution" : count_list}).reset_index(drop = True)
    
    return(Result)   
    

def Distance_Axes_Evolutionary_Converation_all(dim_list, common = True, plot = True):
    
    if common == True:
        common_GO = list(Filter_Common_GO(dim_list))
    
    Results_Final = pd.DataFrame()
    
    for k in dim_list:
        if k <= 500:
            Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(80000, k, "PPMI")
            Results_Final_it = Distance_Axes_Evolutionary_Converation(k, 80000, Axes_association, number = 5)
            Results_Final = Results_Final.append(Results_Final_it)
        else:
            Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(100000, k, "PPMI")
            Results_Final_it = Distance_Axes_Evolutionary_Converation(k, 100000, Axes_association, number = 5)
            Results_Final = Results_Final.append(Results_Final_it)
            
    if common == True:
        
        Results_Final_sub = Results_Final[Results_Final.GO.isin(common_GO)]
        
        if plot == True:
            Density_Plot_Conserved_Distance_Axes(240,Results_Final_sub, "ALL_Common")
        else:
            return(Results_Final_sub)
    
 
def Distance_Axes_Evolutionary_Converation(k, permutations, Axes_association, number = 5):
    
    # Load the information:
    
    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))
    
    # Evolutionary conservation:
    
    # GO embeddings:
    
    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # P_values:
    
    p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/{permutations}/Adjusted_P_value_PPI_PPMI_{k}.csv',  index_col = 0, dtype={0: str})
    
    # Set the result dataframe:
    
    Result          = pd.DataFrame(columns=['GO', 'Dimension', 'Distance', 'Evolution'])
    Result_Clusters = pd.DataFrame(columns=['GO', 'Dimension', 'Distance', 'Evolution'])
    
    # Loop:
    
    dimension_counter = 0
    for dimension in p_values.index:
        list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
        if len(list_Assoc) > 0:
            # For domains:
            Result_it = Give_Dimension_GO_Distance_Domain(k, Taxons_Dic, dimension_counter, dimension, GO_Embeddings, p_values, number)
            Result    = Result.append(Result_it)
            # For clusters:
            Result_Clusters_it = Give_Dimension_GO_Distance_Cluster(k, Taxons_Dic, dimension_counter, dimension, GO_Embeddings, p_values)
            Result_Clusters    = Result_Clusters.append(Result_Clusters_it)
            dimension_counter+=1
        else:
            continue
        
    # Prepare the results for plotting:
    
    Result          = Result.reset_index(drop = True)
    Result          = Result.sort_values("Distance")
    Result_Clusters = Result_Clusters.reset_index(drop = True)
    Result_Clusters = Result_Clusters.sort_values("Distance")
    
    # Plot the results:
    
    #Density_Plot_Conserved_Distance_Axes(k, Result, "Domains")
    #Density_Plot_Conserved_Distance_Axes(k, Result_Clusters, "Clusters")
    
    return(Result)
    
    
def Density_Plot_Conserved_Distance_Axes(k,Result, Name):
    
    # Get the density information:
    
    xy = np.vstack([list(Result["Distance"].values),list(Result["Evolution"].values)])
    z  = stats.gaussian_kde(xy)(xy)
    
    fig, ax = plt.subplots(figsize=(6,5), facecolor='w')
    plt.rcParams.update({'font.size': 22})
    ax.scatter(list(Result["Distance"]), list(Result["Evolution"]), c=z, s=100, edgecolor=None)
    plt.xlabel("Distance")
    plt.ylabel("#Taxons")
               
    # Write the Correlation in the plot:

    Spearman = round(stats.spearmanr(Result["Distance"], Result["Evolution"]).correlation,2)
    ax.text(max(Result.Distance - 2.5), 1, f'Spearman: {Spearman}', ha = "center", size = "small") 
    
    # Save the figure:

    plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Density_Plots/Density_plot_{Name}.png',dpi=300, bbox_inches = "tight")      


def Correlate_Density_Evolution_Adj(k, permutations, Network, Axes_association, data_path):
    
    # Load the network:
    
    network = np.load(f'{data_path}Human_Biogrid_Adj_PPI_.npy', allow_pickle=True)
    
    # Load the gene names:
    
    Path_Genes = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/Human_Biogrid_Genes_PPI_", header = None)
    
    # Get the pandas DataFrame:
    
    Network_db         = pd.DataFrame(network)
    Network_db.columns = Path_Genes[0].map(str)
    Network_db.index   = Path_Genes[0].map(str)
    
    GO_list = list(set([item for sublist in Axes_association for item in sublist]))
    
    # GO annotation:
    
    GO_matrix = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/_Matrix_Genes_GO_BP_Back_Propagation_PPI.csv", index_col = 0,dtype={0: str})  

    # Loop over the PPMI:
    
    GO_terms = []
    density  = []
    
    for GO in GO_list:
        
        genes = GO_matrix[GO]
        genes = list(genes[genes == 1].index)
        genes = [str(i) for i in genes]
        
        # Calculate the density:
        
        if len(genes) > 2:
            
            GO_terms.append(GO)
            
            # Subselect the subnetwork:
            
            subnetwork = Network_db.loc[genes,genes]
            
            # Get the aupper triangle:
            
            upper_diag = np.array(subnetwork.values[np.triu_indices(len(subnetwork), k = 1)])
            density_it = sum(upper_diag)/len(upper_diag)
            
            # Save the information:
            
            density.append(density_it)
    
    # Generate the Matrix:
    
    Density_pd = pd.DataFrame({"GO" : GO_terms, "Density" : density})
    
    # Load the Taxons:
    
    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))
    
    taxons_list = []
    
    Density_pd = Density_pd[Density_pd.GO.isin(Taxons_Dic.keys())]
    
    for GO in Density_pd["GO"]:
        
        taxons_list.append(len(Taxons_Dic[GO]))
    
    # Final Matrix for the Analyses:
    
    Density_pd["Taxons"] = taxons_list
    
    Density_pd = Density_pd.sort_values("Density")

    plt.scatter(Density_pd["Density"], Density_pd["Taxons"])
    round(stats.spearmanr(Density_pd["Density"], Density_pd["Taxons"]).correlation,2)
    

def Get_Euclidean_Clusters(GO_Embeddings, Cluster_pd, dimension):
    
    # Choose the Correct GO dimension and GO terms:
    
    GO_Embedding_subset = GO_Embeddings.loc[dimension, Cluster_pd.GO_Term]
    
    # Calculate the Euclidean pairwise distance:
    
    scores_np       = np.array(GO_Embedding_subset)
    distance_scores = euclidean_distances(scores_np.reshape(-1, 1)) 
    
    GO_embedding_Euclidean = pd.DataFrame(distance_scores, index = [GO_Embedding_subset.index], columns = [GO_Embedding_subset.index])

    # Get the clusters based on Euclidean:
    
    # Get the optimal number of clusters:
    
    num_clusters = Optimal_Eclidean_Clusters(GO_embedding_Euclidean, Cluster_pd)
    
    # Get the clusters:
    
    cluster            = AgglomerativeClustering(n_clusters = num_clusters, affinity='precomputed', linkage = "complete")
    Euclidean_clusters = cluster.fit_predict(GO_embedding_Euclidean)
    
    # Prepare the Dataframe to return
    
    Euclidean_result = pd.DataFrame({"GO_Term" : Cluster_pd.GO_Term, "Group" : Euclidean_clusters})
    return(Euclidean_result)
       
def Optimal_Eclidean_Clusters(GO_embedding_Euclidean, Cluster_pd):
    
    # Number of maximum clusters:
    
    maximum = len(Cluster_pd.Cluster)
    
    # Get the optimal:
    
    Optimal = {}
    
    for clusters in range(2, maximum):

        # Try with a concrete number of clusters:
        
        cluster = AgglomerativeClustering(n_clusters = clusters, affinity='precomputed', linkage = "complete")
        
        # Get the clusters:
        
        Euclidean_clusters = cluster.fit_predict(GO_embedding_Euclidean)
        
        # Calculate the Silhouette score:
        
        Siluette = silhouette_score(GO_embedding_Euclidean, Euclidean_clusters, metric='euclidean')
        
        # Save the score:
        
        Optimal[clusters] = round(Siluette, 2)
        
    # Calculate the optimal one:
    
    # First we keep the those dimensions that have a maximum value of siluette:
    
    max_value = max(Optimal.values())
    
    Optimal_dimensions = [k for k, v in Optimal.items() if v == max_value]
    
    # For the maximum Siluette values, return the one that corresponds to the lower number of clusters:
    
    return(min(Optimal_dimensions))
    
def Calculate_Rand_Index(Cluster_pd, Euclidean_clusters):
    
    # Calculate the rand index:
    
    rand = adjusted_rand_score(Cluster_pd.Group, Euclidean_clusters.Group)
    
    return(rand)
    
def Compute_Inter_Intra_Distance(GO_Embeddings, Cluster_pd, dimension, dimension_counter, color_dic):
  
    # Choose the Correct GO dimension and GO terms:
    
    GO_Embedding_subset = GO_Embeddings.loc[dimension, Cluster_pd.GO_Term]
    
    # Calculate the Euclidean pairwise distance:
    
    scores_np              = np.array(GO_Embedding_subset)
    distance_scores        = euclidean_distances(scores_np.reshape(-1, 1)) 
    GO_embedding_Euclidean = pd.DataFrame(distance_scores, index = [GO_Embedding_subset.index], columns = [GO_Embedding_subset.index])
    
    # Organize the colors:
    
    dic = {new_list: [] for new_list in list(Cluster_pd.GO_Term)} 
    
    Cluster_pd = Cluster_pd.reset_index(drop = True)
    
    for description in range(len(Cluster_pd)):
        
        description_it = Cluster_pd.loc[description, "Description"]
        GO_term        = Cluster_pd.loc[description, "GO_Term"]
        
        # Add the values to the dic:
        
        dic[GO_term] = color_dic[description_it]
    
    # Prepare the Euclidean matrix:
    
    index_real   = [i[0] for i in GO_embedding_Euclidean.index]
    columns_real = [i[0] for i in GO_embedding_Euclidean.columns]
    
    GO_embedding_Euclidean.index   = index_real
    GO_embedding_Euclidean.columns = columns_real
    
    # Calaculate inter and intra cluster distance:
    
    intra_mean, intra_std = Give_Intra_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd)
    inter_mean, inter_std = Give_Inter_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd)
    
    # Return the values:
    
    return(intra_mean, intra_std, inter_mean, inter_std)

def Plot_Dendogram(k, Rand_it, GO_Embeddings, Cluster_pd, dimension, dimension_counter, color_dic, hierarchy = False):
    
    # Choose the Correct GO dimension and GO terms:
    
    GO_Embedding_subset = GO_Embeddings.loc[dimension, Cluster_pd.GO_Term]
    
    # Calculate the Euclidean pairwise distance:
    
    scores_np              = np.array(GO_Embedding_subset)
    distance_scores        = euclidean_distances(scores_np.reshape(-1, 1)) 
    GO_embedding_Euclidean = pd.DataFrame(distance_scores, index = [GO_Embedding_subset.index], columns = [GO_Embedding_subset.index])
    
    # Organize the colors:
    
    dic = {new_list: [] for new_list in list(Cluster_pd.GO_Term)} 
    
    Cluster_pd = Cluster_pd.reset_index(drop = True)
    
    for description in range(len(Cluster_pd)):
        
        description_it = Cluster_pd.loc[description, "Description"]
        GO_term        = Cluster_pd.loc[description, "GO_Term"]
        
        # Add the values to the dic:
        
        dic[GO_term] = color_dic[description_it]
    
    # Prepare the Euclidean matrix:
    
    index_real   = [i[0] for i in GO_embedding_Euclidean.index]
    columns_real = [i[0] for i in GO_embedding_Euclidean.columns]
    
    GO_embedding_Euclidean.index   = index_real
    GO_embedding_Euclidean.columns = columns_real
    
    # Plot the clustermap:
    
    if hierarchy == False:
        
        linkage_t = hc.linkage(GO_embedding_Euclidean, method='average')
        plot    = sns.clustermap(GO_embedding_Euclidean, cmap="jet", row_linkage=linkage_t, col_linkage=linkage_t, yticklabels=True)
        sns.set(font_scale = 2)
        for tick_label in plot.ax_heatmap.axes.get_yticklabels():
            tick_text = tick_label.get_text()
            tick_label.set_color(dic[tick_text])
    
        # Save the results
        
        plot.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Hierarchycal_euclidean_Plots/Hierarchy_{dimension_counter}.png')
    
    elif hierarchy == True:
        
        sns.set_style("white")
        plt.rc('font', weight='bold')
        fig, ax = plt.subplots(figsize=(8,16), facecolor='w')
        
        Z    = hc.linkage(GO_embedding_Euclidean, method='average')
        plt.clf()
        
        with plt.rc_context({'lines.linewidth': 5}):
            dendrogram(Z, leaf_rotation=0, leaf_font_size=12, labels=GO_embedding_Euclidean.index,  
                                color_threshold=0, above_threshold_color = "#0eaccf", orientation = "right")

        # Change colors of the labels:
        
        ax = plt.gca()
        xlbls = ax.get_ymajorticklabels()
        for lbl in xlbls:
            lbl.set_color(dic[lbl.get_text()])
        ax.tick_params(axis='y', which='major', labelsize=15)
        
        # Delete the x labels:
        
        plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
        
        # Write the rand between the two methods:
        
        ax.text(0.78, 0.95, f'Rand Index: {round(Rand_it,2)}', ha = "center", verticalalignment='center', transform = ax.transAxes,
                size = 19)
        
        # Save the plot:
        
        plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Ontology_Plots/Ontology_Euclidean_{dimension_counter}.png',dpi=300, bbox_inches = "tight")
        
def Give_Intra_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd):  
    
    # Intra cluster distance:
    
    intra_cluster_distance = []
    intra_cluster_std      = []
    
    for definition in list(set(Cluster_pd.Description)):
        
        GO_Cluster = Cluster_pd[Cluster_pd.Description == definition]
        
        if len(GO_Cluster) > 1:
            
            GO_Cluster = list(GO_Cluster["GO_Term"])
            
            # Get the distances:
            
            GO_embedding_Euclidean_Cluster = GO_embedding_Euclidean.loc[GO_Cluster,GO_Cluster]
            distance = np.array(GO_embedding_Euclidean_Cluster.values[np.triu_indices(len(GO_embedding_Euclidean_Cluster), k = 1)])
            intra_cluster_distance.append(np.mean(distance))
            intra_cluster_std.append(np.std(distance))
            
        else:
            continue
    
    # Return the info:
    
    return(intra_cluster_distance, intra_cluster_std)

def Give_Inter_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd):
    
    inter_cluster_distance = []
    inter_cluster_std      = []
    
    # For each cluster:
    
    clusters_control = list(set(Cluster_pd.Description))
    
    it = 0
    
    for definition in clusters_control:
        
        GO_Cluster = Cluster_pd[Cluster_pd.Description == definition]
        
        if len(GO_Cluster) > 1:
            
            GO_Cluster = list(GO_Cluster["GO_Term"])
            
            # Choose another cluster:
            
            for definition_2 in clusters_control:
                
                if definition_2 != definition:
                
                    GO_Cluster_2 = list(Cluster_pd[Cluster_pd.Description == definition_2]["GO_Term"])
                
                    # Per each GO term in the first cluster:
                    
                    distance = GO_embedding_Euclidean.loc[GO_Cluster, GO_Cluster_2]
                    
                    inter_cluster_distance.append(distance.mean().mean())
                    inter_cluster_std.append(distance.std().std())
                else:
                    continue
                    
        # Remove the clusters from the comparisons:
                
        del clusters_control[it]
        it+=1
    
    # Return the information:
    
    return(inter_cluster_distance, inter_cluster_std)

def Calculate_Inter_Intra_Distances_Euclidean_SS_All_Axes(k, permutations, number = 5, plot_dendograms = True):

    # Load the p-values:
    
    p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/{permutations}/Adjusted_P_value_PPI_PPMI_{k}.csv',  index_col = 0, dtype={0: str})
    
    # Load the GO Embbedinds:
    
    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # Rand Results:
    
    intra_mean_list = []
    inter_mean_list = []
    
    intra_std_list = []
    inter_std_list = []
    
    # Control the dimensions and the axes:
    
    dimension_counter = 0
    for dimension in p_values.index:
        list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
        if len(list_Assoc) > 0:
            
            # Load the representative domain:
            
            path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{dimension_counter}.csv'
            domain                 = pd.read_csv(path_domain, index_col = 0)
            domains_representative = domain.iloc[:number]
            domains_representative = domains_representative.replace(',','', regex=True)
            
            # Get the clusters based on semantic similarity:
            
            Cluster_pd = Give_clusters(k, dimension_counter)
            Cluster_pd = Cluster_pd.replace(',','', regex=True)
            
            # Choose only the most representative ones:
            
            Cluster_pd            = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative.GO)]            
            label_encoder         = LabelEncoder()
            Cluster_pd["Group"]   = label_encoder.fit_transform(Cluster_pd.Cluster)
            color_dic = Define_Colors_Plot(Cluster_pd, domains_representative)
            
            # Get the clusters based on Euclidean distance:
            
            # If we have more than one GO term
            
            # Calculate the distances intra and inter cluster:
            
            if len(set(Cluster_pd["Group"])) > 2:
                
                intra_mean, intra_std, inter_mean, inter_std = Compute_Inter_Intra_Distance(GO_Embeddings, Cluster_pd, dimension, dimension_counter, color_dic)
                
                # Noramlize:
        
                normalize = []
                normalize.extend(intra_mean)
                normalize.extend(inter_mean)
                
                if intra_mean:
                    intra_mean = intra_mean/max(normalize)
                    intra_mean_list.extend(intra_mean)
                if intra_std:
                    intra_std_list.extend(intra_std/max(normalize))
                if inter_mean:
                    inter_mean = inter_mean/max(normalize)
                    inter_mean_list.extend(inter_mean)
                if inter_std:
                    inter_std_list.extend(inter_std/max(normalize))                
                
            dimension_counter+=1
        else:
            continue
        
    # Get the statistics:
    
    p_value = round(mannwhitneyu(intra_mean_list,inter_mean_list, alternative= "less").pvalue,3)
    Z = round(mannwhitneyu(intra_mean_list,inter_mean_list, alternative= "less").statistic,3)

    print(f'Intra mean: {np.mean([x for x in intra_mean_list if str(x) != "nan"])}')
    print(f'Intra std: {np.std([x for x in intra_mean_list if str(x) != "nan"])}')
    print(f'Inter mean: { np.mean([x for x in inter_mean_list if str(x) != "nan"])}')
    print(f'Inter std: { np.std([x for x in inter_mean_list if str(x) != "nan"])}')
    print(f'p_value: {p_value}')
    print(f'Z-score: {Z} ')
    
    
    # Plot it:
    
    if plot_dendograms == True:
    
        f, ax= plt.subplots(figsize=(8,5), facecolor='w')
        intra_mean_list = [x for x in intra_mean_list if str(x) != 'nan']
        inter_mean_list = [x for x in inter_mean_list if str(x) != 'nan']
        sns.distplot(intra_mean_list, color='green', label="Intra")
        sns.distplot(inter_mean_list, color='red', label="Inter")
        plt.legend(bbox_to_anchor=(0.95, 0.97),borderaxespad=0)
        ax.text(0.80, 0.60, f'P_value: {p_value}', ha = "center", verticalalignment='center', transform = ax.transAxes,
                    size = 15)
    
    # Return the distributions:
    
    intra_mean_list_mean = np.mean([x for x in intra_mean_list if str(x) != "nan"])
    inter_mean_list_mean = np.mean([x for x in inter_mean_list if str(x) != "nan"])
    intra_std_list       =  np.mean([x for x in intra_std_list if str(x) != "nan"])
    inter_std_list       = np.mean([x for x in inter_std_list if str(x) != "nan"])
    
    return(intra_mean_list_mean, inter_mean_list_mean, intra_std_list, inter_std_list)
    
def Rand_Index_All_Spaces_Euclidean_DD(dim_list, plot = True):
    
    Rand_results_Final = []
    
    for k in dim_list:
        if k <= 500:
            p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/80000/Adjusted_P_value_PPI_PPMI_{k}.csv', 
                                   index_col = 0, dtype={0: str})
        else:
            p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/100000/Adjusted_P_value_PPI_PPMI_{k}.csv', 
                                   index_col = 0, dtype={0: str})
            
        GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
        GO_Embeddings[GO_Embeddings<0] = 0
        
        Rand_results = []
        dimension_counter = 0
        
        for dimension in p_values.index:
            list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
            if len(list_Assoc) > 0:
                
                # Load the representative domain:
                
                path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{dimension_counter}.csv'
                domain                 = pd.read_csv(path_domain, index_col = 0)
                domains_representative = domain.iloc[:5]
                domains_representative = domains_representative.replace(',','', regex=True)
                
                # Get the clusters based on semantic similarity:
                
                Cluster_pd = Give_clusters(k, dimension_counter)
                Cluster_pd = Cluster_pd.replace(',','', regex=True)
                
                # Choose only the most representative ones:
                
                Cluster_pd            = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative.GO)]            
                label_encoder         = LabelEncoder()
                Cluster_pd["Group"] = label_encoder.fit_transform(Cluster_pd.Cluster)
                               
                # Get the clusters based on Euclidean distance:
                
                # If we have more than one GO term
                
                if len(set(Cluster_pd["Group"])) > 2:
                    Euclidean_clusters = Get_Euclidean_Clusters(GO_Embeddings, Cluster_pd, dimension)
                    
                # If we only have one GO term:
                else:
                    Euclidean_clusters = pd.DataFrame({"GO_Term" : Cluster_pd.GO_Term, "Group" : Cluster_pd["Group"]})
                if len(set(Cluster_pd["Group"])) > 2:
                    Euclidean_clusters = Get_Euclidean_Clusters(GO_Embeddings, Cluster_pd, dimension)
                # Get the rand index of this axes:
                Rand_results.append(Calculate_Rand_Index(Cluster_pd, Euclidean_clusters))
                dimension_counter+=1
            else:
                continue
            
        Rand_results_Final.append(Rand_results)
        
    Rand_results_Final = [item for sublist in Rand_results_Final for item in sublist]
        
    if plot == True:
        plt.boxplot(Rand_results_Final)
        plt.xlabel("Axes")
        plt.ylabel("Rand Index")
    else:
        return(Rand_results_Final)
            
        
 
def Rand_Index_All_Axes_Euclidean_SS(k, permutations, number = 5, plot_dendograms = True, ontology = False):
    
    # Load the p-values:
    
    p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/{permutations}/Adjusted_P_value_PPI_PPMI_{k}.csv',  index_col = 0, dtype={0: str})
    
    # Load the GO Embbedinds:
    
    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # Rand Results:
    
    Rand_results = []
    
    # Control the dimensions and the axes:
    
    dimension_counter = 0
    for dimension in p_values.index:
        list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
        if len(list_Assoc) > 0:
            
            # Load the representative domain:
            
            path_domain            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{dimension_counter}.csv'
            domain                 = pd.read_csv(path_domain, index_col = 0)
            domains_representative = domain.iloc[:number]
            domains_representative = domains_representative.replace(',','', regex=True)
            
            # Get the clusters based on semantic similarity:
            
            Cluster_pd = Give_clusters(k, dimension_counter)
            Cluster_pd = Cluster_pd.replace(',','', regex=True)
            
            # Choose only the most representative ones:
            
            Cluster_pd            = Cluster_pd[Cluster_pd.Cluster.isin(domains_representative.GO)]            
            label_encoder         = LabelEncoder()
            Cluster_pd["Group"] = label_encoder.fit_transform(Cluster_pd.Cluster)
            color_dic = Define_Colors_Plot(Cluster_pd, domains_representative)
            
            # Get the clusters based on Euclidean distance:
            
            # If we have more than one GO term
            
            if len(set(Cluster_pd["Group"])) > 2:
                Euclidean_clusters = Get_Euclidean_Clusters(GO_Embeddings, Cluster_pd, dimension)
                
            # Plot dendograms if needed:
                if plot_dendograms == True:
                    Rand_it = Calculate_Rand_Index(Cluster_pd, Euclidean_clusters)
                    if ontology == False:
                        Plot_Dendogram(k, Rand_it, GO_Embeddings, Cluster_pd, dimension, dimension_counter, color_dic)
                    else:
                        Plot_Dendogram(k, Rand_it, GO_Embeddings, Cluster_pd, dimension, dimension_counter, color_dic, hierarchy = True)
            # If we only have one GO term:
            else:
                Euclidean_clusters = pd.DataFrame({"GO_Term" : Cluster_pd.GO_Term, "Group" : Cluster_pd["Group"]})
            
            # Get the rand index of this axes:
        
            Rand_results.append(Calculate_Rand_Index(Cluster_pd, Euclidean_clusters))

            dimension_counter+=1
        else:
            continue
        
        # Plot a BoxPlot:
        
    plt.boxplot(Rand_results)
    plt.xlabel("Axes")
    plt.ylabel("Rand Index")
        
    plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Rand_Plot/Density_plot_Rand_Index.png',dpi=300, bbox_inches = "tight")

def Jaccar_index(cluster1, cluster2):
    intersection = len(list(set(cluster1).intersection(cluster2)))
    union = (len(cluster1) + len(cluster2)) - intersection
    return float(intersection) / union 

def Get_Similar_Axes_Pair_list(Results_Jaccard, all_pairs = False):
    
    # To avoid the diagonal we set 0s to it:
    
    Results = Results_Jaccard
    Results.values[[np.arange(Results.shape[0])]*2] = 0
    
    # Now we get the maximum value of each Axis:
    
    # If only we want to report the pairs with the highest value:
    
    if all_pairs == False:
        Pairs  = pd.DataFrame(Results.idxmax()).reset_index()
        values = []
    
        # Get its value:
    
        for pair in range(len(Pairs)):
            position = Pairs.loc[pair]
            values.append(Results_Jaccard.loc[position[0], position["index"]])
   
        # Create the DataFrame:
        
        Pairs["Value"] = values
        Pairs.columns =  ["Axes_1", "Axes_2", "Value"]
        
        # Order the information:
        
        Pairs = Pairs.sort_values("Value", ascending = False)
        Pairs = Pairs[Pairs['Axes_1'] != Pairs['Axes_2']]
        
        return(Pairs)
    
    # If we want all the comparisons:
    
    else:
        
        # Get all the pairs:
        
        Pairs = [["Axes_1", "Axes_2", "Value"]]
        counter = 0
        for dim1 in range(0,len(Results)):
            for dim2 in range(dim1,len(Results)):
                score = Results.loc[dim1,dim2]
                Pairs.append([dim1, dim2,score])
            counter += 1
        Pairs_db = pd.DataFrame(Pairs[1:],columns=Pairs[0])
        Pairs_db = Pairs_db[Pairs_db['Axes_1'] != Pairs_db['Axes_2']]
        return(Pairs_db)

def Plot_Jaccard_Overlap(Results_Jaccard):
    
    # Plot the clustermap using a precomputed distance:
    
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    cax = ax.imshow(Results_Jaccard.values, interpolation='nearest', cmap="jet")
    fig.colorbar(cax)

def Taxons_Jaccard_Specificity(Taxons_Dic,GO_Levels, GO_axes_1, GO_axes_2,Leaf_Annotation):
    
    # Load ways of definding specificity:
    
    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json")) 
    GO_Levels  = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/GO_Terms_Level_Matrix_" +  "BP_Back_Propagation" + ".csv",
                            index_col = 0, dtype={0: str})
    
    # Getting the List of GO terms:
    
    all_list     = set(GO_axes_1 + GO_axes_2)
    GO_axes_1    = set(GO_axes_1)
    GO_axes_2    = set(GO_axes_2)
    intersection = GO_axes_1.intersection(GO_axes_2)
    unique       = all_list - intersection
    
    # Prepare the lists for the distributions:
    
    distribution_both_level    = []
    distribution_unique_level  = []
    distribution_both_taxons   = []
    distribution_unique_taxons = []
    
    # Taxons:
    
    Leaf_Annotation = set(Leaf_Annotation.columns)
    leaf_both       = len(intersection.intersection(Leaf_Annotation))/len(intersection)
    
    try:
        leaf_unique = len(unique.intersection(Leaf_Annotation))/len(unique)
    except ZeroDivisionError:
        leaf_unique = 0
        
    for GO in list(intersection):
        try:
            distribution_both_level.append(int(GO_Levels[GO_Levels.GO == GO].Level))
            distribution_both_taxons.append(len(Taxons_Dic[GO]))
        except KeyError:
            continue
    for GO in list(unique):
        try:
            distribution_unique_level.append(int(GO_Levels[GO_Levels.GO == GO].Level))
            distribution_unique_taxons.append(len(Taxons_Dic[GO]))
        except KeyError:
            continue
    
    # Return information:
    
    return(distribution_both_level, distribution_both_taxons, distribution_unique_level, distribution_unique_taxons,
           leaf_both,leaf_unique)

def Apply_Specificity_Jaccard_All(dimension_list, Leaf_Annotation, all_jacc_bool = False, common = False):
    
    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json")) 
    GO_Levels  = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/GO_Terms_Level_Matrix_" +  "BP_Back_Propagation" + ".csv",
                            index_col = 0, dtype={0: str})
    
    # If only Common GO terms between the samples are needed:
    
    if common == True:
        common_GO = list(Filter_Common_GO(dimension_list))
        
    # Init the final lists:
    
    # Levels:
    
    level_distribution_all_final    = []
    level_distribution_unique_final = []
    
    # Taxons:
    
    Taxons_distribution_all_final      = []
    Taxons_distribution_unique_final   = []
    
    # Leaf:
    
    Leaf_distribution_all_final      = []
    Leaf_distribution_unique_final   = []
    
    # Iterate:
    
    for dimension in dimension_list:
        
        print(dimension)
        
        if dimension < 500:
            repetition = 80000
        else:
            repetition = 100000
        
        # Get the associations:
        Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(repetition, dimension, "PPMI")

        if common == True:
            Axes_association = [x for x in Axes_association if x[0] in common_GO]
            
        # Get the pairs:
        pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes_association, plot = False, all_jacc = all_jacc_bool)
        pairs_axes = pairs_axes.reset_index(drop = True)
        
        # Lists to keep the iter info:
        
        level_both_it    = []
        level_unique_it  = []
        taxons_both_it   = []
        taxons_unique_it = []
        leaf_both_it     = []
        leaf_unique_it   = []
        
         # Per each pair:
         
        for pair in range(len(pairs_axes)):
            
            if (pairs_axes.loc[pair]["Value"] > 0) & (pairs_axes.loc[pair]["Value"] <= 0.6) :
             
                 GO_axes_1 = Axes_association[int(pairs_axes.loc[pair]["Axes_1"])]
                 GO_axes_2 = Axes_association[int(pairs_axes.loc[pair]["Axes_2"])]
                 
                 # Get the results
                 
                 result_it = Taxons_Jaccard_Specificity(Taxons_Dic,GO_Levels, GO_axes_1, GO_axes_2,Leaf_Annotation)
                 
                 # Update the lists:
                 
                 level_both_it.extend(result_it[0])
                 level_unique_it.extend(result_it[2])
                 taxons_both_it.extend(result_it[1])
                 taxons_unique_it.extend(result_it[3])
                 leaf_both_it.extend([result_it[4]])
                 leaf_unique_it.extend([result_it[5]])
            
            else:
                continue
        
        # Keep the info per each dimensions:
        
        # Levels:
        
        level_distribution_all_final.append(level_both_it)
        level_distribution_unique_final.append(level_unique_it)
    
        # Taxons:
    
        Taxons_distribution_all_final.append(taxons_both_it)
        Taxons_distribution_unique_final.append(taxons_unique_it)   
        
        # Leaf:
        
        Leaf_distribution_all_final.append(leaf_both_it)
        Leaf_distribution_unique_final.append(leaf_unique_it)
        
    # Plots:
    
    # Taxons PLots:
    
    Leaf_flat_all    = [item for sublist in Leaf_distribution_all_final for item in sublist]
    Leaf_flat_uniq   = [item for sublist in Leaf_distribution_unique_final for item in sublist]
    
    p_value = mannwhitneyu(Leaf_flat_all,Leaf_flat_uniq, alternative = "less").pvalue
    
    # Plot boxplots:
    
    category_level     = ["Both"] * len(Leaf_flat_all)
    category_level.extend(["Unique"] * len(Leaf_flat_uniq))
    
    # Final information for the format:
    
    Leaf_all = Leaf_flat_all + Leaf_flat_uniq
    Leaf_all = [i * 100 for i in Leaf_all]
    Data_Plot = pd.DataFrame({"Leaf" : Leaf_all, "Group" : category_level})
    
    # Plot it:
        
    f, ax= plt.subplots(figsize=(8,6), facecolor='w')
    
    for group in list(set(category_level)):
        sns.boxplot(x='Group',y='Leaf',data=Data_Plot)
        plt.xlabel('Groups', size = 20)
        plt.ylabel('% Leaf Annotations', size = 20)
        plt.xticks(size = 15)
        plt.yticks(size = 15)

    # levels plot:

    # Flat lists:
    
    level_flat_all      = [item for sublist in level_distribution_all_final for item in sublist]
    level_flat_unique   = [item for sublist in level_distribution_unique_final for item in sublist]
    
    p_value = mannwhitneyu(level_flat_all,level_flat_unique, alternative = "less").pvalue
    
    # Get the labels:
    
    category_level     = ["Both"] * len(level_flat_all)
    category_level.extend(["Unique"] * len(level_flat_unique))
    
    # Final information for the format:
    
    levels_all = level_flat_all + level_flat_unique
    Data_Plot = pd.DataFrame({"Level" : levels_all, "Group" : category_level})
    
    p_value = mannwhitneyu(level_flat_unique,level_flat_unique, alternative = "less").pvalue
    
    # Plot it:
    
    f, ax= plt.subplots(figsize=(8,6), facecolor='w')
    
    for group in list(set(category_level)):
        sns.boxplot(x='Group',y='Level',data=Data_Plot)
        plt.xlabel('Groups', size = 20)
        plt.ylabel('GO Level', size = 20)
        plt.xticks(size = 15)
        plt.yticks(size = 15)
        
    # Taxons:
    
    # Flat the lists:

    taxons_flat_all      = [item for sublist in Taxons_distribution_all_final for item in sublist]
    taxons_flat_unique   = [item for sublist in Taxons_distribution_unique_final for item in sublist]
    
    p_value = mannwhitneyu(taxons_flat_all,taxons_flat_unique, alternative = "greater").pvalue
    
    # Labels:
    
    category_level     = ["Both"] * len(taxons_flat_all)
    category_level.extend(["Unique"] * len(taxons_flat_unique))  

    # Final information for the format:
    
    taxons_all = taxons_flat_all + taxons_flat_unique
    Data_Plot = pd.DataFrame({"Taxons" : taxons_all, "Group" : category_level})
    
    # Plot it:
        
    f, ax= plt.subplots(figsize=(8,6), facecolor='w')
    
    for group in list(set(category_level)):
        sns.boxplot(x='Group',y='Taxons',data=Data_Plot)
        plt.xlabel('Groups', size = 20)
        plt.ylabel('# Taxons', size = 20)
        plt.xticks(size = 15)
        plt.yticks(size = 15)
            
    

def Compare_Distributions_Jaccard(list_distribution, dimension_list):
    
    dimension_list_names = [str(i) for i in dimension_list]

    Result       = pd.DataFrame(0, index= np.arange(len(list_distribution)), columns = dimension_list_names) 
    Result.index = dimension_list_names
    
    for distribution in range(len(list_distribution)):
        
        for distribution_2 in  range(len(list_distribution)):
        
        
            Result.loc[dimension_list_names[distribution], dimension_list_names[distribution_2]] = round(mannwhitneyu(list_distribution[distribution], list_distribution[distribution_2]).pvalue,2)
    
    # Plot it:
    
    Result[Result>0.05] = 1
    Result[Result<0.05] = 0
    
    plt.rc('font', weight='bold')
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111)
    ax.matshow(Result ,interpolation='nearest',cmap="jet")
    plt.xticks([0,1,2,3,4,5,6,7,8], dimension_list, fontsize=14)
    plt.yticks([0,1,2,3,4,5,6,7,8], dimension_list, fontsize = 14)

def Filter_Common_GO(dimension_list):
    
    list_common = []
    
    # Associate GO to Axes:
    
    for dimension in dimension_list:
        if dimension < 500:
            repetition = 80000
        else:
            repetition = 100000
        Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(repetition, dimension, "PPMI")
        Axes_association = [item for sublist in Axes_association for item in sublist]
        list_common.append(Axes_association)
    
    # Create the sets:
    
    lists_sets = [set(i) for i in list_common]
    
    # Get the intersection:
    
    common_GO = set.intersection(*lists_sets)
    
    return(list(common_GO))
    
def Common_GO_Statistics(dimension_list, Leaf_Annotation):
    
    # Read the information needed:

    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json")) 
    GO_Levels  = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/GO_Terms_Level_Matrix_" +  "BP_Back_Propagation" + ".csv",
                            index_col = 0, dtype={0: str})
   
    # Get commons:
    
    list_common = []
    
    # Associate GO to Axes:
    
    for dimension in dimension_list:
        if dimension < 500:
            repetition = 80000
        else:
            repetition = 100000
        Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(repetition, dimension, "PPMI")
        Axes_association = [item for sublist in Axes_association for item in sublist]
        list_common.append(Axes_association)
    
    # Create the sets:
    
    lists_sets = [set(i) for i in list_common]
    
    # Get the intersection:
    
    common_GO = set.intersection(*lists_sets)
    
    # Analyze the common GO:
    
    level_common  = []
    taxons_common = []
    
    for GO in list(common_GO):
        try:
            level_common.append(int(GO_Levels[GO_Levels.GO == GO].Level))
            taxons_common.append(len(Taxons_Dic[GO]))
        except KeyError:
            continue
    
    Leaf_Annotation_it = set(Leaf_Annotation.columns)
    leaf_common        = len(common_GO.intersection(Leaf_Annotation_it))/len(common_GO)
    
    # Analyze the not intersection ones:
    
    flattern_list = [item for sublist in lists_sets for item in sublist]
    flattern_list = list(set(flattern_list) - common_GO)
    
    level_diff  = []
    taxons_diff = []

    for GO in flattern_list:
        try:
            level_diff.append(int(GO_Levels[GO_Levels.GO == GO].Level))
            taxons_diff.append(len(Taxons_Dic[GO]))
        except KeyError:
            continue
    
    leaf_diff = len(set(flattern_list).intersection(Leaf_Annotation_it))/len(flattern_list) 
    
    # Report Results statistics:
    
    print(f'Mean Intersect level: {np.mean(level_common)}')
    print(f'Mean Intersect Taxons: {np.mean(taxons_common)}')
    print(f'Mean Intersect Leaf: {np.mean(leaf_common)}')
    print(f'Mean Unique level: {np.mean(level_diff)}')
    print(f'Mean Unique Taxons: {np.mean(taxons_diff)}')
    print(f'Mean Unique Leaf: {np.mean(leaf_diff)}')
       

def Distribution_Jaccard_Dimensions(dimension_list, all_list = False, MannWhitney = True, common = False):
    
    # Get the Jaccard distribution per each dimension:
    
    list_distribution = []
    
    if common == True:
        common_GO = Filter_Common_GO(dimension_list)
    
    for dimension in dimension_list:
        if dimension < 500:
            repetition = 80000
        else:
            repetition = 100000
        Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(repetition, dimension, "PPMI")
        
        # If the user only want to compare GO terms that are associated in all the samples:
        if common == True:
             Axes_association = [x for x in Axes_association if x[0] in common_GO]
             
        # Give the pairs depending on all the jaccard or not:
        if all_list == False:
            pairs_axes = list(Get_Matrix_Jaccard_Between_Axes(Axes_association, plot = False, all_jacc = False).Value)
        else:
            pairs_axes = list(Get_Matrix_Jaccard_Between_Axes(Axes_association,  plot = False, all_jacc = True).Value)
        list_distribution.append(pairs_axes)
    
    # Compare distributions if needed:
    
    if MannWhitney == True:
        
        Compare_Distributions_Jaccard(list_distribution, dimension_list)
    
    # Prepare the dataFrame for the distribution plot:
    
    list_group = []
    
    for l in range(len(list_distribution)):
        
        list_itera = len(list_distribution[l])
        list_group.append([dimension_list[l]] * list_itera)
    
    # Flattern lists:
    
    list_distribution = [item for sublist in list_distribution for item in sublist]
    list_group        = [item for sublist in list_group for item in sublist]
    
    Data_Plot = pd.DataFrame({"Standardized Score" : list_distribution, "Dimensions" : list_group})
    Data_Plot.Jaccard = Data_Plot.groupby('Standardized Score').transform(lambda x: (x - x.mean()) / x.std())
    
    # Plot the Distributions:
    
    # Choose the palette dor the corresponding distributions
    
    color = ['#f94144','#f3722c','#f8961e','#f9c74f', '#90be6d', "#43aa8b", "#4d908e", "#577590", "#277da1"]
    
    # Global variables for ploting:
    
    sns.set(font_scale=1.5)
    sns.set_style("whitegrid")
    plt.rc('font', weight='bold')
    plt.rc('xtick',labelsize=12)
    plt.rc('ytick',labelsize=12)
    
    # Generate an empty grid:
    
    g = sns.FacetGrid(Data_Plot, #the dataframe to pull from
                  row="Dimensions", #define the column for each subplot row to be differentiated by
                  hue="Dimensions", #define the column for each subplot color to be differentiated by
                  aspect=5, #aspect * height = width
                  height=1.5,
                  palette= color
                 )
    
    # Plot the distributions:
    
    g.map(sns.kdeplot, "Standardized Score", shade=True, alpha=1, lw=1.5, bw=0.2)
    g.map(sns.kdeplot, "Standardized Score", lw=4, bw=0.2)
    
    # Delete the down spin
    g.despine(bottom=True, left=False)
    
    # Add the labels:
    
    labels      = ["A", "B", "C", "D", "E", "F", "G", "H", "K"]
    col_order   = list(Data_Plot.Dimensions.unique())
    counter_lab = 0
    for ax, title in zip(g.axes.flat, col_order):
        ax.set_title(title)
        ax.text(1, 0.85,labels[counter_lab], fontsize=15)
        counter_lab+=1

def Get_Matrix_Jaccard_Between_Axes(Axes_association, plot = False, all_jacc = False):
    
    # DataFrame for the results:
    
    Results_Jaccard = pd.DataFrame(0, index=np.arange(len(Axes_association)), columns=np.arange(len(Axes_association)))
    
    # Iterate per each cluster:
    
    for cluster in range(len(Axes_association)):
        
        # Get the first axis:
        
        cluster_it_1 = Axes_association[cluster]
        
        for cluster_2 in range(len(Axes_association)):
            
            # Get the second axis:
            
            cluster_it_2 = Axes_association[cluster_2]
            
            # Calculate the jaccard index:
            
            jaccard = Jaccar_index(cluster_it_1, cluster_it_2)
            
            # Save the results in an external variable:
            
            Results_Jaccard.loc[cluster,cluster_2] = jaccard
    
    # Plot the Jaccard index for visual pruposes:
    
    if plot == True:
    
        Plot_Jaccard_Overlap(Results_Jaccard)
    
    # Get similar pair of axes or the whole upper diagonal values of the jaccard matrix:
    
    if all_jacc == False:
        pairs = Get_Similar_Axes_Pair_list(Results_Jaccard)
        return(pairs)
    else:
        pairs = Get_Similar_Axes_Pair_list(Results_Jaccard, all_jacc)
        return(pairs)
        
        

def Use_Two_Axes_Represent_GO(k, permutations, pairs,  top_pairs = 10):
    
    # Load GO Embeddings:
    
    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # Get the correspondance between axes:
    
    Corresponding_Axes = Get_Correspondance_Axes(k, permutations)
    
    # Start the comparisons between axes:
    
    pairs = pairs.reset_index(drop = True)
    
    for comparison in range(top_pairs):
        
        # Get the axes that we are comparing:
        
        axis_1 = int(pairs.loc[comparison]["Axes_1"])
        axis_2 = int(pairs.loc[comparison]["Axes_2"])
        
        # Get its corresponding value in the embeddings:
        
        axis_1_embedding = int(Corresponding_Axes[Corresponding_Axes.Axis_Annotated == axis_1]["Embedding_Axis"])
        axis_2_embedding = int(Corresponding_Axes[Corresponding_Axes.Axis_Annotated == axis_2]["Embedding_Axis"])
        
        # Plot the GO terms in the space:
        
        Plot_With_Two_Axes(k, axis_1, axis_1_embedding, axis_2, axis_2_embedding, GO_Embeddings, number = 5)
        
        
def Plot_With_Two_Axes(k, axis_1, axis_1_embedding, axis_2, axis_2_embedding, GO_Embeddings, number = 5):
    
    # Load the domains for each axis:
    
    # Axis 1:
    
    path_domain_1            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{axis_1}.csv'
    domain_1                 = pd.read_csv(path_domain_1, index_col = 0)
    domains_representative_1 = domain_1.iloc[:number]
    domains_representative_1 = domains_representative_1.replace(',','', regex=True)  
    
    # Axis 2:
    
    path_domain_2            = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{axis_2}.csv'
    domain_2                 = pd.read_csv(path_domain_2, index_col = 0)
    domains_representative_2 = domain_2.iloc[:number]
    domains_representative_2 = domains_representative_2.replace(',','', regex=True)    
    
    # Get the clusters per each axis:
    
    # Axis 1:
    
    Cluster_pd_1     = Give_clusters(k, axis_1)
    Cluster_pd_1     = Cluster_pd_1.replace(',','', regex=True)    
    Cluster_pd_sub_1 = Cluster_pd_1[Cluster_pd_1.Cluster.isin(domains_representative_1["GO"])]
    
    # Axis 2:
    
    Cluster_pd_2     = Give_clusters(k, axis_2)
    Cluster_pd_2     = Cluster_pd_2.replace(',','', regex=True)    
    Cluster_pd_sub_2 = Cluster_pd_2[Cluster_pd_2.Cluster.isin(domains_representative_2["GO"])]  
    
    # Get the colors per each axis:
    
    dic_1 = Define_Colors_Plot(Cluster_pd_sub_1, domains_representative_1)
    dic_2 = Define_Colors_Plot(Cluster_pd_sub_2, domains_representative_2)
    
    # Combine the colors into an unique dictionary (GO terms that appears in both will have an specific color):
    
    Shared_Domains = list(set(dic_1.keys()).intersection(set(dic_2.keys())))
    Only_1         = list(set(dic_1.keys()) - set(dic_2.keys()))
    Only_2         = list(set(dic_2.keys()) - set(dic_1.keys()))
    
    Final_keys = []
    Final_keys.extend(Shared_Domains)
    Final_keys.extend(Only_1)
    Final_keys.extend(Only_2)
    
    # Create a dictionary of shapes:
    
    dic_shapes = {new_list: [] for new_list in Final_keys}
    
    for definition in dic_shapes.keys():
        if definition in Shared_Domains:
            dic_shapes[definition] = "o"
        elif definition in Only_1:
            dic_shapes[definition] = "P"
        elif definition in Only_2:
            dic_shapes[definition] = "s" 
    
    # Create the dictionary of colors for domains:
    
    dic_comb = {new_list: [] for new_list in Final_keys}
    cmap     = plt.get_cmap('Spectral')
    colors   = [cmap(i) for i in np.linspace(0, 1, len(Shared_Domains))]
    
    colors = ["#ef476f", "#06d6a0", "#118ab2", "#073b4c", "#219ebc"]
    
    count = 0
    for definition in dic_comb.keys():
        if definition in Shared_Domains:
            dic_comb[definition] = colors[count]
            count+=1
        elif definition in Only_1:
            dic_comb[definition] = dic_1[definition]
        elif definition in Only_2:
            dic_comb[definition] = dic_2[definition]
    
    # Get the embeddings using both axes:
    
    GO_terms = list(Cluster_pd_sub_1.GO_Term)
    GO_terms.extend(list(Cluster_pd_sub_2.GO_Term))
    GO_terms = list(set(GO_terms))
    
    GO_Embeddings_filtered = GO_Embeddings.loc[[axis_1_embedding, axis_2_embedding], GO_terms]
    Cluster_pd_sub         = Cluster_pd_sub_1.append(Cluster_pd_sub_2).reset_index(drop = True)
    
    # Get the coordinates:
    
    x = []
    y = []
    
    for GO in Cluster_pd_sub.GO_Term:
        
        x.append(GO_Embeddings_filtered.loc[axis_1_embedding, GO])
        y.append(GO_Embeddings_filtered.loc[axis_2_embedding, GO])
    
    Cluster_pd_sub["X"] = x
    Cluster_pd_sub["Y"] = y
    
    
    # Plot the distance:
    
    fig, ax = plt.subplots(figsize=(12,9), facecolor='w')
    plt.rcParams.update({'font.size': 22})

    # Plot the line:
    
    # Adapt the line:
    
    # X:
    
    if max(Cluster_pd_sub["X"]) > 1:
        x = int(max(Cluster_pd_sub["X"])+1)
    else:
        x = 1
    
    # Y:
    
    if max(Cluster_pd_sub["Y"]) > 1:
        y = int(max(Cluster_pd_sub["Y"])+1)
    else:
        y = 1
    
    plt.plot([0,x], [0,0],lw=4, color = "#2495d2")
    plt.plot([0,0], [y,0],lw=4, color = "#2495d2")
        
    # Add the ticks directly to the line:
    
    # Get the labels:
    
    # X:
    
    line_ticks = np.linspace(0, x, 6)
    labels     = ['{:.2f}'.format(tt) for tt in line_ticks]
    
    # Draw the things that we need (numbers and ticks):
    
    for label in range(len(labels)):
        ax.text(line_ticks[label],   -0.20, labels[label], ha = "center")
        plt.vlines(line_ticks[label], -0.05, 0.05, colors='#2495d2', lw=3)
                   
    # Y:
    
    line_ticks = np.linspace(0, y, 6)
    labels     = ['{:.2f}'.format(tt) for tt in line_ticks]
    
    # Draw the things that wee need (numbers and ticks):

    for label in range(len(labels)):
        ax.text(-0.4, line_ticks[label], labels[label], va = "center")
        plt.hlines(line_ticks[label], -0.03, 0.03, colors='#2495d2', lw=3)
                   
    # Plot the dots:
    
    groups = Cluster_pd_sub.groupby("Description") 
    Cluster_pd_sub = Cluster_pd_sub.reset_index(drop = True)
    plots_dots = []
    
    names_legend = []
    
    for i, (name, group) in enumerate(groups):
        print(name)
        plt.plot(group["X"], group["Y"], marker=dic_shapes[name], linestyle="", label=name, color = "black", markersize= 23)
        p2 = plt.plot(group["X"], group["Y"], marker= dic_shapes[name], linestyle="", label=name, color = dic_comb[name], markersize=20)
        plots_dots.append(p2)
        names_legend.append(name)
    plots_dots = [item for sublist in plots_dots for item in sublist]
    
    plt.axis('off')
    
    # Prepare the legends:
    
    list_description = []
    for description in names_legend:
        list_description.append(dic_comb[description][0])
    
    for i in range(len(list_description)):
        if list_description[i] == "#":
            list_description[i] = 100
    
    # Orther the legend by group:
    
    order_list = []
    for description in names_legend:
        if description in Shared_Domains:
            order_list.append(1)
        elif description in Only_1:
             order_list.append(2)
        elif description in Only_2:
            order_list.append(3)
                   
        
    Order_Stuff = pd.DataFrame({"Value" : list_description, "Description" : names_legend, "Colors" : plots_dots, "Order" : order_list})
    Order_Stuff = Order_Stuff.sort_values("Order")
    
    # Legend:
    
    leg1 = plt.legend(Order_Stuff["Colors"], Order_Stuff["Description"], prop={'size': 15}, bbox_to_anchor=(0.9, 1), loc='upper left', borderaxespad=0.)
    
    legend_elements = [Line2D([0], [0], marker = "o", color='w', label='Shared',             markerfacecolor = "black",    markersize=12),
                       Line2D([0], [0], marker = "P", color='w', label=f'Only in {axis_1}',  markerfacecolor = "black", markersize=12),
                       Line2D([0], [0], marker = "s", color='w', label=f'Only in {axis_2}',  markerfacecolor = "black",  markersize=12) ]
   
    plt.legend(handles=legend_elements, bbox_to_anchor=(0.9, 0.5), loc='upper left', prop={'size': 15}, borderaxespad=0.)
    ax.add_artist(leg1)
    
    # Save the plot:
    
    plt.savefig(f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Scatter_2D/Scatter_2D_{axis_1}_vs_{axis_2}.png',dpi=300, bbox_inches = "tight")
  

def Get_Correspondance_Axes(k, permutations):
    
    # Load the p_values:
    
    p_values = pd.read_csv(f'/media/sergio/sershiosdisk/Human/Axes/P_Values/{permutations}/Adjusted_P_value_PPI_PPMI_{k}.csv',  index_col = 0, dtype={0: str})
    
    # Prepare the list for saving the results:
    
    dimension_in_GO_Embeddings  = []
    dimension_annotated         = []

    # Pair the axes in the embeddings with the name of the annotated axis:
    
    dimension_counter = 0
    for dimension in p_values.index:
        list_Assoc = list(p_values.columns[p_values.loc[dimension] <= 0.05])
        if len(list_Assoc) > 0:
            dimension_in_GO_Embeddings.append(dimension)
            dimension_annotated.append(dimension_counter)
            dimension_counter+=1
        else:
            continue
        
    # return the two lists:
    
    Pairs_Axes = pd.DataFrame({"Embedding_Axis": dimension_in_GO_Embeddings, "Axis_Annotated" : dimension_annotated })
    
    return(Pairs_Axes)
    
def Intra_Inter_Distance_Same_Sample_Different_Axes(k, repetition, threshold = 0.5, num_dimensions = 2):
    
    # Get the GO associations:
    
    Axes_association = Axes_Study_Scripts_Human.Associate_GO_Axes(repetition, k, "PPMI")
    
    # Get the conversions:
    
    Correspondance = Get_Correspondance_Axes(k, repetition)
    
    # Get the GO embeddings:

    GO_Embeddings = pd.read_csv(f'/media/sergio/sershiosdisk/Human/All_Annotation/_GO_Embeddings_BP_Back_Propagation_PPI_{k}_PPMI.csv', index_col = 0)
    GO_Embeddings[GO_Embeddings<0] = 0
    
    # Get the Jaccard distribution between the axes:
    
    Matrix_Jaccard = Get_Matrix_Jaccard_Between_Axes(Axes_association, plot = False, all_jacc = True)
    
    # Get the threshold to consider that tro axis are functionally similar:
    
    Matrix_Jaccard_ordered = Matrix_Jaccard.sort_values("Value", ascending = False)
    
    # Choose the similar axis based on the threshold:
    
    Matrix_Jaccard_ordered = Matrix_Jaccard_ordered[Matrix_Jaccard_ordered.Value >= threshold]
    Matrix_Jaccard_ordered = Matrix_Jaccard_ordered.reset_index(drop = True)
    
    # Choose the comparison that the user wants to perform by using the num_dimensions:
    
    list_dimensions = Matrix_Jaccard_ordered.groupby("Axes_1").count() + 1
    list_dimensions = list(list_dimensions[list_dimensions.Axes_2 >= num_dimensions]["Axes_2"].index)
    
    Matrix_Jaccard_ordered = Matrix_Jaccard_ordered[Matrix_Jaccard_ordered.Axes_1.isin(list_dimensions)]
    
    # Start the comparisons 1D in this case:
    
    intra_distance_1D = []
    inter_distance_1D = []
    
    for dimension1 in list(set(Matrix_Jaccard_ordered.Axes_1)):
        Results_1D_1 = Intra_Inter_Distance_Jaccard_Axes_1D(k, dimension1,Correspondance, GO_Embeddings)
        intra_distance_1D.extend(Results_1D_1[0])
        inter_distance_1D.extend(Results_1D_1[1])
    for dimension2 in list(set(Matrix_Jaccard_ordered.Axes_2)):
        Results_1D_1 = Intra_Inter_Distance_Jaccard_Axes_1D(k, dimension2,Correspondance, GO_Embeddings)
        intra_distance_1D.extend(Results_1D_1[0])
        inter_distance_1D.extend(Results_1D_1[1]) 
    
    # 2D:
    
    intra_distance_2D = []
    inter_distance_2D = []
    
    for dimension_pair in range(len(Matrix_Jaccard_ordered)):
        Axes_1       = int(Matrix_Jaccard_ordered.loc[dimension_pair, "Axes_1"])
        Axes_2       = int(Matrix_Jaccard_ordered.loc[dimension_pair, "Axes_2"])
        Results_2D   = Intra_Inter_Distance_Jaccard_Axes(k, Axes_1, Axes_2, Correspondance, GO_Embeddings)
        intra_distance_2D.extend(Results_2D[0])
        inter_distance_2D.extend(Results_2D[1])
    
    # Print the results:
    
    print(str(np.mean(intra_distance_1D)))
    print(str(np.mean(inter_distance_1D)))
    print(str(np.mean(intra_distance_2D)))
    print(str(np.mean(inter_distance_2D)))
    
    mannwhitneyu(intra_distance_1D, intra_distance_2D)
    mannwhitneyu(inter_distance_1D, inter_distance_2D)
    
def Intra_Inter_Distance_Jaccard_Axes_1D(k, Axes_1, Correspondance, GO_Embeddings):
    
    # Get the domains:
    
    path_domain_Ax1 = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{Axes_1}.csv'
    domain_Ax1 = pd.read_csv(path_domain_Ax1, index_col = 0)
    domain_Ax1 = domain_Ax1.iloc[:5]
    domain_Ax1 = domain_Ax1.replace(',','', regex=True)

    # Get clusters based on semanitc simmilarity:
    
    Cluster_SS_Ax1 = Give_clusters(k, Axes_1)
    Cluster_SS_Ax1 = Cluster_SS_Ax1.replace(',','', regex=True)

    # Get the representative domains:
    
    Cluster_SS_Ax1 = Cluster_SS_Ax1[Cluster_SS_Ax1.Cluster.isin(domain_Ax1.GO)]
    
    # Give Groups 1D:
    
    label_encoder            = LabelEncoder()
    Cluster_SS_Ax1["Group"]  = label_encoder.fit_transform(Cluster_SS_Ax1.Cluster)
    color_dic_Ax1            = Define_Colors_Plot(Cluster_SS_Ax1, domain_Ax1)
    
    # Calculate the intra distance using 1D:
    
    intra_distance_1D = []
    inter_distance_1D = []
    
    # Axis 1:
    
    dimension_1 = int( Correspondance[Correspondance.Axis_Annotated == Axes_1]["Embedding_Axis"])
    
    if len(set(Cluster_SS_Ax1["Group"])) > 2:
        intra_mean, intra_std, inter_mean, inter_std = Compute_Inter_Intra_Distance(GO_Embeddings, Cluster_SS_Ax1, 
                                                                                    dimension_1, Axes_1,color_dic_Ax1)
        # Update the 1D lists:
        
        normalize = []
        normalize.extend(intra_mean)
        normalize.extend(inter_mean)
        
        if intra_mean:
            intra_distance_1D.extend(intra_mean/max(normalize))
        if inter_mean:
            inter_distance_1D.extend(inter_mean/max(normalize))
     
    return(intra_distance_1D, inter_distance_1D)
            

def Intra_Inter_Distance_Jaccard_Axes(k, Axes_1, Axes_2, Correspondance, GO_Embeddings):
    
    # Get the domains:
    
    path_domain_Ax1 = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{Axes_1}.csv'
    path_domain_Ax2 = f'/media/sergio/sershiosdisk/Human/Axes/Meaning/PPMI_PPI_{k}/Domain_{Axes_2}.csv'
    
    domain_Ax1 = pd.read_csv(path_domain_Ax1, index_col = 0)
    domain_Ax2 = pd.read_csv(path_domain_Ax2, index_col = 0)
    
    domain_Ax1 = domain_Ax1.iloc[:5]
    domain_Ax2 = domain_Ax2.iloc[:5]
    
    domain_Ax1 = domain_Ax1.replace(',','', regex=True)
    domain_Ax2 = domain_Ax2.replace(',','', regex=True)
    
    # Get clusters based on semanitc simmilarity:
    
    Cluster_SS_Ax1 = Give_clusters(k, Axes_1)
    Cluster_SS_Ax2 = Give_clusters(k, Axes_2)
    
    Cluster_SS_Ax1 = Cluster_SS_Ax1.replace(',','', regex=True)
    Cluster_SS_Ax2 = Cluster_SS_Ax2.replace(',','', regex=True)
    
    # Get the representative domains:
    
    Cluster_SS_Ax1 = Cluster_SS_Ax1[Cluster_SS_Ax1.Cluster.isin(domain_Ax1.GO)]
    Cluster_SS_Ax2 = Cluster_SS_Ax2[Cluster_SS_Ax2.Cluster.isin(domain_Ax2.GO)]
    
    # Give Groups 1D:
    
    label_encoder = LabelEncoder()
    
    Cluster_SS_Ax1["Group"]  = label_encoder.fit_transform(Cluster_SS_Ax1.Cluster)
    Cluster_SS_Ax2["Group"]  = label_encoder.fit_transform(Cluster_SS_Ax2.Cluster)
    
    color_dic_Ax1 = Define_Colors_Plot(Cluster_SS_Ax1, domain_Ax1)
    color_dic_Ax2 = Define_Colors_Plot(Cluster_SS_Ax2, domain_Ax2)
       
    # Calculate using 2D:
    
    intra_distance_2D = []
    inter_distance_2D = []
    
    dimension_1 = int( Correspondance[Correspondance.Axis_Annotated == Axes_1]["Embedding_Axis"])
    dimension_2 = int( Correspondance[Correspondance.Axis_Annotated == Axes_2]["Embedding_Axis"])
    
    # For groups in the first axes
    
    if len(set(Cluster_SS_Ax1["Group"])) > 2:
        intra_mean, intra_std, inter_mean, inter_std = Compute_Inter_Intra_Distance_2D(GO_Embeddings, Cluster_SS_Ax1, 
                                                                                    dimension_1, dimension_2, Axes_1,Axes_2, color_dic_Ax1)
        # Update the 1D lists:
        
        normalize = []
        normalize.extend(intra_mean)
        normalize.extend(inter_mean)
        
        if intra_mean:
            intra_distance_2D.extend(intra_mean/max(normalize))
        if inter_mean:
            inter_distance_2D.extend(inter_mean/max(normalize)) 
    
    # For groups in the second axes:
    
    if len(set(Cluster_SS_Ax2["Group"])) > 2:
        intra_mean, intra_std, inter_mean, inter_std = Compute_Inter_Intra_Distance_2D(GO_Embeddings, Cluster_SS_Ax2, 
                                                                                    dimension_2, dimension_1, Axes_2,Axes_1,color_dic_Ax2)
        # Update the 1D lists:
        
        normalize = []
        normalize.extend(intra_mean)
        normalize.extend(inter_mean)
        
        if intra_mean:
            intra_distance_2D.extend(intra_mean/max(normalize))
        if inter_mean:
            inter_distance_2D.extend(inter_mean/max(normalize)) 
            
    return(intra_distance_2D, inter_distance_2D)
    
            

def Compute_Inter_Intra_Distance_2D(GO_Embeddings, Cluster_pd, dimension_1, dimension_2, dimension_counter1,dimension_counter2, color_dic):
  
    # Choose the Correct GO dimension and GO terms:
    
    GO_Embedding_subset = GO_Embeddings.loc[[dimension_1, dimension_2], Cluster_pd.GO_Term]
    
    # Calculate the Euclidean pairwise distance:

    scores_np              = np.array(GO_Embedding_subset.T)
    distance_scores        = euclidean_distances(scores_np) 
    GO_embedding_Euclidean = pd.DataFrame(distance_scores, index = [GO_Embedding_subset.columns], columns = [GO_Embedding_subset.columns])
    
    # Organize the colors:
    
    dic = {new_list: [] for new_list in list(Cluster_pd.GO_Term)} 
    
    Cluster_pd = Cluster_pd.reset_index(drop = True)
    
    for description in range(len(Cluster_pd)):
        
        description_it = Cluster_pd.loc[description, "Description"]
        GO_term        = Cluster_pd.loc[description, "GO_Term"]
        
        # Add the values to the dic:
        
        dic[GO_term] = color_dic[description_it]
    
    # Prepare the Euclidean matrix:
    
    index_real   = [i[0] for i in GO_embedding_Euclidean.index]
    columns_real = [i[0] for i in GO_embedding_Euclidean.columns]
    
    GO_embedding_Euclidean.index   = index_real
    GO_embedding_Euclidean.columns = columns_real
    
    # Calaculate inter and intra cluster distance:
    
    intra_mean, intra_std = Give_Intra_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd)
    inter_mean, inter_std = Give_Inter_Cluster_Distance(GO_embedding_Euclidean, Cluster_pd)
    
    # Return the values:
    
    return(intra_mean, intra_std, inter_mean, inter_std)
   
def Intra_Inter_Distance_Different_Samples(dimension_list, number = 5, plot_dendograms = True, plot_comparisons = True):
    
    intra_distribution = []
    inter_distribution = []
    intra_std          = []
    inter_std          = []
    
    for dimension in dimension_list:
        if dimension <= 500:
            repetition = 80000
        else:
            repetition = 100000
        Result = Calculate_Inter_Intra_Distances_Euclidean_SS_All_Axes(dimension, repetition, number, plot_dendograms)
        intra_distribution.append(Result[0])
        inter_distribution.append(Result[1])
        intra_std.append(Result[2])
        inter_std.append(Result[3])

                 
    # Plot the line plots to compare different dimensions if needed:
    
    if plot_comparisons == True:
        
        Line_Plot_Distances(intra_distribution,inter_distribution, intra_std, inter_std, dimension_list)
    else:
        data_1 = {
        'x': dimension_list,
        'y': intra_distribution,
        'yerr': intra_std}
        data_2 = {
        'x': dimension_list,
        'y': inter_distribution,
        'yerr': inter_std}
        
        return(data_1, data_2)
        
              
def Line_Plot_Distances(intra_distribution,inter_distribution, intra_std, inter_std, dimension_list):
     
    # Prepare the data:
    
    data_1 = {
    'x': dimension_list,
    'y': intra_distribution,
    'yerr': intra_std}
    data_2 = {
    'x': dimension_list,
    'y': inter_distribution,
    'yerr': inter_std}

    # plot
    plt.rc('font', weight='bold')
    plt.figure(figsize=(14,7))
    for data in [data_1, data_2]:
        plt.errorbar(**data, fmt=':', capsize=3, capthick=1, marker = "o")
        plt.xticks(dimension_list, size = 15)
        plt.xlabel('Dimensions', size = 20)
        plt.yticks(size = 15)
        plt.ylabel('Distance', size = 20)
        data = {
            'x': data['x'],
            'y1': [y - e for y, e in zip(data['y'], data['yerr'])],
            'y2': [y + e for y, e in zip(data['y'], data['yerr'])]}
        plt.fill_between(**data, alpha=.25) 
    plt.legend(["Intra", "Inter"], prop={'size': 15})
    
    
    
        
def Apply_REVIGO_Cancer_Control(Cancer_axes, Control_axes, pairs,  Cancer, save_path): 
    
    # Create the main directory:
    
    main_folder = save_path + str(Cancer)
    os.mkdir(main_folder)
    
    pairs = pairs.reset_index(drop = True)
    
    # Start the comparisons:
    
    for comparison in range(len(pairs)):
        
        # Get the list of GO terms:
        
        GO_cancer_list  = Cancer_axes[pairs.loc[comparison, "Cancer"]]
        GO_control_list = Control_axes[pairs.loc[comparison, "Control"]]
        
        # Create the subfolder
        
        sub_folder = main_folder + "/" + str(comparison)
        os.mkdir(sub_folder)
        
        # Query REVIGO:
        
        # Cancer:
        
        Submit_Query(GO_cancer_list, sub_folder)
        
        # Rename the REVIGO Cancer file:
        
        os.rename(sub_folder + '/Revigo.csv', sub_folder +'/Cancer_' + str(pairs.loc[comparison, "Cancer"]) + ".csv") 
        
        # Control:
        
        Submit_Query(GO_control_list, sub_folder)
        
        # Rename the REVIGO Cancer file:
        
        os.rename(sub_folder + '/Revigo.csv', sub_folder +'/Control_' + str(pairs.loc[comparison, "Control"]) + ".csv")            
        
def Create_Domains_Files(Cancer, pairs, save_path):
    
    # Count the folders:
    
    main_folder       = save_path + str(Cancer)
    path, dirs, files = next(os.walk(main_folder))
    
    # Count the number of folders:
    
    folder_count = len(dirs)
    
    # Iterate in each folder:
    
    pairs = pairs.reset_index(drop = True)
    
    for folder in range(folder_count):
        
        # Get the dimensions that we are gonna compare:
        
        dimension_Cancer  = pairs.loc[folder, "Cancer"]
        dimension_Control = pairs.loc[folder, "Control"]
        
        # Get the corresponding path:
        
        Cancer_path  =  main_folder + "/" + str(folder) + "/" + "Cancer_" + str(dimension_Cancer) + ".csv"
        Control_path =  main_folder + "/" + str(folder) + "/" + "Control_" + str(dimension_Control) + ".csv"



    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    