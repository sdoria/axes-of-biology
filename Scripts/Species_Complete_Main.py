'''
This script contains the final main for the analyses of the axes of biology. To run it, it is necessary to produce first the NMTF ("Maresnostrum.py")
) and the LINE/Deepwalk locally.'''


import os

# Our packages:

os.chdir("/media/sergio/sershiosdisk/Scripts/") # path tho the scripts

import Species_Scripts

save_path = " " # Path to save the data.

# Choose the number of dimensions and species:

Species     = ["Human","Yeast", "Pombe", "Fly", "Mouse", "Rat"]
dimensions  = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]
embeddings  = ["PPMI", "No_Orth_PPMI", "Deepwalk"]

# 1. Produce the initial data.

# PPMI matrices:

taxons_id  = [9606, 559292, 4896, 7227, 10090, 10116]

Species_Scripts.Get_PPMI_Matrix(Species, context_window = 10)

# GO BP annotations (Back propagated):

Species_Scripts.Get_BP_Annotations(Species, taxons_id)
Species_Scripts.Generate_the_GO_Matrix(Species, annotation = "BP_Back")

# 2. Do the gene embedding spaces using Deepwalk, NMTF, and ONMTF:

name = "" # name of the output to be saved-

# NMTF:

Solver = Species_Scripts.SNMTF(500, 50)
Solver.Solve_MUR_Hum(Species, embeddings, dimensions, "PPMI", "False", name, "SVD" )

# ONMTF:

Solver = Species_Scripts.SNMTF(500, 50)
Solver.Solve_MUR_Hum(Species, embeddings, dimensions, "PPMI", "True", name, "SVD" )

# Deepwalk: use the scripts in the terminal (sh and bash to run it can be found in the LINE folder)

# 3. Do gene clustering followed by functional enrichment analysis:

Species_Scripts.Enrichment_Analyses(Species, dimensions, matrix = "PPMI", Orthonormal = True,  annotation = "BP_Back")
Species_Scripts.Enrichment_Analyses(Species, dimensions, matrix = "PPMI", Orthonormal = False, annotation = "BP_Back")
Species_Scripts.Enrichment_Analyses(Species, dimensions, matrix = "Adj",  Orthonormal = True,  annotation = "BP_Back")
Species_Scripts.Enrichment_Analyses(Species, dimensions, matrix = "Adj",  Orthonormal = False, annotation = "BP_Back")
Species_Scripts.Enrichment_Analyses(Species, dimensions, matrix = "Deepwalk", annotation = "BP_Back")

# Plot the Enrichments line (% Clusters, % Genes, and % GO):

# A) Line plot (human):

Species_Scripts.Plot_Enrichments_all(["Human"], dimensions, embeddings, annotations = "BP_Back")

# B) Bar plot (human):

Species_Scripts.Plot_Enrichments_Avg_Human(["Human"], dimensions, embeddings, annotations = "BP_Back")

# C) Bar plot all species:

Species_Scripts.Plot_Enrichments_Avg_Sp(Species, dimensions, embeddings, annotations = "BP_Back")

# 4. Embed GO BP terms into the ONMTF, NMTF and Deepwalk gene embedding spaces:

# Obtain the bases of the Deepwalk embedding spaces (ONMTF and NMTF produces their own bases as output):

Species_Scripts.Preprocess_Deepwalk_Embeddings(Species, dimensions)
Species_Scripts.Get_Deepwalk_Bases(Species, dimensions)

# Embed the GO BP terms to all the spaces:

Species_Scripts.Embedd_GO_terms(embeddings, Species, dimensions, annotation = "BP_Back", orthonormal = "True") # ONMTF
Species_Scripts.Embedd_GO_terms(embeddings, Species, dimensions, annotation = "BP_Back", orthonormal = "True") # NMTF
Species_Scripts.Embedd_GO_terms(embeddings, Species, dimensions, annotation = "BP_Back", orthonormal = "True") # Deepwalk

# 4. Associate the annotations to the Axes:

# Run the permutations:

Species_Scripts.Run_Permutations(organism, optimal_dimension, matrix, times, name, orthonormal, annotations)

# Generate the p-values of the permutations:

Species_Scripts.Sum_Permutations_Dist(org, dim, matrix, times, orthonormal, annotations)

# Adjust p-values:

Species_Scripts.Adjust_Pvalues(Species, embeddings, dimensions, annotations = "BP_Back", alpha = 0.05)

# Associate the annotations to the axes:

Species_Scripts.Associate_Annotations_Axes_Hard(Species, embeddings, dimensions, annotations = "BP_Back", alpha = 0.05)

# Plot the Associations (% Axes and % GO):

# A) Line plot:

Species_Scripts.Plot_Percentage_Axes_Across_Dimensions_Human(["Human"], embeddings, dimensions, annotations = "BP_Back", association = "_Hard")

# B) Bar plot (human):

Species_Scripts.Plot_Percentage_Axes_Across_Dimensions_Human_avg(["Human"], embeddings, dimensions, annotations = "BP_Back")

# C) Plot all the species:

Species_Scripts.Plot_Percentage_Axes_Across_Dimensions_SP_avg(Species, embeddings, dimensions, annotations = "BP_Back", association = "_Hard")

# 5. Analyze the increment of annotations across the dimensions (Generic vs Specific):

# Calculate the fold matrix (these results are not affected but the clustering method used):

Species_Scripts.Axes_Taxons_Across_Dimensions(Species, embeddings, dimensions, annotations = "BP_Back")

# Plot the lines:

Species_Scripts.plot_Fold_enrichment(Species, embeddings, annotations = "BP_Back")

# 6. Analyze the functional correlaation between the axes:

# A) JI (only for the soft clustering method):

Species_Scripts.Jaccard_Disit_Across_Dim(Species, embeddings, dimensions, annotations = "BP_Back", common = False)

# B) Average number of annotaations per axis:

Species_Scripts.Avg_GO_Across_Dim(Species, embeddings, dimensions,  annotations = "BP_Back", association = "_Hard")

# 7. Analyze the functional separation between the axes:

# Semantic similarity (Intra and Inter):

Species_Scripts.Calculate_Intra_Inter_Axes(Species, embeddings, dimensions, annotations = "BP_Back")
Species_Scripts.plot_mean_SS_line(["Human"], embeddings, dimensions, annotations = "BP_Back")

# Shortest Paths:

Species_Scripts.Shortest_Paths(["Human"], embeddings, dimensions, annotations = "BP_Back")

# 8. The Biological meaning of the axes:

# 8.1 Meaning of the axes using TF-IDF (new functional annotations):

Species_Scripts.Axes_Meaning(Species, ["PPMI"], dimensions, "BP_Back", associations = "_Hard")

# 8.2 Plot some examples of meaning:

# Hard Clustering:

Species_Scripts.Plot_Axes_Meaning_Examples("Human", 500, "BP_Back", "PPMI", associations = "_Hard", n_axes = 20)
Species_Scripts.Plot_Axes_Meaning_Examples("Yeast", 200, "BP_Back", "PPMI", associations = "_Hard", n_axes = 20)

# Some individual examples:

Species_Scripts.Give_Example_Axes("Human", 500, "BP_Back", "PPMI", axis = "12", associations = "_Hard")
Species_Scripts.Give_Example_Axes("Human", 500, "BP_Back", "PPMI", axis = "495", associations = "_Hard")
Species_Scripts.Give_Example_Axes("Human", 500, "BP_Back", "PPMI", axis = "51", associations = "_Hard")

# Write the complete Tables for the species (ASFAs from a GO perspective):

Species_Scripts.Generate_Suppl_Table_Axes("Human", 500, "BP_Back", "PPMI", associations = "_Hard") 
Species_Scripts.Generate_Suppl_Table_Axes("Yeast", 200, "BP_Back", "PPMI", associations = "_Hard") 
Species_Scripts.Generate_Suppl_Table_Axes("Pombe", 200, "BP_Back", "PPMI", associations = "_Hard") 
Species_Scripts.Generate_Suppl_Table_Axes("Mouse", 300, "BP_Back", "PPMI", associations = "_Hard") 
Species_Scripts.Generate_Suppl_Table_Axes("Rat"  , 250, "BP_Back", "PPMI", associations = "_Hard") 
Species_Scripts.Generate_Suppl_Table_Axes("Fly"  , 400, "BP_Back", "PPMI", associations = "_Hard") 

# 8.3 Evlutionary study:

# Add the information about evolution:

Species_Scripts.Add_Information_Functions(["Human"], ["PPMI"], "BP_Back", [500], "_Hard", "set")

# Ordering the axes based on the taxons (prokaryotes, eukaryotes and vertebrata):

Species_Scripts.Plot_Axes_Evolution("Human", "PPMI", "BP_Back", 500, "_Hard", "set")

# 9. Finally investigate the axes that do not have annotations:

# 9.1 Check the connectivity of the genes in the network:

Species_Scripts.Connectivity(Species, dimensions, "BP_Back", associations = "_Hard") 

# Plot the distributions (an example of the distributions):

Species_Scripts.plot_connctivity("Human", 500, "BP_Back", associations = "_Hard")
Species_Scripts.plot_connctivity("Yeast", 500, "BP_Back", associations = "_Hard")

# 9.2 Get the information from ENSMBL (gene descriptions) and build the AxNotations:

Species_Scripts.Generate_TF_IDF_Genes("Human", "PPMI", "BP_Back", 500, associations = "_Hard", Table = "biomart")

# Give an example of the ASFAs:

# See those axes that do not have annotations based on GO:

Species_Scripts.Print_No_Annotated_Axes("Human", "PPMI", "BP_Back", 500, associations = "_Hard")

# Give examples:

# A) Axes without annotation:

Species_Scripts.Give_Example_Axes_ENSEMBL("Human", "461")
Species_Scripts.Give_Example_Axes_ENSEMBL("Human", "9")

# 10 New functional interactions between the GO terms:

# 10.1 Prepare the matrix for the experiment:

dim_op = [500, 200, 200, 300, 400, 250] # Optimal dimensionalities.

Species_Scripts.Prepare_Axes_SS(Species, dim_op, matrix = "PPMI", associations = "_Hard", annotations = "BP_Back")

# 10.2 Plot the distribution in order to chose the axes with the lowest SS:

Species_Scripts.Plot_Distribution_SS_axes(Species)

# 10.3 For the axes with the lowest SS (percentile 5), show the heatmaps:

Species_Scripts.Plot_Heatmaps_SS_axes(Species, dim_op)

# 11 Comparison between axes-method, FMM, and enrichments:

# 11.1 Generic comparison with Enrichments (Fold information, semantic similarity, JI, and level of the terms).

Species_Scripts.Give_Generic_Comparison_Methods(Species, embeddings, dimensions)

# 11.2 Do an agreement study with Enrichments (GO terms captured by both).

Species_Scripts.Individual_Analysis(Species, embeddings, dimensions)

# 10.3 Assess the agreement of our method with the FMM.

Species_Scripts.Comparison_FMM(Species, embeddings, dimensions)































