# This Script Contains all the functions to Analyze the fucntional organization of different species:

from scipy.linalg import svd
import re
import os
import time
import umap
import nltk
import math 
import json
import kneed
import random
import numpy as np
import pandas as pd
import seaborn as sns
import networkx as nx
from Bio import Entrez
import multiprocessing
from random import sample
from sklearn import metrics
from collections import Counter
import matplotlib.pyplot as plt
from goatools import obo_parser
from multiprocessing import Pool
from biomart import BiomartServer
from string import ascii_lowercase
from scipy.spatial import distance
import matplotlib.patches as mpatches
from sklearn.manifold import TSNE, MDS
from pygosemsim import graph, similarity
from sklearn_extra.cluster import KMedoids
from matplotlib.transforms import Affine2D
from matplotlib.ticker import NullFormatter
from matplotlib.offsetbox import AnchoredText
from statsmodels.stats.multitest import multipletests
from scipy.stats import mannwhitneyu, pearsonr, spearmanr
import scipy.spatial as sp, scipy.cluster.hierarchy as hc
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
from sklearn.metrics import pairwise_distances, adjusted_rand_score

# Our packages:

import generate_embedding_spaces_Human 
import generate_embedding_spaces
import enrichment_analysis
import Taxon_Human_Script  
import GO_Embeddings
import REVIGO

# Functions:

def run_process(process):                                                             
    os.system(format(process)) 

def Get_Adjancency(Species, taxons_id, save_path):
    '''
    This function produces the Adj matrix from the tab2 Biogrid data. Additionally, it also produces the information of the network.
    '''
    
    name    = []
    nodes   = []
    edges   = []
    density = []
        
    for org, tax in zip(Species,taxons_id):
        
        # Define the save names:
        
        file_Adj   = f'{save_path}Adj_{org}_PPI'
        file_genes = f'{save_path}Genes_{org}_PPI'
        
        # Get the path to the data:
        
        file_path = f'{save_path}{org}.txt' 
        
        # Get the PPI:
        
        required_experimental_evidence = ['two-hybrid', 'affinity capture-luminescence',
                                          'affinity capture-ms', 'affinity capture-rna', 'affinity capture-western']
        
        G = nx.Graph()
        evidence_accepted = set()
        evidence_accepted_2 = set()

        # Generate the network:
        
        with open(file_path, 'r') as i_stream:
            for line in i_stream:
                line = line.strip().split('\t')
                experimental_system_type = line[12]
                if experimental_system_type.lower() == "physical":
                    experimental_evidence = line[11]
                    evidence_accepted_2.add(experimental_evidence)
                    if experimental_evidence.lower() in required_experimental_evidence or len(required_experimental_evidence) == 0:
                                evidence_accepted.add(experimental_evidence)
                                tax_id_1 = line[15]
                                tax_id_2 = line[16]
                                if tax_id_1 == str(tax) and tax_id_2 == str(tax):
                                    gene_id_1 = line[1]
                                    gene_id_2 = line[2]
                                    if gene_id_1 != gene_id_2:
                                        G.add_edge(gene_id_1, gene_id_2)
        
        # Save the network as np and the genes as list:
        
        G_adj = np.asarray(nx.to_numpy_matrix(G))
        np.save(file_Adj, G_adj)
        
        with open(file_genes,  "w") as os:
            for gene in G.nodes():
                os.write("%s\n" % gene)
        
        # Save the info for generating the information table:
        
        name.append(org)
        nodes.append(G.number_of_nodes())
        edges.append(G.number_of_edges())
        density.append(nx.density(G))
        
    # Write the information table:
    
    with open(f'{save_path}Network_Statistics.txt', 'a') as the_file:
        
        the_file.write("# Specie"  + "\t")
        the_file.write("# Nodes"   + "\t")
        the_file.write("# Edges"   + "\t")
        the_file.write("# Density" + "\n")
        
        for i in range(len(Species)):
            
            the_file.write(str(Species[i])  + "\t")
            the_file.write(str(nodes[i])   + "\t")
            the_file.write(str(edges[i])  + "\t")
            the_file.write(str(density[i]) + "\n")   
        
        the_file.close() 
        
def Get_PPMI_Matrix(Species, context_window = 10):       

    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    counter      = 1
    
    for org in Species:
        
        # Load the Adj matrix:
        
        Adj_matrix = np.load(f'{network_path}Adj_{org}_PPI.npy', allow_pickle= True) # Path to the Adjancency Matrices.
        
        # Generate the PPMI matrix:
        
        PPMI_matrix = generate_embedding_spaces_Human.deep_walk_ppmi(Adj_matrix,  context_window)
        
        # Save the matrix:
        
        np.save(f'{network_path}PPMI_{org}', PPMI_matrix)
        
        # Report the iteration number:
    
        print(f'{counter}/{len(Species)}')
        counter = counter + 1     
        
def get_node2vec_matrix(Species, taxons_id, save_path):
    
    data_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
        
    for org, tax in zip(Species,taxons_id):
        
        # Get the path to the data:
        
        file_path = f'{data_path}{org}.txt' 
        
        # Get the PPI:
        
        required_experimental_evidence = ['two-hybrid', 'affinity capture-luminescence',
                                          'affinity capture-ms', 'affinity capture-rna', 'affinity capture-western']
        
        G = nx.Graph()
        evidence_accepted = set()
        evidence_accepted_2 = set()

        # Generate the network:
        
        with open(file_path, 'r') as i_stream:
            for line in i_stream:
                line = line.strip().split('\t')
                experimental_system_type = line[12]
                if experimental_system_type.lower() == "physical":
                    experimental_evidence = line[11]
                    evidence_accepted_2.add(experimental_evidence)
                    if experimental_evidence.lower() in required_experimental_evidence or len(required_experimental_evidence) == 0:
                                evidence_accepted.add(experimental_evidence)
                                tax_id_1 = line[15]
                                tax_id_2 = line[16]
                                if tax_id_1 == str(tax) and tax_id_2 == str(tax):
                                    gene_id_1 = line[1]
                                    gene_id_2 = line[2]
                                    if gene_id_1 != gene_id_2:
                                        G.add_edge(gene_id_1, gene_id_2)
        
        # Prepare the input for node2vec:
        
        label_mapping   = {name:n for n,name in enumerate(G)}
        reverse_mapping = {value:key for key,value in label_mapping.items()}
        
        # 1.3 Save the reverse mapping (as dictionary):
    
        with open(f'{save_path}{org}_node2vec_reverse_mapping.txt', 'w') as outfile:
            json.dump(reverse_mapping, outfile)
        
        # 1.4 Save the edge list (as txt file):
            
        G = nx.relabel_nodes(G, label_mapping) 
        nx.write_edgelist(G, f'{save_path}{org}_node2vec_input.txt',data=False)      
        
def Generate_NMTFs(Species, dimensions, max_iter= 400, verbose = 50):
    
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    
    counter = 0
    total   = len(Species) * len(dimensions)
    
    for org in Species:
        
        Solver = generate_embedding_spaces.SNMTF(max_iter, verbose) 
        
        # Load the corresponding matrices:
        
        Adj_matrix  = np.load(f'{network_path}Adj_{org}_PPI.npy', allow_pickle= True)
        PPMI_matrix = np.load(f'{network_path}PPMI_{org}.npy',    allow_pickle= True)
        
        for dim in dimensions:
            
            # Do the NMTF:
            
            Solver.Solve_MUR(PPMI_matrix, dim, network_path, network = f'PPI_{org}', matrix = "PPMI", init="SVD")
            Solver.Solve_MUR(Adj_matrix,  dim, network_path, network = f'PPI_{org}', matrix = "Adj",  init="SVD")
            
            # Show the progress:
            
            counter +=1
            print(f'{total}/{counter}') 

def Parallel_Run_Node2Vec(Species, dimensions):
    
    data_path     = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    node2vec_path = "/media/sergio/sershiosdisk/Axes_Species/Node2vec/"
    
    for dim in dimensions:
        commands = []
        for org in Species:
            write_it = [f'python2 {node2vec_path}src/main.py --input {data_path}{org}_node2vec_input.txt --output {node2vec_path}{dim}_{org}_Node2vec.emd --dimensions {dim}']
            commands.append(write_it)
        print(dim)   
        n_cores = multiprocessing.cpu_count()
        pool1 = Pool(processes  = n_cores) 
        pool1.starmap(run_process, commands)
    print("Finished")

def Re_Order_Node2Vec(Species, dimensions):
    
    # Paths:
    
    node2vec_path   = "/media/sergio/sershiosdisk/Axes_Species/Node2vec/"
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"

    for org in Species:
        
        # Load the correct names for the specie:
    
        with open(f'{node2vec_path}{org}_node2vec_reverse_mapping.txt', "r") as read_file:
            reverse_mapping_load = json.load(read_file) 
        reverse_mapping_load_correct = {int(key):values for key,values in reverse_mapping_load.items()}
        
        # Load the GO/gene matrix for the specie:
        
        GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', 
                                index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
        
        for dim in dimensions:
            
            # Load the original embeddings:
            
            node2vec_emb = pd.read_csv(f'{node2vec_path}{dim}_{org}_Node2vec.emd', skiprows=1,
                             sep = " ", index_col = 0, header = None)
            
            # Rename the genes:
            
            node2vec_emb       = node2vec_emb.rename(index=reverse_mapping_load_correct)
            
            # Reorder the Genes using the GO/gene Matrix:
            
            embeddings_ordered = node2vec_emb.loc[GO_Matrix.index, :]
            
            # Transform it to a numpy array:
            
            embeddings_np = np.array(embeddings_ordered)
            
            # Save the embeddings:
            
            np.save(f'{node2vec_path}node2vec_array_{org}_{dim}', embeddings_np)
            
            
def Get_BP_Annotations(Species, taxons_id):
    
    '''
    This function generates the annotations. Be carefull with the versio of the obo file and gene2go (NCBI) file. To avoid possible
    annotations that have been deleted.
    '''

    # Paths:
    
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Data       = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    
    # Load the whole gene2go file:
    
    GO_db           = pd.read_table(f'{Path_Annotation}gene2go')
    GO_db           = GO_db[["#tax_id", "GeneID", "GO_ID", "Evidence", "Category"]]
    GO_db["GeneID"] = GO_db["GeneID"].astype(str) 
    
    # Per each specie:
    
    for org, tax in zip(Species,taxons_id):
        
        # Filter specie, evidence and category (BP in this case):
        
        GO_db_filtered    = GO_db[GO_db['#tax_id'] == tax] 
        GO_db_filtered    = GO_db_filtered[GO_db_filtered['Evidence'].isin(['EXP', 'IDA', 'IPI', 'IMP', 'IGI', 'IEP'])]
        GO_db_filtered_BP = GO_db_filtered[GO_db_filtered['Category'].isin(['Process'])]
        
        # Load the genes:
        
        Genes             = pd.read_table(f'{Path_Data}Genes_{org}_PPI', header = None, dtype={0: str})
        GO_db_filtered_BP = GO_db_filtered_BP[GO_db_filtered_BP["GeneID"].isin(list(Genes[0]))]
        GO_db_filtered_BP = GO_db_filtered_BP.drop(["#tax_id", "Evidence", "Category"], axis = 1)
        GO_db_filtered_BP = GO_db_filtered_BP.drop_duplicates()
        
        # Do the back propagation:
        
        GO_file = f'{Path_Annotation}go-basic.obo'
        go_dag  = obo_parser.GODag(GO_file)
        
        sergio_df = GO_db_filtered_BP
        annotation = "biological_process"
        sergio_df = sergio_df.reset_index(drop = True)
        
        # Final DataFrame:
        
        Final_DataFrame = pd.DataFrame(columns=['GeneID','GO_ID'])
        
        # Start the loop to get the corresponding parents:
        
        for _, row in sergio_df.iterrows():
            
            Final_DataFrame = Final_DataFrame.append(row)
            
            parents = pd.DataFrame([(row.GeneID,parent_id) for parent_id in go_dag[row.GO_ID].get_all_parents()
                                   if go_dag[parent_id].namespace== annotation])
            parents.columns = ['GeneID','GO_ID']
            Final_DataFrame = Final_DataFrame.append(parents) 
        
        # Last steps to get the final file:
    
        Final_DataFrame = Final_DataFrame.drop_duplicates()
        Dic = dict(Final_DataFrame.groupby('GeneID')['GO_ID'].apply(list))
    
        Dic = {key:list(set(values)) for key,values in Dic.items()}
    
        # Save the dictionary:
    
        with open(f'{Path_Annotation}_BP_Back_{org}.json', 'w') as fp:
            json.dump(Dic, fp)
        

def Generate_the_GO_Matrix(Species, annotation = "BP_Back"):
    
    # Paths:
    
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Data       = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    
    for org in Species:
        
        # Load the annotations:
        
        if annotation == "BP_Back":       
            BP = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))        
        else:
            BP = json.load(open(f'{Path_Annotation}_BP_Leaf_{org}.json'))
                   
        # Load the genes:
        
        genes = list(pd.read_table(f'{Path_Data}Genes_{org}_PPI', header = None, dtype={0: str})[0])
        
        # Generate the empty Matrix Gene/GO:
        
        go_terms = list(set([item for sublist in BP.values() for item in sublist])) 
        array_np = np.zeros((len(genes),len(go_terms)))
        
        # Make the binary matrix:
        
        filtered_gene_annotations = [i for i in BP.keys() if i in genes] 
        
        for key in filtered_gene_annotations:
            for value in BP[key]: 
                array_np[genes.index(key)][go_terms.index(value)] = 1
            
        # Generate the Binary pandas:
    
        Binary_pd = pd.DataFrame(array_np, columns = go_terms, index = genes)
        Binary_pd = Binary_pd.astype(int)
        Binary_pd = Binary_pd[sorted(Binary_pd.columns)]
    
        # Save Matrix Genes/GO:
        
        if annotation == "BP_Back":    
            Binary_pd.to_csv(f'{Path_Annotation}Matrix_GO_{org}.csv',index=True)
        else:
            Binary_pd.to_csv(f'{Path_Annotation}Matrix_GO_Leaf_{org}.csv',index=True)
            

class SNMTF(object):
    
    """Compute Partial Orthonormal Non-negative Matrix Tri-Factorization (NMTF) Embeddings"""
    
    def __init__(self, max_iter=1000, verbose = 10):

        super(SNMTF,self).__init__()
        self.max_iter = max_iter
        self.verbose = verbose


    def Score(self, R1, P, U, G, norm_R1):
        GT = np.transpose(G)

        ErR1 = R1 - np.matmul(P, np.matmul(U, GT))
        norm_erR1 = np.linalg.norm(ErR1, ord='fro')

        rel_erR1 = norm_erR1 / norm_R1

        return norm_erR1, rel_erR1


    def Get_Clusters(self, M, nodes):
        n,k = np.shape(M)
        Clusters = [[] for i in range(k)]
        for i in range(n):
            idx = np.argmax(M[i])
            Clusters[idx].append(nodes[i])
        return Clusters
    
    
    def SVD_Matrix(self, R1, matrix,file_name_array_path):
        
        print("Generating the SVD Matrix")
        
        J,K,L = svd(R1)
        
        file_name_array_path_1 = file_name_array_path + "_J_Matrix_" + str(matrix) + "_Human"
        file_name_array_path_2 = file_name_array_path + "_K_Matrix_" + str(matrix) + "_Human"
        file_name_array_path_3 = file_name_array_path + "_L_Matrix_" + str(matrix) + "_Human"
        
        np.save(file_name_array_path_1, J)
        np.save(file_name_array_path_2, K)
        np.save(file_name_array_path_3, L)
        

    def Solve_MUR_Hum(self, specie, R1, k1, matrix, orthonormality, network = "PPI", init= "SVD"):
        
        '''
        This Script is similar to the one used for Yeast. The only change is that the SVD is done one
        time (SVD_Matrix function), and this function recuperates it from the folder. In that way, we
        avoid repeating it for a lot of times and only two, one per each, is needed.
        '''
        
        # Paths:
        
        file_name_array_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
         
        print("Starting NMTF")
        n,n=np.shape(R1)
        norm_R1 = np.linalg.norm(R1, ord='fro')

        if init == "rand":
            print(" * Using random initialization")
            P = np.random.rand(n,k1) + 1e-5
            U = np.random.rand(k1,k1) + 1e-5
            G = np.random.rand(n,k1) + 1e-5
        elif init == 'SVD':
            
            # Check the species to init the NMTF:
            
            if specie == "Human":
                
                # We load the SVD that is already computed:
                
                J = np.load(file_name_array_path + "_J_Matrix_" + str(matrix) + "_Human.npy")
                K = np.load(file_name_array_path + "_K_Matrix_" + str(matrix) + "_Human.npy")
                L = np.load(file_name_array_path + "_L_Matrix_" + str(matrix) + "_Human.npy")
                
            else:
                
                # For the species we compute the SVD directly:
                
                J,K,L = svd(R1)
            
            P = np.zeros((n,k1)) + 1e-5
            G = np.zeros((n,k1)) + 1e-5
            U = np.zeros((k1,k1)) + 1e-5

            for i in range(n):
                for j in range(k1):
                    if J[i][j] > 0.:
                        P[i][j] = J[i][j]

            for i in range(k1):
                U[i][i] = abs(K[i])

            for i in range(n):
                for j in range(k1):
                    if L[i][j] > 0.:
                        G[i][j] = L[i][j]
            
        else :
            print("Unknown initializer: %s"%(init))
            exit(0)
        OBJ, REL2 = self.Score(R1,P, U, G, norm_R1)
        print(" - Init:\t OBJ:%.4f\t REL2:%.4f"%(OBJ, REL2))            
            
        # Begining M.U.R. (depending on the orthonormality constrain these update rules change):
        
        # If orthonormal update rules are needed:
        
        if orthonormality == "True":
            
            for it in range(1,self.max_iter+1):
    
                R1T = np.transpose(R1)
                UT = np.transpose(U)
    
                PT = np.transpose(P)
                PT_P = np.matmul(PT,P)
    
                GT = np.transpose(G)
                GT_G = np.matmul(GT,G)
    
                #update rule for G
    
                #R1 matrix
    
                #update rule for P
    
                R1_G_UT = np.matmul(np.matmul(R1, G), UT)
                U_GT_G_UT = np.matmul(np.matmul(U,GT_G),UT)
    
                P_U_GT_G_UT = np.matmul(P,U_GT_G_UT)
    
                P_mult = np.sqrt( np.divide(R1_G_UT,P_U_GT_G_UT + 1e-7) )
    
                #update rule for U
    
                PT_R1_G = np.matmul(PT, np.matmul(R1, G))
                PT_P_U_GT_G = np.matmul(PT_P, np.matmul(U, GT_G))
    
                U_mult = np.sqrt( np.divide(PT_R1_G,PT_P_U_GT_G + 1e-7))
    
                #update rule for G
    
                R1T_P_U = np.matmul(np.matmul(R1T, P), U)
                G_GT_R1T_P_U = np.matmul(G, np.matmul(GT, R1T_P_U))
    
                G_mult = np.sqrt( np.divide(R1T_P_U, G_GT_R1T_P_U + 1e-7) )
    
                # Applying M.U.R.
                P = np.multiply(P, P_mult) + 1e-7
                U = np.multiply(U, U_mult) + 1e-7
                G = np.multiply(G, G_mult) + 1e-7
    
    
                if (it%self.verbose == 0) or (it==1):
                    OBJ, REL2 = self.Score(R1, P, U, G, norm_R1)
                    print(" - It %i:\t OBJ:%.4f\t REL2:%.4f"%(it, OBJ, REL2))
             
            # Save the matrices:
            
            file_name_array_path_1 = file_name_array_path + "P_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_2 = file_name_array_path + "U_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_3 = file_name_array_path + "G_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            
            np.save(file_name_array_path_1, P)
            np.save(file_name_array_path_2, U)
            np.save(file_name_array_path_3, G)
        
        # If non-orthonormal update rules are needed (M.U.R change):
            
        else:
            
        	# Begining M.U.R. (no orthonormal)
                
            for it in range(1,self.max_iter+1):
        
                R1T = np.transpose(R1)
        
                PT   = np.transpose(P)
                PT_P = np.matmul(PT,P)
        
                GT   = np.transpose(G)
                GT_G = np.matmul(GT,G)
        
                # R1 matrix
        
                # Update rule for S
        
                GTG_inv = np.linalg.inv(GT_G)
                PTP_inv = np.linalg.inv(PT_P)
                PT_R1_G = np.matmul(PT, np.matmul(R1, G))
        
                PTP_inv_PT_R1_G = np.matmul(PTP_inv, PT_R1_G)
                U_mult = np.matmul(PTP_inv_PT_R1_G, GTG_inv)
        
                U = U_mult + 1e-7
        
                # Update rule for P
        
                U1_T = np.transpose(U)
        
                R1_G_U1_T     = np.matmul(np.matmul(R1, G), U1_T)
                R1_G_U1_T_pos = (np.absolute(R1_G_U1_T)+R1_G_U1_T)/2
                R1_G_U1_T_neg = (np.absolute(R1_G_U1_T)-R1_G_U1_T)/2
        
                U1_GT_G          =  np.matmul(U,GT_G)
                U1_GT_G_U1_T     = np.matmul(U1_GT_G,U1_T)
                U1_GT_G_U1_T_pos = (np.absolute(U1_GT_G_U1_T)+U1_GT_G_U1_T)/2
                U1_GT_G_U1_T_neg = (np.absolute(U1_GT_G_U1_T)-U1_GT_G_U1_T)/2
        
                P_U1_GT_G_U1_T_pos = np.matmul(P,U1_GT_G_U1_T_pos)
                P_U1_GT_G_U1_T_neg = np.matmul(P,U1_GT_G_U1_T_neg)
        
                P_nom   = R1_G_U1_T_pos + P_U1_GT_G_U1_T_neg
                P_denom = R1_G_U1_T_neg + P_U1_GT_G_U1_T_pos
        
                P_mult = np.sqrt( np.divide(P_nom,P_denom + 1e-5) )
                P = np.multiply(P, P_mult) + 1e-5
        
                # Update rule for G
        
                P1_T = np.transpose(P)
        
                R1T_P1_U1 = np.matmul(np.matmul(R1, P), U)
                R1T_P1_U1_pos = (np.absolute(R1T_P1_U1)+R1T_P1_U1)/2
                R1T_P1_U1_neg = (np.absolute(R1T_P1_U1)-R1T_P1_U1)/2
        
                U1_T_P1_T = np.matmul(U1_T,P1_T)
        
                U1_T_P1_T_P1_U1     = np.matmul(U1_T_P1_T,np.matmul(P,U))
                U1_T_P1_T_P1_U1_pos = (np.absolute(U1_T_P1_T_P1_U1)+U1_T_P1_T_P1_U1)/2
                U1_T_P1_T_P1_U1_neg = (np.absolute(U1_T_P1_T_P1_U1)-U1_T_P1_T_P1_U1)/2
        
                G_U1_T_P1_T_P1_U1_pos = np.matmul(G,U1_T_P1_T_P1_U1_pos)
                G_U1_T_P1_T_P1_U1_neg = np.matmul(G,U1_T_P1_T_P1_U1_neg)
        
                G_nom   = R1T_P1_U1_pos + G_U1_T_P1_T_P1_U1_neg
                G_denom = R1T_P1_U1_neg + G_U1_T_P1_T_P1_U1_pos
        
                G_mult = np.sqrt(np.divide(G_nom,G_denom + 1e-5) )
                G = np.multiply(G, G_mult) + 1e-5
        
                if (it%self.verbose == 0) or (it==1):
                    OBJ, REL2 = self.Score(R1, P, U, G, norm_R1)
                    print(" - It %i:\t OBJ:%.4f\t REL2:%.4f"%(it, OBJ, REL2))
         
            # Save the matrices (No orthonormal):
            
            file_name_array_path_1 = file_name_array_path + "No_Orth_P_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_2 = file_name_array_path + "No_Orth_U_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            file_name_array_path_3 = file_name_array_path + "No_Orth_G_Matrix_" + str(k1) + "_" + str(network) + "_" + str(matrix)
            
            np.save(file_name_array_path_1, P)
            np.save(file_name_array_path_2, U)
            np.save(file_name_array_path_3, G)    
            
        
def Preprocess_Deepwalk_Embeddings(Species, dimensions):
    
    # Paths:
    
    Deep_path       = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    for org in Species:
        
        # Load the correct name of the genes:
        
        with open(f'{Deep_path}{org}_node2vec_reverse_mapping.txt', "r") as read_file:
            reverse_mapping_load = json.load(read_file) 
        reverse_mapping_load_correct = {int(key):values for key,values in reverse_mapping_load.items()}  
        
        # Load the GO matrix:
        
        GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', 
                        index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
        
        # Now per each dimension:
        
        for dim in dimensions:
            
            # Load the embedding for line:
            
            Deep_emb = pd.read_csv(f'{Deep_path}{org}_{dim}_Deepwalk.txt', skiprows=1, sep = " ", header = None)
            
            # Delete the extra column:
            
            Deep_emb.index = Deep_emb[0]
            del Deep_emb[0]
            names_col        = [str(i) for i in range(len(Deep_emb.columns))]
            Deep_emb.columns = names_col
            Deep_emb  = Deep_emb.sort_index()  
            
            # Change the correct names of the genes:
            
            Deep_emb  = Deep_emb.sort_index()
            Deep_emb  = Deep_emb.rename(index=reverse_mapping_load_correct)
            
            # Reorder with the GO matrix
            
            Line_emb_final = Deep_emb.loc[GO_Matrix.index, :]
            embeddings_np = np.array(Line_emb_final)
            
            # Save the np:
            
            np.save(f'{Deep_path}Deepwalk_Coordinates_{org}_{dim}', embeddings_np)    
    
def Get_Deepwalk_Bases(Species, dimensions): 

    '''
    For the correct use of DeepWalks gensim==3.8.3 is needed.
    '''          
            
    # Paths:
    
    Deep_path       = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    network_path    = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    for org in Species:
        
        # Compute the PPMI:
        
        if org != "Human":
        
            Adj_matrix  = np.load(f'{network_path}Adj_{org}_PPI.npy', allow_pickle= True)
            PPMI_matrix = generate_embedding_spaces_Human.deep_walk_ppmi(Adj_matrix,  10) # In this case should be 10.
        
        else:
            
            PPMI_matrix = np.load("/media/sergio/sershiosdisk/Axes_Species/Data/PPMI_Human.npy",
                                  allow_pickle= True)
        
        # Get the GO matrix:
        
        GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', 
                        index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
        
        for dim in dimensions:
            
            # Load embeddings:
            
            embeddings             = np.load(f'{Deep_path}Deepwalk_Coordinates_{org}_{dim}.npy', allow_pickle=True)
            embeddings_pd          = pd.DataFrame(embeddings)
            embeddings_pd.index    =  GO_Matrix.index
            PPMI_matrix_pd         = pd.DataFrame(PPMI_matrix)
            PPMI_matrix_pd.columns = embeddings_pd.index
            PPMI_matrix_pd.index   = embeddings_pd.index  
            
            # Get the bases:
            
            embedding_inv = pd.DataFrame(np.linalg.pinv(embeddings_pd.values), embeddings_pd.columns, embeddings_pd.index)
            bases         = embedding_inv.dot(PPMI_matrix_pd) 
            bases_np      = np.array(bases)
            
            # Save the bases:
            
            np.save(f'{Deep_path}Deepwalk_Bases_{org}_{dim}', bases_np)    
    

def KMedoids_format(matrix, labels):
    
    # Calculate cosine distance:
    
    distance = pairwise_distances(matrix, metric="cosine")
    
    # Number of clusters (thum-rule):
    
    n_clusters        = round(math.sqrt(len(matrix)/2))
    
    # Compute the kmedoids using a precomputed distance matrix:
    
    clustering   = KMedoids(n_clusters=n_clusters, metric='precomputed',random_state=0).fit(distance)
    
    # Get the clusters:
    
    clusters_lab = clustering.labels_
    
    # Adapt it to the correct format:
    
    clusters = [[] for i in range(n_clusters)]
    n = len(clusters_lab)
    
    for i in range(n):
        clusters[clusters_lab[i]].append(labels[i])
    
    # Return:
    
    return clusters           
     
def Enrichment_Analyses(Species, dimension_list, matrix = "PPMI", Orthonormal = True, annotation = "BP_Back"):
    
    # Paths:
    
    Path_Annotation  = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Data        = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Path_enrichments = "/media/sergio/sershiosdisk/Axes_Species/Enrichments/"
    Path_node2vec    = "/media/sergio/sershiosdisk/Axes_Species/Node2vec/"
    Line_path        = "/media/sergio/sershiosdisk/Axes_Species/LINE/"
    Deep_path        = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    
    # Start the enrichments:
    
    for org in Species:
        
        print(org)
        
        # Load the annotations:
        
        if annotation == "BP_Back":        
            BP = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
        else:
            BP = json.load(open(f'{Path_Annotation}_BP_Leaf_{org}.json'))
                    
        # Do enrichments per each dimension:
        
        for dim in dimension_list: 
            
            print(dim)
            
            # Load the genes:
            
            genes = list(pd.read_table(f'{Path_Data}Genes_{org}_PPI', header = None, dtype={0: str})[0])
            
            # Load the G matrices:
            
            if matrix == "node2vec":
                
                G_Matrix_sp = np.load(f'{Path_node2vec}node2vec_array_{org}_{dim}.npy', allow_pickle=True)
                
                # K-medoids for clustering: (clusters equal dim to be comparable with NMTF):
                
                G_sp_Clustering = KMedoids_format(G_Matrix_sp, genes) 
            
            elif matrix == "LINE":
            
                G_Matrix_sp = np.load(f'{Line_path}Line_Coordinates_{org}_{dim}.npy', allow_pickle=True)
                
                # K-medoids for clustering: (clusters equal dim to be comparable with NMTF):
                
                G_sp_Clustering = KMedoids_format(G_Matrix_sp, genes)
                               
            elif matrix == "Deepwalk":
                
                G_Matrix_sp = np.load(f'{Deep_path}Deepwalk_Coordinates_{org}_{dim}.npy', allow_pickle=True)
                
                # K-medoids for clustering: (clusters equal dim to be comparable with NMTF):
                
                G_sp_Clustering = KMedoids_format(G_Matrix_sp, genes)            
            
            else:
                # Calculate the coordinates:
                
                if org == "Human":
                    
                    if Orthonormal == True:
                        Path_Data_NMTF = Path_Data
                        P = np.load(f'{Path_Data_NMTF}P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                        U = np.load(f'{Path_Data_NMTF}U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                    else:
                        Path_Data_NMTF = Path_Data
                        P = np.load(f'{Path_Data_NMTF}No_Orth_P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                        U = np.load(f'{Path_Data_NMTF}No_Orth_U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)  
                else:
                    if Orthonormal == True:
                        Path_Data_NMTF = Path_Data
                        P = np.load(f'{Path_Data_NMTF}P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                        U = np.load(f'{Path_Data_NMTF}U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                    else:
                        Path_Data_NMTF = Path_Data
                        P = np.load(f'{Path_Data_NMTF}No_Orth_P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                        U = np.load(f'{Path_Data_NMTF}No_Orth_U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)                         
                
                G_Matrix_sp = P.dot(U)
                
                # K-medoids for clustering: (clusters equal dim to be comparable with NMTF):
                
                G_sp_Clustering = KMedoids_format(G_Matrix_sp, genes) 
            
            # Compute the Enrichments:
            
            if Orthonormal == True:
            
                sub_command1 = (G_sp_Clustering,  BP, dim, Path_enrichments, 0.05, annotation, f'PPI_{org}', matrix, "terms in clusters")
                sub_command2 = (G_sp_Clustering,  BP, dim, Path_enrichments, 0.05, annotation, f'PPI_{org}', matrix, "statistics")
            else:
                sub_command1 = (G_sp_Clustering,  BP, dim, Path_enrichments, 0.05, annotation, f'PPI_{org}_No_Orth', matrix, "terms in clusters")
                sub_command2 = (G_sp_Clustering,  BP, dim, Path_enrichments, 0.05, annotation, f'PPI_{org}_No_Orth', matrix, "statistics")                
            
            Process = [sub_command1,sub_command2]

            n_cores = multiprocessing.cpu_count()    
            pool1 = Pool(processes  = n_cores)  
            pool1.starmap(enrichment_analysis.enrichment_analysis, Process)
                            
            pool1.close()
            pool1.join() 

def Plot_Enrichments_Avg_Sp(Species, dimensions, matrix_list, annotations = "BP_Back"):
       
    save      = "/media/sergio/sershiosdisk/Axes_Species/Enrichments/"
    save_plot = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    # Load information:
    
    Data = pd.DataFrame()
    org_list    = []  
    matrix_fin  = []
    for matrix in matrix_list:        
        for org in Species:                      
            for dim in dimensions:                
                Data_it = pd.read_csv(f'{save}Enrichments_{annotations}_PPI_{org}_{matrix}_{dim}_Statistics.csv', index_col = 0)
                Data = Data.append(Data_it)
                org_list.append(org) 
                matrix_fin.append(matrix) 
                             
    Data = Data.reset_index(drop = True)                             
    Data["Species"] = org_list
    Data["Matrix"]  = matrix_fin
    
    # % Clusters:
    
    x      =  np.arange(len(Species))
    x      = x*2
    width  = 0.20
    sum_it = -0.2
    
    fig, axs = plt.subplots(1, 1, figsize=(15,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Species").mean()
        std  = Data[Data.Matrix == matrix].groupby("Species").std()
        
        plt.bar(x + sum_it, mean["Enriched_Clusters"], width, label='%Clusters', yerr =std["Enriched_Clusters"], capsize = 10)
        sum_it = sum_it+0.20 
        
    axs.set_ylim(0, 100)    
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=13) 
    axs.set_ylabel("% Clusters", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    plt.xticks(x+width,['Fruit fly', 'Human', 'Mouse', 'Fission yeast', 'Rat', 'Budding yeast'], fontsize=15)
    
    fig.savefig(f'{save_plot}Perc_Clusters_Species.png', format="png", dpi=600,
                    bbox_inches='tight') 
    
    #fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))
    
    # % GO:
    
    x      =  np.arange(len(Species))
    x      = x*2
    width  = 0.20
    sum_it = -0.2
    
    fig, axs = plt.subplots(1, 1, figsize=(15,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Species").mean()
        std  = Data[Data.Matrix == matrix].groupby("Species").std()
        
        plt.bar(x + sum_it, mean["%GO"], width, label='%Clusters', yerr =std["%GO"], capsize = 10)
        sum_it = sum_it+0.20 
        
    axs.set_ylim(0, 40)     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=13) 
    axs.set_ylabel("% GO", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    plt.xticks(x+width,['Fruit fly', 'Human', 'Mouse', 'Fission yeast', 'Rat', 'Budding yeast'], fontsize=15)
    
    fig.savefig(f'{save_plot}Perc_GO_Species.png', format="png", dpi=600,
                    bbox_inches='tight') 
    
    #fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))     
    
    # % Genes:
    
    x      =  np.arange(len(Species))
    x      = x*2
    width  = 0.20
    sum_it = -0.2
    
    fig, axs = plt.subplots(1, 1, figsize=(15,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Species").mean()
        std  = Data[Data.Matrix == matrix].groupby("Species").std()
        
        plt.bar(x + sum_it, mean["%Genes"], width, label='%Genes', yerr =std["%Genes"], capsize = 10)
        sum_it = sum_it+0.20 
        
    axs.set_ylim(0, 80)    
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=13) 
    axs.set_ylabel("% Genes", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    plt.xticks(x+width,['Fruit fly', 'Human', 'Mouse', 'Fission yeast', 'Rat', 'Budding yeast'], fontsize=15)

    fig.savefig(f'{save_plot}Perc_Genes_Species.png', format="png", dpi=600,
                    bbox_inches='tight') 
    
    #fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list)) 


def Plot_Enrichments_Avg_Human(Species, dimensions, matrix_list, annotations = "BP_Back"):
    
    save = "/media/sergio/sershiosdisk/Axes_Species/Enrichments/"
    
    for org in Species:
           
        # Get the corresponding information per each algorithm:
        
        matrix_total = []
        for matrix in matrix_list:
            Enrichments_list  = []
            for dim in dimensions:
                Data_it = pd.read_csv(f'{save}Enrichments_{annotations}_PPI_{org}_{matrix}_{dim}_Statistics.csv', index_col = 0) 
                Enrichments_list.append(Data_it)
            matrix_total.append(Enrichments_list)
            
        # Enrichment Clusters:
        
        Clusters_total  = []
        for j in range(len(matrix_list)):
            Clusters = [float(i["Enriched_Clusters"]) for i in matrix_total[j]]
            Clusters_total.append(Clusters)
            
        cluster_mean = [np.mean(i) for i in Clusters_total]
        cluster_std  = [np.std(i) for i in Clusters_total]
    
        
        # Enrichment GO terms:
        
        GO_total  = []
        for j in range(len(matrix_list)):
            GO = [float(i["%GO"]) for i in matrix_total[j]]
            GO_total.append(GO)
      
        GO_mean = [np.mean(i) for i in GO_total]
        GO_std  = [np.std(i) for i in GO_total] 
        
        # Genes:
        
        Genes_total  = []
        for j in range(len(matrix_list)):
            Genes = [float(i["%Genes"]) for i in matrix_total[j]]
            Genes_total.append(Genes)
      
        Genes_mean = [np.mean(i) for i in Genes_total]
        Genes_std  = [np.std(i) for i in Genes_total]      
        
        # Generate the data frame for the plot:
        
        x = np.arange(len(matrix_list))
        width = 0.25

        fig, axs = plt.subplots(1, 1, figsize=(12,5)) 
        bar1 = axs.bar(x, cluster_mean, width, label='%Clusters', yerr =cluster_std, capsize = 10)
        bar2 = axs.bar(x + width, GO_mean, width, label='%GO', yerr =GO_std, capsize = 10)
        bar3 = axs.bar(x + width*2, Genes_mean, width, label='%Genes', yerr =Genes_std, capsize = 10)
        
        plt.xticks(x+width,matrix_list, fontsize=15)

        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=15) 
        axs.set_ylabel("% Enrichment", fontsize=20, fontweight='bold') 
        
        fig.legend( (bar1, bar2, bar3), ('Clusters', 'GO BP terms', 'Genes') ,prop={'size': 16},
                   loc="upper right", frameon=True)

def Plot_Enrichments_all(Species, dimensions, matrix_list, annotations = "BP_Back"):
    
    # Paths:
    
    save      = "/media/sergio/sershiosdisk/Axes_Species/Enrichments/"
    save_plot = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    labels = []
    
    for i in matrix_list:
        if i == "PPMI":
            labels.append("ONMTF")
        elif i == "No_Orth_PPMI":
            labels.append("NMTF")
        elif i == "Deepwalk":
            labels.append("Deepwalk")
        elif i == "LINE":
            labels.append("LINE")
        elif i == "Adj":
            labels.append("Adj")
                   
    for org in Species:
    
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 50})
        plt.tight_layout()
        fig, axs = plt.subplots(1, 1, figsize=(10,5))
        
        # Get the corresponding information per each algorithm:
        
        matrix_total = []
        for matrix in matrix_list:
            Enrichments_list  = []
            for dim in dimensions:
                Data_it = pd.read_csv(f'{save}Enrichments_{annotations}_PPI_{org}_{matrix}_{dim}_Statistics.csv', index_col = 0) 
                Enrichments_list.append(Data_it)
            matrix_total.append(Enrichments_list)
            
        # Start with the three different plots:
        
        # Enrichment Clusters:
        
        for j in range(len(matrix_list)):
            Clusters = [float(i["Enriched_Clusters"]) for i in matrix_total[j]]
            axs.plot(dimensions, Clusters ,  marker='o',  linewidth =3)
        
        axs.set_xticks(dimensions)
        axs.set_xticklabels(dimensions)
        axs.set_ylim(0, 100)
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=13) 
        axs.set_ylabel("% Clusters", fontsize=20, fontweight='bold') 
        axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold') 
                        
        fig.savefig(f'{save_plot}%_Clusters_{org}.svg', format="svg", dpi=600,
                    bbox_inches='tight')

        # Enrichment GO terms:  

        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 50})
        plt.tight_layout()
        fig, axs = plt.subplots(1, 1, figsize=(10,5))        
            
        for j in range(len(matrix_list)):
            Clusters = [float(i["%GO"]) for i in matrix_total[j]]
            axs.plot(dimensions, Clusters ,  marker='o',  linewidth =3)
        
        axs.set_xticks(dimensions)
        axs.set_xticklabels(dimensions)
        axs.set_ylim(0, 50)
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=13) 
        axs.set_ylabel("% GO", fontsize=20, fontweight='bold') 
        axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold')


        fig.savefig(f'{save_plot}%_GO_{org}.svg', format="svg", dpi=600,
                   bbox_inches='tight')        
        
        # Enrichment genes: 
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 50})
        plt.tight_layout()
        fig, axs = plt.subplots(1, 1, figsize=(10,5))          

        for j in range(len(matrix_list)):
            Clusters = [float(i["%Genes"]) for i in matrix_total[j]]
            axs.plot(dimensions, Clusters ,  marker='o',  linewidth =3)
        
        axs.set_xticks(dimensions)
        axs.set_xticklabels(dimensions)
        axs.set_ylim(0, 70)
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=13) 
        axs.set_ylabel("% Genes", fontsize=20, fontweight='bold')  
        axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold') 
        
        fig.savefig(f'{save_plot}%_genes_{org}.svg', format="svg", dpi=600,
                    bbox_inches='tight')  
        
        fig.legend(labels= labels,
                   borderaxespad=0.1,
                   bbox_to_anchor=(0.5, 0.02),
                   loc="upper center", frameon=True, ncol = len(matrix_list),prop={'size': 20})  

    
def Plot_Enrichments(Species, dimensions, matrix = "PPMI", annotations = "BP_Back", orthonormality = True):

    save = "/media/sergio/sershiosdisk/Axes_Species/Enrichments/"

    column_control = 0
    
    if orthonormality == True:
        label = "_"
    else:
        label = "_No_Orth_"
    
    # Set the grid:
    
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 50})
    plt.tight_layout()
    
    if "Human" in Species:
        fig, axs = plt.subplots(3, 1, figsize=(13,9))
    else:
        fig, axs = plt.subplots(3, len(Species), figsize=(30,9))

    column_control = 0
    
    for org in Species:
        
        Enrichments_list  = []
        
        for dim in dimensions:
            
            Data_it = pd.read_csv(f'{save}Enrichments_{annotations}_PPI_{org}{label}{matrix}_{dim}_Statistics.csv', index_col = 0) 
            Enrichments_list.append(Data_it)
        
        Clusters = [float(i["Enriched_Clusters"]) for i in Enrichments_list]
        GO       = [float(i["%GO"]) for i in Enrichments_list]
        genes    = [float(i["%Genes"]) for i in Enrichments_list]
        
        # Start plotting:
            
        # Enriched clusters Plot:
        
        if org != "Human":
        
            axs[0][column_control].plot(dimensions, Clusters ,  marker='o',  linewidth =3, color="#f1948a")  
            axs[0][column_control].set_xticks(dimensions)
            axs[0][column_control].set_xticklabels([])
            axs[0][column_control].set_title(f'{matrix}_{org.capitalize()}', fontsize=20, fontweight='bold')
            axs[0][column_control].spines['right'].set_visible(False)
            axs[0][column_control].spines['top'].set_visible(False)
            axs[0][column_control].spines['left'].set_linewidth(4)
            axs[0][column_control].spines['bottom'].set_linewidth(4)
            axs[0][column_control].spines['left'].set_color("grey")
            axs[0][column_control].spines['bottom'].set_color("grey")
            axs[0][column_control].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[0][column_control].set_ylabel("% Clusters", fontsize=20, fontweight='bold')
                
            # Enriched Genes plot:
            
            axs[1][column_control].plot(dimensions, genes,  marker='o', linewidth =3, color="#f1948a")
            axs[1][column_control].set_xticks(dimensions)
            axs[1][column_control].set_xticklabels([])
            axs[1][column_control].spines['right'].set_visible(False)
            axs[1][column_control].spines['top'].set_visible(False)
            axs[1][column_control].spines['left'].set_linewidth(4)
            axs[1][column_control].spines['bottom'].set_linewidth(4)
            axs[1][column_control].spines['left'].set_color("grey")
            axs[1][column_control].spines['bottom'].set_color("grey")
            axs[1][column_control].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[1][column_control].set_ylabel("% Genes", fontsize=20, fontweight='bold')
    
            # Enriched GO terms:
            
            axs[2][column_control].plot(dimensions, GO, marker='o', linewidth =3, color="#f1948a")
            axs[2][column_control].set_xticks(dimensions)
            axs[2][column_control].spines['right'].set_visible(False)
            axs[2][column_control].spines['top'].set_visible(False)
            axs[2][column_control].spines['left'].set_linewidth(4)
            axs[2][column_control].spines['bottom'].set_linewidth(4)
            axs[2][column_control].spines['left'].set_color("grey")
            axs[2][column_control].spines['bottom'].set_color("grey")
            axs[2][column_control].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[2][column_control].set_ylabel("% GO", fontsize=20, fontweight='bold')
                
            # Change to the next column:
            
            column_control = column_control + 1
    else:
        
            axs[0].plot(dimensions, Clusters ,  marker='o',  linewidth =3, color="#f1948a") 
            axs[0].set_title("A", fontsize=20, fontweight='bold')
            axs[0].set_xticks(dimensions)
            axs[0].set_xticklabels([])
            axs[0].set_title(f'{matrix}_{org.capitalize()}', fontsize=20, fontweight='bold')
            axs[0].spines['right'].set_visible(False)
            axs[0].spines['top'].set_visible(False)
            axs[0].spines['left'].set_linewidth(4)
            axs[0].spines['bottom'].set_linewidth(4)
            axs[0].spines['left'].set_color("grey")
            axs[0].spines['bottom'].set_color("grey")
            axs[0].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[0].set_ylabel("% Clusters", fontsize=20, fontweight='bold')
                
            # Enriched Genes plot:
            
            axs[1].plot(dimensions, genes,  marker='o', linewidth =3, color="#3bbed5")
            axs[1].set_title("B", fontsize=20, fontweight='bold')
            axs[1].set_xticks(dimensions)
            axs[1].set_xticklabels([])
            axs[1].spines['right'].set_visible(False)
            axs[1].spines['top'].set_visible(False)
            axs[1].spines['left'].set_linewidth(4)
            axs[1].spines['bottom'].set_linewidth(4)
            axs[1].spines['left'].set_color("grey")
            axs[1].spines['bottom'].set_color("grey")
            axs[1].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[1].set_ylabel("% Genes", fontsize=20, fontweight='bold')
    
            # Enriched GO terms:
            
            axs[2].plot(dimensions, GO, marker='o', linewidth =3, color="#6fed6f")
            axs[2].set_title("B", fontsize=20, fontweight='bold')
            axs[2].set_xticks(dimensions)
            axs[2].spines['right'].set_visible(False)
            axs[2].spines['top'].set_visible(False)
            axs[2].spines['left'].set_linewidth(4)
            axs[2].spines['bottom'].set_linewidth(4)
            axs[2].spines['left'].set_color("grey")
            axs[2].spines['bottom'].set_color("grey")
            axs[2].tick_params(axis='both', labelsize=15)
            
            if column_control == 0:
                axs[2].set_ylabel("% GO", fontsize=20, fontweight='bold')
                
            # Change to the next column:
            
            column_control = column_control + 1
        
    fig.text(0.5, 0.05, 'Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=20, fontweight = "bold")
    
    # Save the plot:
    
    if org != "Human":
    
        fig.savefig(f'{save}Enrichments_Species_{matrix}_BP.png', format="png", dpi=600,
                        bbox_inches='tight')
    else:
        fig.savefig(f'{save}Human_Enrichments_Species_{matrix}_BP.png', format="png", dpi=600,
                        bbox_inches='tight')
       

def Embedd_GO_terms(matrix, organism, dimensions, annotation = "BP_Back", orthonormal = "True"):
    
    if matrix == "PPMI":
        Annotate_Gene_Space(organism, dimensions,  matrix, annotation, orthonormal)
    elif matrix == "Deepwalk":
        Annotate_Deepwalk_space(organism, dimensions, annotation)
    else:
        print("Error")
       
def Annotate_Deepwalk_space(organism, dimensions, annotation):
    
    # Networks path:
    
    Annotation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/" 
    Bases_path        = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
    save              = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Get the specie information:
        
    if annotation == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)   
        
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]
        
    # Delete the annotations without genes (>0):
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
    
    # Load the bases of Deepwalk:
                   
    G_sp       = np.load(f'{Bases_path}Deepwalk_Bases_{organism}_{dimensions}.npy', allow_pickle=True).T
    G_sp       = pd.DataFrame(G_sp)
    G_sp.index = GO_Matrix_filt.index
    G_sp_inver = pd.DataFrame(np.linalg.pinv(G_sp.values), G_sp.columns, G_sp.index)
    GO_embedd  = G_sp_inver.dot(GO_Matrix_filt) 
            
    # Save file:
               
    GO_embeddings_norm_db         = pd.DataFrame(GO_embedd)
    GO_embeddings_norm_db.columns = GO_embedd.columns       
    GO_embeddings_norm_db.to_csv(f'{save}_GO_Embeddings_{annotation}_PPI_{organism}_{dimensions}_Deepwalk.csv')

def Annotate_Gene_Space(organism, dimensions, matrix, annotation, orthonormal):
    
    # Networks path:
    
    Annotation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/" 
    Bases_path        = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
      
    # Get the specie information:
    
    if annotation == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)        
         
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]
        
    # Delete the annotations without genes (>0):
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
        
    # Start the embeddings:
                 
    # Fix the gene vectorial representation:
    
    if orthonormal == "True":           
        G_sp  = np.load(f'{Bases_path}G_Matrix_{dimensions}_PPI_{organism}_{matrix}.npy', allow_pickle=True)
    else:
        G_sp  = np.load(f'{Bases_path}No_Orth_G_Matrix_{dimensions}_PPI_{organism}_{matrix}.npy', allow_pickle=True)
                    
    # Do the embeddings:
            
    GO_Embeddings_Human(GO_Matrix_filt,  G_sp, Annotation_path,  GO = annotation, network = f'PPI_{organism}', matrix = matrix, orthonormal=str(orthonormal))


def GO_Embeddings_Human(Matrix_Gene_GO, gene_embeddings, save_path, GO, network, matrix, orthonormal):
    
    '''
    Imputs:
            
        - Matrix_Gene_GO  : DataFrame with genes and GO terms (0 : Not annotated, 1 : Annotated)
        - gene_embeddings : Array with the embeddings of the genes (the bases, matrix G).
        - save_path       : string, path to save the output.
        - GO              : string, annotations that are used.
        - network         : string, network (e.g., PPI for protein-protein interaction network).
        - matrix          : string, network matrix representation (e.g., Adj for adjancency matrix).
                          
    '''
    
    save              = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    
    # Matrix with gene embeddings (if the previous function was used to create the gene/GO the order of the genes is the same):
    
    gene_embeddings_db = pd.DataFrame(gene_embeddings)
    gene_embeddings_db.index = Matrix_Gene_GO.index
    
    # Calculate the GO embeddings:

    gene_embeddings_db_inverse = pd.DataFrame(np.linalg.pinv(gene_embeddings_db.values), gene_embeddings_db.columns, gene_embeddings_db.index)
    GO_embeddings = gene_embeddings_db_inverse.dot(Matrix_Gene_GO)
    
    # Save the output:
    
    if orthonormal == "True":        
        save_path_1 = save + "_GO_Embeddings_" + str(GO) + "_" + str(network) + "_"+ str(len(gene_embeddings_db.columns))+ "_" + str(matrix) + ".csv"
        GO_embeddings.to_csv(save_path_1)
    else:
        save_path_1 = save + "_GO_Embeddings_" + str(GO) + "_" + str(network) + "_"+ str(len(gene_embeddings_db.columns))+ "_No_Orth_" + str(matrix) + ".csv"
        GO_embeddings.to_csv(save_path_1)        

def Run_Permutations(organism, optimal_dimension, matrix, times, name, orthonormal, annotations):
    
    # Paths:
    
    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/"    
    Annotation_path    = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Embeddings/"
    GO_embeddings_path = "/gpfs/projects/bsc79/bsc79321/Axes_Species/GO_Emb_Results/"
    network_path       = "/gpfs/projects/bsc79/bsc79321/Axes_Species/NMTF/"
       
    # Get the specie information:
      
    # Check if a folder exist:
    
    if orthonormal == "True":
        label = ""
    else:
        label = "_No_Orth"
        
    path_it = f"{permutation_path}Permutations_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}/"       
    os.makedirs(path_it, exist_ok=True)
    
    # Load Genes embeddings:
    
    if matrix == "PPMI":
        if orthonormal == "True":
            path = f'{network_path}G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)
        else:
            path = f'{network_path}No_Orth_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)            
    elif matrix == "Adj":
        if orthonormal == "True":
            path = f'{network_path}G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)
        else:
            path = f'{network_path}No_Orth_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
            G_sp = np.load(path, allow_pickle=True)                        
    elif matrix == "node2vec":
        path = f'{network_path}node2vec_array_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True)
    elif matrix == "LINE":
        path = f'{network_path}Line_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
    elif matrix == "Deepwalk":
        path = f'{network_path}Deepwalk_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
        
    # Load the annotations:
    
    if annotations == "BP_Back":    
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
    else:
        GO_Matrix = pd.read_csv(f'{Annotation_path}Matrix_GO_Leaf_{organism}.csv', 
                                    index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)  

    # Load the genes and reorder the annotations accordingly:        
          
    genes = list(pd.read_table(f'{Annotation_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]  
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
    
    # Load the GO embeddings (Orthonormal or not for NMTF):
    
    if orthonormal == "True":
    
        GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{organism}_{optimal_dimension}_{matrix}.csv', 
                                index_col = 0)
        GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
        
    elif orthonormal == "False":
        GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{organism}_{optimal_dimension}_No_Orth_{matrix}.csv', 
                                index_col = 0)
        GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
        
    # Start the permutations
                                
    Count_db = pd.DataFrame(0, columns=GO_embeddings.columns, index = GO_embeddings.index)

    # Keep the names:
    
    index_orig   = GO_Matrix_filt.index
    columns_orig = GO_Matrix_filt.columns
    
    # Init the permutation test for N times:
       
    for rep in range(times):
        
        # Randomly suffle the Gene/GO matrix:
            
        Gene_GO_matrix_it = GO_Matrix_filt.sample(frac=1).reset_index(drop=True)
        Gene_GO_matrix_it.index = index_orig
        Gene_GO_matrix_it.columns = columns_orig
            
        # Do the Embeddings using the suffled Gene/GO matrix (direct calculation):
        
        gene_embeddings_db_inverse = pd.DataFrame(np.linalg.pinv(G_sp) , columns = index_orig)
        GO_emebddings_it = gene_embeddings_db_inverse.dot(Gene_GO_matrix_it)

        # Compare the new scores vs the original scores:
            
        comparison = GO_emebddings_it >= GO_embeddings
            
        # Update the counts:
            
        Count_db = Count_db + comparison
        
    # Finished:
    
    # Save the matrix in a folder with the names:
    
    Count_db.to_csv(f'{path_it}/P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{name}.csv', 
                          header = True, index=True)


def Sum_Permutations_Dist(organism, optimal_dimension, matrix, times, orthonormal, annotations):
    
    # Generic path:
    
    permutation_path   = "/gpfs/projects/bsc79/bsc79321/Axes_Species/Perm_Results/"
    
    if orthonormal == "True":
        label = ""
    else:
        label = "_No_Orth"
    
    # Count the number of files in the directory:
    
    directory = f'{permutation_path}Permutations_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}/'   
    number    = len(os.listdir(directory))
    
    # Load all the files in the folder:
    
    Final = pd.DataFrame()

    for i in range(number):
        
        if i == 0: 
            permutations_it   = pd.read_csv(f'{directory}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{int(i)}.csv',index_col = 0, dtype={0: str})
            Final = permutations_it

        else:
            permutations_it   = pd.read_csv(f'{directory}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{times}_{int(i)}.csv',index_col = 0, dtype={0: str})
            Final = Final + permutations_it
            
    # Sum all the permutations in the list:
    
    Total_counts = times * number
    
    # Calculate the p-values:
    
    Final = (Final + 1)/(Total_counts + 1)
        
    # Save the final total permutations outside of the folder:
     
    Final.to_csv(f'{permutation_path}P_value_{organism}_{optimal_dimension}_{matrix}{label}_{annotations}_{Total_counts}.csv', 
                          header = True, index=True)  

def Calculate_limits(list_mean, list_std):
    
    lower_error = []
    upper_error = []
    
    for i in range(len(list_mean)):
        
        lower = list_mean[i] - list_std[i]
        upper = list_mean[i] + list_std[i]
        
        if lower < 0:
            lower = list_mean[i]
        else:
            lower = list_std[i] 
            
        if upper > 1:
            upper = 1 - list_mean[i]
        else:
            upper = list_std[i] 
            
        
        lower_error.append(lower)
        upper_error.append(upper)
    
    result = [lower_error, upper_error]
    
    return(result)


def Parallel_Correlations(Species, dimensions, matrix_list, statistics = False):
    
    commands = []
    for dim in dimensions:
        write = ([Species, [dim], matrix_list, statistics])
        commands.append(write)
    n_cores = multiprocessing.cpu_count()
    pool1 = Pool(processes  = n_cores) 
    pool1.starmap(Correlation_Gene_Spaces, commands)
    print("Finished")
      
        
def Correlation_Gene_Spaces(Species, dimensions, matrix_list, statistics = False):
    
    # Paths:
    
    deep_path        = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    node2vec_path    = "/media/sergio/sershiosdisk/Axes_Species/Node2vec/"
    Line_path        = "/media/sergio/sershiosdisk/Axes_Species/LINE/"
    network_path     = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    correlation_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Gene_Correlation/"
    
    for org in Species:
    
        avg_list    = []
        std_list    = []
        matrix_fin  = []

        for dim in dimensions:
            
            print(f'{org} / {dim}')
            
            # If statistics between the matrices are needed prepare the dataframe:
            
            if statistics == True:
                    
                    full_correlations = []
                    Comparison_pd = pd.DataFrame(0, index=np.arange(len(matrix_list)), 
                                         columns=matrix_list)
                    Comparison_pd.index = matrix_list
        
            for matrix in matrix_list:
                
                # Get the gene embeddings (bases):
                
                if matrix == "PPMI":
                    path = f'{network_path}_G_Matrix_{dim}_PPI_{org}_{matrix}.npy'
                    G_sp = np.load(path, allow_pickle=True)
                elif matrix == "Adj":
                    path = f'{network_path}_G_Matrix_{dim}_PPI_{org}_{matrix}.npy'
                    G_sp = np.load(path, allow_pickle=True)
                elif matrix == "node2vec":
                    path = f'{node2vec_path}node2vec_array_{org}_{dim}.npy'
                    G_sp = np.load(path, allow_pickle=True)
                elif matrix == "LINE":
                    path = f'{Line_path}Line_Bases_{org}_{dim}.npy'
                    G_sp = np.load(path, allow_pickle=True).T
                elif matrix == "Deepwalk":
                    path = f'{deep_path}Deepwalk_Bases_{org}_{dim}.npy'
                    G_sp = np.load(path, allow_pickle=True).T                    
            
                # Calculate the correlation between its dimensions:
                
                G_sp_pd     = pd.DataFrame(G_sp)
                correlation = G_sp_pd.corr("pearson")  
                correlation = correlation.where(np.triu(np.ones(correlation.shape)).astype(np.bool))  
                correlation = correlation.stack().reset_index()
                correlation = correlation[correlation.level_0 != correlation.level_1]
                
                if statistics == True:
                    full_correlations.append(list(abs(correlation[0])))
                else:
                    
                    # Get the deviation and the avg of the correlations:
                    
                    average           = np.mean(abs(correlation[0]))
                    deviation         = np.std(abs(correlation[0]))
                    
                    # Keep information for the list:
    
                    avg_list.append(average)
                    std_list.append(deviation)
                    matrix_fin.append(matrix)
            
            # If statistics between the matrices are needed:
            
            if statistics == True:
                for i in range(len(full_correlations)):
                    print("GO")
                    corr_1 = full_correlations[i]
                    for j in range(len(full_correlations)):
                        corr_2 = full_correlations[j]
                        P_value2 = round(mannwhitneyu(corr_1, corr_2, alternative = "less").pvalue,4)
                        Comparison_pd.iloc[i, j] = P_value2
                
                # Save the values:
                
                Comparison_pd.to_csv(f'{correlation_path}P_Value_Less_{org}_{dim}.txt') 
        
        if statistics == False:
        
            # Save dictionary:
                    
            save_dic = {"Avg"    : avg_list,
                        "std"    : std_list,
                        "Matrix" : matrix_fin}
            
            # Save the dictionary:
      
            sa = json.dumps(save_dic)
            f = open(f'{correlation_path}Correlation_Axes_Genes_{org}.json' ,"w")
            f.write(sa)
            f.close()    

def Plot_Correlation(Species, dimensions, matrix_list):
    
    # Paths:
    
    path_corr = "/media/sergio/sershiosdisk/Axes_Species/Axes_Gene_Correlation/"
    
    # Controlers:
    
    row_control = 0
    label_count = 0
    
    # Authomatic Labels:
    
    plot_labels = []
    
    for i in range(len(Species) * 3):
        
        plot_labels.append(ascii_lowercase[i])
    
    # Prepare the grid:
        
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
    fig, axes = plt.subplots(len(Species), 1, figsize=(25,15))

    # Start the plotting:
    
    for org in Species:
        
        # Load the information of the functional organization:
        
        Organization  = open(f'{path_corr}Correlation_Axes_Genes_{org}.json' ,)
        Organization = json.load(Organization)
        
        Organization_pd = pd.DataFrame(Organization)
        
        # Error bars:
        
        # Transform count:
        
        transf = -0.15
                
        for matrix in matrix_list:
            
            info = Organization_pd[Organization_pd.Matrix == matrix].Avg
            std  =  Organization_pd[Organization_pd.Matrix == matrix]["std"]
            
            info = info.reset_index(drop = True)
            std  = std.reset_index(drop = True)
            
            # Transform:
            
            transform = Affine2D().translate(transf, 0.0) + axes[row_control].transData
            
            axes[row_control].errorbar(dimensions, info,   yerr= Calculate_limits(info, std),  
                marker = "o", capsize=5, lw = 4, fmt = " ", transform = transform)
            
            transf = transf + 3
            
        # Setting for the line plot:
            
        axes[row_control].spines['right'].set_visible(False)
        axes[row_control].spines['top'].set_visible(False)
        axes[row_control].spines['left'].set_linewidth(4)
        axes[row_control].spines['bottom'].set_linewidth(4)
        axes[row_control].spines['left'].set_color("grey")
        axes[row_control].spines['bottom'].set_color("grey") 
        axes[row_control].set_ylabel(' ', fontsize=16, fontweight='bold')
        axes[row_control].set_xlabel(' ', fontsize=16, fontweight='bold')
        axes[row_control].xaxis.set_major_formatter(NullFormatter())
        axes[row_control].set_title(f'{plot_labels[label_count].capitalize()}) {org}', 
            fontweight="bold", fontsize = 17, y =1)
            
        if row_control == len(Species) - 1:
            axes[row_control].set_xlabel('Dimensions', fontsize=20, fontweight='bold')
            axes[row_control].set_xticklabels(dimensions)
            fig.text(0.07, 0.5, 'Correlation', ha='center', va='center', 
                     rotation='vertical', fontsize=20, fontweight = "bold")
            fig.legend(labels= matrix_list,
                   borderaxespad=0.1,
                   bbox_to_anchor=(0.5, 0.08),
                   loc="upper center", frameon=True, ncol = len(matrix_list))            
            
        row_control +=1
        label_count +=1
        
    if "Human" in Species == True:           
        fig.savefig(f'{path_corr}Pearson_Correlation_Human.png', format="png", dpi=600,
                            bbox_inches='tight') 
    else:
        fig.savefig(f'{path_corr}Pearson_Correlation.png', format="png", dpi=600,
                            bbox_inches='tight')      
        
def Plot_Correlation_Human(Species, dimensions, matrix_list):
    
    # Paths:
    
    path_corr = "/media/sergio/sershiosdisk/Axes_Species/Axes_Gene_Correlation/"
    
    # Controlers:
    
    label_count = 0
    
    # Authomatic Labels:
    
    plot_labels = []
    
    for i in range(len(Species) * 3):
        
        plot_labels.append(ascii_lowercase[i])
    
    # Prepare the grid:
        
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
    fig, axes = plt.subplots(len(Species), 1, figsize=(15,5))

    # Start the plotting:
    
    for org in Species:
        
        # Load the information of the functional organization:
        
        Organization  = open(f'{path_corr}Correlation_Axes_Genes_{org}.json' ,)
        Organization = json.load(Organization)
        
        Organization_pd = pd.DataFrame(Organization)
        
        # Error bars:
        
        # Transform count:
        
        transf = 30
                
        for matrix in matrix_list:
            
            info = Organization_pd[Organization_pd.Matrix == matrix].Avg
            std  =  Organization_pd[Organization_pd.Matrix == matrix]["std"]
            
            info = info.reset_index(drop = True)
            std  = std.reset_index(drop = True)
            
            # Transform:
            
            transform = Affine2D().translate(transf, 0.0) + axes.transData
            
            axes.errorbar(dimensions, info,   yerr= Calculate_limits(info, std),  
                marker = "o", capsize=5, lw = 4, fmt = " ", transform = transform)
            
            transf = transf + 12
            
        # Setting for the line plot:
            
        axes.spines['right'].set_visible(False)
        axes.spines['top'].set_visible(False)
        axes.spines['left'].set_linewidth(4)
        axes.spines['bottom'].set_linewidth(4)
        axes.spines['left'].set_color("grey")
        axes.spines['bottom'].set_color("grey") 
        axes.set_ylabel(' ', fontsize=16, fontweight='bold')
        axes.set_xlabel(' ', fontsize=16, fontweight='bold')
        axes.xaxis.set_major_formatter(NullFormatter())
        axes.set_title(f'{plot_labels[label_count].capitalize()}) {org}', 
            fontweight="bold", fontsize = 17, y =1)

        axes.set_xlabel('Dimensions', fontsize=20, fontweight='bold')
        axes.set_xticklabels(dimensions)
        fig.text(0.07, 0.5, 'Correlation', ha='center', va='center', 
                     rotation='vertical', fontsize=20, fontweight = "bold")
        fig.legend(labels= matrix_list,
                   borderaxespad=0.1,
                   bbox_to_anchor=(0.5, 0.08),
                   loc="upper center", frameon=True, ncol = len(matrix_list))            
          
        fig.savefig(f'{path_corr}Pearson_Correlation_Human.png', format="png", dpi=600,
                            bbox_inches='tight') 

        
def Optimal_Dim(Species, comparison_list, matrix_list): 
    
    # Paths:
    
    Optimal_path = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    with open(f'{Optimal_path}Optimal_Dimensions.txt', 'a') as the_file:
        
        the_file.write("# Specie"  + "\t")
        the_file.write("# Matrix"   + "\t")
        the_file.write("# Optimal"   + "\n")
                       
        for org in Species:
            for matrix in matrix_list:
                
                # Load the error:
                
                with open(f'{Optimal_path}Relative_{org}_{matrix}.txt', "r") as fp:
                        error_list = json.load(fp)
                
                # Get the optimal:
                
                dimensions = [i for i in range(len(error_list))]
            
                kn = kneed.KneeLocator(dimensions, error_list, curve='convex', 
                                       direction='decreasing', interp_method='polynomial')
                
                optimal = comparison_list[kn.knee]
                optimal = optimal.split("-")[1]
                
                # Write to the file:
                
                the_file.write(f'{org}\t')
                the_file.write(f'{matrix}\t')
                the_file.write(f'{optimal}\n')
    
    # Close the file:
    
    the_file.close() 

def Human_Optimal_Dim(Species, comparison_list, matrix_list): 
    
    # Paths:
    
    Optimal_path = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    with open(f'{Optimal_path}Human_Optimal_Dimensions.txt', 'a') as the_file:
        
        the_file.write("# Specie"  + "\t")
        the_file.write("# Matrix"   + "\t")
        the_file.write("# Optimal"   + "\n")
                       
        for org in Species:
            for matrix in matrix_list:
                
                # Load the error:
                
                with open(f'{Optimal_path}Relative_{org}_{matrix}.txt', "r") as fp:
                        error_list = json.load(fp)
                
                # Get the optimal:
                
                dimensions = [i for i in range(len(error_list))]
            
                kn = kneed.KneeLocator(dimensions, error_list, curve='convex', 
                                       direction='decreasing', interp_method='polynomial')
                
                optimal = comparison_list[kn.knee]
                optimal = optimal.split("-")[1]
                
                # Write to the file:
                
                the_file.write(f'{org}\t')
                the_file.write(f'{matrix}\t')
                the_file.write(f'{optimal}\n')
    
    # Close the file:
    
    the_file.close() 

    
def Run_Permutations(organism, optimal_dimension, matrix, times):
    
    # Paths:
    
    permutation_path   = "/media/sergio/sershiosdisk/Axes_Species/Permutations/"
    network_path       = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    node2vec_path      = "/media/sergio/sershiosdisk/Axes_Species/Node2vec/"
    Line_path          = "/media/sergio/sershiosdisk/Axes_Species/LINE/"
    Path_Annotation    = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Deep_path          = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    GO_embeddings_path = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"
    
    # Load Genes embeddings:
    
    if matrix == "PPMI":
        path = f'{network_path}_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
        G_sp = np.load(path, allow_pickle=True)
    elif matrix == "Adj":
        path = f'{network_path}_G_Matrix_{optimal_dimension}_PPI_{organism}_{matrix}.npy'
        G_sp = np.load(path, allow_pickle=True)
    elif matrix == "node2vec":
        path = f'{node2vec_path}node2vec_array_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True)
    elif matrix == "LINE":
        path = f'{Line_path}Line_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
    elif matrix == "Deepwalk":
        path = f'{Deep_path}Deepwalk_Bases_{organism}_{optimal_dimension}.npy'
        G_sp = np.load(path, allow_pickle=True).T
        
    # Load the annotations:
    
    GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{organism}.csv', 
                                index_col = 0, dtype={0: str}) 
    GO_Matrix.index = GO_Matrix.index.astype(str)        
    
    genes = list(pd.read_table(f'{network_path}Genes_{organism}_PPI', header = None, dtype={0: str})[0])
        
    GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]  
        
    GO_sp     = GO_Matrix_filt.sum(axis=0)
    filter_sp = set(GO_sp[GO_sp > 0].index)
        
    GO_Matrix_filt = GO_Matrix_filt[filter_sp]
    GO_Matrix_filt = GO_Matrix_filt.loc[genes]
    
    # Load the GO embeddings:
    
    GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_BP_PPI_{organism}_{optimal_dimension}_{matrix}.csv', 
                                index_col = 0)
    GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
    
    # Start the permutations
                                
    Count_db = pd.DataFrame(0, columns=GO_embeddings.columns, index = GO_embeddings.index)

    # Keep the names:
    
    index_orig   = GO_Matrix_filt.index
    columns_orig = GO_Matrix_filt.columns
    
    # Init the permutation test for N times:
       
    for rep in range(times):
        
        # Control de iteration:
       
        if rep%100 == 0:
            print(f'{organism} \ {matrix} \ {rep}')
        
        # Randomly suffle the Gene/GO matrix:
            
        Gene_GO_matrix_it = GO_Matrix_filt.sample(frac=1).reset_index(drop=True)
        Gene_GO_matrix_it.index = index_orig
        Gene_GO_matrix_it.columns = columns_orig
            
        # Do the Embeddings using the suffled Gene/GO matrix (direct calculation):
        
        gene_embeddings_db_inverse = pd.DataFrame(np.linalg.pinv(G_sp) , columns = index_orig)
        GO_emebddings_it = gene_embeddings_db_inverse.dot(Gene_GO_matrix_it)

        # Compare the new scores vs the original scores:
            
        comparison = GO_emebddings_it >= GO_embeddings
            
        # Update the counts:
            
        Count_db = Count_db + comparison
        
    # Finished:
            
    print("the " + str(rep + 1) +  " iterations are finished")
             
    # Calculate the p-values:
    
    Count_db_Final = (Count_db + 1)/(times + 1)
    
    # Save the matrix.
    
    Count_db_Final.to_csv(f'{permutation_path}P_value_{organism}_{optimal_dimension}_{matrix}_{times}.csv', 
                          header = True, index=True)
              
def Run_Permutations_Terminal():
    
    print("Running Permutations")
    
    command = "/home/sergio/Project_2/Scripts/python3 Run_Species_Permutations.py"
    run_process(command)
    
    print("Finished")      
    
    
def Adjust_Pvalues(Species, matrix_list, dimension_list, annotations, alpha = 0.05): 
       
    # Paths:
    
    pvalue_path        = "/media/sergio/sershiosdisk/Axes_Species/Permutations/"
    GO_embeddings_path = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"
    Path_Annotation    = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    network_path       = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    
    # Get the information for the iterations:
    
    for org in Species:
        
        
        # Choose the number of permutations that we used for the permutation test:
        
        if org == "Human":
            times = 100000
        else:
            times = 60000
                                                                           
        # Load the annotations: 
        
        if annotations == "BP_Back":
        
            GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', 
                                    index_col = 0, dtype={0: str}) 
            GO_Matrix.index = GO_Matrix.index.astype(str)      
        
        else:
            
            GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_Leaf_{org}.csv', 
                                    index_col = 0, dtype={0: str}) 
            GO_Matrix.index = GO_Matrix.index.astype(str)             
            
        genes = list(pd.read_table(f'{network_path}Genes_{org}_PPI', header = None, dtype={0: str})[0])
        
        GO_Matrix_filt = GO_Matrix[GO_Matrix.index.isin(genes)]  
        
        GO_sp     = GO_Matrix_filt.sum(axis=0)
        filter_sp = set(GO_sp[GO_sp > 0].index)
        
        GO_Matrix_filt = GO_Matrix_filt[filter_sp]
        GO_Matrix_filt = GO_Matrix_filt.loc[genes]                                                       
                                                                                                           
        for matrix in matrix_list:
            
            # This is for the name of the files:
            
            if matrix == "No_Orth_PPMI":
                tag = "PPMI_No_Orth"
            elif matrix == "No_Orth_Adj":
                tag = "Adj_No_Orth"
            else:
                tag = matrix
                
            # Iterate over the dimensions:
                               
            for optimal_dimension in dimension_list: 
                
                print(f'{matrix} - {org} - {optimal_dimension}-d - {tag}')
                      
                # Load the annotations:                                                  
                                                      
                # Load the GO embeddings:
    
                GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{org}_{optimal_dimension}_{matrix}.csv', 
                                    index_col = 0)
                GO_embeddings = GO_embeddings[GO_Matrix_filt.columns]
                                         
                # Load the p_values:
                
                p_values = pd.read_csv(f'{pvalue_path}P_value_{org}_{optimal_dimension}_{tag}_{annotations}_{times}.csv',
                                       index_col = 0, dtype={0: str}) 
                p_values = p_values[GO_embeddings.columns]                           
                
                # Prepare the empty matrix:
                
                P_adj_Matrix = pd.DataFrame(0, columns = p_values.columns, index = p_values.index)
                
                for GO in p_values.columns:
                    
                    p_values_list = p_values[GO]
                    
                    p_values_list_corrected = multipletests(p_values_list.values, alpha=alpha, 
                                                             method='fdr_bh', is_sorted=False, returnsorted=False)
                     
                    P_adj_Matrix[GO] = p_values_list_corrected[1]
                    
                P_adj_Matrix.to_csv(f'{pvalue_path}Adjust_P_value_{org}_{optimal_dimension}_{matrix}_{annotations}_{times}.csv', header = True, index=True) 
                    
                           
def Associate_Annotations_Axes(Species, matrix_list, dimensions, annotations, alpha = 0.05):          
                                      
    # Paths:
    
    pvalue_path  =  "/media/sergio/sershiosdisk/Axes_Species/Permutations/"
    Axes_path    =  "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"                                             

    # Get the list of comparisons:
    
    for org in Species:
        
        if org == "Human":
            times = 100000
        else:
            times = 60000
                                            
        for matrix in matrix_list:
            
            for optimal_dimension in dimensions: 
                                                                                           
            # Load the GO embeddings:
                                                                  
                pvalues = pd.read_csv(f'{pvalue_path}Adjust_P_value_{org}_{optimal_dimension}_{matrix}_{annotations}_{times}.csv', 
                                      index_col=0) 
                
                # Init the dictionaries:
                
                pvalue_dic = dict.fromkeys(list(pvalues.index))
                
                for dim in pvalues.index:
                    
                    list_Assoc = list(pvalues.columns[pvalues.loc[dim] <= alpha])
                     
                    if not list_Assoc:
                        pvalue_dic[dim] = []
                    else:
                        pvalue_dic[dim] = list_Assoc
                        
                # Save the dictionaries:
    
                json_1 = json.dumps(pvalue_dic)
                f      = open(f'{Axes_path}Associations_{org}_{matrix}_{annotations}_{optimal_dimension}.json', 'w')
                f.write(json_1)
                f.close()

def Associate_Annotations_Axes_Hard(Species, matrix_list, dimensions, annotations, alpha = 0.05):
    
    # Paths:
    
    pvalue_path        =  "/media/sergio/sershiosdisk/Axes_Species/Permutations/"
    Axes_path          =  "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"  
    GO_embeddings_path =  "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"                                        

    # Get the list of comparisons:
    
    for org in Species:
        
        if org == "Human":
            times = 100000
        else:
            times = 60000
                                            
        for matrix in matrix_list:
            
            print(f'{org} - {matrix}')
            
            for optimal_dimension in dimensions: 
                                                                                           
            # Load the GO embeddings and the p_values:
                                                                  
                pvalues = pd.read_csv(f'{pvalue_path}Adjust_P_value_{org}_{optimal_dimension}_{matrix}_{annotations}_{times}.csv', 
                                      index_col=0) 
                
                GO_embeddings = pd.read_csv(f'{GO_embeddings_path}_GO_Embeddings_{annotations}_PPI_{org}_{optimal_dimension}_{matrix}.csv', 
                                index_col = 0)
                
                GO_embeddings = GO_embeddings[pvalues.columns] 
                GO_embeddings[GO_embeddings < 0] = 0
                
                # Init the empty dictionary of associacions:
                        
                pvalue_dic = dict.fromkeys(list(pvalues.index))
                for i in pvalue_dic.keys():
                    pvalue_dic[i] = []
                
                # Fill the dictionary:
                                    
                for GO in GO_embeddings.columns:
                    
                    # Subset P-values and select those that are signficant:
                    
                    GO_pvalues = pvalues[GO] 
                    GO_pvalues = GO_pvalues[GO_pvalues <= 0.05] 
                    
                    # If at least one p-value is signidicant:
                    
                    if len(GO_pvalues) > 0:
                    
                        # Get the embedding scores of those entries that are significant:
                               
                        GO_emb = GO_embeddings[GO] 
                        GO_emb = GO_emb[GO_pvalues.index]
                        GO_emb = GO_emb[GO_emb == max(GO_emb)]
                        
                        pvalue_dic[GO_emb.index[0]].extend([GO])
                    
                    else:
                        continue
                
                json_1 = json.dumps(pvalue_dic)
                f      = open(f'{Axes_path}Associations_{org}_{matrix}_{annotations}_{optimal_dimension}_Hard.json', 'w')
                f.write(json_1)
                f.close()
                                   

def Associate_Annotations_Axes_Global(Species, matrix_list, alpha = 0.05):          
                                      
    # Paths:
    
    pvalue_human_path  =  "/media/sergio/sershiosdisk/Human/Axes/P_Values/"
    Axes_path          =  "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"                                               

    # Get the list of comparisons:
    
    for matrix in matrix_list:
        for org in Species:
            if org == "Human":            
                dimensions = [48, 96, 144, 192, 240, 288, 500, 800, 1000]            
                for dim in dimensions:
                    if dim < 500:            
                        path = f'{pvalue_human_path}80000/Adjusted_P_value_PPI_{matrix}_{dim}.csv'
                    else:
                        path = f'{pvalue_human_path}100000/Adjusted_P_value_PPI_{matrix}_{dim}.csv'
                    
                    # Read p-value:
                    
                    pvalues = pd.read_csv(f'{path}', index_col=0) 
                    
                    # Init the dictionaries:
                    
                    pvalue_dic = dict.fromkeys(list(pvalues.index))
                               
                    for dim1 in pvalues.index:
                        
                        list_Assoc = list(pvalues.columns[pvalues.loc[dim1] <= alpha])
                         
                        if not list_Assoc:
                            pvalue_dic[dim1] = []
                        else:
                            pvalue_dic[dim1] = list_Assoc
                        
                    # Save the dictionaries:
                    
                    json_1 = json.dumps(pvalue_dic)
                    f      = open(f'{Axes_path}Associations_{org}_{matrix}_{dim}.json', 'w')
                    f.write(json_1)
                    f.close()
                    
def Jaccar_index(cluster1, cluster2):
    intersection = len(list(set(cluster1).intersection(cluster2)))
    union = (len(cluster1) + len(cluster2)) - intersection
    return float(intersection) / union         
  
def Calculate_Jaccard(Species, matrix_list):
    
    # Paths:
    
    Asso_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    for org in Species:
        
        Final_Jaccard = []
        Final_numbers = []
          
        for matrix in matrix_list:
            
            # Load the dictionaries:
            
            Associations = json.load(open(f'{Asso_path}Associations_{org}_{matrix}.json'))
            
            # Calculate the number and Jaccard index:
            
            Associations = {k: v for k, v in Associations.items() if v}
            
            number = [len(Associations[i]) for i in Associations.keys()]     
        
            Results_Jaccard         = pd.DataFrame(0, index=np.arange(len(Associations)), 
                                                   columns=np.arange(len(Associations)))             
            Results_Jaccard.index   = Associations.keys()
            Results_Jaccard.columns = Associations.keys()
            
            # Iterate per each cluster:
            
            for cluster in list(Associations.keys()):
                
                # Get the first axis:
                
                cluster_it_1 = Associations[cluster]
                
                for cluster_2 in list(Associations.keys()):
                    
                    # Get the second axis:
                    
                    cluster_it_2 = Associations[cluster_2]
                    
                    # Calculate the jaccard index:
                    
                    jaccard = Jaccar_index(cluster_it_1, cluster_it_2)
                    
                    # Save the results in an external variable:
                    
                    Results_Jaccard.loc[cluster,cluster_2] = jaccard   
            
            # Get the diagonal mean:
        
            Results_Jaccard = Results_Jaccard.where(np.triu(np.ones(Results_Jaccard.shape)).astype(np.bool))  
            Results_Jaccard = Results_Jaccard.stack().reset_index()
            Results_Jaccard = Results_Jaccard[Results_Jaccard.level_0 != Results_Jaccard.level_1]
            
            # Upload the result and Jaccard lists:
            
            Final_Jaccard.append(list(Results_Jaccard[0]))
            Final_numbers.append(list(number))
        
        # Compare the Jaccard indexes of the species:
        
        Final_pd  = pd.DataFrame(0, index=np.arange(len(matrix_list)), 
                                         columns=matrix_list)
        Final_pd.index = matrix_list 
        
        for i in range(len(Final_Jaccard)):
            corr_1 = Final_Jaccard[i]
            for j in range(len(Final_Jaccard)):
                corr_2 = Final_Jaccard[j]
                P_value2 = round(mannwhitneyu(corr_1, corr_2, alternative = "less").pvalue,4)
                Final_pd.iloc[i, j] = P_value2
        
        # Add the new column:
        
        Final_numbers = [np.mean(i) for i in Final_numbers]
        
        Final_pd["Number"] = Final_numbers
        
        # Save the output:
        
        Final_pd.to_csv(f'{Asso_path}Jaccard_Correlations_{org}.txt') 
        
def Plot_Paper_Fig_1(Species, dimensions, matrix_list, number_similar = 500, low_dim = 48, high_dim = 1000):
    
    # Paths:
    
    path_func        = "/media/sergio/sershiosdisk/Axes_Species/Functional_Organization/"
    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Path_Annotation  = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    # Controlers:
    
    row_control = 0
    label_count = 0
    
    # Authomatic Labels:
    
    plot_labels = []
    
    for i in range(len(matrix_list) * 3):
        
        plot_labels.append(ascii_lowercase[i])
    
    # Prepare the grid:
        
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
    if "node2vec" in matrix_list:
        fig, axes = plt.subplots(len(matrix_list), 4, figsize=(15,13), gridspec_kw={"width_ratios": [2.5,1,1,0.5]})
    else:
        fig, axes = plt.subplots(len(matrix_list), 4, figsize=(15,8), gridspec_kw={"width_ratios": [2.5,1,1,0.5]})
    
    fig.tight_layout(pad = 0.5)
    fig.text(0., 0.5, 'Semantic Similarity', ha='center', va='center', 
              rotation='vertical', fontsize=17, fontweight = "bold")
    
    # Start the plotting:
    
    for org in Species:
        
        label_2 = []
        for matrix in matrix_list:
            
            # For the labels (supplementary or main)
            
            if "Adj" in matrix_list:
                label_2.append(matrix)
            else:
                if matrix == "PPMI":
                    label_2.append("NMTF")
                else:
                    label_2.append(matrix)
 
        # Load the information of the functional organization:
        
            Organization  = open(f'{path_func}Similarity_{org}_{matrix}.json' ,)
            Organization = json.load(Organization)
            
            # Delete dimension 288 for visualization
            
            del Organization["Similar"][5]
            del Organization["Similar_std"][5]
            del Organization["Disimilar"][5]
            del Organization["Disimilar_std"][5]
            
            # Start with the line plots (row is Specie):
            
            # Error bars:
            
            l3 = axes[row_control,0].errorbar(dimensions, Organization["Similar"],   yerr= Calculate_limits(Organization["Similar"], Organization["Similar_std"]),  
                marker = "o", capsize=5, lw = 3, color = "#76de61")
            axes[row_control,0].errorbar(dimensions, Organization["Disimilar"],  yerr= Calculate_limits(Organization["Similar"], Organization["Disimilar_std"]),  
                marker = "o", capsize=5, lw = 3, color = "#e5658a")
            
            # Generic setting for all error bars:
        
            axes[row_control,0].set_xticks(dimensions)
            axes[row_control,0].xaxis.set_major_formatter(NullFormatter())
            axes[row_control,0].spines['right'].set_visible(False)
            axes[row_control,0].spines['top'].set_visible(False)
            axes[row_control,0].spines['left'].set_linewidth(4)
            axes[row_control,0].spines['bottom'].set_linewidth(4)
            axes[row_control,0].spines['left'].set_color("grey")
            axes[row_control,0].spines['bottom'].set_color("grey")  
            axes[row_control,0].set_title(f'{plot_labels[label_count].capitalize()}) {label_2}', 
                fontweight="bold", fontsize = 17, y = 1) 
            label_count += 1
            
            # Setting for specific error bars:
            
            if row_control == len(matrix_list) - 1:
                axes[row_control,0].set_xticks(dimensions)
                axes[row_control,0].get_xticklabels()[dimensions.index(low_dim)].set_color("red")
                axes[row_control,0].get_xticklabels()[dimensions.index(high_dim)].set_color("red")
                axes[row_control,0].set_xlabel("Dimensions",  fontsize=17, fontweight = "bold")
                axes[row_control,0].set_xticklabels(dimensions, rotation=45, fontweight='bold', fontsize=15)
                
                # Legend in the last iteration:
                
                fig.legend(l3[0], labels= ["Similar","Dissimilar"],
                           borderaxespad=0.1,
                           bbox_to_anchor=(0.25, -0.12),
                           loc="lower center", frameon=True, ncol = 3)
            
            # Color maps:
            
            # Load the cos-cos matrix:
            
            cos_low  = pd.read_csv(f'{save_cosine}Cosine_{org}_{low_dim}_{matrix}.csv',  index_col = 0) 
            cos_high = pd.read_csv(f'{save_cosine}Cosine_{org}_{high_dim}_{matrix}.csv', index_col = 0) 
            
            # Filter the cosines:
            
            BP = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
            
            annotation_list = [name for sublist in BP.values() for name in sublist]
            occurance_of_each_annotation_in_network = Counter(annotation_list)
            terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
            
            cos_low  =  cos_low.loc[terms_filtering, terms_filtering]  
            cos_high =  cos_high.loc[terms_filtering, terms_filtering]  
            
            # Average distance:
            
            cos_low_avg   = np.mean(list(cos_low.values[np.triu_indices(len(cos_low),   k = 1)]))
            cos_high_avg  = np.mean(list(cos_high.values[np.triu_indices(len(cos_high), k = 1)]))
            
            # Plot the low dimensional space:
            
            anchored_text = AnchoredText(f'avg: {round(cos_low_avg,2)}' , loc=3)
            im = axes[row_control,1].matshow(cos_low.values, interpolation='nearest', cmap="jet", vmin=0, vmax=1)
            axes[row_control,1].set_title(f'{plot_labels[label_count].capitalize()}) {low_dim}-d {label_2}', fontweight="bold", 
                fontsize = 17, y =1)
            axes[row_control,1].add_artist(anchored_text)  
            axes[row_control,1].axis('off')  
            label_count += 1
            
            # Plot the high dimensional space:
        
            anchored_text = AnchoredText(f'avg: {round(cos_high_avg,2)}' , loc=3)
            im = axes[row_control,2].matshow(cos_high.values, interpolation='nearest', cmap="jet", vmin=0, vmax=1)
            axes[row_control,2].set_title(f'{plot_labels[label_count].capitalize()}) {high_dim}-d {label_2}', fontweight="bold", fontsize = 17, y =1)
            axes[row_control,2].add_artist(anchored_text)  
            axes[row_control,2].axis('off') 
            label_count += 1
            
            # Specific conditions for the color maps:
            
            if row_control != 1:
                axes[row_control,3].axis('off')
            else:
                ip = InsetPosition(axes[row_control,2], [1.1, 0, 0.05, 1])  
                axes[row_control,3].set_axes_locator(ip)
                
                if "Adj" in matrix_list:
                
                    fig.colorbar(im, cax=axes[row_control,3], ax=[axes[0,1],axes[0,2],
                                              axes[1,1], axes[1,2]]) 
                elif "node2vec" in matrix_list:
                    fig.colorbar(im, cax=axes[row_control,3], ax=[axes[0,1],axes[0,2],
                                              axes[1,1], axes[1,2], axes[2,1],axes[2,2],axes[3,1],axes[3,2]]) 
                else:
                    fig.colorbar(im, cax=axes[row_control,3], ax=[axes[0,1],axes[0,2],
                                              axes[1,1], axes[1,2], axes[2,1],axes[2,2]])                     
                                 
            row_control += 1
        

    # Save the plot:
    
    if "Adj" in matrix_list:
    
        fig.savefig(f'{path_func}Supp_Human_plot1.png', format="png", dpi=600,
                        bbox_inches='tight') 
    elif "node2vec" in matrix_list:
        fig.savefig(f'{path_func}Supp_node2vec_Human_plot1.png', format="png", dpi=600,
                        bbox_inches='tight')          
    else:        
        fig.savefig(f'{path_func}Human_plot1.png', format="png", dpi=600,
                        bbox_inches='tight')             
            
def Plot_Paper_Fig_2(Species, comparison_list, matrix_list):

    # Path:
    
    optimal_path  = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    # Controllers:
    
    row_count    = 0
    label_count  = 0
    
    # Authomatic Labels:
    
    plot_labels = []
    
    for i in range(len(Species) * 3):
        
        plot_labels.append(ascii_lowercase[i])
    
    # Set the plot characteristics:
    
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 15})
    plt.tight_layout()
    fig, axs = plt.subplots(len(Species), 1, figsize=(15,5)) 
    
    # Reference to the Plots:
    
    for org in Species:
        
        lables = []
        
        # Load the Relative error for each matrix selected:
        
        for matrix in matrix_list:
            with open(f'{optimal_path}Relative_{org}_{matrix}.txt', "r") as fp:
                    Error_info = json.load(fp)
            
            if "Adj" in matrix_list:
                lables.append(matrix)
            else:
                if matrix == "PPMI":
                    lables.append("NMTF")
                else:
                    lables.append(matrix)
                
            # Plot the line for each matrix:

            axs.plot(comparison_list, Error_info, marker='o', label=f'{org} {matrix}')  

        # Settings for the first complex line plot:

        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey") 
        axs.set_ylabel(' ', fontsize=16, fontweight='bold')
        axs.set_xlabel(' ', fontsize=16, fontweight='bold')
        axs.xaxis.set_major_formatter(NullFormatter())
        
        if row_count == len(Species) - 1:
            axs.set_xlabel('Dimensions', fontsize=20, fontweight='bold')
            axs.set_xticklabels(comparison_list)
            fig.text(0.07, 0.5, 'Relative Error', ha='center', va='center', 
                     rotation='vertical', fontsize=20, fontweight = "bold")
            fig.legend(labels= lables,
                   borderaxespad=0.1,
                   bbox_to_anchor=(0.5, 0.0),
                   loc="upper center", frameon=True, ncol = len(matrix_list))
        row_count   +=1
        label_count +=1
        
    if "Adj" in matrix_list:
        fig.savefig(f'{optimal_path}Supp_Adj_Human_plot2.png', format="png", dpi=600,
                            bbox_inches='tight')   
    elif "node2vec" in matrix_list:
        fig.savefig(f'{optimal_path}Supp_node2vec_Human_plot2.png', format="png", dpi=600,
                            bbox_inches='tight') 
    else:
        fig.savefig(f'{optimal_path}Human_plot2.png', format="png", dpi=600,
                            bbox_inches='tight')             
            
                     
def Calculate_Similarity_matrix(Species): 

    # Paths:

    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Semantic   = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    
    # For each specie:

    for org in Species:
        
        print(org)
        
        # Load the GO/genes matrix for the specie:
        
        GO_Matrix = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', 
                                index_col = 0, dtype={0: str}) 
        GO_Matrix.index = GO_Matrix.index.astype(str)
        
        # Take the list of the GO to compare:
        
        GO_1 = list(GO_Matrix.columns)
        
        # Do an empty semantic matrix:
        
        Semantic_db       = pd.DataFrame(0, index=np.arange(len(GO_1)), columns=GO_1)
        Semantic_db.index = GO_1
        
        # Calculate the Semantic Similarity:
        
        G = graph.from_resource("go-basic")
        similarity.precalc_lower_bounds(G)
        
        counter = len(Semantic_db)
        control = 0
        
        for i in range(len(Semantic_db)):
            for j in range(i+1, len(Semantic_db)):
                
                try:
                    distance = similarity.lin(G, GO_1[i], GO_1[j]) 
                    
                    Semantic_db.loc[GO_1[i],GO_1[j]] = distance 
                    Semantic_db.loc[GO_1[j],GO_1[i]] = distance
                                     
                except Exception as PGSSLookupError:
                        continue  
                
                control+=1
                
                if control%1000000 == 0:
                    print(f'{control}/{counter*counter/2}')
                
        # Save the corresponding matrix:
        
        Semantic_db.to_csv(f'{Path_Semantic}Semantic_{org}.csv')
        

def Calculate_Correlations(Species, matrix_list):
        
    # Paths:

    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Semantic   = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    for org in Species:
        
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 300, 400, 500, 600, 700, 800, 900, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]
        
        # Load the Semantic Similarity:
        
        Semantic = pd.read_csv(f'{Path_Semantic}Semantic_{org}.csv', index_col = 0)
        
        with open(f'{Path_Semantic}Correlation_{org}.txt', 'a') as the_file:
            
            # Do the column names:
            
            the_file.write("# Embedding"  + "\t")
            
            for dim in dimensions: 
            
                the_file.write(f'{dim}-D'  + "\t")
            
            the_file.write("\n") 
               
            for matrix in matrix_list:
                                
                # Name of the embedding:
                    
                the_file.write(f'{matrix}'  + "\t")
                    
                # Load the cosine distance:
                
                x = []
                
                    
                for dim in dimensions:
                    
                    cosine = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{dim}_{matrix}.csv',  index_col = 0)
                        
                    # Filter the cosine:
                        
                    BP              = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
                    annotation_list = [name for sublist in BP.values() for name in sublist]
                        
                    occurance_of_each_annotation_in_network = Counter(annotation_list)
                    terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
                    
                    cosine    =  cosine.loc[terms_filtering, terms_filtering]                     
                    Semantic  =  Semantic.loc[terms_filtering, terms_filtering] 
                        
                    # Get the two lists to Pearson, for cosine we get the similarity:
                    
                    cosine = 1 -cosine
                        
                    Semantic_pearson = np.array(Semantic)[np.triu_indices(np.array(Semantic).shape[0], k = 1)]
                    cosine_pearson   = np.array(cosine)[np.triu_indices(np.array(cosine).shape[0], k = 1)]
                    
                    prueba       = pd.DataFrame()
                    prueba["SS"] = Semantic_pearson
                    prueba["PP"] = cosine_pearson
                    
                    prueba["SS"] = prueba["SS"].round(1)  
                    prueba = prueba.sort_values("SS")
                    prueba["SS"] = prueba["SS"].astype(str)
                    
                    x = list(set(prueba["SS"]))
                    x.sort()
                    
                    mean = []
                    std  = []
                    
                    for i in x:
                        count = prueba[prueba["SS"] == i]
                        mean.append(np.mean(count["PP"]))
                        std.append(np.std(count["PP"]))


                    
                    print(f'{round(result[0],3)} {result[1]}' )
                    
                    the_file.write(f'{round(result[0],3)} {result[1]}'  + "\t")
                
                # Next line:
                
                the_file.write("\n")
                
        the_file.close()


def Calculate_limits_GOGO(list_mean, list_std):
    
    lower_error = []
    upper_error = []
    
    for i in range(len(list_mean)):
        
        lower = list_mean[i] - list_std[i]
        upper = list_mean[i] + list_std[i]
        
        if lower < 0:
            lower = list_mean[i]
        else:
            lower = list_std[i] 
        
        lower_error.append(lower)
        upper_error.append(upper)
    
    result = [lower_error, upper_error]
    
    return(result)


def Calculate_Clusters_GOGO(Species, matrix_list): 
    
    # Paths:

    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Semantic   = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    for matrix in matrix_list:
    
        for org in Species:
            
            print(f'{matrix}-{org}')
            
            if org == "Human":
                dimensions = [48, 96, 144, 192, 240, 288, 300, 400, 500, 600, 700, 800, 900, 1000]
            else:
                dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]
            
            # Load the Semantic Similarity:
            
            Semantic = pd.read_csv(f'{Path_Semantic}Semantic_{org}.csv', index_col = 0) 
            
            # Set the dictionary:
            
            keys = []
            
            for dm in dimensions:
                
                keys.append(f'{dm}_Inter')
                keys.append(f'{dm}_Intra')
            
            Result = dict(([(key, []) for key in keys]))
            
            # Iterate by dimensions:
            
            for dim in dimensions:
            
                    cosine = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{dim}_{matrix}.csv',  index_col = 0)
                
                    # Filter the cosine:
                                
                    BP              = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
                    annotation_list = [name for sublist in BP.values() for name in sublist]
                    
                    occurance_of_each_annotation_in_network = Counter(annotation_list)
                    terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
                            
                    cosine    =  cosine.loc[terms_filtering, terms_filtering]                     
                    Semantic  =  Semantic.loc[terms_filtering, terms_filtering] 
                    
                    # Choose the number of clusters and apply k-medoids:
                    
                    n_clusters = round(math.sqrt(len(cosine.index)/2))
                    grouping   = KMedoids(n_clusters=n_clusters, metric='precomputed',).fit(cosine)
                    Cluster_db = pd.DataFrame(cosine.index)
                    Cluster_db.columns    = ["GO"]
                    Cluster_db["Cluster"] = grouping.labels_
                    
                    intra_distance = []
                    inter_distance = []
                    
                    # Intra cluster semantic similarity:
                    
                    for cluster in list(set(Cluster_db.Cluster)):
                        
                        # Get the intra cluster distance:
                        
                        GO_cluster_1 = Cluster_db[Cluster_db.Cluster == cluster].GO                    
                        Semantic_1   = Semantic.loc[GO_cluster_1, GO_cluster_1]
                        Semantic_1   = np.array(Semantic_1)[np.triu_indices(np.array(Semantic_1).shape[0], k = 1)]
                        intra_distance.append(round(np.mean(Semantic_1),3))
                    
                    # Inter cluster semantic similarity:
                    
                    lista = list(set(Cluster_db.Cluster))
                    
                    for i in range(len(lista)):
                        
                        cluster_1    = lista[i]
                        GO_cluster_1 = Cluster_db[Cluster_db.Cluster == cluster_1].GO 
                        
                        for j in range(i+1, len(lista)):
                            
                            cluster_2    = lista[j]
                            GO_cluster_2 = Cluster_db[Cluster_db.Cluster == cluster_2].GO 
                            
                            # Take the semantic similarity:
                            
                            Semantic_1 = Semantic.loc[GO_cluster_1, GO_cluster_2]
                            inter_distance.append(round(Semantic_1.mean().mean(),3))
                
                    # Add the info to the list:
                    
                    Result[f'{dim}_Inter'] = inter_distance
                    Result[f'{dim}_Intra'] = intra_distance
                    
            sa = json.dumps(Result)
            f = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}.json' ,"w")
            f.write(sa)

            f.close()

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


def Fold_Intra_Inter_Cluster(Species, matrix_list):

    # Paths:

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    for org in Species:           
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 300, 400, 500, 600, 700, 800, 900, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]  
            
        with open(f'{Path_Clust}Folds_Intra_Inter_{org}.txt', 'a') as the_file:
            
            
            # Write the columns:
            
            for dim in dimensions:
                the_file.write(f'{dim}-D\t')
            the_file.write(f'{dim}-D\n')
               
            # Get the information for each sub-plot:
                
            # Load the dictionary:
                            
            file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_PPMI.json', "r") 
                
            info  = json.load(file)
            file.close()
                
            # Generate the lists to plot:
                
            for dim in dimensions:
                    
                # Build the key:
                    
                key1 = f'{dim}_Inter'
                key2 = f'{dim}_Intra'
                    
                Inter_SS = info[key1]
                Intra_SS = info[key2]
            
                the_file.write(f'{np.nanmean(Intra_SS)/np.nanmean(Inter_SS)}\t')           
        the_file.close()
                


def Plot_Intra_Inter(Species, matrix_list):
    
    # Paths:

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    for org in Species:           
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 300, 400, 500, 600, 700, 800, 900, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]        
        
        # Start the plot:
        
        row_count   = 0 
        label_count = 0
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 17})
        plt.rc('font', weight='bold')
            
        fig, axes = plt.subplots(len(matrix_list), 1, figsize=(14,5))
        fig.tight_layout(pad = 0.5)
        
        fig.text(-0.001, 0.5, 'Semantic Similarity', ha='center', va='center', 
                 rotation='vertical', fontsize=17, fontweight = "bold")  
        
        fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=17, fontweight = "bold")

        plot_labels = []
    
        for i in range(len(matrix_list) * 2):       
            plot_labels.append(ascii_lowercase[i])        
        
        for matrix in matrix_list:
            
            if matrix == "PPMI":
                lab = "NMTF"
            else:
                lab = matrix
            
            # Get the information for each sub-plot:
            
            # Load the dictionary:
                        
            file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}.json', "r") 
            
            info  = json.load(file)
            file.close()
            
            list_intra = []
            list_inter = []
            
            # Generate the lists to plot:
            
            for dim in dimensions:
                
                # Build the key:
                
                key1 = f'{dim}_Inter'
                key2 = f'{dim}_Intra'
                
                Inter_SS = info[key1]
                Intra_SS = info[key2] 
                
                Intra_SS = [x for x in Intra_SS if str(x) != 'nan']
                Inter_SS = [x for x in Inter_SS if str(x) != 'nan']
                
                list_intra.append(Intra_SS)
                list_inter.append(Inter_SS)
                              
                # Test the distributions:
                
                p_value = mannwhitneyu(Inter_SS, Intra_SS, alternative = "less").pvalue
                print(p_value)
            
            # continue with the plot:
            
            bpl = axes[row_count].boxplot(list_inter, positions=np.array(range(len(list_inter)))*2.0-0.4, sym='', widths=0.6)
            bpr = axes[row_count].boxplot(list_intra, positions=np.array(range(len(list_intra)))*2.0+0.4, sym='', widths=0.6)
            axes[row_count].xaxis.set_major_formatter(NullFormatter())
            
            set_box_color(bpl, '#D7191C')
            set_box_color(bpr, '#2C7BB6')
                          
            axes[row_count].spines['right'].set_visible(False)
            axes[row_count].spines['top'].set_visible(False)
            axes[row_count].spines['left'].set_linewidth(4)
            axes[row_count].spines['bottom'].set_linewidth(4)
            axes[row_count].spines['left'].set_color("grey")
            axes[row_count].spines['bottom'].set_color("grey")  
            axes[row_count].set_title(f'{plot_labels[label_count].capitalize()}) {lab}', 
                fontweight="bold", fontsize = 17, y = 1) 
            
            if row_count == len(matrix_list)-1:
                
                # Prepare the legends:
                
                patch_list = []
                color      = ['#D7191C', '#2C7BB6']
                label      = ["Inter", "Intra"]
        
                for patch in range(2):           
                    patch_list.append(mpatches.Patch(color= color[patch], label= label[patch]))
                
                # Plot the legend:
    
                plt.legend(handles=patch_list,borderaxespad=0.1,
                   bbox_to_anchor=(0.5, -1),
                       loc="lower center", frameon=True, fontsize=17, ncol =2)           
            
            label_count+=1           
            row_count +=1
            
        fig.savefig(f'{Path_Clust}Intra_Inter_SS_{org}.png', format="png", dpi=600,
                            bbox_inches='tight') 
            
 






        
            
            

      
           
            
            
            
            
            
            
            
            
            
            
        
                        
        
def Calculate_Correlations_Taxons_GOGO(Species, matrix_list, dimensions, distance = "cosine"):  

    # Paths:

    Path_Annotation   = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Cosine       = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Path_Correlations = "/media/sergio/sershiosdisk/Axes_Species/Correlations_Taxons/"
    Path_embedding    = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/" 
    
    # For the specie and the dimensions:
    
    for matrix in matrix_list:

        for org in Species:
            
            print(f'{matrix}-{dimensions}-{org}')
                        
            # Load the Taxons:
            
            Taxons_Dic      = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))                    
            counts          = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
            GO              = list(Taxons_Dic.keys())                    
            counts_db       = pd.DataFrame({"GO" : GO, "Count" : counts})
            counts_db.index = counts_db.GO 
            
            # Load the cosine or compute euclidean:
            
            if distance == "cosine":           
                cosine = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{dimensions}_{matrix}.csv',  index_col = 0)
            else:
                embeddings = pd.read_csv(f'{Path_embedding}_GO_Embeddings_BP_Back_PPI_{org}_{dimensions}_{matrix}.csv',  index_col = 0).T 
                
                embeddings[embeddings < 0] = 0
                embeddings_index           = embeddings.index
                embeddings                 = np.array(embeddings)
                cosine_embeddings          = pairwise_distances(embeddings, metric="euclidean")
                cosine                     = pd.DataFrame(cosine_embeddings,embeddings_index,embeddings_index)
            
            # Filter the distance:
                            
            BP              = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
            annotation_list = [name for sublist in BP.values() for name in sublist]
                            
            occurance_of_each_annotation_in_network = Counter(annotation_list)
            terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
                        
            cosine    =  cosine.loc[terms_filtering, terms_filtering]                     
                          
            # Filter the taxons and the cosine:
                                           
            common        = list(set(counts_db.GO).intersection(cosine.index))                    
            cosine        =  cosine[common]
            counts_db     =  counts_db.loc[common] 
            
            # Start the dictionary:
            
            keys    = list(set(counts_db.Count))
            Result = dict(([(key, []) for key in keys]))
    
            # Iterate over the taxons:
            
            for taxons in list(set(counts_db.Count)):
                
                # Subset the GOGO matrix:
                
                taxon_sub = counts_db[counts_db.Count == taxons].GO
                cosine_it = cosine.loc[taxon_sub]
                
                # Get the upper diagonal:
                
                cosine_it = np.array(cosine_it)[np.triu_indices(np.array(cosine_it).shape[0], k = 1)]
                
                # Save the mean and the distribution un the dictionary:
                
                mean           = np.mean(cosine_it)
                std            = np.std(cosine_it)
                Result[taxons] = [mean, std]
            
            # Save the dictionary:
            
            sa = json.dumps(Result)
            f = open(f'{Path_Correlations}Distance_GOGO_Taxons_{org}_{matrix}_{distance}.json' ,"w")
            f.write(sa)
            f.close()
            
def Plot_Correlation_Taxons_GOGO(Species, matrix_list):
    
    # Paths:
    
    Correlation_path = "/media/sergio/sershiosdisk/Axes_Species/Correlations_Taxons/"
    save_path = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    for org in Species:
    
        for matrix in matrix_list:
            
            # Set the plot:
        
            plt.style.use("seaborn-whitegrid")
            plt.rcParams.update({'font.size': 17})
            plt.rc('font', weight='bold')
        
            fig, axes = plt.subplots(1, 1, figsize=(8, 4))
            fig.tight_layout(pad = 0.5)
            
            # Load the dictionary:
            
            #file  = open(f'{Correlation_path}Distance_GOGO_Taxons_{org}_{matrix}_cosine.json',    "r")
            file2 = open(f'{Correlation_path}Distance_GOGO_Taxons_{org}_{matrix}_euclidean.json', "r")
            
            #dic_cosine    = json.load(file)
            dic_euclidean = json.load(file2)
            
            #file.close()
            file2.close()
            
            # Set the error bars:
            
            taxons = list(dic_euclidean.keys())
            
            #cosine_means    = [dic_cosine[key][0] for key in dic_cosine.keys()]
            #cosine_std      = [dic_cosine[key][1] for key in dic_cosine.keys()]
            euclidean_means = [dic_euclidean[key][0] for key in dic_euclidean.keys()]
            euclidean_std   = [dic_euclidean[key][1] for key in dic_euclidean.keys()]
            
            #axes[row_control,0].errorbar(taxons, cosine_means,   yerr= Calculate_limits(cosine_means, cosine_std),  
            #marker = "o", capsize=5, lw = 3, color = "#76de61")
            axes.errorbar(taxons, euclidean_means,   yerr= Calculate_limits_GOGO(euclidean_means, euclidean_std),  
            marker = "o", capsize=5, lw = 3, color = "#e5658a")
            
            # Set the setting for all error bars:
        
            #axes[row_control,0].set_xticks(taxons)
            #axes[row_control,0].xaxis.set_major_formatter(NullFormatter())
            #axes[row_control,0].spines['right'].set_visible(False)
            #axes[row_control,0].spines['top'].set_visible(False)
            #axes[row_control,0].spines['left'].set_linewidth(4)
            #axes[row_control,0].spines['bottom'].set_linewidth(4)
            #axes[row_control,0].spines['left'].set_color("grey")
            #axes[row_control,0].spines['bottom'].set_color("grey")  
            #axes[row_control,0].set_title(f'{plot_labels[label_count].capitalize()}', 
            #    fontweight="bold", fontsize = 17, y = 1) 
            #label_count += 1 
            
            axes.set_xlabel("Conservation degree", fontsize=20, fontweight='bold')
            axes.set_ylabel("Pariwise euclidean distance", fontsize=20, fontweight='bold')

            axes.set_xticks(taxons)
            axes.spines['right'].set_visible(False)
            axes.spines['top'].set_visible(False)
            axes.spines['left'].set_linewidth(4)
            axes.spines['bottom'].set_linewidth(4)
            axes.spines['left'].set_color("grey")
            axes.spines['bottom'].set_color("grey")  
            
            # Save the figure:
        
            fig.savefig(f'{save_path}Euclidean_Taxons_{org}_{matrix}.png', format="png", dpi=600,
                        bbox_inches='tight') 

                  
def Calculate_Correlations_Taxons_Embeddings(Species, matrix_list):
        
    # Paths:

    Path_Annotation   = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_embedding    = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"
    Path_Correlations = "/media/sergio/sershiosdisk/Axes_Species/Correlations_Taxons/"
    
    for org in Species:
        
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 300, 400, 500, 600, 700, 800, 900, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]
        
        # Load the Taxons:
        
        Taxons_Dic      = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))                    
        counts          = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
        GO              = list(Taxons_Dic.keys())                    
        counts_db       = pd.DataFrame({"GO" : GO, "Count" : counts})
        counts_db.index = counts_db.GO 
        
        with open(f'{Path_Correlations}Taxons_Correlation_Embbeding_scores_{org}.txt', 'a') as the_file:
            
            # Do the column names:
            
            the_file.write("# Embedding"  + "\t")
            
            for dim in dimensions: 
            
                the_file.write(f'{dim}-D'  + "\t")
            
            the_file.write("\n") 
               
            for matrix in matrix_list:
                                
                # Name of the embedding:
                    
                the_file.write(f'{matrix}'  + "\t")
                    
                # Load the emeddings:
                    
                for dim in dimensions:
                    
                    embeddings = pd.read_csv(f'{Path_embedding}_GO_Embeddings_BP_PPI_{org}_{dim}_{matrix}.csv',  index_col = 0)
                        
                    # Filter the cosine:
                        
                    BP              = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
                    annotation_list = [name for sublist in BP.values() for name in sublist]
                        
                    occurance_of_each_annotation_in_network = Counter(annotation_list)
                    terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
                    
                    embeddings    =  embeddings[terms_filtering]
                    embeddings[embeddings < 0] = 0
                    
                    # Filter taxons and cosine:
                                       
                    common        = list(set(counts_db.GO).intersection(embeddings.columns))                    
                    embeddings    =  embeddings[common]
                    counts_db     =  counts_db.loc[common]
                    
                    # Calculat the mean embedding score:
                    
                    mean   = embeddings.mean()                   
                    result = spearmanr(mean, counts_db.Count)                                      
                    the_file.write(f'{round(result[0],3)} {result[1]}'  + "\t")
                
                # Next line:                
                the_file.write("\n")               
        the_file.close()


def Get_Functional_Specific_terms(Species):
    
    # Paths:
    
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/" 
    Path_bio        = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    
    for org in Species:
        
        Matrix_Gene_GO_BP = pd.read_csv(f'{Path_Annotation}Matrix_GO_{org}.csv', index_col=0) 

        # Functional Specific terms:
        
        Leaf_Annotation   = Taxon_Human_Script.Define_Leaf_Annotations_V2(Matrix_Gene_GO_BP)
        Leaf_Annotation   = pd.DataFrame(list(Leaf_Annotation.columns))
        
        # Generic Terms:
        
        levels = Taxon_Human_Script.Get_Level_GO_Terms(Matrix_Gene_GO_BP, annotation = "BP_Back_Propagation") 
        generic = levels[levels.Level <= 4].GO  
        generic = generic[~generic.isin(list(Leaf_Annotation.columns))]  
        
        # Generate the data Frame:
        
        generic_pd                =  pd.DataFrame(generic)
        generic_pd["Specificity"] = ["generic"] * len(generic_pd)
       
        Leaf_Annotation["Specificity"] = ["specific"] * len(Leaf_Annotation)
        Leaf_Annotation.columns =  ["GO", "Specificity"]
       
        final = generic_pd.append(Leaf_Annotation)
        final = final.reset_index(drop = True)
                
        final.to_csv(f'{Path_bio}Function_Specificity_Terms_{org}.csv')


def Biology_Across_Dimensions(Species, Matrix, jump = False):
    
    # Paths:
    
    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    biology_path     = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    semantic_path    = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    if jump == True:
        name = "Big_Jump"
    else:
        name = "Linear"
    
    # Per each emebdding method:
    
    for matrix in Matrix:
        
        # Open the file to write:
        
        with open(f'{biology_path}Results_Dimensionality_{matrix}_{name}.txt', 'a') as the_file:
            
            # Write the name of comlumns:
            
            the_file.write("# Comparison"  + "\n")
                           
            for org in Species:
                
                # Choose the dimensionalities to compare:
                
                if org == "Human":
                    if jump == False:
                        dimension_list = ["48-96", "96-144", "144-192", "192-240", "240-300", 
                       "300-400", "400-500", "500-600","600-700","700-800", "800-900", "900-1000"] 
                    else:
                        dimension_list = ["48-96", "48-144", "48-192", "48-240", "48-300", 
                       "48-400", "48-500", "48-600","48-700","48-800", "48-900", "48-1000"]                
                else:
                    if jump == False:
                        dimension_list = ["20-40", "40-60", "60-80", "80-100", "100-150", "150-200",
                                         "200-250", "250-300"]
                    else:
                        dimension_list = ["20-40", "20-60", "20-80", "20-100", "20-150", "20-200",
                                      "20-250", "20-300"]
                # Load the corresponding specie semantic similarity:
                
                SS = pd.read_csv(f'{semantic_path}Semantic_{org}.csv', index_col=0)
                
                # Empty lists for the results:
                               
                # Iterate by dimensional comparison:
                
                for dim in dimension_list:
                    
                     comparison = dim.split("-")
                     d1 = int(comparison[0])
                     d2 = int(comparison[1])
                    
                     # Load the cosines:
                    
                     cosine_1 = pd.read_csv(f'{save_cosine}Cosine_{org}_{d1}_{matrix}.csv',  index_col = 0)
                     cosine_2 = pd.read_csv(f'{save_cosine}Cosine_{org}_{d2}_{matrix}.csv',  index_col = 0)
                                
                     BP_1              = json.load(open(f'{annotations_path}_BP_Back_{org}.json'))
                     annotation_list_1 = [name for sublist in BP_1.values() for name in sublist]                                    
                     occurance_of_each_annotation_in_network_1 = Counter(annotation_list_1)                       
                     terms_filtering_1 = [key for key,values in occurance_of_each_annotation_in_network_1.items() if values >= 3]
            
                     cosine_1    =  cosine_1.loc[terms_filtering_1, terms_filtering_1]         
                     cosine_2    =  cosine_2.loc[terms_filtering_1, terms_filtering_1]
                     
                     # Cosine difference:
                        
                     move = cosine_1 - cosine_2
                        
                     move1 = move.apply(np.linalg.norm, axis=1)
                     move1 = move1.sort_values(ascending = False)
                     
                     # Total movement threshold also move1 = move1[move1 > (np.mean(move1) + 2*np.std(move1))]
                     # the problem is that is too stringent for some species. move1 = move1[:round(len(move1) * 0.10)]
                     
                     move1 = move1[:2000]
                     
                     SS_far_final   = []
                     SS_close_final = []
            
                     for GO in list(move1.index):
                                    
                         GO_terms = move.loc[GO]
                         GO_terms = GO_terms[1:]                                             
                         ra_it = 1 - GO_terms
                         
                         # Another way of filtering moving or not moving:
                                        
                         far   =  ra_it[ra_it > (1 + 2*np.std(GO_terms))] 
                         close =  ra_it[ra_it < (1 - 2*np.std(GO_terms))] 
                                                             
                         # Calculate semantic simlarity between the sets:
 
                                    
                         SS_far  = SS.loc[far.index,far.index ]
                         SS_close = SS.loc[close.index, close.index]
                                    
                         SS_far   = np.array(SS_far)[np.triu_indices(np.array(SS_far).shape[0], k = 1)]
                         SS_close = np.array(SS_close)[np.triu_indices(np.array(SS_close).shape[0], k = 1)]
                                    
                         mean_far = np.mean(SS_far)
                         mean_close = np.mean(SS_close)
                                    

                         if math.isnan(mean_far) == True:
                             continue
                         else:
                             SS_far_final.append(mean_far)
                                    
                         if math.isnan(mean_close) == True:
                             continue
                         else:
                             SS_close_final.append(mean_close) 
                    
                     # Calculate the numbers:
                             
                     Mannw      = mannwhitneyu(SS_far_final, SS_close_final, alternative = "less")
                     mean_far   = np.mean(SS_far_final)
                     std_far    = np.std(SS_far_final)               
                     mean_close = np.mean(SS_close_final)
                     std_close  = np.std(SS_close_final)
                     
                     # Write the info to the file:
                     print(f'{org} {d1} with {d2} Far: mean - {mean_far} std - {std_far}, Close: mean - {mean_close} std - {std_close} / and {Mannw.pvalue}')
                     the_file.write(f'{org} {d1} with {d2} Far: mean - {mean_far} std - {std_far}, Close: mean - {mean_close} std - {std_close} / and {Mannw.pvalue}\n')
                     
        the_file.close()
                     

def Calculate_Common_Species_Annotations(Species, dimensions, save_cosine, annotations_path):
    
    # Path:
    
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    
    GO_terms = []
    
    for org, dim in zip(Species,dimensions):
        
        # Load cosine:
        
        cosine = pd.read_csv(f'{save_cosine}Cosine_{org}_{dim}_PPMI.csv',  index_col = 0) 

        BP_1              = json.load(open(f'{annotations_path}_BP_Back_{org}.json'))
        annotation_list_1 = [name for sublist in BP_1.values() for name in sublist]                                    
        occurance_of_each_annotation_in_network_1 = Counter(annotation_list_1)                       
        terms_filtering_1 = [key for key,values in occurance_of_each_annotation_in_network_1.items() if values >= 3]
        
        cosine =  cosine.loc[terms_filtering_1, terms_filtering_1]
        
        # keep the name of the GO terms:
        
        GO_terms.append(list(cosine.index))

    # Compute the intersection between the GO terms
    
    GO_terms_common = set.intersection(*map(set,GO_terms)) 
    
    # Return the data:
    
    return(GO_terms_common)
                        
            
def Pann_Species_Comparison(Species, Matrix):
    
    # Paths:
    
    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    biology_path     = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"

    # We choose similar dimensionalities:

    dimensions = ["192", "150", "150", "150", "150", "150"]
    
    # We take the common set of GO terms:
    
    GO_terms_common = Calculate_Common_Species_Annotations(Species, dimensions, save_cosine, annotations_path)
        
    # Per each embedding algorithm:
    
    for matrix in Matrix:
        
        # Set the Results data frames:
        
        Resultado_dist  = pd.DataFrame(0, columns=Species, index = Species)
        
        # Start the comparisons:

        for org1, dim1 in zip(Species, dimensions):
            print(org1, dim1)
            
            cosine_1 = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix}.csv',  index_col = 0)
            cosine_1 = cosine_1.loc[GO_terms_common,GO_terms_common]
            #cosine_1 = np.array(cosine_1)[np.triu_indices(np.array(cosine_1).shape[0], k = 1)]
               
            for org2, dim2 in zip(Species,dimensions):
                
                    cosine_2 = pd.read_csv(f'{save_cosine}Cosine_{org2}_{dim2}_{matrix}.csv',  index_col = 0)
                    cosine_2 = cosine_2.loc[GO_terms_common,GO_terms_common]
                    #cosine_2 = np.array(cosine_2)[np.triu_indices(np.array(cosine_2).shape[0], k = 1)]
                    
                    # Distance:
                    
                    cosine_1_filt = cosine_1
                    cosine_2_filt = cosine_2
                            
                    norm_R1 = np.linalg.norm(cosine_1_filt, ord='fro')
                    norm_R2 = np.linalg.norm(cosine_2_filt, ord='fro')
                    Error   = cosine_1_filt - cosine_2_filt
                    norm    = np.linalg.norm(Error, ord='fro')
                    rel     = norm/max(norm_R1,norm_R2) 
                    Resultado_dist.loc[org1,org2] = rel
                            
        
                    #dst = distance.euclidean(cosine_1, cosine_2)
                    #Resultado_dist.loc[org1,org2] = dst
                    
        # Normalize the results:
        
        Resultado_norm = (Resultado_dist-Resultado_dist.min().min())/(Resultado_dist.max().max()-Resultado_dist.min().min())
                    
         # Save result:
           
        Resultado_norm.to_csv(f'{biology_path}Species_Comparison_{matrix}.csv')           
                    
                                   
def Specie_Specie_Comparison(Species, Matrix):
    
    # Paths:
    
    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    biology_path     = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"

    # We choose similar dimensionalities:

    dimensions = ["192", "150", "150", "150", "150", "150"]
    
    # Load the Taxons:
    
    Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))
    counts = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
    GO     = list(Taxons_Dic.keys())
            
    counts_db = pd.DataFrame({"GO" : GO, "Count" : counts})
    counts_db.index = counts_db.GO     
        
    for matrix in Matrix:
        
        with open(f'{biology_path}Specie_Individual_{matrix}.txt', 'a') as the_file:
            
            # Write the columns:
            
            the_file.write("# Comparison"  + "\t")
            the_file.write("# Pearson (correlation and p-value)"+ "\t")
            the_file.write("# Mannwhitney"  + "\n")
              
            for org1 , dim1 in zip(Species, dimensions):
                
                    print(org1, dim1)
                    
                    cosine_1 = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix}.csv',  index_col = 0)
                                        
                    BP_1              = json.load(open(f'{annotations_path}_BP_Back_{org1}.json'))
                    annotation_list_1 = [name for sublist in BP_1.values() for name in sublist]                                    
                    occurance_of_each_annotation_in_network_1 = Counter(annotation_list_1)                       
                    terms_filtering_1 = [key for key,values in occurance_of_each_annotation_in_network_1.items() if values >= 3]            
                    cosine_1          =  cosine_1.loc[terms_filtering_1, terms_filtering_1]         
                                  
                    for org2, dim2 in zip(Species,dimensions):
                        
                        if org1 != org2:
                        
                            cosine_2 = pd.read_csv(f'{save_cosine}Cosine_{org2}_{dim2}_{matrix}.csv',  index_col = 0)
                                                
                            BP_2              = json.load(open(f'{annotations_path}_BP_Back_{org2}.json'))
                            annotation_list_2 = [name for sublist in BP_2.values() for name in sublist]                                    
                            occurance_of_each_annotation_in_network_2 = Counter(annotation_list_2)                       
                            terms_filtering_2 = [key for key,values in occurance_of_each_annotation_in_network_2.items() if values >= 3]            
                            cosine_2          =  cosine_2.loc[terms_filtering_2, terms_filtering_2]  
                            
                            # Take the common set of GO terms between the species:
                            
                            common =  set(cosine_1.index).intersection(set(cosine_2.index))
                            common =  list(common.intersection(set(counts_db.index)))
                            

                            
                            
                            
                            
                            # Substract the cosine distances:
                            
                            movement = cosine_1_filt - cosine_2_filt
                            
                            # Calculate the total movement (stable and not stable):
                            
                            movement = movement.apply(np.linalg.norm, axis=1)
                            movement = movement.sort_values(ascending = False) 
                            movement = pd.DataFrame(movement)
                            
                            # Specific and not specific:
                            
                            movement.columns  = ["Move"]
                            movement["Count"] = list(counts_db.loc[movement.index].Count)
                            
                            # Correlation:
                            
                            pearson = pearsonr(movement.Move, movement.Count)
                            
                            # Distributions:
                            
                            movement_unique  = movement[movement.Count == 2]
                            movement_generic = movement[movement.Count > 13]
                            
                            Manni = mannwhitneyu(movement_unique.Move, movement_generic.Move, alternative = "less")
                            
                            # Write the corresponding line:
                            p = round(pearson[0], 3)
                            the_file.write(f'{org1}-{org2}\t')
                            the_file.write(f'{p}-{pearson[1]}\t')
                            the_file.write(f'{Manni.pvalue}\n')
                        else:
                            continue
         
        # Close the file:
              
        the_file.close()


def Comparison_Embeddings(Species, dimensions, Matrix):
    
    # Paths:

    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    biology_path     = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"

    for org1 , dim1 in zip(Species, dimensions):
        
        Resultado_emb_dist = pd.DataFrame(0, columns=Matrix, index = Matrix)
        
        for matrix in Matrix:
            
            # Load cosine:
            
            cosine_1 = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix}.csv',  index_col = 0)
            
            BP_1              = json.load(open(f'{annotations_path}_BP_Back_{org1}.json'))
            annotation_list_1 = [name for sublist in BP_1.values() for name in sublist]                                    
            occurance_of_each_annotation_in_network_1 = Counter(annotation_list_1)                       
            terms_filtering_1 = [key for key,values in occurance_of_each_annotation_in_network_1.items() if values >= 3]
                
            cosine_1  =  cosine_1.loc[terms_filtering_1, terms_filtering_1] 
            cosine_1  = np.array(cosine_1)[np.triu_indices(np.array(cosine_1).shape[0], k = 1)]             
        
            for matrix2 in Matrix:
                
                cosine_2    = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix2}.csv',  index_col = 0)
                cosine_2    =  cosine_2.loc[terms_filtering_1, terms_filtering_1]
                cosine_2    = np.array(cosine_2)[np.triu_indices(np.array(cosine_2).shape[0], k = 1)]
                
                # Distance:

                dst = distance.euclidean(cosine_1, cosine_2)
                Resultado_emb_dist.loc[matrix,matrix2] = dst
        
        # Save the distance:
        
        Resultado_norm = (Resultado_emb_dist-Resultado_emb_dist.min().min())/(Resultado_emb_dist.max().max()-Resultado_emb_dist.min().min())
        
        Resultado_norm.to_csv(f'{biology_path}Embedding_comparison_{org1}') 


def Biological_Comparison_Embeddings(Species, dimensions, Matrix):

    # Paths:

    save_cosine      = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    biology_path     = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    for org1 , dim1 in zip(Species, dimensions):
        
        # Open the file:
        
        with open(f'{biology_path}Bio_comparison_embeddings_{org1}.txt', 'a') as the_file:
            
            # Write the name of the columns:
            
            the_file.write("# Comparison:"  + "\n")
            
            # Iterate by Matrix comparison:               
            
            for matrix in Matrix:
                
                # Load cosine of one embedding algorithm:
                
                cosine_1 = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix}.csv',  index_col = 0)
                
                BP_1              = json.load(open(f'{annotations_path}_BP_Back_{org1}.json'))
                annotation_list_1 = [name for sublist in BP_1.values() for name in sublist]                                    
                occurance_of_each_annotation_in_network_1 = Counter(annotation_list_1)                       
                terms_filtering_1 = [key for key,values in occurance_of_each_annotation_in_network_1.items() if values >= 3]
                    
                cosine_1  =  cosine_1.loc[terms_filtering_1, terms_filtering_1] 
                
                # Load cosine of the second embedding algorithm:
               
                for matrix2 in Matrix:
                    
                    if matrix2 != matrix:
                    
                        cosine_2    = pd.read_csv(f'{save_cosine}Cosine_{org1}_{dim1}_{matrix2}.csv',  index_col = 0)
                        cosine_2    =  cosine_2.loc[terms_filtering_1, terms_filtering_1]
                        
                        # Subtract the information:
                        
                        moving = cosine_1 - cosine_2
                        
                        # Get which GO changes the most between spaces:
                        
                        moving = moving.apply(np.linalg.norm, axis=1)
                        moving = moving.sort_values(ascending = False)
                        
                        # Load specificity function information:
                
                        # A) Function:
                
                        funcion = pd.read_csv(f'/media/sergio/sershiosdisk/Axes_Species/Biological_Study/Function_Specificity_Terms_{org1}.csv',
                                      index_col=0) 
                        funcion = funcion[funcion.GO.isin(moving.index)] 
                        
                        # B) Taxons:
                        
                        Taxons_Dic = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))
                     
                        counts = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
                        GO     = list(Taxons_Dic.keys())
                    
                        counts_db = pd.DataFrame({"GO" : GO, "Count" : counts})
                        counts_db.index = counts_db.GO 
                        
                        # Mannwhitney test to show the differences in function specificity
                        
                        moving_specificos = moving[moving.index.isin(funcion[funcion.Specificity == "specific"].GO)]
                        moving_genericos  = moving[moving.index.isin(funcion[funcion.Specificity == "generic"].GO)]
                        
                        x = mannwhitneyu(moving_specificos, moving_genericos, alternative = "greater")
                        x2 = mannwhitneyu(moving_specificos, moving_genericos, alternative = "less")
                        
                        the_file.write(f'{matrix} vs {matrix2} generic func: {round(np.mean(moving_genericos),3)} std: {round(np.std(moving_genericos),3)} / specific func: {round(np.mean(moving_specificos),3)} std: {round(np.std(moving_specificos),3)} / more: {x.pvalue}\n')
                        the_file.write(f'{matrix} vs {matrix2} generic func: {round(np.mean(moving_genericos),3)} std: {round(np.std(moving_genericos),3)} / specific func: {round(np.mean(moving_specificos),3)} std: {round(np.std(moving_specificos),3)} / less: {x2.pvalue}\n')
                        
                        # Pearson correlation to show the differences in the taxons:
                        
                        filtering = set(counts_db.GO).intersection(set(moving.index))
                        
                        x = pearsonr(list(counts_db.loc[filtering].Count), list(moving.loc[filtering]))
                        
                        the_file.write(f'{matrix} vs {matrix2} corr taxons: {round(x[0],2)}, pvalue = {x[1]}\n')
                    else:
                        continue
        
        # Close the file:
        
        the_file.close()

def Calculate_Semantic(SS, axes):
    
    final = []
    
    for axis in axes.keys():
        GO = axes[axis]
        GO = list(set(GO).intersection(set(SS.index)))
        if len(GO) > 1:
            semantic = SS.loc[GO,GO]
            semantic = np.array(semantic)[np.triu_indices(np.array(semantic).shape[0], k = 1)]
            semantic = np.mean(semantic)
            final.append(semantic)
    return(final)


def Calculate_Semantic_Similarity_Across_Dim(Species, Matrix, dimensions, common = False):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    SS_path         = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    
    for matrix in Matrix:
        for org in Species:
            
            # Load the information of the Semantic Similarity:
            
            SS = pd.read_csv(f'{SS_path}Semantic_{org}.csv', index_col = 0)
            
            # Get the information of the axes:
            
            Associations_list = []
            
            for dim in dimensions:
                
                # Load the corresponding dimensional axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json')) 
                Associations_list.append(Axes)
            
            # If common is true:
            
            if common == True:
                list_axes = []
                for dim in dimensions:                 
                    Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json'))
                    Axes = {k: v for k, v in Axes.items() if v}
                    Axes = [item for sublist in Axes.values() for item in sublist]
                    Axes = list(set(Axes))
                    list_axes.append(Axes) 
                common_set = set.intersection(*[set(list) for list in list_axes])
                
                Axes_info_b = [] 
                for dim in range(len(Associations_list)):
                    axes = Associations_list[dim]
                    for axis in list(axes.keys()):
                        GO_1 = list(set(axes[axis]).intersection(set(common_set)))
                        axes[axis] = GO_1
                    Axes_info_b.append(axes)
                Associations_list = Axes_info_b 
                            
            # Delete empty axes:
            
            for dicti in range(len(Associations_list)):   
                Associations_list[dicti] ={k: v for k, v in Associations_list[dicti].items() if v}
                     
            
            # Calculate the semantic similarity:
            
            final_semantic = []
            
            for i in range(len(Associations_list)):
                axes     = Associations_list[i]
                Semantic = Calculate_Semantic(SS, axes)
                final_semantic.append(Semantic)
            
            # Plot the corresponding box plots:
            
            plt.style.use("seaborn-whitegrid")
            plt.rcParams.update({'font.size': 17})
            plt.rc('font', weight='bold')
            
            fig, axes = plt.subplots(1, 1, figsize=(9,5))
            fig.tight_layout(pad = 0.5)
            
            fig.text(-0.001, 0.5, 'Mean Semantic Similarity', ha='center', va='center', 
                 rotation='vertical', fontsize=17, fontweight = "bold") 
            fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=17, fontweight = "bold")
           
            axes.boxplot(final_semantic)
            
            axes.set_xticklabels(dimensions)
            axes.spines['right'].set_visible(False)
            axes.spines['top'].set_visible(False)
            axes.spines['left'].set_linewidth(4)
            axes.spines['bottom'].set_linewidth(4)
            axes.spines['left'].set_color("grey")
            axes.spines['bottom'].set_color("grey")  
            axes.set_title(f'{org.capitalize()} {matrix}', 
            fontweight="bold", fontsize = 17, y = 1) 
            
            if common == False:                
                fig.savefig(f'{Axes_Annot_Path}Semantic_Similarity_{org}_{matrix}_BP.png', format="png", dpi=600,
                            bbox_inches='tight')
            else:
                fig.savefig(f'{Axes_Annot_Path}Semantic_Similarity_Common_{org}_{matrix}_BP.png', format="png", dpi=600,
                            bbox_inches='tight')            

            
def Calculate_Intra_Inter_Axes(Species, matrix_list, dimensions, annotations, associations = ""): 
    
    # Paths:

    Path_Semantic   = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    for matrix in matrix_list:
    
        for org in Species:           
            print(f'{matrix}-{org}')
            
            # Load the Semantic Similarity:
            
            Semantic = pd.read_csv(f'{Path_Semantic}Semantic_{org}.csv', index_col = 0) 
            
            # Set the dictionary:
            
            keys = []
            
            for dm in dimensions:
                
                keys.append(f'{dm}_Inter')
                keys.append(f'{dm}_Intra')
                keys.append(f'{dm}_Inter_Random')
                keys.append(f'{dm}_Intra_Random')
            
            Result = dict(([(key, []) for key in keys]))
            
            # Iterate by dimensions:
            
            for dim in dimensions:
                
                # Load the corresponding associacions
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{associations}.json')) 
                
            # Noel.
            #Axes         = pd.read_csv("/media/sergio/sershiosdisk/Axes_Species/Associations.csv", sep = "\t")
            #Axes.columns = ["GO", "gene"]
            #boh          = dict.fromkeys(set(Axes.gene))
                
             #for i in list(boh.keys()):
                    #boh[i] = list(Axes[Axes.gene == i].GO)

                
                # Delete empty axes:
             
                Axes     = {k: v for k, v in Axes.items() if v}
                average  = round(np.mean([len(Axes[i]) for i in Axes.keys()]))
                
                # Analyze the intra/inter cluster distance:                    
                    
                intra_distance = []
                inter_distance = []
                
                intra_random   = []
                inter_random   = []
                    
                # Intra cluster semantic similarity:
                    
                for cluster in list(set(Axes.keys())):
                        
                        # Get the intra cluster distance:
                        
                        GO_cluster_1 = Axes[cluster]
                        
                        if len(GO_cluster_1) > 1:
                            
                            GO_cluster_1        = list(set(GO_cluster_1).intersection(set(Semantic.index))) 
                            GO_cluster_1_Random = random.sample(list(Semantic.index),average )
                            
                            Semantic_1   = Semantic.loc[GO_cluster_1, GO_cluster_1]
                            Semantic_1   = np.array(Semantic_1)[np.triu_indices(np.array(Semantic_1).shape[0], k = 1)]
                            
                            Semantic_1_random   = Semantic.loc[GO_cluster_1_Random, GO_cluster_1_Random]
                            Semantic_1_random   = np.array(Semantic_1_random)[np.triu_indices(np.array(Semantic_1_random).shape[0], k = 1)]
                                                                                   
                            intra_distance.append(round(np.mean(Semantic_1),3))
                            intra_random.append(round(np.mean(Semantic_1_random),3))
                    
                # Inter cluster semantic similarity:
                    
                lista = list(set(Axes.keys()))
                    
                for i in range(len(lista)):                       
                    cluster_1    = lista[i]
                    GO_cluster_1 = Axes[cluster_1]
                            
                    for j in range(i+1, len(lista)):

                        cluster_2           = lista[j]
                        GO_cluster_2        = Axes[cluster_2]
                        
                        GO_cluster_2_Random = random.sample(list(Semantic.index),average)
                        GO_cluster_1_Random = random.sample(list(Semantic.index),average)
                            
                        # Take the semantic similarity:
                        
                        GO_cluster_1 = list(set(GO_cluster_1).intersection(set(Semantic.index)))
                        GO_cluster_2 = list(set(GO_cluster_2).intersection(set(Semantic.index)))
                            
                        Semantic_1 = Semantic.loc[GO_cluster_1, GO_cluster_2]
                        inter_distance.append(round(Semantic_1.mean().mean(),3))

                        Semantic_1_Random = Semantic.loc[GO_cluster_1_Random, GO_cluster_2_Random]
                        inter_random.append(round(Semantic_1_Random.mean().mean(),3))
                
                # Add the info to the list:
                    
                Result[f'{dim}_Inter'] = inter_distance
                Result[f'{dim}_Intra'] = intra_distance
                
                Result[f'{dim}_Inter_Random'] = inter_random
                Result[f'{dim}_Intra_Random'] = intra_random   
                    
            sa = json.dumps(Result)
            f = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes_{annotations}{associations}.json' ,"w")
            f.write(sa)
            f.close()            


def Calculate_Avg_GO_Axes(Species, matrix_list, dimensions):
    
    # Paths:
    
    Asso_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"   
    
    for org in Species:  
        last = []
        for matrix in matrix_list:            
            final = []            
            for dim in dimensions:
                f     = open(f'{Asso_path}Associations_{org}_{matrix}_{dim}.json')
                data  = json.load(f)
                clean = {k: v for k, v in data.items() if v}
                
                count = [len(clean[i]) for i in clean.keys()]
                count = np.mean(count)
                final.append(count)
            last.append(final)
    
                          
def Calculat_Basic_Statistics_Axes(Species, Matrix):
    
    # Paths:
    
    Asso_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Iterate over species:
    
    for org in Species:
        
        # Open the txt file:
        
        with open(f'{Asso_path}Statistics_Axes_{org}.txt', 'a') as the_file:
            
            # Write the colun names of the file:
            
            the_file.write("# Emebdding"  + "\t")
            the_file.write("# Mean Semantic Similarity"  + "\t")
            the_file.write("# Std Semantic Similarity"  + "\n")
        
            # Iterate over embedding algorithms:
            
            for matrix in Matrix:
                
                # Open the axes associations:
                
                f    = open(f'{Asso_path}Associations_{org}_{matrix}.json')
                data = json.load(f)
                
                # Calculate percentage of annotated axes:
                
                first_element_of_non_empty = [l[0] for l in data.values() if l]          
                num_non_empty              = len(first_element_of_non_empty)
                num_empty                  = len(data) - num_non_empty               
                percentage_annotated       = (num_non_empty/(num_non_empty + num_empty))*100
                
                # Caculate the semantic Similarity:
                
                distance_list_fin = []
                
                G = graph.from_resource("go-basic")
                similarity.precalc_lower_bounds(G)
                
                for key in list(data.keys()):
                    
                    list_GO = data[key]
                    distance_list = []
                   
                    if len(list_GO) > 1:
                        for i in range(len(list_GO)):
                            for j in range(i+1, len(list_GO)):
                                try:
                                    
                                    distance = similarity.lin(G, list_GO[i], list_GO[j]) 
                                    distance_list.append(distance)
                                    
                                except Exception as PGSSLookupError:
                                        continue
                    
                    distance_list_fin.append(np.mean(distance_list))           
                    
                cleanedList = [x for x in distance_list_fin if str(x) != 'nan']
                mean_ss     = np.mean(cleanedList)
                std_ss      = np.std(cleanedList)
                    
                # Write the information to the file:
                    
                the_file.write(f'{matrix}\t')        
                the_file.write(f'{round(percentage_annotated,3)}\t')
                the_file.write(f'{round(mean_ss,3)}\t')
                the_file.write(f'{round(std_ss,3)}\n')

        # Close the file:
        
        the_file.close()

def Correlate_Taxons_Levels_Genes():
    
    # Paths:
    
    taxon_path       = "/media/sergio/sershiosdisk/Human/Axes/"
    levels_path      = "/media/sergio/sershiosdisk/Human/All_Annotation/"
    annotations_path = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
      
    # Load Taxons:
    
    Taxons_Dic = json.load(open(f'{taxon_path}GO_Taxons_Whole_Annotations.json'))                     
    counts     = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
    GO         = list(Taxons_Dic.keys())                   
    counts_db  = pd.DataFrame({"GO" : GO, "Count" : counts})
    counts_db.index = counts_db.GO 
    
    # Load levels:
    
    Levels = pd.read_csv(f'{levels_path}GO_Terms_Level_Matrix_BP_Back_Propagation.csv',
                        index_col = 0)
    Levels.index = Levels.GO
    
    # Load the genes Annotations
        
    Annotations = pd.read_csv(f'{annotations_path}Matrix_GO_Human.csv', index_col = 0)
    Annotations = Annotations.sum()
    
    # Compute the correlations with the number of genes:
    
    common = set(counts_db.GO).intersection(set(Annotations.index))
    x      = counts_db.loc[common]
    y      = Annotations.loc[common]
    
    w = pearsonr(x.Count,y)
    
    plt.scatter(x.Count, y)
    plt.text(10, 8500, f'Pearson: {round(w[0],3)}', fontsize=20)

    # Compute the correlation with the levels: 
    
    common = set(counts_db.GO).intersection(set(Levels.index))
    
    x = counts_db.loc[common]
    y = Levels.loc[common] 
 
    w = pearsonr(x.Count,y.Level)    
    plt.scatter(x.Count, y.Level)
    plt.text(12, 10, f'Pearson: {round(w[0],3)}', fontsize=20)


def Distance_Axes_Taxons(Species, Matrix, dimensions):
    
    # Paths:
    
    Axes_Annot_Path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    GO_embeddings_Path = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"
    optimal_path       = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    correlation_path   = "/media/sergio/sershiosdisk/Axes_Species/Correlations_Taxons/"
    
    # Load Taxons:
    
    Taxons_Dic      = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))                    
    counts          = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
    GO              = list(Taxons_Dic.keys())                    
    counts_db       = pd.DataFrame({"GO" : GO, "Count" : counts})
    counts_db.index = counts_db.GO     
    
    for org in Species:
        
        # Open Table:
        
        with open(f'{correlation_path}Axes_Correlation_Taxons_{org}.txt', 'a') as the_file:
            
            # Write the name of the columns:
            
            the_file.write("# Emebdding"  + "\t")
            the_file.write("# Spearman Coeficient"  + "\t")
            the_file.write("# P-value"  + "\n")            
        
            # Load the information of the specie:
            
            if org == "Human":
                file_information = pd.read_csv(f'{optimal_path}Human_Optimal_Dimensions.txt', sep = "\t")
                information      = file_information[file_information["# Specie"] == org]
            else:
                file_information = pd.read_csv(f'{optimal_path}Optimal_Dimensions.txt', sep = "\t")
                information      = file_information[file_information["# Specie"] == org]
                   
            for matrix in Matrix:
                
                # Substract the information for the matrix:
                
                information_it    = information[information["# Matrix"] == matrix] 
                dim               = int(information_it["# Optimal"])
                            
                # Load the annotations:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}.json'))
                
                # Load the GO embeddings:
                
                GO_embeddings                    = pd.read_csv(f'{GO_embeddings_Path}_GO_Embeddings_BP_PPI_{org}_{dim}_{matrix}.csv',  index_col = 0) 
                GO_embeddings[GO_embeddings < 0] = 0
                GO_embeddings = GO_embeddings.T
                
                # Get the information of the position in the axes:
                
                position = []
                Taxons   = []
                
                for axis in list(set(Axes.keys())):
                    
                    GO_terms = Axes[str(axis)]
                    
                    # Check that we have the information of the taxons:
                    
                    common   = list(set(counts_db.GO).intersection(set(GO_terms)))
                    
                    # Take the information:
                    
                    if len(GO_terms) > 0:                    
                        position_it  = list(GO_embeddings.loc[common, int(axis)])
                        counts_db_it = list(counts_db.loc[common].Count)
                        
                        position.extend(position_it)
                        Taxons.extend(counts_db_it)
                    else:
                        continue
                    
                # Correlation:
                    
                result_corr = spearmanr(position, Taxons)                
                the_file.write(f'{matrix}\t')
                the_file.write(f'{round(result_corr[0],3)}\t')
                the_file.write(f'{result_corr[1]}\n')                  

def overlap_coefficient(list_a,list_b):
    set_a = set(list_a)
    set_b = set(list_b)
    cardinality_a = len(set_a)
    cardinality_b = len(set_b)
    nominator = len(set_a.intersection(set_b))
    denominator = min(cardinality_a,cardinality_b)
    return round(nominator/denominator,2)     

def Pann_Species_Comparison_Axes(Species, matrix_list):
    
    # Paths:
    
    Axes_Annot_Path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    optimal_path       = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    axes_bio           = "/media/sergio/sershiosdisk/Axes_Species/Biological_Study_Axes/"
    annotations_path   = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    # Get the common set of annnotations:
    
    dimensions      = ["192", "150", "150", "150", "150", "150"]
    GO_terms_common = Calculate_Common_Species_Annotations(Species, dimensions, optimal_path, annotations_path)
    
    # Start the comparisons:
    
    for matrix in matrix_list:
        
        print(matrix)
        
        Axes_info = []
        
        # Per each specie load the information of the axes:
        
        for org in Species:
          
            # Load the associacions:
                                       
            Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}.json')) 
            Axes_info.append(Axes)                                                        

        # Filter the axes to only keep the common set of annotations: 
        
        Axes_info_it = []                                                     
        for org in range(len(Axes_info)):
            axes = Axes_info[org]
            for axis in list(axes.keys()):
                GO_1 = list(set(axes[axis]).intersection(set(GO_terms_common)))
                axes[axis] = GO_1
            Axes_info_it.append(axes)
        
        Axes_info = Axes_info_it
                                                       
        # Delete empty axes:
                        
        for dicti in range(len(Axes_info)):           
            Axes_info[dicti] ={k: v for k, v in Axes_info[dicti].items() if v} 

        # Start the comparisons:  

        Result = pd.DataFrame(0,columns=Species, index = Species)                   
            
        for org1 in range(len(Axes_info)):
            
            axes1 = Axes_info[org1]
            
            for org2 in range(len(Axes_info)):
                
                axes2 = Axes_info[org2]
                
                # Set comparison between their axes:
                
                distance_list_result = []
                
                for axis1 in list(axes1.keys()):
                    
                    GO_1 = axes1[axis1]
                    distance_list = []
                                             
                    for axis2 in list(axes2.keys()):
                            
                            GO_2    = axes2[axis2]                    
                            overlap = overlap_coefficient(GO_1,GO_2)
                            distance_list.append(overlap)
                           
                    distance_list_result.append(max(distance_list))
        
                Result.iloc[org1,org2] += sum(distance_list_result)/(len(axes1) + len(axes2)) 
                Result.iloc[org2,org1] += sum(distance_list_result)/(len(axes1) + len(axes2))             
    
        Result.to_csv(f'{axes_bio}Pann_Sepcies_Comparison_{matrix}.csv')    


def Plot_Percentage_Axes_Across_Dimensions_Human(Species, matrix_list, dimensions, annotations, association = ""):

    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Annot_path      = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    save_path       = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    for org in Species:
        
        matrix_total = []
        
        if org == "Human":
            d1 = 48
        else:
            d1 = 20
        
        for matrix in matrix_list:
            
            Associations_list = []
            
            for dim in dimensions:
                
                # Load the corresponding dimensional axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{association}.json')) 
                Associations_list.append(Axes)
            
            matrix_total.append(Associations_list)
            
        # Compute the % of enriched clusters in each dimensional space: 
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 50})
        plt.tight_layout()
        fig, axs = plt.subplots(1, 1, figsize=(12,3))
        
        # Get the corresponding information per each algorithm:
        
        final_perc = []
        for i in range(len(matrix_list)):
            axes_algorithm = matrix_total[i]
            percentage_matrix = []
            for dimensions_ax in range(len(axes_algorithm)):
                
                non_zero   = len([array for key, array in axes_algorithm[dimensions_ax].items() if array])
                percentage = non_zero/len(axes_algorithm[dimensions_ax])*100
                percentage_matrix.append(percentage)
            final_perc.append(percentage_matrix)
            
            axs.plot(dimensions, percentage_matrix,  marker='o',  linewidth =3)
            axs.set_xticks(dimensions)
            axs.spines['right'].set_visible(False)
            axs.spines['top'].set_visible(False)
            axs.spines['left'].set_linewidth(4)
            axs.spines['bottom'].set_linewidth(4)
            axs.spines['left'].set_color("grey")
            axs.spines['bottom'].set_color("grey")
            axs.tick_params(axis='both', labelsize=15) 
            axs.set_ylabel("% Axes", fontsize=20, fontweight='bold') 
            axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold') 
        
        
        fig.savefig(f'{save_path}%_Human_Axes.svg', format="svg", dpi=600,
                    bbox_inches='tight') 
        
        #fig.legend(labels= matrix_list,
         #          borderaxespad=0.1,
         #         loc="upper center", frameon=True, ncol = len(matrix_list),prop={'size': 20}) 
        
        # Compute the % of enriched GO terms:

        if annotations == "BP_Back":
            BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
            cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{d1}_Deepwalk.csv',  index_col = 0)
        else:
            BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
            cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_{matrix}.csv',  index_col = 0)
                        
        annotation_list = [name for sublist in BP.values() for name in sublist]
        occurance_of_each_annotation_in_network = Counter(annotation_list)
        terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
        cosine          =  cosine.loc[terms_filtering, terms_filtering]
                    
        Total = len(cosine.index)
        
        # Start the computations:
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 50})
        plt.tight_layout()
        fig, axs = plt.subplots(1, 1, figsize=(12,3))
                     
        for i in range(len(matrix_list)):            
            axes_algorithm = matrix_total[i]
            
            for dicti in range(len(axes_algorithm)):
                axes_algorithm[dicti] ={k: v for k, v in axes_algorithm[dicti].items() if v}
            total_ind = []
            for i in range(len(axes_algorithm)):
                axes_dim = axes_algorithm[i]
                GO_dim   = len(set([item for sublist in axes_dim.values() for item in sublist]))
                total_ind.append(GO_dim)
            
            axs.plot(dimensions, [round(i*100/Total,2) for i in total_ind], linewidth=3, marker = "o")
              
            axs.set_xticks(dimensions)
            axs.spines['right'].set_visible(False)
            axs.spines['top'].set_visible(False)
            axs.spines['left'].set_linewidth(4)
            axs.spines['bottom'].set_linewidth(4)
            axs.spines['left'].set_color("grey")
            axs.spines['bottom'].set_color("grey")
            axs.tick_params(axis='both', labelsize=15) 
            axs.set_ylabel("% GO", fontsize=20, fontweight='bold') 
            axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold') 
            
        fig.savefig(f'{save_path}%_Human_GO_Axes.svg', format="svg", dpi=600,
                    bbox_inches='tight') 
        
        #fig.legend(labels= matrix_list,
         #          borderaxespad=0.1,
         #          loc="upper center", frameon=True, ncol = len(matrix_list),prop={'size': 20}) 
            
        
def Plot_Percentage_Axes_Across_Dimensions_Human_avg(Species, matrix_list, dimensions, annotations):         

    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Annot_path      = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    for org in Species:
        
        if org == "Human":
            d1 = 48
        else:
            d1 = 20
                
        matrix_total = []
        
        for matrix in matrix_list:
            
            Associations_list = []
            
            for dim in dimensions:
                
                # Load the corresponding dimensional axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}.json')) 
                Associations_list.append(Axes)
            
            matrix_total.append(Associations_list)
            
        # Compute the % of enriched clusters in each dimensional space: 
        
        # Get the corresponding information per each algorithm:
        
        percentage_total = []
        for i in range(len(matrix_list)):
            axes_algorithm = matrix_total[i]
            percentage_matrix = []
            for dimensions_ax in range(len(axes_algorithm)):
                
                non_zero   = len([array for key, array in axes_algorithm[dimensions_ax].items() if array])
                percentage = non_zero/len(axes_algorithm[dimensions_ax])*100
                percentage_matrix.append(percentage)
            percentage_total.append(percentage_matrix)
        
        # Get the percentage of annotations:

        if annotations == "BP_Back":
            BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
            cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{d1}_Deepwalk.csv',  index_col = 0)
        else:
            BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
            cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_{matrix}.csv',  index_col = 0)       
        
        annotation_list = [name for sublist in BP.values() for name in sublist]
        occurance_of_each_annotation_in_network = Counter(annotation_list)
        terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
        cosine          =  cosine.loc[terms_filtering, terms_filtering]
                    
        Total = len(cosine.index)
        
        total_all = []
        for i in range(len(matrix_list)):            
            axes_algorithm = matrix_total[i]
            for dicti in range(len(axes_algorithm)):
                axes_algorithm[dicti] ={k: v for k, v in axes_algorithm[dicti].items() if v}
            total_ind = []
            for i in range(len(axes_algorithm)):
                axes_dim = axes_algorithm[i]
                GO_dim   = len(set([item for sublist in axes_dim.values() for item in sublist]))
                total_ind.append(GO_dim)
            total_all.append([round(i*100/Total,2) for i in total_ind])
            
        # Do the average plot:
        
        total_all_avg = [np.mean(i) for i in total_all]
        total_all_std = [np.std(i) for i in total_all]
        
        percentage_total_avg = [np.mean(i) for i in percentage_total]
        percentage_total_std = [np.std(i)  for i in percentage_total]
        
        x = np.arange(len(matrix_list))
        width = 0.15       
                
        fig, axs = plt.subplots(1, 1, figsize=(5,5)) 
        bar1 = axs.bar(x, percentage_total_avg, width, label='%Axes', yerr =percentage_total_std, capsize = 10)
        bar2 = axs.bar(x + width, total_all_avg, width, label='%GO', yerr =total_all_std, capsize = 10)
        
        plt.xticks(x + width/2,matrix_list, fontsize=15)

        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=15) 
        axs.set_ylabel("% Enrichment", fontsize=20, fontweight='bold') 
        
        fig.legend( (bar1, bar2), ('Axes', 'GO BP terms') ,prop={'size': 16},
                   loc="upper right", frameon=True)


def Plot_Percentage_Axes_Across_Dimensions_SP_avg(Species, matrix_list, dimensions, annotations, association = ""):
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Annot_path      = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    
    # Load the information:
    
    Data = pd.DataFrame()
    
    for org in Species:
        
        if org == "Human":
            
            # Get info for the specie:
            
            if annotations == "BP_Back":
                BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
                cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_48_PPMI.csv',  index_col = 0)
            else:
                BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
                cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_Deepwalk.csv',  index_col = 0)
        
        else:
            
            if annotations == "BP_Back":
                BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
                cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_20_PPMI.csv',  index_col = 0)
            else:
                BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
                cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_Deepwalk.csv',  index_col = 0)            
        
        annotation_list = [name for sublist in BP.values() for name in sublist]
        occurance_of_each_annotation_in_network = Counter(annotation_list)
        terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
        cosine          =  cosine.loc[terms_filtering, terms_filtering]
                    
        Total = len(cosine.index)

        # Get the associations:       
        
        matrix_total = []
        for matrix in matrix_list:
            Associations_list = []
            for dim in dimensions:
                # Load the corresponding dimensional axes:              
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{association}.json')) 
                Associations_list.append(Axes)
            matrix_total.append(Associations_list)
        
        # Get the information about the % Clusters:
        
        percentage_total = []
        for i in range(len(matrix_total)):
            axes_algorithm = matrix_total[i]
            percentage_matrix = []
            for dimensions_ax in range(len(axes_algorithm)):
                
                non_zero   = len([array for key, array in axes_algorithm[dimensions_ax].items() if array])
                percentage = non_zero/len(axes_algorithm[dimensions_ax])*100
                percentage_matrix.append(percentage)
            percentage_total.append(percentage_matrix)
        
        # Get the percentage of annotations:
        
        total_all = []
        for i in range(len(matrix_list)):            
            axes_algorithm = matrix_total[i]
            for dicti in range(len(axes_algorithm)):
                axes_algorithm[dicti] ={k: v for k, v in axes_algorithm[dicti].items() if v}
            total_ind = []
            for i in range(len(axes_algorithm)):
                axes_dim = axes_algorithm[i]
                GO_dim   = len(set([item for sublist in axes_dim.values() for item in sublist]))
                total_ind.append(GO_dim)
            total_all.append([round(i*100/Total,2) for i in total_ind])   
        
        # Generate the DataFrame:
        
        for matrix in range(len(matrix_list)):
        
            Data_it           = pd.DataFrame({"Axes" : percentage_total[matrix], "GO" : total_all[matrix]})
            Data_it["Specie"] = [org] * len(percentage_total[matrix])
            Data_it["Matrix"] = [matrix_list[matrix]] * len(percentage_total[matrix])
            Data = Data.append(Data_it)
                       
    # Do the plot:
    
    x     =  np.arange(len(Species))
    x      = x*2
    width = 0.20
    sum_it = -0.2
    
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 50})
    plt.tight_layout()
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))
    
    # % GO:
    
    x     =  np.arange(len(Species))
    x      = x*2
    width = 0.20
    sum_it = -0.2
    
    fig, axs = plt.subplots(1, 1, figsize=(5,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Specie").mean()
        std  = Data[Data.Matrix == matrix].groupby("Specie").std()
        
        plt.bar(x + sum_it, mean["GO"], width, label='%GO', yerr =std["GO"], capsize = 10)
        sum_it = sum_it+0.20 
        
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=25) 
    axs.set_ylabel("% GO", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    axs.set_yticks([0, 20, 40, 60,80])
    plt.xticks(x+width,list(mean.index), fontsize=15)
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))               
    

def Plot_Percentage_GO_Across_Dimensions(Species, matrix_list, dimensions):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Annot_path      = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    
    for matrix in matrix_list:
        for org in Species:
            
            # Load the cosine:
            
            cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_{dimensions[0]}_{matrix}.csv',  index_col = 0)
            BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
            annotation_list = [name for sublist in BP.values() for name in sublist]
            occurance_of_each_annotation_in_network = Counter(annotation_list)
            terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
            cosine          =  cosine.loc[terms_filtering, terms_filtering]
            
            Total = len(cosine.index)
            
            # Prepare the plot:
            
            plt.style.use("seaborn-whitegrid")
            plt.rcParams.update({'font.size': 17})
            plt.rc('font', weight='bold')
            
            fig, axes = plt.subplots(1, 1, figsize=(9,5))
            fig.tight_layout(pad = 0.5)
        
            fig.text(-0.001, 0.5, '% GO terms', ha='center', va='center', 
                 rotation='vertical', fontsize=17, fontweight = "bold")  
        
            fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=17, fontweight = "bold")
            
            Associations_list = []
            
            for dim in dimensions:
                
                # Load the corresponding dimensional axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json')) 
                Associations_list.append(Axes)
            
            # Delete empty axes:
            
            for dicti in range(len(Associations_list)):   
                Associations_list[dicti] ={k: v for k, v in Associations_list[dicti].items() if v}
            
            # Plot the corresponding line plot:
            
            total_ind = []
            for i in range(len(Associations_list)):
                axes_dim = Associations_list[i]
                GO_dim   = len(set([item for sublist in axes_dim.values() for item in sublist]))
                total_ind.append(GO_dim)
            
            
            axes.plot(dimensions, [round(i*100/Total,2) for i in total_ind], linewidth=5.0)
            
            axes.set_xticks(dimensions)
            axes.spines['right'].set_visible(False)
            axes.spines['top'].set_visible(False)
            axes.spines['left'].set_linewidth(4)
            axes.spines['bottom'].set_linewidth(4)
            axes.spines['left'].set_color("grey")
            axes.spines['bottom'].set_color("grey")  
            axes.set_title(f'{org.capitalize()} {matrix}', 
            fontweight="bold", fontsize = 17, y = 1) 
            
            fig.savefig(f'{Axes_Annot_Path}Percentage_GO_{org}_{matrix}_BP.png', format="png", dpi=600,
                        bbox_inches='tight')

def Axes_Taxons_Across_Dimensions(Species, matrix_list, dimensions, annotations):
    
    
    '''
    This has to be changed for all the species and embeddings.
    '''
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Cosine     = "/media/sergio/sershiosdisk/Axes_Species/Optimal_Dimensionality/"
    Annot_path      = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
   
    # Load the taxons:
   
    Taxons_Dic      = json.load(open("/media/sergio/sershiosdisk/Human/Axes/GO_Taxons_Whole_Annotations.json"))                    
    counts          = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
    GO              = list(Taxons_Dic.keys())                    
    counts_db       = pd.DataFrame({"GO" : GO, "Count" : counts})
    counts_db.index = counts_db.GO 
    
    # Load the name of the GO terms:
    
    for matrix in matrix_list:
    
        for org in Species:
            
            if org == "Human":
                
                # Get info for the specie:
                
                if annotations == "BP_Back":
                    BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
                    cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_48_PPMI.csv',  index_col = 0)
                else:
                    BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
                    cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_Deepwalk.csv',  index_col = 0)
            
            else:
                
                if annotations == "BP_Leaf":
                    BP              = json.load(open(f'{Annot_path}_BP_Back_{org}.json'))
                    cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_20_PPMI.csv',  index_col = 0)
                else:
                    BP              = json.load(open(f'{Annot_path}_BP_Leaf_{org}.json'))
                    cosine          = pd.read_csv(f'{Path_Cosine}Cosine_{org}_50_Deepwalk.csv',  index_col = 0)              
            
        
            annotation_list = [name for sublist in BP.values() for name in sublist]
            occurance_of_each_annotation_in_network = Counter(annotation_list)
            terms_filtering = [key for key,values in occurance_of_each_annotation_in_network.items() if values >= 3]
            cosine          =  cosine.loc[terms_filtering, terms_filtering]
          
            counts_db = counts_db[counts_db.index.isin(cosine.index)]
              
            # Load the dictionaries:
           
            Associations_list = []
           
            for dim in dimensions:
               
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}.json')) 
                Associations_list.append(Axes)
            
            # Delete empty axes:
            
            for dicti in range(len(Associations_list)):   
                Associations_list[dicti] ={k: v for k, v in Associations_list[dicti].items() if v}
            
            # Start the experiment:
            
            Experiment_db = pd.DataFrame(0, columns=dimensions, index = list(set(counts_db.Count))) 
            
            for i in range(len(Associations_list)):
                
                dim      = dimensions[i]
                axes_dim = Associations_list[i]
                GO_dim   = [item for sublist in axes_dim.values() for item in sublist]
                
                for tax in list(set(counts_db.Count)):
                    
                    counts_tax = list(counts_db[counts_db.Count == tax].GO )
                    total      = len(counts_tax)
                    total_axis = len(set(GO_dim).intersection(set(counts_tax)))
                    
                    percentage = (total_axis/total)*100 
                    
                    Experiment_db.loc[tax, dim] = percentage
                    
            # Save the information: 

            Experiment_db.to_csv(f'{Axes_Annot_Path}Percentage_Taxons_Captured_{org}_{matrix}_{annotations}.csv',index=True)                 
                    
def plot_Heat_map(Species, matrix_list):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    for matrix in matrix_list:
        for org in Species:            
            Table = pd.read_csv(f'{Axes_Annot_Path}Percentage_Taxons_Captured_{org}_{matrix}.csv',index_col=0)
        
            # Do the plot:
            
            fig, ax = plt.subplots(figsize=(10,15))
            
            sns.heatmap(Table/100, cmap="jet", vmin=0, vmax=1, linewidths = 0.6, annot=True,cbar_kws={'shrink': 0.6})
            ax.set_title(f'{org.capitalize()} {matrix}',fontweight="bold", fontsize = 17, y = 1)
            fig.savefig(f'{Axes_Annot_Path}HeatMap_{org}_{matrix}_BP.png', format="png", dpi=200,
                        bbox_inches='tight')


def Calculate_Fold_avg(set_tax):
    
    result = pd.DataFrame()
    for i in set_tax.index:
        
        set_tax_it = set_tax.loc[i]
        set_tax_it = set_tax_it/set_tax_it.loc[str(set_tax_it.index[0])]
        result     = result.append(set_tax_it)
      
    result.replace([np.inf, -np.inf], np.nan, inplace=True)
    final  = result.mean() 
    final  = final.loc[set_tax.columns]
    
    return(final)
        

def plot_Fold_enrichment(Species, matrix_list, annotations):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    save_path       = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    for matrix in matrix_list:
        
        for org in Species:            
            Table = pd.read_csv(f'{Axes_Annot_Path}Percentage_Taxons_Captured_{org}_{matrix}_{annotations}.csv',index_col=0)
            
            # Divide the generic, specific and background:
            
            specific   = Table.head(3)
            generic    = Table.tail(3)
            background = Table.iloc[3:17]
            
            # Set the folds:
            
            specific_fold   = Calculate_Fold_avg(specific)
            generic_fold    = Calculate_Fold_avg(generic)
            background_fold = Calculate_Fold_avg(background)
            
            fig, axs = plt.subplots(1, 1, figsize=(12,5))
            
            fig.suptitle(f'{org}', fontsize=16)
            
            axs.plot(specific_fold,     marker='o',  linewidth =3)
            axs.plot(generic_fold,      marker='o',  linewidth =3)
            axs.plot(background_fold,   marker='o',  linewidth =3)
        
            axs.set_xticks(Table.columns)
            axs.spines['right'].set_visible(False)
            axs.spines['top'].set_visible(False)
            axs.spines['left'].set_linewidth(4)
            axs.spines['bottom'].set_linewidth(4)
            axs.spines['left'].set_color("grey")
            axs.spines['bottom'].set_color("grey")
            axs.tick_params(axis='both', labelsize=15) 
            axs.set_ylabel("Fold", fontsize=20, fontweight='bold') 
            axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold') 
            #axs.set_xticklabels([f'{Table.columns[0]} - {i}' for i in list(Table.columns)])
            
            fig.legend(["Specific", "Generic", "Background"] ,prop={'size': 16}, frameon=True, ncol = 3) 
            
            fig.savefig(f'{save_path}Fold_{org}_Species.png', format="png", dpi=600,
                    bbox_inches='tight') 

def jaccard_similarity(A, B):
    #Find intersection of two sets
    nominator = A.intersection(B)

    #Find union of two sets
    denominator = A.union(B)

    #Take the ratio of sizes
    similarity = len(nominator)/len(denominator)
    
    return similarity


def JI(Axes_association):
    
    Results_Jaccard = pd.DataFrame(0, index=np.arange(len(Axes_association)), columns=np.arange(len(Axes_association)))
    
    for cluster in range(len(Axes_association)):
        cluster_it_1 = Axes_association[cluster]
        for cluster_2 in range(len(Axes_association)):
            cluster_it_2 = Axes_association[cluster_2]
            
            jaccard = jaccard_similarity(set(cluster_it_1), set(cluster_it_2))
            
            if cluster == cluster_2:
                Results_Jaccard.loc[cluster,cluster_2] = 0
            else:
                Results_Jaccard.loc[cluster,cluster_2] = jaccard 
    
    # Take the max JI avg:
    
    final = np.mean(Results_Jaccard.max())
    
    return(final)
    
                 
       
def Get_Matrix_Jaccard_Between_Axes(Axes_association,plot = False, all_jacc = False):
    
    # DataFrame for the results:
    
    Results_Jaccard = pd.DataFrame(0, index=np.arange(len(Axes_association)), columns=np.arange(len(Axes_association)))
    
    # Iterate per each cluster:
    
    for cluster in range(len(Axes_association)):
        
        # Get the first axis:
        
        cluster_it_1 = Axes_association[cluster]
        
        for cluster_2 in range(len(Axes_association)):
            
            # Get the second axis:
            
            cluster_it_2 = Axes_association[cluster_2]
            
            # Calculate the jaccard index:
            
            jaccard = REVIGO.Jaccar_index(cluster_it_1, cluster_it_2)
            
            # Save the results in an external variable:
            
            if cluster == cluster_2:
                Results_Jaccard.loc[cluster,cluster_2] = 0
            else:
                Results_Jaccard.loc[cluster,cluster_2] = jaccard
    
    # Plot the Jaccard index for visual pruposes:

    min_max = Results_Jaccard.max()
    
    if plot == True:
    
        REVIGO.Plot_Jaccard_Overlap(Results_Jaccard)
    
    # Get similar pair of axes or the whole upper diagonal values of the jaccard matrix:
    
    if all_jacc == False:
        pairs = REVIGO.Get_Similar_Axes_Pair_list(Results_Jaccard)
        return(pairs)
    else:
        #pairs = REVIGO.Get_Similar_Axes_Pair_list(Results_Jaccard, all_jacc)
        #return(pairs)
        return(min_max)


def Jaccard_Test(Species, matrix_list, dimensions, common = True):
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    fuera = []
    for matrix in matrix_list: 
        for org in Species:
            
            # Load the information:
            itera = []
            for dim in dimensions:
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json'))
                Axes = {k: v for k, v in Axes.items() if v}
                Axes = list(Axes.values())
                
                # Calculate the Jaccards:
                
                joaquin = JI(Axes)
                itera.append(joaquin)
            fuera.append(itera)


def Jaccard_Line_Plot(Species, matrix_list, dimensions, common = True):
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    Data = pd.DataFrame()
    
    for matrix in matrix_list: 
        for org in Species:
            
            
            if org == "Human":
                dimensions = [48, 96, 144, 192, 240, 288, 500, 800, 1000]
            else:
                dimensions = [20, 40, 60, 80, 100, 150, 200, 250, 300]
                
            
            # Get the common set of GO terms if needed for the comparison:
            
            if common == True: 
                 list_axes = []
                 for dim in dimensions:                 
                     Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json'))
                     Axes = {k: v for k, v in Axes.items() if v}
                     Axes = [item for sublist in Axes.values() for item in sublist]
                     Axes = list(set(Axes))
                     list_axes.append(Axes) 
                 common_set = set.intersection(*[set(list) for list in list_axes])
            
            # Then we do the Jaccard index in each of the dimensional spaces:
            
            list_distribution = []
            avg_GO            = []
            
            for dim in dimensions:
                             
                # Load the axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json'))
                # Delete empty axes:
                
                Axes = {k: v for k, v in Axes.items() if v}
                Axes = Axes.values()
                
                avg_GO.append(np.mean([len(i) for i in Axes]))
                
                # If needed, only take the common set of GO terms
                
                if common == True:
                    Axes = [x for x in Axes if x[0] in common_set]
                    pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True)
                    list_distribution.append(pairs_axes)
                else:
                    Axes = list(Axes)
                    pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True)
                    list_distribution.append(pairs_axes)
            
            # Take the mean of the JI:
                            
            line = [np.mean(i) for i in list_distribution] 
            line = np.mean(line)
             
            # Take the mean of GO associated:
             
            GO = np.mean(avg_GO)
             
            # Update the DataFrame:
             
            Data_it = pd.DataFrame({"GO" : [GO], "JI" : [line]})
            Data_it["Specie"] = [org]
            Data_it["Matrix"] = [matrix]
            
            Data = Data.append(Data_it) 
            
    # Plot Avg JI:
        
    x     =  np.arange(len(Species))
    width = 0.20
    sum_it = 0
    
    fig, axs = plt.subplots(1, 1, figsize=(10,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Specie").mean()
        std  = Data[Data.Matrix == matrix].groupby("Specie").std()
        
        plt.bar(x + sum_it, mean["JI"], width, label='%Clusters', yerr =std["JI"], capsize = 10)
        sum_it = sum_it+0.20 
     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Mean Max Jaccard Index", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    plt.xticks(x+width,list(mean.index), fontsize=15)
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))
    
    # % GO:
    
    x     =  np.arange(len(Species))
    width = 0.20
    sum_it = 0
    
    fig, axs = plt.subplots(1, 1, figsize=(10,5)) 
    
    for matrix in matrix_list:
        
        mean = Data[Data.Matrix == matrix].groupby("Specie").mean()
        std  = Data[Data.Matrix == matrix].groupby("Specie").std()
        
        plt.bar(x + sum_it, mean["GO"], width, label='%GO', yerr =std["GO"], capsize = 10)
        sum_it = sum_it+0.20 
        
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Avg GO", fontsize=20, fontweight='bold')  
    axs.set_ylim(0, None)
    plt.xticks(x+width,list(mean.index), fontsize=15)
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))

        
def Avg_GO_Across_Dim(Species, matrix_list, dimensions, annotations, associations = ""):
       
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    Data = pd.DataFrame()
    
    # We take the avg interesection between the axes:
    
    
    for matrix in matrix_list: 
        for org in Species:  
            
             for dim in dimensions:
                             
                # Load the axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{associations}.json'))
                
                # Delete empty axes:
                
                Axes = {k: v for k, v in Axes.items() if v}
                Axes = Axes.values()                
                Axes = list(Axes)
                
                avg_GO = np.mean([len(i) for i in Axes])
                test_it = [len(i) for i in Axes]
 
                # Update the DataFrame:
             
                Data_it = pd.DataFrame({"Avg":[avg_GO]})
                Data_it["Specie"] = [org]
                Data_it["Matrix"] = [matrix] 
                Data_it["Max"]    = [max(test_it)]
                Data = Data.append(Data_it)
    
    # Now we start the plot:
    
            
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
   
    fig, axs = plt.subplots(1, 1, figsize=(9,3)) 
    fig.tight_layout(pad = 0.5)
     
    for matrix in matrix_list:
        
        y = Data[Data.Matrix == matrix]
        x = dimensions
        
        plt.plot(x, y.Avg,marker='o',  linewidth =3)
     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Average #GO", fontsize=20, fontweight='bold')  
    axs.set_xticks(dimensions)
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))    

def Avg_Axes_Across_Dim(Species, matrix_list, dimensions, annotations):
       
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    Data = pd.DataFrame()
    
    # We take the avg interesection between the axes:
    
    for matrix in matrix_list: 
        for org in Species:  
            
             for dim in dimensions:
                             
                # Load the axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}.json'))
 
                # Delete empty axes:
                
                Axes = {k: v for k, v in Axes.items() if v}
                Axes = Axes.values()                
                Axes = list(Axes)
                
                # Flattern the list:
                
                Axes = [item for sublist in Axes for item in sublist]
                
                # Count number of repetitions:
                
                Count    = Counter(Axes)
                avg_Axes = np.mean(list(Count.values()))
 
                # Update the DataFrame:
             
                Data_it = pd.DataFrame({"Avg":[avg_Axes]})
                Data_it["Specie"] = [org]
                Data_it["Matrix"] = [matrix]           
                Data = Data.append(Data_it)
    
    # Now we start the plot:
               
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')

    fig, axs = plt.subplots(1, 1, figsize=(9,3)) 
    fig.tight_layout(pad = 0.5)
     
    for matrix in matrix_list:
        
        y = Data[Data.Matrix == matrix]
        x = dimensions
        
        plt.plot(x, y.Avg,marker='o',  linewidth =3)
     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Average #Axes per GO", fontsize=20, fontweight='bold')  
    axs.set_xticks(dimensions)
    
    fig.legend( matrix_list ,prop={'size': 16}, frameon=True, ncol = len(matrix_list))


def plot_mean_SS_line(Species, matrix_list, dimensions, annotations, associations = ""):

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    Data = pd.DataFrame()
  
    for org in Species:           
        
        for matrix in matrix_list:
                          
            # Load the dictionary:
                            
            file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes_{annotations}{associations}.json', "r") 
                
            info  = json.load(file)
            file.close()
            
            # Generate the lists to plot:
            
            mean_intra   = []
            mean_inter   = []
            random_intra = []
                
            for dim in dimensions:
                    
            # Build the key:
                    
                key2 = f'{dim}_Intra'
                key1 = f'{dim}_Inter'
                key3 = f'{dim}_Intra_Random'
                    
                Intra_SS  = info[key2] 
                Inter_SS  = info[key1] 
                Random_SS = info[key3]
                    
                Intra_SS  = np.mean([x for x in Intra_SS if str(x) != 'nan'])
                Inter_SS  = np.mean([x for x in Inter_SS if str(x) != 'nan'])
                Random_SS = np.mean([x for x in Random_SS if str(x) != 'nan'])
                
                mean_intra.append(Intra_SS)
                mean_inter.append(Inter_SS)
                random_intra.append(Random_SS)
                    
                Data_it = pd.DataFrame({"SS" : [Intra_SS], "SS_inter" : [Inter_SS], "SS_random" : [Random_SS]})
                
                Data_it["Specie"] = [org]
                Data_it["Matrix"] = [matrix]
            
                Data = Data.append(Data_it) 
            
    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
   
    fig, axs = plt.subplots(1, 1, figsize=(10,4)) 
    fig.tight_layout(pad = 0.5)
           
    y = Data[Data.Matrix == "PPMI"]
    x = dimensions
        
    plt.plot(x, y.SS,      marker='o',  linewidth =3)
    plt.plot(x, y.SS_inter,marker='o',  linewidth =3)

     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Semantic Similarity", fontsize=20, fontweight='bold')  
    axs.set_xticks(dimensions)
    axs.set_xlabel("Dimensions", fontsize=20, fontweight='bold')
    
    plt.legend(labels= ["intra-SeSi", "inter-SeSi"],
                   borderaxespad=0.1,bbox_to_anchor=(-2, 0), frameon=True, ncol = 2,prop={'size': 20})   


def Jaccard_Disit_Across_Dim(Species, matrix_list, dimensions, annotations, common = False):

    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    list_distribution_final = []
    
    
    for matrix in matrix_list: 
        for org in Species:
            
            # Get the common set of GO terms if needed for the comparison:
            
             if common == True: 
                 list_axes = []
                 for dim in dimensions:                 
                     Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}.json'))
                     Axes = {k: v for k, v in Axes.items() if v}
                     Axes = [item for sublist in Axes.values() for item in sublist]
                     Axes = list(set(Axes))
                     list_axes.append(Axes) 
                 common_set = set.intersection(*[set(list) for list in list_axes])
            
            # Then we do the Jaccard index in each of the dimensional spaces:
            
             list_distribution = []
            
             for dim in dimensions:
                 
                print(f'{matrix} - {org} - {dim}-d')
                             
                # Load the axes:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}.json'))
                # Delete empty axes:
                
                Axes = {k: v for k, v in Axes.items() if v}
                Axes = Axes.values()
                
                # If needed, only take the common set of GO terms
                
                if common == True:
                    Axes = [x for x in Axes if x[0] in common_set]
                    pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True)
                    list_distribution.append(pairs_axes)
                else:
                    Axes = list(Axes)
                    pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True)
                    list_distribution.append(pairs_axes)
                    
             line = [np.mean(i) for i in list_distribution]
             list_distribution_final.append(line)

            
            # Plot the distribution:
            
             plt.style.use("seaborn-whitegrid")
             plt.rcParams.update({'font.size': 17})
             plt.rc('font', weight='bold')
                
             fig, axes = plt.subplots(1, 1, figsize=(9,5))
             fig.tight_layout(pad = 0.5)
               
             fig.text(-0.001, 0.5, 'Max Jaccard Index', ha='center', va='center', 
                     rotation='vertical', fontsize=17, fontweight = "bold") 
             fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                     rotation='horizontal', fontsize=17, fontweight = "bold")
               
             #axes.boxplot(list_distribution, whis = 4,showmeans=True)
             axes.boxplot(list_distribution,showmeans=True)
             
             line = [np.mean(i) for i in list_distribution]
             x    = np.arange(len(list_distribution)) +1
             axes.plot(x,line,marker='o',  linewidth =3)
             
             list_distribution_final.append(line)
                
             axes.set_xticklabels(dimensions)
             axes.spines['right'].set_visible(False)
             axes.spines['top'].set_visible(False)
             axes.spines['left'].set_linewidth(4)
             axes.spines['bottom'].set_linewidth(4)
             axes.spines['left'].set_color("grey")
             axes.spines['bottom'].set_color("grey")  
             axes.set_title(f'{org.capitalize()} {matrix}', 
             fontweight="bold", fontsize = 17, y = 1) 
             
             fig.savefig(f'{Axes_Annot_Path}Max_Jaccard_{org}_{matrix}_BP.png', format="png", dpi=200,
                            bbox_inches='tight')




def Correlation_Axes(Species, matrix_list, dimensions, common = True):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    for org in Species:
        
        # Get the common set of GO terms if needed for the comparison:
        
         if common == True: 
             list_axes = []
             for dim in dimensions:                 
                 Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_PPMI_{dim}.json'))
                 Axes = {k: v for k, v in Axes.items() if v}
                 Axes = [item for sublist in Axes.values() for item in sublist]
                 Axes = list(set(Axes))
                 list_axes.append(Axes) 
             common_set = set.intersection(*[set(list) for list in list_axes])
        
        # Then we do the Jaccard index in each of the dimensional spaces:
        
         list_distribution = []
        
         for dim in dimensions:
                         
            # Load the axes:
            
            Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_PPMI_{dim}.json'))
            
            # Delete empty axes:
            
            Axes = {k: v for k, v in Axes.items() if v}
            Axes = Axes.values()
            
            # If needed, only take the common set of GO terms
            
            if common == True:
                Axes = [x for x in Axes if x[0] in common_set]
            else:
                continue
            
            # Calculate the Jaccard index matrix:
            
            pairs_axes = Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True)
            #pairs_axes = list(Get_Matrix_Jaccard_Between_Axes(Axes,  plot = False, all_jacc = True).Value)
            list_distribution.append(pairs_axes)
        
        # Once we have the distributions, we start with the plot:
        
         list_group = []
    
         for l in range(len(list_distribution)):
        
            list_itera = len(list_distribution[l])
            list_group.append([dimensions[l]] * list_itera)
            
         list_distribution = [item for sublist in list_distribution for item in sublist]
         list_group        = [item for sublist in list_group for item in sublist]
        
         Data_Plot = pd.DataFrame({"Standardized Score" : list_distribution, "Dimensions" : list_group})
         Data_Plot.Jaccard = Data_Plot.groupby('Standardized Score').transform(lambda x: (x - x.mean()) / x.std())
        
         new_names = []
        
         for dimensions in Data_Plot.Dimensions:
            
             string_line = f'{dimensions}-d {org} NMTF'
             new_names.append(string_line)
        
         Data_Plot.Dimensions = new_names 

         color = ['#f94144','#f3722c','#f8961e','#f9c74f', '#90be6d', "#43aa8b", "#4d908e", "#577590", "#277da1"]           
                
        # Global variables for ploting:
        
         sns.set(font_scale=1.5)
         sns.set_style("whitegrid")
         plt.rc('font', weight='bold')
         plt.rc('xtick',labelsize=14)
         plt.rc('ytick',labelsize=14)  

         g = sns.FacetGrid(Data_Plot, #the dataframe to pull from
                      row="Dimensions", #define the column for each subplot row to be differentiated by
                      hue="Dimensions", #define the column for each subplot color to be differentiated by
                      aspect=3, #aspect * height = width
                      height=2,
                      palette= color
                     )
        
        # Plot the distributions:
        
         g.map(sns.kdeplot, "Standardized Score", shade=True, alpha=1, lw=1.5, bw=0.2)
         g.map(sns.kdeplot, "Standardized Score", lw=4, bw=0.2)
        
        # Delete the down spin
         g.despine(bottom=True, left=False)
        
        # Change the thickness of the plos:
        
         col_order   = list(Data_Plot.Dimensions.unique())
         for ax, title in zip(g.axes.flat, col_order):
             for axis in ['top','bottom','left','right']:
                 ax.spines[axis].set_linewidth(5)
            
        # Add the labels:
        
         labels      = ["A)", "B)", "C)", "D)", "E)", "F)", "G)", "H)", "K)"]
         col_order   = list(Data_Plot.Dimensions.unique())
         counter_lab = 0
         for ax, title in zip(g.axes.flat, col_order):
             ax.set_title(title)
             ax.text(0.2, 74,labels[counter_lab], fontsize=15)
             counter_lab+=1          


def Calculate_Overlapping_DataFrame(Axes_1, Axes_2):
    
    Over_pd = pd.DataFrame(0, columns=list(Axes_1.keys()), index = list(Axes_2.keys()))
    
    for bb1 in list(Axes_1.keys()):
        for bb2 in list(Axes_2.keys()):
            GO1 = Axes_1[bb1]
            GO2 = Axes_2[bb2]
            
            overlapp = overlap_coefficient(GO1, GO2)
            Over_pd.loc[bb2, bb1] = overlapp
    
    return(Over_pd)
       
            
def Maximum_Mapping(Species, dimensions, matrix_list, comparison_list, common = True):
    
    # Paths:

    assoc_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/" 
    
    for matrix in matrix_list:
        for org in Species:
            
            Associations_list = []
            
            for dim in dimensions:
                
                # Load the corresponding dimensional axes:
                
                Axes = json.load(open(f'{assoc_path}Associations_{org}_{matrix}_{dim}.json')) 
                Associations_list.append(Axes)
            
            # If common is true:
            
            if common == True:
                list_axes = []
                for dim in dimensions:                 
                    Axes = json.load(open(f'{assoc_path}Associations_{org}_{matrix}_{dim}.json'))
                    Axes = {k: v for k, v in Axes.items() if v}
                    Axes = [item for sublist in Axes.values() for item in sublist]
                    Axes = list(set(Axes))
                    list_axes.append(Axes) 
                common_set = set.intersection(*[set(list) for list in list_axes])
                
                Axes_info_b = [] 
                for dim in range(len(Associations_list)):
                    axes = Associations_list[dim]
                    for axis in list(axes.keys()):
                        GO_1 = list(set(axes[axis]).intersection(set(common_set)))
                        axes[axis] = GO_1
                    Axes_info_b.append(axes)
                Associations_list = Axes_info_b 
                
            # Delete the empty parts of the dictionary:
            
            for dicti in range(len(Associations_list)):   
                Associations_list[dicti] ={k: v for k, v in Associations_list[dicti].items() if v}
            
            # Start the comparisons:
            
            distance = []
            
            for i in range(len(comparison_list)):
                            
                comp = comparison_list[i]
                comp1 = comp.split("-")[0]
                comp2 = comp.split("-")[1]
                
                position_comp1 = dimensions.index(int(comp1))
                positon_comp2  = dimensions.index(int(comp2))
                
                Axes_1 = Associations_list[position_comp1]
                Axes_2 = Associations_list[positon_comp2]
                
                Overlapping   = Calculate_Overlapping_DataFrame(Axes_1, Axes_2)
                
                Overlapping_1 = sum(Overlapping.T.max())
                Overlapping_2 = sum(Overlapping.max())
                
                value =(Overlapping_1 + Overlapping_2)/(len(Overlapping.index) + len(Overlapping.columns) )
                distance.append(value)
            
            # Plot the differences:
            
            plt.style.use("seaborn-whitegrid")
            plt.rcParams.update({'font.size': 17})
            plt.rc('font', weight='bold')
            
            fig, axes = plt.subplots(1, 1, figsize=(9,5))
            fig.tight_layout(pad = 0.5)
        
            fig.text(-0.07, 0.5, 'Similarity', ha='center', va='center', 
                 rotation='vertical', fontsize=17, fontweight = "bold")  
        
            fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=17, fontweight = "bold")
            
            axes.plot(list(range(len(comparison_list))),distance, linewidth=5.0)
            
            axes.set_xticks(list(range(len(comparison_list))))
            axes.set_xticklabels(comparison_list)
            axes.spines['right'].set_visible(False)
            axes.spines['top'].set_visible(False)
            axes.spines['left'].set_linewidth(4)
            axes.spines['bottom'].set_linewidth(4)
            axes.spines['left'].set_color("grey")
            axes.spines['bottom'].set_color("grey")  
            axes.set_title(f'{org.capitalize()} {matrix}', 
            fontweight="bold", fontsize = 17, y = 1) 
            
            if common == True:
            
                fig.savefig(f'{assoc_path}Common_Best_Match_{org}_{matrix}_BP.png', format="png", dpi=200,
                            bbox_inches='tight')
            else:
            
                fig.savefig(f'{assoc_path}Best_Match_{org}_{matrix}_BP.png', format="png", dpi=200,
                            bbox_inches='tight')            
            

def Plot_Intra_Inter_Axes(Species, matrix):
    
    # Paths:

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    for org in Species:           
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 500, 800, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 250, 300] # Add 200       
        
        # Start the plot:
          
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 17})
        plt.rc('font', weight='bold')
            
        fig, axes = plt.subplots(1, 1, figsize=(10,3))
        fig.tight_layout(pad = 0.5)
        
        fig.text(-0.008, 0.5, 'Semantic Similarity', ha='center', va='center', 
                 rotation='vertical', fontsize=20, fontweight = "bold")  
        
        fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=20, fontweight = "bold")
        
                  
        # Load the dictionary:
                        
        file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes_{annotations}.json', "r") 
            
        info  = json.load(file)
        file.close()
            
        list_intra = []
        list_inter = []
            
        # Generate the lists to plot:
            
        for dim in dimensions:
                
        # Build the key:
                
            key1 = f'{dim}_Inter'
            key2 = f'{dim}_Intra'
                
            Inter_SS = info[key1]
            Intra_SS = info[key2] 
                
            Intra_SS = [x for x in Intra_SS if str(x) != 'nan']
            Inter_SS = [x for x in Inter_SS if str(x) != 'nan']
                
            list_intra.append(Intra_SS)
            list_inter.append(Inter_SS)
                              
            # Test the distributions:
                
            p_value = mannwhitneyu(Inter_SS, Intra_SS, alternative = "less").pvalue
            print(matrix, p_value)
            
        # continue with the plot:
        
        bpl = axes.boxplot(list_inter, positions=np.array(range(len(list_inter)))*2.0-0.4, sym='', widths=0.9)
        bpr = axes.boxplot(list_intra, positions=np.array(range(len(list_intra)))*2.0+0.4, sym='', widths=0.9)
        axes.xaxis.set_major_formatter(NullFormatter())
             
        set_box_color(bpl, '#D7191C')
        set_box_color(bpr, '#2C7BB6')
                          
        axes.spines['right'].set_visible(False)
        axes.spines['top'].set_visible(False)
        axes.spines['left'].set_linewidth(4)
        axes.spines['bottom'].set_linewidth(4)
        axes.spines['left'].set_color("grey")
        axes.spines['bottom'].set_color("grey") 
        
        axes.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
       
        patch_list = []
        color      = ['#D7191C', '#2C7BB6']
        label      = ["Inter", "Intra"]
        
        for patch in range(2):           
            patch_list.append(mpatches.Patch(color= color[patch], label= label[patch]))
                
        # Plot the legend:
    
        plt.legend(handles=patch_list,borderaxespad=0.1,
                   bbox_to_anchor=(0.5, -0.5),
                       loc="lower center", frameon=True, fontsize=17, ncol =2)           

                        
def Plot_Intra_Inter_Axes_Avg(Species, matrix_list):
    
    # Paths:

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
    
    for org in Species:           
        if org == "Human":
            dimensions = [48, 96, 144, 192, 240, 288, 500, 800, 1000]
        else:
            dimensions = [20, 40, 60, 80, 100, 150, 250, 300] # Add 200       
        
        # Start the plot:
          
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 17})
        plt.rc('font', weight='bold')
            
        fig, axes = plt.subplots(1, 1, figsize=(10,3))
        fig.tight_layout(pad = 0.5)
        
        fig.text(-0.008, 0.5, 'Semantic Similarity', ha='center', va='center', 
                 rotation='vertical', fontsize=20, fontweight = "bold")  
        
        fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=20, fontweight = "bold")

        intra_mean_fin = []
        intra_std_fin  = []
        inter_mean_fin = []
        inter_std_fin  = []  

        for matrix in matrix_list:
        
            # Load the dictionary:
                            
            file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes.json', "r") 
                
            info  = json.load(file)
            file.close()

            # Generate the lists to plot:
            
            list_intra = []
            list_inter = []
                
            for dim in dimensions:
                    
            # Build the key:
                    
                key1 = f'{dim}_Inter'
                key2 = f'{dim}_Intra'
                    
                Inter_SS = info[key1]
                Intra_SS = info[key2] 
                    
                Intra_SS = [x for x in Intra_SS if str(x) != 'nan']
                Inter_SS = [x for x in Inter_SS if str(x) != 'nan']
                    
                list_intra.append(np.mean(Intra_SS))
                list_inter.append(np.mean(Inter_SS))
        
            # Get the information for the bar plots:
        
            intra_mean = np.mean(list_intra)
            intra_std  = np.std(list_intra)
            inter_mean = np.mean(list_inter)
            inter_std  = np.std(list_inter) 
            
            intra_mean_fin.append(intra_mean)
            intra_std_fin.append(intra_std)
            inter_mean_fin.append(inter_mean)
            inter_std_fin.append(inter_std)          
            
        # Generate the data frame for the plot:
        
        x = np.arange(len(matrix_list))
        width = 0.25

        fig, axs = plt.subplots(1, 1, figsize=(10,5)) 
        bar1 = axs.bar(x, intra_mean_fin, width, label='Intra', yerr =intra_std_fin, capsize = 10)  
        bar2 = axs.bar(x + width, inter_mean_fin, width, label='Inter', yerr =inter_std_fin, capsize = 10, color = "red")
             
        plt.xticks(x+width,matrix_list, fontsize=15)
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=15) 
        axs.set_ylabel("Semantic Similarity", fontsize=20, fontweight='bold')
        
        fig.legend( (bar1, bar2), ('Intra', 'Inter') ,prop={'size': 16},
                   loc="upper right", frameon=True)

                        
def Plot_Intra_Inter_Axes_Avg_Species(Species, matrix_list, dimensions, annotations):
    
    # Paths:

    Path_Clust      = "/media/sergio/sershiosdisk/Axes_Species/Clustering_GOGO/"
       
    final_intra_avg = []
    final_intra_std = []    
    final_inter_avg = []    
    final_inter_std = []

    for matrix in matrix_list:
        
        final_intra_avg_it = []
        final_inter_avg_it = []
                
        for org in Species:
            
            # Start getting the information:
            
            file  = open(f'{Path_Clust}Intra_Inter_Cluster_SS_{org}_{matrix}_Axes_{annotations}.json', "r") 
            info  = json.load(file)
            file.close()
            
            # Loop for all the dimensions:

            list_intra = []
            list_inter = []
                            
            for dim in dimensions:
                                                
                # Build the key:
                                
                key1 = f'{dim}_Inter'
                key2 = f'{dim}_Intra'
                                
                Inter_SS = info[key1]
                Intra_SS = info[key2] 
                                
                Intra_SS = [x for x in Intra_SS if str(x) != 'nan']
                Inter_SS = [x for x in Inter_SS if str(x) != 'nan']
                                
                list_intra.append(np.mean(Intra_SS))
                list_inter.append(np.mean(Inter_SS))            
            
            intra_mean = np.mean(list_intra)
            inter_mean = np.mean(list_inter)  
            
            # Take the averge for the specific species
            
            final_intra_avg_it.append(intra_mean)
            final_inter_avg_it.append(inter_mean)
        
        # Do the average and the std across species for a specific algorithm:
        
        final_intra_avg.append(np.mean(final_intra_avg_it))
        final_intra_std.append(np.std(final_intra_avg_it)) 
        final_inter_avg.append(np.mean(final_inter_avg_it))  
        final_inter_std.append(np.std(final_inter_avg_it))     
            
        # Plot:
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 17})
        plt.rc('font', weight='bold')
            
        fig, axs = plt.subplots(1, 1, figsize=(5,5))
        fig.tight_layout(pad = 0.5)
        
        #fig.text(-0.008, 0.5, 'Semantic Similarity', ha='center', va='center', 
                 #rotation='vertical', fontsize=20, fontweight = "bold")  
        
        fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=20, fontweight = "bold")
        
        x = np.arange(len(matrix_list))
        width = 0.25

        #fig, axs = plt.subplots(1, 1, figsize=(10,5)) 
        bar1 = axs.bar(x, final_intra_avg, width, label='Intra', yerr =final_intra_std, capsize = 10)  
        bar2 = axs.bar(x + width, final_inter_avg, width, label='Inter', yerr =final_inter_std, capsize = 10, color = "red")
             
        plt.xticks(x+width,matrix_list, fontsize=15)
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=15) 
        axs.set_ylabel("Semantic Similarity", fontsize=20, fontweight='bold')
        
        fig.legend( (bar1, bar2), ('Intra', 'Inter') ,prop={'size': 16},
                   loc="upper right", frameon=True)

def Calculate_Connections(Axes):
    
    final = []
    
    for i in Axes.keys():
        
        n           = len(Axes[i])
        calculation = (n*(n-1))/2
        final.append(calculation)
        
    return(sum(final)/len(Axes))
        
    
def Avg_Connections_Axes(Species, dimensions, matrix_list, annotations):
    
    # Paths:

    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
       
    for org in Species:           
        
        # Go to the different algorithms:
        
        final = []
        
        for matrix in matrix_list:
            
            # Calculate the connections:
            
            final_it = []
                 
            for dim in dimensions:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{dim}_{annotations}.json')) 
                                
                # Delete empty axes:
             
                Axes     = {k: v for k, v in Axes.items() if v}
                
                # Calculate the mean connections:
                
                final_it.append(Calculate_Connections(Axes))
            
            final.append(final_it)   
            
        # Plot:
                    
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 19})
        plt.rc('font', weight='bold')
            
        fig, axs = plt.subplots(1, 1, figsize=(8,3))
        fig.tight_layout(pad = 0.5)
        
        fig.text(0.5, 0.0, '# Dimensions', ha='center', va='center', 
                 rotation='horizontal', fontsize=20, fontweight = "bold") 

        for i in range(len(matrix_list)):            
            axs.plot(dimensions, np.log10(final[i]), linewidth = 3)           
            
        axs.spines['right'].set_visible(False)
        axs.spines['top'].set_visible(False)
        axs.spines['left'].set_linewidth(4)
        axs.spines['bottom'].set_linewidth(4)
        axs.spines['left'].set_color("grey")
        axs.spines['bottom'].set_color("grey")
        axs.tick_params(axis='both', labelsize=15) 
        axs.set_ylabel("Log Avg Connections", fontsize=20, fontweight='bold')
        
        fig.legend(labels= matrix_list,bbox_to_anchor=(0.5, -0.2),
                       loc="lower center",borderaxespad=0.1, frameon=True, ncol = len(matrix_list),prop={'size': 15})         

def Average_Shortest_Paths(G1, Axes):
    
    # Take a matrix:
       
    final = []

    for axis in Axes.keys():        
        GO_list  = Axes[axis]
        final_it = []
        
        for GO1 in range(len(GO_list)):
            for GO2 in range(GO1+1, len(GO_list)): 
                try:
                    final_it.append(nx.shortest_path_length(G1, GO_list[GO1],GO_list[GO2]))
                except nx.NodeNotFound:
                    continue  
        final.append(np.nanmean(final_it))
            
    return(final)
        
            
def Shortest_Paths(Species, matrix_list, dimensions, annotations, associations = ""):
    
    # Paths:
    
    Path_Semantic   = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    
    Data = pd.DataFrame()

    # Calculate the shortest paths:

    G              = graph.from_resource("go-basic")
    G1             = G.to_undirected()

    # Start with the calculations:
    
    # For each specie:
        
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    for matrix in matrix_list:
        for org in Species:
            
            # Load the associations:
            
            for dim in dimensions:   
                
                print(f'{org} - {matrix} - {dim}-d')
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{associations}.json'))           
                Axes = {k: v for k, v in Axes.items() if v}
                
                Avg = Average_Shortest_Paths(G1, Axes)
                
                Data_it = pd.DataFrame({"Shortest Paths" : [np.nanmean(Avg)]})
                Data_it["Specie"] = [org]
                Data_it["Matrix"] = [matrix]
            
                Data = Data.append(Data_it) 
    
    # Save shortest Paths:
    
    Data.to_csv(f'{Path_Semantic}Shortest_Paths_{annotations}_All_Species.csv')

    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
       
    fig, axs = plt.subplots(1, 1, figsize=(9,3)) 
    fig.tight_layout(pad = 0.5)
     
    for matrix in matrix_list:
        
        y = Data[Data.Matrix == matrix]
        x = dimensions
        
        plt.plot(x, y.SS,marker='o',  linewidth =3)
     
    axs.spines['right'].set_visible(False)
    axs.spines['top'].set_visible(False)
    axs.spines['left'].set_linewidth(4)
    axs.spines['bottom'].set_linewidth(4)
    axs.spines['left'].set_color("grey")
    axs.spines['bottom'].set_color("grey")
    axs.tick_params(axis='both', labelsize=15) 
    axs.set_ylabel("Shortest Paths", fontsize=20, fontweight='bold')  
    axs.set_xticks(dimensions)


def Perform_Distances_Axes(Species, matrix_list, dimensions, annotations, associations = ""):
    
    # Get the similarity of the Axes based on the distances of their associated annotations:
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    network_path    = "/media/sergio/sershiosdisk/Axes_Species/GO_Embeddings/"
    meaning_path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Load the Axes association data:
    # Iterate over all the differente conditions:
    
    for species in Species:
        for matrix in matrix_list:
            for dim in dimensions:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{species}_{matrix}_{annotations}_{dim}{associations}.json')) 
                Axes = {k: v for k, v in Axes.items() if v}
                
                # Load the GO embeddings:
                
                embeddings = pd.read_csv(f'{network_path}_GO_Embeddings_{annotations}_PPI_{species}_{dim}_{matrix}.csv', index_col = 0).T 
                embeddings[embeddings < 0] = 0
                embeddings_index           = embeddings.index
                embeddings         = np.array(embeddings)
                
                # Caculate the euclidean distances between the GO embeddings:
                
                distance_emb   = pairwise_distances(embeddings, metric="cosine")
                distance_emb_pd = pd.DataFrame(distance_emb,embeddings_index,embeddings_index)
                   
                # Start an empty matrix:
                
                Result_dist = pd.DataFrame(0, columns=list(Axes.keys()), index = list(Axes.keys()))
                
                axes = list(set(Axes.keys()))
                
                for i in range(len(axes)): 
                    axis1    = axes[i]
                    GO_axis1 = Axes[axis1]        
                    for j in range(i+1, len(axes)): 
                        axis2    = axes[j] 
                        GO_axis2 = Axes[axis2]
                        print(axis1, axis2)
                        
                        GO_embeddings_subset     = distance_emb_pd.loc[GO_axis1, GO_axis2]            
                        best_match               = (sum(GO_embeddings_subset.min()) + sum(GO_embeddings_subset.T.min()))
                        best_match               = best_match/(len(GO_embeddings_subset.min())+len(GO_embeddings_subset.T.min()))
                        
                        Result_dist.loc[axis1,axis2] = best_match
                        Result_dist.loc[axis2,axis1] = best_match  
                           
                # Save the results:
                
                Result_dist.to_csv(f'{meaning_path}Cosine_Distances_Embeddings_{species}_{matrix}_{dim}_{annotations}{associations}.csv')
                

def tfidf(word, sentence, corpus):
    tf = sentence.count(word)/len(sentence)
    idf = np.log10(len(corpus)/ sum([1 for doc in corpus if word in doc]))
    return round(tf*idf,4)

def Perform_IDF_Axes(Axes, go_dag):
    
    # Get the full vocabulary (it consists in the whole words that appears in at least one
    # of the associated GO definitions):
    
    vocabulary = []
    
    for i in Axes.keys():
        annotations = Axes[i]
        for GO in annotations:
            try:
                defin = go_dag[GO].name
                defin = np.char.replace(defin, "-", " ")
                defin = np.char.replace(defin, ",", " ")
                vocabulary.append(str(defin))
            except KeyError:
                continue
    
    # Preprocess the vocabulary:
        
    vocabulary = [i.split() for i in vocabulary]
    vocabulary = [item for sublist in vocabulary for item in sublist]
    vocabulary = [word for word in vocabulary if word.lower() not in nltk.corpus.stopwords.words('english')]
    vocabulary = [w for w in vocabulary if len(w) > 1]  
    vocabulary = set(vocabulary) 
    
    # Get the corpus (each axis have its own corpus of words):
  
    corpus = []
    
    for i in Axes.keys():
        annotations = Axes[i]
        corpus_it   = []
        for GO in annotations:
            try:
                defin = go_dag[GO].name
                defin = np.char.replace(defin, "-", " ")
                defin = np.char.replace(defin, ",", " ")
                corpus_it.append(str(defin))    
            except KeyError:
                continue
            
            corpus_it = ' '.join(word for word in corpus_it)
            corpus_it = corpus_it.split()
            corpus_it = [word for word in corpus_it if word.lower() not in nltk.corpus.stopwords.words('english')]
            corpus_it = [w for w in corpus_it if len(w) > 1] 
            
        corpus.append(corpus_it)   
    
    # Compute the TF-IDF:
    
    Final = pd.DataFrame(0, columns=list(Axes.keys()), index = list(vocabulary))
    
    for axis in range(len(Axes.keys())):        
        sentence = corpus[axis]       
        if len(sentence) > 0:
            for word in vocabulary:   
                Final.loc[word, Final.columns[axis]] = tfidf(word, sentence, corpus)
        else:
            continue  
    
    return(Final)  

def Axes_Meaning(Species, matrix_list, dimensions, annotations, associations = ""):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Path_Annotation = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Load the information of the ontology:
                
    GO_file = f'{Path_Annotation}go-basic.obo'
    go_dag  = obo_parser.GODag(GO_file)
    
    # Start the computations:
    
    for species in Species:
        for matrix in matrix_list:
            
            print(f'{species} - {matrix}')
            
            for dim in dimensions:
    
                # Load the Axes association data:
                
                Axes = json.load(open(f'{Axes_Annot_Path}Associations_{species}_{matrix}_{annotations}_{dim}{associations}.json')) 
                Axes = {k: v for k, v in Axes.items() if v}
                
                # Get the corresponding TF-IDF:
                
                Results = Perform_IDF_Axes(Axes, go_dag)
                
                # Save the Results:
                
                Results.to_csv(f'{Path_Meaning}Axes_Meaning_{species}_{dim}_{annotations}_{matrix}{associations}.csv')


def Give_Example_Axes(species, dim, annotations, matrix, axis, associations = ""):
    
    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"  
    
    # Load meaning of the axes:
    
    meaning = pd.read_csv(f'{Path_Meaning}Axes_Meaning_{species}_{dim}_{annotations}_{matrix}{associations}.csv', index_col = 0)
    
    # Choose the axis:
    
    meaning_axis = meaning[axis]
    meaning_axis = meaning_axis[meaning_axis > 0.0]
    
    print(meaning_axis)
    
def Generate_Suppl_Table_Axes(species, dim, annotations, matrix, associations = ""):
    
    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Taxons_path    = "/media/sergio/sershiosdisk/Human/Axes/"
    
    # Load meaning of the axes:
    
    meaning = pd.read_csv(f'{Path_Meaning}Axes_Meaning_{species}_{dim}_{annotations}_{matrix}{associations}.csv', index_col = 0)
    
    # Load the axes associations:
    
    Axes = json.load(open(f'{Axes_Annot_Path}Associations_{species}_{matrix}_{annotations}_{dim}{associations}.json'))
    
    # Load Taxons:
    
    Taxons_Dic = json.load(open(f'{Taxons_path}GO_Taxons_Whole_Annotations.json')) 
    
    # Load 
    
    # Start writting:

    with open(f'{Path_Meaning}Table_ASFAs_{species}_{dim}.txt', 'a') as the_file:
        
        # write the name of the columns:
        
        the_file.write(f'Axis\t')
        the_file.write(f'& Terms\t')
        the_file.write(f'& \#GO \t')
        the_file.write(f'& \GO_annot \t')
        the_file.write(f'& \ Taxons \n')            
                       
        for d in meaning.columns:
            
            meaning_d = meaning[d]
            meaning_d = meaning_d[meaning_d > 0.0]
            
            the_file.write(f'{d}\t')
            the_file.write(f'& ')
            
            for word in meaning_d.index:               
                the_file.write(f'{word}, ')
            the_file.write(f'\t')
            
            # Add the number of GO BP:
                
            the_file.write(f'& {len(Axes[d])}\t')
            
            # Add the name of the GO BP:
            
            the_file.write(f'& ')
            for GO in Axes[d]:    
                the_file.write(f'{GO} , ')
            the_file.write(f'\t')
            
            # Add the Taxons:
            
            GO_list = Axes[d]
            
            GO_list = list(set(GO_list).intersection(set(Taxons_Dic.keys())))

            Taxons  = [Taxons_Dic[i] for i in GO_list]

            Taxons  = set([item for sublist in Taxons for item in sublist])
            
            the_file.write(f'& ')
            for tx in Taxons:
                the_file.write(f'{tx}, ')
            the_file.write(f'\n')
    the_file.close() 
                
 
def Plot_Axes_Meaning_Examples(species, dim, annotations, matrix, associations = "", n_axes = 20):
    
    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    save            = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    # Load meaning of the axes:
    
    meaning = pd.read_csv(f'{Path_Meaning}Axes_Meaning_{species}_{dim}_{annotations}_{matrix}{associations}.csv', index_col = 0)

    # Randomly choose n nunmber of axes:
    
    Axes_random = random.choices(list(meaning.columns), k=n_axes)
    
    # For each axis select the top 5 words:
    
    Axes_random_list = []  
    for axis in Axes_random:
        Axes_random_list.append(meaning[axis].sort_values(ascending = False)[:5].index)
    
    # Plot the meaning of the axis:
    
    plt.style.use("default")
    plt.rcParams.update({'font.size': 17})
    plt.rc('font', weight='bold')
    
    fig, axs = plt.subplots(1, 1, figsize=(15,15)) 
    n_dots  = n_axes   # Number of axes
    angs    = np.linspace(0, 2*np.pi, n_dots) 
    cx, cy  = (0, 0)  
    ra      = 0.5   # radius  
    
    control = 0
    
    for ang in angs:
        # radius:
        ra_it = ra 
                    
        # Cancer distances:
                                         
        x = cx + ra_it*np.cos(ang)
        y = cy + ra_it*np.sin(ang)
        
        plt.plot([0,x], [0, y], color = "#0ee3b6", linewidth=7.0)
        plt.scatter(x,y, s=50, color = "red")
        
        write = list(Axes_random_list[control])
        write = '\n '.join(word for word in write)
            
        axs.annotate(
                f'{write}',
                xy=(x, y), xycoords='data',
                xytext=(x,y), textcoords='offset points', size=12, 
                bbox=dict(boxstyle="round4,pad=.5", fc="#e3ac0e"),
                arrowprops=dict(arrowstyle="->",
                                connectionstyle="angle,angleA=0,angleB=-90,rad=0"),
                                horizontalalignment='center', verticalalignment='center')
        control += 1    
       
    axs.spines['right'].set_visible(False)
    axs.spines['left'].set_visible(False) 
    axs.spines['top'].set_visible(False)
    axs.spines['bottom'].set_visible(False)
    axs.set_yticklabels(" ")
    axs.set_xticklabels(" ")  
    
    fig.savefig(f'{save}Examples_meaning_{species}_{dim}.svg', format="svg", dpi=600,
                    bbox_inches='tight') 
    
      
def Perform_Distances_Axes_IDF(Species, matrix_list, dimensions, annotations):
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"  
    
    # Load the TF-IDF:
    
    TF_IDF = pd.read_csv(f'{Path_Meaning}Axes_Meaning_{Species}_{dimensions}_{annotations}_{matrix_list}.csv', index_col = 0)
    
    # Calculate the cosine distances between the vectors:
    
    TF_IDF    = TF_IDF.T
    index     = TF_IDF.index
    TF_IDF_np = np.array(TF_IDF)
    
    TF_IDF_distance = pairwise_distances(TF_IDF_np, metric="cosine") 
    TF_IDF_distance = pd.DataFrame(TF_IDF_distance, index, index)   
    
    # Save the results:
    
    TF_IDF_distance.to_csv(f'{Path_Meaning}TF_IDF_Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv')
 
    
def Correlation_Jaccard(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"):
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Load the matrix with the distances:
    
    Distance = pd.read_csv(f'{Path_Meaning}{distance}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    Distance = Distance.mask(np.triu(np.ones(Distance.shape)).astype(bool)).stack().sort_values(ascending=True)        
       
    # Load the Axes information:
    
    Axes = json.load(open(f'{Axes_Annot_Path}Associations_Human_{matrix_list}_{annotations}_{dimensions}.json')) 
    Axes = {k: v for k, v in Axes.items() if v}    
    
    # Calculate the JI between the pairs:
    
    ji = []
      
    for i in Distance.index:
        x1 = i[0]
        x2 = i[1]        
        ji.append(Jaccar_index(Axes[str(x1)],Axes[str(x2)]))    
    
    # Plot the corresponding scatter:
    
    fig, axes = plt.subplots()
    plt.scatter(ji, Distance, alpha = 0.3)
    axes.set_ylabel("Jaccard Index", fontsize=20, fontweight='bold') 
    axes.set_xlabel("Distance", fontsize=20, fontweight='bold') 
    
    # Correlation:
    
    print(pearsonr(Distance, ji))  
    
    
def Correlation_Distances(Species, matrix_list, dimensions, annotations):
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    
    # Load the matrix with the distances:
    
    Distance_TF = pd.read_csv(f'{Path_Meaning}TF_IDF_Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    Distance_TF = Distance_TF.mask(np.triu(np.ones(Distance_TF.shape)).astype(bool)).stack().sort_values(ascending=True)        

    Distance_Space = pd.read_csv(f'{Path_Meaning}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    Distance_Space = Distance_Space.mask(np.triu(np.ones(Distance_Space.shape)).astype(bool)).stack().sort_values(ascending=True)        
     
    fig, axes = plt.subplots()
    plt.scatter(Distance_Space, Distance_TF.loc[Distance_Space.index], alpha = 0.3)
    axes.set_ylabel("Distance 1", fontsize=20, fontweight='bold') 
    axes.set_xlabel("Distance 2", fontsize=20, fontweight='bold') 
    
    plt.axvline(0.48, color = "red")
    plt.axhline(0.74, color = "red")
    

    print(pearsonr(Distance_TF.loc[Distance_Space.index], Distance_Space))    

def Generate_Adj_from_Distribution(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"):   
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    
    # Load the matrix with the distances:
    
    Distance      = pd.read_csv(f'{Path_Meaning}{distance}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)       
    Distance.index   = [str(i) for i in Distance.index]
    Distance.columns = [str(i) for i in Distance.columns]
    
    # Get the upper diagonal of the distance matrix:
    
    Distance_flat = Distance.mask(np.triu(np.ones(Distance.shape)).astype(bool)).stack().sort_values(ascending=True)        
    
    # Get the thresholds based on the 5th percentile:
    
    threshold = np.percentile(Distance_flat, 5)
    
    # Generate the Adjancency Matrix:
   
    Adj = pd.DataFrame(0, columns=list(Distance.index), index = list(Distance.index))
    
    for pair in Distance_flat.index:
        if Distance_flat[pair] <= threshold:
            Adj.loc[str(pair[0]), str(pair[1])] = 1
            Adj.loc[str(pair[1]), str(pair[0])] = 1
        else:
            Adj.loc[str(pair[0]), str(pair[1])] = 0
            Adj.loc[str(pair[1]), str(pair[0])] = 0   
    
    # Save the Adjancency Matrix:
    
    Adj.to_csv(f'{Path_Meaning}{distance}Distribution_Adjancency_{Species}_{matrix_list}_{dimensions}_{annotations}.csv')
    
def Differences_Between_Networks(Species, matrix_list, dimensions, annotations, technique = "Distribution_"):  
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Load the Adjancencies:

    Adj_TF     = pd.read_csv(f'{Path_Meaning}TF_IDF_{technique}Adjancency_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    Adj_Space  = pd.read_csv(f'{Path_Meaning}{technique}Adjancency_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)

    # Flattern the Adjancencies:
    
    Adj_TF_flat    = Adj_TF.mask(np.triu(np.ones(Adj_TF.shape)).astype(bool)).stack().sort_values(ascending=True)
    Adj_Space_Flat = Adj_Space.mask(np.triu(np.ones(Adj_Space.shape)).astype(bool)).stack().sort_values(ascending=True)   
    
    # Order equally:
    
    Adj_TF_flat    = Adj_TF_flat.loc[Adj_Space_Flat.index]
    
    # Substract the Adjancencies:
    
    Difference = Adj_TF - Adj_Space 
    Difference = Difference.mask(np.triu(np.ones(Difference.shape)).astype(bool)).stack().sort_values(ascending=True)    
    Difference = Difference.loc[Adj_Space_Flat.index]
    
    edges_same = 0
    for i in Difference.index: 
        
        if (Difference.loc[i] == 0) & (Adj_TF_flat.loc[i] == 1) == True:
            edges_same +=1
       
    edges_TF       =  sum(Difference == 1)   
    edges_distance =  sum(Difference == -1)
    Total          =  edges_same + edges_TF + edges_distance
    
    plot_list = [0] * edges_same
    plot_list.extend([-1] * edges_distance)
    plot_list.extend([1] * edges_TF)
    
    # Plot and show some statistics:
    
    fig, axes = plt.subplots()
    
    plt.hist(plot_list)
    axes.set_ylabel("Edges", fontsize=20, fontweight='bold') 
    axes.set_xlabel("Differences", fontsize=20, fontweight='bold') 
    
    print(f'Connections unique in TF: {edges_TF} / {round(edges_TF/Total,3)}%')
    print(f'Connections unique in Space: {edges_distance} / {round(edges_distance/Total,3)}%')
    
def Plot_Distribution_5th(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"):

    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    
    # Load the matrix with the distances:
    
    Distance      = pd.read_csv(f'{Path_Meaning}{distance}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)       
    Distance.index   = [str(i) for i in Distance.index]
    Distance.columns = [str(i) for i in Distance.columns]
    
    # Get the upper diagonal of the distance matrix:
    
    Distance_flat = Distance.mask(np.triu(np.ones(Distance.shape)).astype(bool)).stack().sort_values(ascending=True)        
    
    # Get the thresholds based on the 5th percentile:
    
    threshold = np.percentile(Distance_flat, 5)
    
    print(threshold)
    
    # Do the plot:
    
    fig, axes = plt.subplots()
    plt.hist(Distance_flat, bins = 200)
    plt.axvline(x=threshold, color = "red")
    axes.set_ylabel("Axes Pairs", fontsize=20, fontweight='bold') 
    axes.set_xlabel("Distance", fontsize=20, fontweight='bold')  

def Paint_Connected(Adj):
    
    Gcc        = sorted(nx.connected_components(Adj), key=len, reverse=True)
    components = Adj.subgraph(Gcc[0])
    
    color = []
    for node in Adj.nodes():
        if node in components.nodes:
            color.append("#1f77b4")
        else:
            color.append("red")
    return(color)
    
    
def Plot_Network_Components(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_",  technique = "Distribution_", plot = "MDS"):

    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 
    
    # Load the corresponding Adjancency Matrix:
    
    Adj = pd.read_csv(f'{Path_Meaning}{distance}{technique}Adjancency_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    Adj.index = Adj.columns
    
    # Load the distance matrix:
    
    Distance      = pd.read_csv(f'{Path_Meaning}{distance}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)
    
    # Convert the Adjancency to a networkx object:
    
    Adj_nx = nx.from_pandas_adjacency(Adj)
    
    # Get largest components:
    
    Gcc        = sorted(nx.connected_components(Adj_nx), key=len, reverse=True)
    components = Adj_nx.subgraph(Gcc[0]) 
    
    print(f'Number of nodes: {Adj_nx.number_of_nodes()}')
    print(f'Number of nodes: {Adj_nx.number_of_edges()}')  
    print(f'Density: {nx.density(Adj_nx)}')  
    print(f'Number of nodes largest: {components.number_of_nodes()}')
    print(f'Number of nodes largest: {components.number_of_edges()}')      
    print(f'Density: {nx.density(components)}') 
    
    plt.subplots(figsize=(15,15))
    nx.draw(components, with_labels=False, font_weight='bold' )
    plt.show()
    
    # Plot the components in the space:
    
    palette = Paint_Connected(Adj_nx)
    
    # MDS:
    
    if plot == "MDS":
    
        MDS_cod =  MDS(dissimilarity='precomputed')                
        Transf  = MDS_cod.fit_transform(Distance.T)  
        plt.scatter(Transf[:,0], Transf[:,1], c=palette)
    
    # TSNE:
    
    elif plot == "TSNE":
    
        TSNE_cod =  TSNE(metric='precomputed', random_state=0)                 
        Transf   = TSNE_cod.fit_transform(Distance.T)   
        plt.scatter(Transf[:,0], Transf[:,1], c=palette)
    
    # UMAP:
    
    elif plot == "UMAP":
    
        UMAP_cod =  umap.UMAP(n_components=2, metric = "precomputed")                
        Transf   = UMAP_cod.fit_transform(Distance.T)   
        plt.scatter(Transf[:,0], Transf[:,1], c=palette)  
    
    else:
        print("Done")

def shuffle2(df):     
    df_random = df.copy()
    for i in df_random.index:
        random           = df_random.loc[i].sample(frac=1)
        random.index     = df.columns
        df_random.loc[i] = random
    for i in df_random.columns:
        random           = df_random[i].sample(frac=1)
        random.index     = df.index
        df_random[i]     = random
        
    return(df_random)
    
def Permutations_TF_IDF(Species, matrix_list, dimensions, annotations, times = 10000):
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 

    # Load the TF-IDF:
    
    TF_IDF = pd.read_csv(f'{Path_Meaning}Axes_Meaning_{Species}_{dimensions}_{annotations}_{matrix_list}.csv',
                         index_col = 0, dtype={0: str})
            
    TF_IDF.index = [str(i) for i in TF_IDF.index] 
        
    # Load the real distances based on the TF-IDF:
    
    TF_IDF_distance = pd.read_csv(f'{Path_Meaning}TF_ID_Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv',
                                  index_col = 0, dtype={0: str})
            
    TF_IDF_distance.index = [str(i) for i in TF_IDF_distance.index]

    # Start the permutations:
    
    counts = pd.DataFrame(0, columns=list(TF_IDF_distance.index), 
                          index = list(TF_IDF_distance.index))
    
    for rep in range(times):
    
        # Randomize the TF-IDF and calculate the distances:
         
        TF_IDF_random   = shuffle2(TF_IDF.T) # Shaffle 2 changes both, rows and columns.
        index           = TF_IDF_random.index
        F_IDF_np_random = np.array(TF_IDF_random)
        
        # Calculate the random distances from the random TF-IDF
        
        TF_IDF_distance_random = pairwise_distances(F_IDF_np_random, metric="cosine") 
        TF_IDF_distance_random = pd.DataFrame(TF_IDF_distance_random, index, index)
        
        if rep%100 == 0:
            print(rep)
        
        # Compare the random distances with our real random matrix:
        
        compare = TF_IDF_distance_random <= TF_IDF_distance
        counts  = counts + compare
    
    # Compute p-values:
    
    p_values = (counts + 1)/(times + 1)
    
    # Save the p-values:
    
    p_values.to_csv(f'{Path_Meaning}TF_IDF_P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv')  
    
    
def Correct_P_values_Meaning(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"):
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/" 

    # Load the P-values:
    
    p_value       = pd.read_csv(f'{Path_Meaning}{distance}P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)  
    p_value.index = p_value.columns 
    
    P_adj_Matrix = pd.DataFrame(0, columns = p_value.columns, index = p_value.index)
    
    # Correct the P-values (we take the upper diagonal and correct all the p-values together).  

    p_value = p_value.mask(np.triu(np.ones(p_value.shape)).astype(bool)).stack().sort_values(ascending=True)
 
    p_values_list_corrected = multipletests(p_value.values, alpha=0.05, 
                                method='fdr_bh', is_sorted=False, returnsorted=False)
    
    p_value_vect = p_values_list_corrected[1] 
    
    # Generate the matrix with the corrected p_values:
    
    for ax in range(len(p_value.index)):
        
        indices = list(p_value.index)
        ax1     = indices[ax][0]
        ax2     = indices[ax][1]
        
        P_adj_Matrix.loc[ax1, ax2] = p_value_vect[ax]
        P_adj_Matrix.loc[ax2, ax1] = p_value_vect[ax]
    
    # Save the corrected p_values:
    
    P_adj_Matrix.to_csv(f'{Path_Meaning}{distance}Corrected_P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv')  
        
def Adj_from_P_values(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"):

    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Load the corrected p-values:

    p_value = pd.read_csv(f'{Path_Meaning}{distance}Corrected_P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)        
    p_value.index   = [str(i) for i in p_value.index]
    p_value.columns = [str(i) for i in p_value.columns]
    
    # Vect p_value:
    
    p_value_vect = p_value.mask(np.triu(np.ones(p_value.shape)).astype(bool)).stack().sort_values(ascending=True)

    # Start the Adj:
    
    Adj = pd.DataFrame(0, columns=list(p_value.index), index = list(p_value.index))
    
    for pair in p_value_vect.index:
        if p_value_vect[pair] <= 0.05:
            Adj.loc[str(pair[0]), str(pair[1])] = 1
            Adj.loc[str(pair[1]), str(pair[0])] = 1
        else:
            Adj.loc[str(pair[0]), str(pair[1])] = 0
            Adj.loc[str(pair[1]), str(pair[0])] = 0 
    
    # Save the Adjancency matrix:
    
    Adj.to_csv(f'{Path_Meaning}{distance}P_value_Adjancency_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index=True)
    

def Scatter_5th_P_value_Adj(Species, matrix_list, dimensions, annotations, distance = "TF_IDF_"): 
    
    # Paths:
    
    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Load distance matrix:
    
    distance_pd = pd.read_csv(f'{Path_Meaning}{distance}Cosine_Distances_Embeddings_{Species}_{matrix_list}_{dimensions}_{annotations}.csv',
                                  index_col = 0, dtype={0: str})

    distance_pd.index = [str(i) for i in distance_pd.index]
    
    # Load the corrected p-values:
    
    p_value = pd.read_csv(f'{Path_Meaning}{distance}Corrected_P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', index_col = 0)        
    p_value.index   = [str(i) for i in p_value.index]
    p_value.columns = [str(i) for i in p_value.columns]

    # Get the upper diagonal of p_values and distance:

    distance_vec = distance_pd.mask(np.triu(np.ones(distance_pd.shape)).astype(bool)).stack().sort_values(ascending=True)
    p_value_vec  = p_value.mask(np.triu(np.ones(p_value.shape)).astype(bool)).stack().sort_values(ascending=True)
    p_value_vec  = p_value_vec.loc[distance_vec.index]
    
    # Give some statistics:
    
    statistics = pd.DataFrame(distance_vec)
    statistics["P_value"] = p_value_vec
    
    
    common  = statistics[(statistics[0] <= np.percentile(distance_vec,5)) & (statistics["P_value"] <= 0.05)]
    unique  = statistics[(statistics[0] <= np.percentile(distance_vec,5)) & (statistics["P_value"] > 0.05)]
    unique2 = statistics[(statistics[0] > np.percentile(distance_vec,5)) & (statistics["P_value"] <= 0.05)]
    
    
    # Start the comparisons:
    
    print(pearsonr(distance_vec, p_value_vec))
    
    print(f'Common edges: {len(common)}')
    print(f'P_value unique edges: {len(unique2)}')
    print(f'5th unique edges: {len(unique)}')
    
    # Do the plot:
    
    fig, axes = plt.subplots(figsize=(8,7))
    plt.scatter(p_value_vec, distance_vec, alpha = 0.3)
    axes.set_ylabel("Distance", fontsize=20, fontweight='bold') 
    axes.set_xlabel("P_Value", fontsize=20, fontweight='bold') 
    
    plt.axvline(0.05, color = "red")
    plt.axhline(np.percentile(distance_vec,5), color = "red")    

    
def Sum_Counts_Dist(Species, matrix_list, dimensions, annotations, times = 20000):

    # Paths (only if the server was not able to run the p-values):
  
    permutation_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/Permutations_Human_500_PPMI_BP_Back/"
    save_path        = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    
    # Set the information to sum the different files:    
    
    permutations = int(times/1000) 
    tasks_n      = int(times/permutations)
    jobs_n       = int(tasks_n/100)
    
    # Sum all the counts:
    
    Final = pd.DataFrame()
    
    for job in range(jobs_n):
        
        print(job)
        
        for task in range(100):
            
            if (job == 0) & (task == 0): 
                
                 permutations_it   = pd.read_csv(f'{permutation_path}Distance_Count_{Species}_{dimensions}_{matrix_list}_{annotations}_{permutations}_{task}_{job}.csv',index_col = 0, dtype={0: str})
                 Final = permutations_it
            else:
               
                permutations_it   = pd.read_csv(f'{permutation_path}Distance_Count_{Species}_{dimensions}_{matrix_list}_{annotations}_{permutations}_{task}_{job}.csv',index_col = 0, dtype={0: str})
                Final = Final + permutations_it
        
    
    # Compute the p-value:
    
    Total_counts = permutations * (100 * jobs_n)
    Final        = (Final + 1)/(Total_counts + 1)   
    
    # Save the p-value:
    
    Final.to_csv(f'{save_path}P_value_{Species}_{matrix_list}_{dimensions}_{annotations}.csv', 
                          header = True, index=True)    

def Generate_Taxons_Dic():

    # Paths:
    
    Taxons_path       = "/media/sergio/sershiosdisk/Human/Axes/"
    
    # Load the data:
    
    Taxons_Dic = json.load(open(f'{Taxons_path}GO_Taxons_Whole_Annotations.json')) 
    
    # Take the values of the Taxons:
    
    Taxons_Dic_set = set([x for xs in Taxons_Dic.values() for x in xs])
    
    Dic_Final = {key:[] for key in list(set(Taxons_Dic_set))}
    
    Dic_Final[3702].extend(["Arabidopsis", "Multi", "Plant", "Eucariota", "Invertebrado"])
    Dic_Final[4896].extend(["Pombe", "Uni", "Fungus", "Eucariota", "Invertebrado"])
    Dic_Final[6239].extend(["Worm",  "Multi", "Insect", "Eucariota", "Invertebrado"])    
    Dic_Final[7227].extend(["Drosophila",  "Multi", "Insect", "Eucariota", "Invertebrado"])      
    Dic_Final[7955].extend(["ZebraFish",  "Multi", "Fish", "Eucariota", "Vertebrado"])      
    Dic_Final[9031].extend(["Chicken",  "Multi", "Bird", "Eucariota", "Vertebrado"])         
    Dic_Final[9606].extend(["Human",  "Multi", "Mamal", "Eucariota", "Vertebrado"])       
    Dic_Final[9615].extend(["Wolf",  "Multi", "Mamal", "Eucariota", "Vertebrado"])      
    Dic_Final[9823].extend(["Pig",  "Multi", "Mamal", "Eucariota", "Vertebrado"])      
    Dic_Final[9913].extend(["Taurus",  "Multi", "Mamal", "Eucariota", "Vertebrado"])      
    Dic_Final[10090].extend(["Mouse",  "Multi", "Mamal", "Eucariota", "Vertebrado"])       
    Dic_Final[10116].extend(["Rat",  "Multi", "Mamal", "Eucariota", "Vertebrado"])     
    Dic_Final[36329].extend(["Plasmodius",  "Uni", "Bacteria", "Procariota", "Invertebrado"])         
    Dic_Final[39947].extend(["Raice",  "Multi", "Plant", "Eucariota", "Invertebrado"])   
    Dic_Final[195103].extend(["Gastro",  "Uni", "Bacteria", "Procariota", "Invertebrado"])       
    Dic_Final[214684].extend(["Coco",  "Uni", "Fungus", "Eucariota", "Invertebrado"])       
    Dic_Final[227321].extend(["Aspergilus",  "Multi", "Fungus", "Eucariota", "Invertebrado"])       
    Dic_Final[352472].extend(["Disctoest",  "Uni", "Ameba", "Eucariota", "Invertebrado"])       
    Dic_Final[511145].extend(["Coli",  "Uni", "Bacteria", "Procariota", "Invertebrado"])       
    Dic_Final[559292].extend(["Cereveciae",  "Uni", "Fungus", "Eucariota", "Invertebrado"])   
        
    return(Dic_Final)

def Taxonitos(Taxons_Dic, associations):
    
    # Take the information about the taxons:
    
    Taxon_info = Generate_Taxons_Dic()
    
    final = []
    for i in associations:
        
        try:       
            final.extend(Taxons_Dic[i])
        except KeyError:
            continue
        
    count = len(set(final))
    
    # Check the if all are multi:
    
    final_set = list(set(final))
    
    counters = []
    counter2 = []
    counter3 = []
       
    for i in final_set:        
        if i != 9606:
            counters.append(Taxon_info[i][1])
            counter2.append(Taxon_info[i][3])
            counter3.append(Taxon_info[i][4])
        else:
            continue
        
    procariota    = "Procariota" in counter2
    
    # Vertebrata:

    try:    
        percentage_vert = Counter(counter3)["Vertebrado"]/len(counter3)                       
    except ZeroDivisionError:
        percentage_vert = 0
   
    # Unicellular:
    
    try:    
        percetage_uni   = Counter(counters)["Uni"]/len(counters)
        only_uni        = counters == 1.0
                       
    except ZeroDivisionError:
        percetage_uni = 0
        only_uni      = False      
     
    return([count, percetage_uni, only_uni, procariota, percentage_vert])

def Do_Table(specie, matrix, annotations, dimensions, associations, method):
    
    # Paths: GO:2000374 GO:2000376
    
    Axes_meaning_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    Genes_path        = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Axes_path         = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Taxons_path       = "/media/sergio/sershiosdisk/Human/Axes/"
    
    # Load the data:
    
    Meaning    = pd.read_csv(f'{Axes_meaning_path}Axes_Meaning_{specie}_{dimensions}_{annotations}_{matrix}{associations}.csv', index_col = 0)
    Taxons_Dic = json.load(open(f'{Taxons_path}GO_Taxons_Whole_Annotations.json')) 
    genes      = pd.read_csv(f'{Genes_path}Matrix_GO_{specie}.csv', index_col = 0)
    Axes       = json.load(open(f'{Axes_path}Associations_{specie}_{matrix}_{annotations}_{dimensions}{associations}.json')) 
    
    # Transform the Taxons and Genes to a DataFrame:
    
    counts          = [len(Taxons_Dic[k]) for k in list(Taxons_Dic.keys())]
    GO              = list(Taxons_Dic.keys())                    
    Taxons_db       = pd.DataFrame({"GO" : GO, "Count" : counts})  
    genes_db        = pd.DataFrame({"GO" : list(genes.sum().index), "Count" : genes.sum().values})
    
    Axis_list = []
    Tax_list  = []
    Gen_list  = []
    Total     = []
    Perc_Uni  = []
    Only_Uni  = []
    Procarito = []
    Vertebra  = []
    
    for axis in Meaning.columns:
        
        associations_d = Axes[axis]
    
        try:
            if method == "avg":
                Tax_list.append(round(Taxons_db[Taxons_db.GO.isin(associations_d)].Count.mean()))
                Gen_list.append(round(genes_db[genes_db.GO.isin(associations_d)].Count.mean()))
                Axis_list.append(axis)
                Total.append(len(Axes[axis]))
            else:
                Tax_list.append(Taxonitos(Taxons_Dic, associations_d)[0])
                Perc_Uni.append(Taxonitos(Taxons_Dic, associations_d)[1])
                Only_Uni.append(Taxonitos(Taxons_Dic, associations_d)[2])
                Procarito.append(Taxonitos(Taxons_Dic, associations_d)[3]) 
                Vertebra.append(Taxonitos(Taxons_Dic, associations_d)[4]) 
                                   
                Gen_list.append(sum(genes[associations_d].T.sum() >= 1))
                Axis_list.append(axis)
                Total.append(len(Axes[axis]))           
        except ValueError:
            continue   
        
    # Do the finral data frame:
    
    Complete_db         = pd.DataFrame([Axis_list, Gen_list, Tax_list, Total, Perc_Uni, Only_Uni, Procarito, Vertebra]).T   
    Complete_db.columns = ["Axis", "Gene", "Taxons", "Total", "Perc_Uni", "Only_Uni", "Procaria", "Vertebra"] 
    
    # Return it:
    
    return(Complete_db)
    
    
def Add_Cancer_Info(Data, specie, matrix, annotations, dimensions, associations = "", method = "set"):
    
    # Paths:
    
    cancer_path    = "/media/sergio/sershiosdisk/Human/Cancer/"
    network_path   = "/media/sergio/sershiosdisk/Cancer/Networks/"
    Axes_path      = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    Genes_path     = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Hallmarks_path = "/media/sergio/sershiosdisk/Cancer/Cancer_GO/"
    
    # Load the hallmarks:
    
    hallmarks = list(set(pd.read_csv(f'{Hallmarks_path}Hallmarks_Cancer_GO_list.csv',index_col = 0)["GO"]))
    
    # Get the onocogenes:
    
    oncogenes = pd.read_csv("/media/sergio/sershiosdisk/Cancer/Cancer_GO/cancer_gene_census_COSMIC.csv",sep = ",", encoding='latin-1')["Entrez GeneId"]
    oncogenes = oncogenes.dropna()
    oncogenes = [str(int(i)) for i in oncogenes]
    
    # Load the files that are needed:
    
    genes      = pd.read_csv(f'{Genes_path}Matrix_GO_Human.csv', index_col = 0)
    Axes       = json.load(open(f'{Axes_path}Associations_{specie}_{matrix}_{annotations}_{dimensions}{associations}.json')) 
              
    # Read the pathology file and get the total number of cancers from it:
    
    cancer_pb = pd.read_csv(f'{cancer_path}pathology.tsv', sep='\t')
    cancers   = list(set(cancer_pb.Cancer)) 
    
    # Do a dictionary of genes to cancer:
    
    genes_list = []
    
    for cancer  in cancers:
        
        # Load the Adj: 
        
        genes_list.append(list(pd.read_csv(f'{network_path}Pan_Cancer_{cancer}_Genes.csv',dtype={"0": str})["0"]))

    
    genes_list_flat = [x for xs in genes_list for x in xs]
    Dic_cancer = {key:[] for key in list(set(genes_list_flat))}
    
    for cancer in cancers:       
        gene_it = list(pd.read_csv(f'{network_path}Pan_Cancer_{cancer}_Genes.csv',dtype={"0": str})["0"])        
        for gene in gene_it:           
            Dic_cancer[gene].append(cancer) 
                   
    # Start doing the counting:
    
    counting_list  = []
    oncogenes_list = []
    hallmarks_list = []
    cancer_pecr    = []
    
    for axis in Data.Axis:
        
        # Get the annotations asscoiated to an axis:
        
        associations = Axes[axis]
        
        # Get the hallamrks percentage:
        
        hallmarks_list.append(len(set(hallmarks).intersection(set(associations)))/len(associations))
        
        # Get the genes that are annotated by these associations:
        
        genes_annotated = genes[associations]
        
        set_gens = genes_annotated.T.sum()
        set_gens = list(set_gens[set_gens > 0].index)
        set_gens = [str(i) for i in set_gens]    
        
        oncogenes_list.append(len(set(set_gens).intersection(set(oncogenes))))
        
        cancer_pecr.append(len(set(set_gens).intersection(set(Dic_cancer.keys())))/len(set_gens))
        
        if method == "set":
            
            cancer_fin = []            
            for gn in set_gens: 
                try:
                    cancer_fin.append(Dic_cancer[gn])   
                except KeyError:
                    continue       
            counting_list.append(len(set([x for xs in cancer_fin for x in xs])))
                      
        else:
            cancer_fin = []            
            for gn in set_gens:
                try:           
                    cancer_fin.append(len(Dic_cancer[gn]))     
                except KeyError:
                    cancer_fin.append(0) 
                    
            counting_list.append(round(np.mean(cancer_fin)))            
        
        
    Data["Cancer"]         = counting_list
    Data["Oncogene"]       = oncogenes_list
    Data["Perc_Cancer"]    = cancer_pecr
    Data["Avg"]            = Data["Oncogene"] / Data["Gene"]
    Data["Hallmarks"]      = hallmarks_list 
    
    return(Data)
    
    
def Add_Information_Functions(specie, matrix, annotations, dimensions, associations = "_Hard", method = "set"):
    
    # Paths:
    
    path_funct = "/media/sergio/sershiosdisk/Axes_Species/Functional_Annotations/"
    
    # Add information about evolution:
    
    for sp in specie:        
        for mt in matrix:            
            print(f'{sp}-{mt}')
            
            for dim in dimensions:
   
                Information = Do_Table(sp, mt, annotations, dim, associations, method)
            
                # Add information about cancer: 
                
                if sp == "Human":
                    Information = Add_Cancer_Info(Information, sp, mt, annotations, dim, associations, method)
            
                # Save information:   
                
                Information.to_csv(f'{path_funct}Functional_Table_{sp}_{mt}_{annotations}_{dim}_{associations}_{method}.csv',
                                   header = True, index=True)
                
                
def Plot_Axes_Evolution(specie, matrix, annotations, dimensions, associations = "_Hard", method = "set"): 

    # Paths:
    
    Axes_meaning_path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Meaning/"
    path_funct        = "/media/sergio/sershiosdisk/Axes_Species/Functional_Annotations/"
    save_plot         = "/media/sergio/sershiosdisk/Axes_Species/Plots_Paper/"
    
    # Load the definitions:
    
    Meaning    = pd.read_csv(f'{Axes_meaning_path}Axes_Meaning_{specie}_{dimensions}_{annotations}_{matrix}{associations}.csv', index_col = 0)
    
    filtering  =  Meaning.sum()
    filtering  = filtering[filtering > 0.0].index
    
    # Load the coplete Table:
    
    Data = pd.read_csv(f'{path_funct}Functional_Table_{specie}_{matrix}_{annotations}_{dimensions}_{associations}_{method}.csv')
    Data = Data[Data.Axis.isin(filtering)]
    Data = Data.sort_values("Taxons", ascending = True)

    color = []
    
    for i in Data.index:
        data = Data.loc[i]        
        if data['Vertebra'] == 1:
            color.append("green")
        elif (data['Vertebra'] != 1) & (data['Procaria'] == False):
            color.append("blue")
        elif (data['Vertebra'] != 1) & (data['Procaria'] == True):
            color.append("red") 
            
    # Show number of each class:
    
    print(Counter(color))
    
    # Plot:
    
    x_controler = []
    
    for taxon in list(set(Data.Taxons)):
        
        Data_it     = Data[Data.Taxons == taxon]
        x_controler.extend(list(np.arange(0,len(Data_it),1)))
        
    Data["X"] = x_controler
       
    
    plt.style.use("seaborn-whitegrid")
    plt.tight_layout()
    plt.rcParams.update({'font.size': 17})
    fig, ax = plt.subplots(figsize=(16,12))
    points1 = ax.scatter(Data.Taxons, Data.X)
    
    label1 = list(Data.Axis)
    x1     = list(Data.Taxons)
    y1     = list(Data.X)
    
    
    for i in range(len(label1)): 
        ax.annotate(str(label1[i]), (x1[i],y1[i]), rotation=45, color = color[i], fontsize=16)


    points1.remove()
    
    ax.xaxis.set_ticks(np.arange(1, 21, 1))

    ax.set_ylabel("# Axes", fontsize=20, fontweight='bold')     
    ax.set_xlabel("Conservation Degree", fontsize=20, fontweight='bold') 
        
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_linewidth(4)
    ax.spines['bottom'].set_linewidth(4)
    ax.spines['left'].set_color("grey")
    ax.spines['bottom'].set_color("grey")
    
    # Generate the legend:
    
    Vert_patch     = mpatches.Patch(color='green', label='Vertebrates')
    Eucr_patch     = mpatches.Patch(color='blue', label='Eukaryotes')
    Proc_patch     = mpatches.Patch(color='red', label='Prokaryotes')
    
    plt.legend(handles=[Vert_patch, Eucr_patch, Proc_patch], borderaxespad=0.1,
           bbox_to_anchor=(0.5, -0.2), loc="lower center", frameon=True, ncol = 3) 
                                      
  
    fig.savefig(f'{save_plot}Taxons_Dist_{specie}.svg', format="svg", dpi=600,
                    bbox_inches='tight') 
               
    
def Axes_Differentiation(Axes):
    
    # List with those axes that do not have any associated GO.
    
    empty_axes = [] 
    
    for key in Axes.keys():
        if not Axes[key]:
            empty_axes.append(key)
        else:
            continue
    
    # List with those axes that have at least one associated GO.
        
    not_empty_axes = [] 
    
    for key in Axes.keys():
        if Axes[key]:
            not_empty_axes.append(key)
        else:
            continue
    
    return(empty_axes, not_empty_axes)
                  
def Annotate_Genes_Axes(specie, dim, genes, network_path): 
    
    # Load the corresponding matrix factors:
        
    P     = np.load(f'{network_path}P_Matrix_{dim}_PPI_{specie}_PPMI.npy', allow_pickle=True)
    U     = np.load(f'{network_path}U_Matrix_{dim}_PPI_{specie}_PPMI.npy', allow_pickle=True)
    
    # Get the representation of the genes in an embedding space spanned by the bases:
            
    embeddings         = P.dot(U) 
    embeddings         = pd.DataFrame(embeddings)
    embeddings.index   = genes
    embeddings.columns = np.arange(dim)
    
    # Annotate the axes using hard-clustering (since they are orthonormal):
    
    genes_ass = dict(([(key, []) for key in embeddings.columns]))
    
    for i in embeddings.index:
    
        gene_it = embeddings.loc[i]
        axis    = gene_it[gene_it == max(gene_it)].index[0]
        genes_ass[axis].append(i)   
    
    return(genes_ass)

    
def connectivity_Calulation(genes_ass, axes_list, Adj):
    
    # Subset the genes that are in the axes_list with the genes:
    
    genes_list = []
    
    for empty in axes_list:    
        axis_gene = genes_ass[int(empty)]
        genes_list.append(axis_gene)
        
    # Calculate the connectivity of the genes in the PPI:
    
    intra_con = []
    
    for i in genes_list:
        
        Adj_it = Adj.loc[i,i]
        G = nx.from_pandas_adjacency(Adj_it)
        
        try:
            intra_con.append(nx.average_clustering(G))        
        except ZeroDivisionError:
                continue 
    
    return(intra_con)
    
def Random_Connectivity(genes_ass, axes_list, Adj):
    
    # Subset the genes that are in the axes_list with the genes:
    
    genes_list = []
    
    for empty in axes_list:   
        
        axis_gene    = genes_ass[int(empty)]
        number_genes = len(axis_gene)
        
        # randomly pick few genes:
        
        genes_Adj = list(Adj.index)
        genes_Adj = sample(genes_Adj,number_genes)   
        genes_list.append(genes_Adj)
    
    # Calculate the connectivity of the genes in the PPI:
    
    intra_con = []
    
    for i in genes_list:
        
        Adj_it = Adj.loc[i,i]
        G = nx.from_pandas_adjacency(Adj_it)
        
        try:
            intra_con.append(nx.average_clustering(G))        
        except ZeroDivisionError:
                continue 
    
    return(intra_con)    
            
def Connectivity(Species, dimensions, annotations, associations = "_Hard"):
    
    # Paths:
    
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Axes_path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    save_path    = '/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/'
    
    for specie in Species:
        
        # Start the writting of the Result file:
        
        with open(f'{save_path}Connectivity_Genes_{specie}.txt', 'a') as the_file:
            
            # Write the column names:
            
            the_file.write(f'\t')
            the_file.write(f'Empty_Axes_Connectivity\t')
            the_file.write(f'not_Empty_Axes_Connectivity\t')
            the_file.write(f'Manwhitney\t')
            the_file.write(f'Random_Manwhitney_Empty\t')   
            the_file.write(f'Random_Manwhitney_not_Empty\n') 
             
            # Load the Adj matrix and the genes:
            
            Adj   = np.load(f'{network_path}Adj_{specie}_PPI.npy', allow_pickle= True)        
            genes = list(pd.read_table(f'{network_path}Genes_{specie}_PPI', header = None, dtype={0: str})[0])
            
            Adj         = pd.DataFrame(Adj)
            Adj.columns = genes
            Adj.index   = genes
            
            for dim in dimensions:
                
                print(f'{specie}-{dim}')
                
                the_file.write(f'{dim}-D\t')
            
                # Load the Axes:
                
                Axes = json.load(open(f'{Axes_path}Associations_{specie}_PPMI_{annotations}_{dim}{associations}.json')) 
                
                # Annotate the axes with genes:
                
                genes_ass = Annotate_Genes_Axes(specie, dim, genes, network_path)
                
                # Differnciate between annotated (GO BP) axes and not:
                
                empty_axes, not_empty_axes = Axes_Differentiation(Axes)
                
                # Calculate both connectivities:
                
                not_annotated_conn = connectivity_Calulation(genes_ass, empty_axes,     Adj)
                annotated_conn     = connectivity_Calulation(genes_ass, not_empty_axes, Adj)
                
                # Calaculate both connectivities but random:

                Random_not_annotated_conn = Random_Connectivity(genes_ass, empty_axes, Adj)
                Random_annotated_conn     = Random_Connectivity(genes_ass, not_empty_axes, Adj)
                
                the_file.write(f'{np.mean(not_annotated_conn)}\t')
                the_file.write(f'{np.mean(annotated_conn)}\t')
                
                # Calculate the Mannwhitney-U (test if the connectivity is lower in the non-annoted axes):
                
                pvalue       = mannwhitneyu(not_annotated_conn,        annotated_conn,     alternative = "less").pvalue
                pvalue_empty = mannwhitneyu(Random_not_annotated_conn, not_annotated_conn, alternative = "less").pvalue
                pvalue_annot = mannwhitneyu(Random_annotated_conn,     annotated_conn,     alternative = "less").pvalue
                 
                the_file.write(f'{pvalue}\t')
                the_file.write(f'{pvalue_empty}\t')                
                the_file.write(f'{pvalue_annot}\n')                
                
                
        the_file.close()            
            
    
def Ensembl_query(Species):
    
    # Path:
    
    path = '/media/sergio/sershiosdisk/Axes_Species/BioMart/' 
    
    # Connect to the server:
 
    server   = BiomartServer( "http://useast.ensembl.org/biomart" )
    
    # For each organism:
    
    for specie in Species:
        
        print(specie)
        
        # Choose the correct dataset (interestingly pombe is not in ensembl):
        
        if specie == "Human":           
            ensmbl   = server.datasets['hsapiens_gene_ensembl']        
        elif specie == "Mouse":            
            ensmbl   = server.datasets['mmusculus_gene_ensembl']
        elif specie == "Rat":
            ensmbl   = server.datasets['rnorvegicus_gene_ensembl']
        elif specie == "Yeast":
            ensmbl   = server.datasets['scerevisiae_gene_ensembl']
        elif specie == "Fly":
            ensmbl   = server.datasets['dmelanogaster_gene_ensembl']
                
        if specie == "Pombe":
            continue
        else:
            
            # First we get the information about the GO terms:
            
            response_GO = ensmbl.search({'attributes': ['go_id', 'entrezgene_id', 'go_linkage_type', 'namespace_1003']})
            
            with open(f'{path}Biomart_{specie}_GO.txt', 'a') as the_file:
                
                # Write the name of the columns:
                
                the_file.write(f'GO\t')
                the_file.write(f'Gene\t')   
                the_file.write(f'GO Experiment\t')   
                the_file.write(f'GO Type\n') 
                
                # Start writting the file:
                
                for line in response_GO.iter_lines():
                    
                    line = line.decode('utf-8')
                    line = line.split("\t")
                    
                    the_file.write(f'{line[0]}\t')
                    the_file.write(f'{line[1]}\t')
                    the_file.write(f'{line[2]}\t')  
                    the_file.write(f'{line[3]}\n')   
                    
            the_file.close()  
            
def plot_connctivity(specie, dim, annotations, associations = "_Hard"):
    
    # Paths:
    
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Axes_path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Load the Adj matrix and the genes:
            
    Adj   = np.load(f'{network_path}Adj_{specie}_PPI.npy', allow_pickle= True)        
    genes = list(pd.read_table(f'{network_path}Genes_{specie}_PPI', header = None, dtype={0: str})[0])
            
    Adj         = pd.DataFrame(Adj)
    Adj.columns = genes
    Adj.index   = genes    
    
    # Load the axes:
    
    Axes = json.load(open(f'{Axes_path}Associations_{specie}_PPMI_{annotations}_{dim}{associations}.json'))

    # Annotate the axes with genes:
                
    genes_ass = Annotate_Genes_Axes(specie, dim, genes, network_path)
                
    # Differnciate between annotated (GO BP) axes and not:
                
    empty_axes, not_empty_axes = Axes_Differentiation(Axes)
                
    # Calculate both connectivities:
                
    not_annotated_conn = connectivity_Calulation(genes_ass, empty_axes,     Adj)
    annotated_conn     = connectivity_Calulation(genes_ass, not_empty_axes, Adj)  
    
    # For visualization porpuses we delete the 0 values:
    
    not_annotated_conn = [i for i in not_annotated_conn if i != 0]
    annotated_conn     = [i for i in annotated_conn if i != 0]
    
    # We plot the distributions:

    plt.style.use("seaborn-whitegrid")
    plt.rcParams.update({'font.size': 15})
    plt.tight_layout()
    fig, axs = plt.subplots(1, 1, figsize=(7,4)) 
    
    axs.hist(annotated_conn)
    axs.hist(not_annotated_conn)
    axs.set_xlabel("Mean Connectivity", fontsize=15, fontweight='bold') 
    axs.set_ylabel("Counts", fontsize=15, fontweight='bold') 
        
    axs.legend(labels= ["Annotated Axes", "Empty Axes"],
                       loc="best", frameon=True, ncol = 1,prop={'size': 12}) 

def SS(list_GO, G):
    distance_list = []
    for i in range(len(list_GO)):
        for j in range(i+1, len(list_GO)):
            try:
                distance = similarity.lin(G, list_GO[i], list_GO[j]) 
                distance_list.append(distance)
            except Exception as PGSSLookupError:
                continue
    return(np.mean(distance_list))
    
def Analyze_other_Functions(Species, dimensions, annotations, associations = "_Hard", function = "cellular_component"):
    
    # Paths:
    
    Biomart_path = '/media/sergio/sershiosdisk/Axes_Species/BioMart/'
    Axes_path    = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    save_path    = '/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/'
    
    for specie in Species:
        
        # Load the biomart informations (BP, CC, and MF):
        
        Biomart_data = pd.read_csv(f'{Biomart_path}Biomart_{specie}_GO.txt', sep = '\t', dtype={1: str})
        Biomart_data = Biomart_data[Biomart_data['GO Experiment'].isin(['EXP', 'IDA', 'IPI', 'IMP', 'IGI', 'IEP'])]
        
        # Load the gene names:
        
        genes = list(pd.read_table(f'{network_path}Genes_{specie}_PPI', header = None, dtype={0: str})[0])
        
        # Start analyzing the SS:
        
        G = graph.from_resource("go-basic")
        similarity.precalc_lower_bounds(G)
        
        with open(f'{save_path}SS_{function}_Genes_{specie}.txt', 'a') as the_file:
            
            the_file.write(f'\t')
            the_file.write(f'SS_Empty_Axes_{function}\t')
            the_file.write(f'SS_not_Empty_Axes_{function}\t')
            the_file.write(f'Manwhitney\n')
        
            for dim in dimensions:
                
                print(f'{specie}-{dim}')
                
                the_file.write(f'{dim}-D\t')
                
                # Load the axes:
                
                Axes = json.load(open(f'{Axes_path}Associations_{specie}_PPMI_{annotations}_{dim}{associations}.json')) 
                
                # Annotate them with genes:
                
                genes_ass = Annotate_Genes_Axes(specie, dim, genes, network_path)
                
                # Differnciate between annotated (GO BP) axes and not:
                
                empty_axes, not_empty_axes = Axes_Differentiation(Axes) 
                
                # Investigate the semantic similarity of the annotations:
                
                Biomart_data_subset = Biomart_data[Biomart_data["GO Type"] == function]
                
                # Empty axes:
                
                SS_empty = []
                
                for empty in empty_axes: 
                    
                    axis_gene = genes_ass[int(empty)]
                    go        = Biomart_data_subset[Biomart_data_subset.Gene.isin(axis_gene)]
                    
                    if list(set(go.GO)):
                        SS_empty.append(SS(list(set(go.GO)), G))
                
                the_file.write(f'{np.nanmean(SS_empty)}\t')
                
                # Not empty axes:
                
                SS_not_empty = []
                
                for not_empty in not_empty_axes: 
                    
                    axis_gene = genes_ass[int(not_empty)]
                    go        = Biomart_data_subset[Biomart_data_subset.Gene.isin(axis_gene)]
                    
                    if list(set(go.GO)):
                        SS_not_empty.append(SS(list(set(go.GO)), G))  
                
                the_file.write(f'{np.nanmean(SS_not_empty)}\t')
                
                # Do a Statistical test (we test if the SS is greater in empty axes):
                
                pvalue = mannwhitneyu(SS_empty, SS_not_empty, alternative = "greater").pvalue                
                the_file.write(f'{pvalue}\n')                
                        
        the_file.close()                  
                 
def Annotate_Genes_Axes_p(specie, dim, genes, network_path):

    # Load the corresponding matrix factors:
        
    P     = np.load(f'{network_path}P_Matrix_{dim}_PPI_{specie}_PPMI.npy', allow_pickle=True)
    U     = np.load(f'{network_path}U_Matrix_{dim}_PPI_{specie}_PPMI.npy', allow_pickle=True)
    
    # Get the representation of the genes in an embedding space spanned by the bases:
            
    embeddings         = P.dot(U) 
    embeddings         = pd.DataFrame(embeddings)
    embeddings.index   = genes
    embeddings.columns = np.arange(dim)
    
    # Annotate the axes using hard-clustering (since they are orthonormal):
    
    genes_ass = dict(([(key, []) for key in embeddings.columns]))
    
    for i in embeddings.index:
    
        gene_it = embeddings.loc[i]
        axis    = gene_it[gene_it == max(gene_it)].index[0]
        genes_ass[axis].append(i)   
    
    return(genes_ass)            
               
def Ensembl_query_p(Species):
    
    '''
    This fucntions checks if the file of Entrez2Gene exists and if not it generates it.
    '''   
    # Path:
    
    path = '/media/sergio/sershiosdisk/Axes_Species/BioMart/' 
       
    # Pandas with the transformation parameter:
    
    if os.path.exists(f'{path}Biomart_Entrez2Gene_Human.txt') == False:
        
        # Connect to the server:
 
        server   = BiomartServer( "http://useast.ensembl.org/biomart")
    
        # Select the database:
    
        ensmbl   = server.datasets['hsapiens_gene_ensembl']
    
        # Query attributes:
    
        response_GO = ensmbl.search({'attributes': ['entrezgene_id', 'external_gene_name']})
    
        with open(f'{path}Biomart_Entrez2Gene_Human.txt', 'a') as the_file:
                    
            # Write the name of the columns:
                    
            the_file.write(f'Entrez\t')
            the_file.write(f'Gene_name\n')   
                    
            # Start writting the file:
                    
            for line in response_GO.iter_lines():                        
                line = line.decode('utf-8')
                line = line.split("\t")                        
                the_file.write(f'{line[0]}\t')
                the_file.write(f'{line[1]}\n')                        
        the_file.close()
        
        # Load the table:
        
        Table = pd.read_csv(f'{path}Biomart_Entrez2Gene_Human.txt', sep = '\t', dtype={0: str})
        Table = Table.dropna()
        Table = Table.reset_index(drop = True)
        
        return(Table)
    
    else: 
        
        Table = pd.read_csv(f'{path}Biomart_Entrez2Gene_Human.txt', sep = '\t', dtype={0: str})
        Table = Table.dropna()
        Table = Table.reset_index(drop = True)
        
        return(Table)
                    
            
def search(query):
    Entrez.email = 'your.email@example.com'
    handle = Entrez.esearch(db='pubmed',
                            sort='relevance',
                            retmax='20',      # For now we take 20 (we can discuss how many)
                            retmode='xml',
                            term=query)
    results = Entrez.read(handle)
    handle.close()
    return results 

def fetch_details(id_list):
    
    ids = ','.join(id_list)
    Entrez.email = 'your.email@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    handle.close()
    return results

def fetch_details_alternative(id_list):
    
    id_correct = []
    
    for ids in id_list:
        
        try:        
            Entrez.email = 'your.email@example.com'
            handle       = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
            results = Entrez.read(handle)
            id_correct.append(ids)
        except NotImplementedError:
            continue

            
    ids = ','.join(id_correct)
    Entrez.email = 'your.email@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results


def Perpare_Query(Gene_names):
    
    query_list = []
    counter    = 0
    
    for gene in Gene_names:
               
        string =  f'{gene}[Text Word])'
        query_list.append(string)
        
        if counter != len(Gene_names):
            query_list.append(string)
            query_list.append(" OR ")
            
def Query_Abstracts_Pubmed(genes_ass, Table_Ent2Gen):
    
    dic_results = dict.fromkeys(list(genes_ass.keys()))
    
    for axis in genes_ass.keys(): 
        
        print(axis)
        
        axis_it = list(genes_ass[axis])
        
        # Transform the genes to a gene name for the query:
        
        Gene_names = Table_Ent2Gen[Table_Ent2Gen["Entrez"].isin(axis_it)].Gene_name
        
        # Start quering the genes:
        
        dic_genes = {new_list: [] for new_list in list(Gene_names)}
        
           
        for gene in Gene_names:
            
            # Do the query:
            
            print(gene)
            
            query         = f'({gene}[Text Word]))'
            query_file    = search(query)                  
                              
            citation_list = query_file["IdList"]            
                       
            # Format the query:
            
            if len(citation_list) > 1:
                
                # 4 Tries just in case the server fails:
                
                for x in range(0, 4):
                    try:
                        papers    = fetch_details(citation_list) 
                        str_error = None
                    except Exception as HTTPError:
                        print("bruh")
                        str_error = str(HTTPError)
                    if str_error:
                        time.sleep(15)
                    else:
                        break
                             
                # Take the list of the abstracts:
                
                for i, paper in enumerate(papers['PubmedArticle']):
                    
                    try:
                        abstract = paper['MedlineCitation']['Article']['Abstract']['AbstractText']
                     
                        paragraph_list = []
                     
                         # Format the paragraphs of the abstracts to be all in the same one:
                         
                        for paragraph in abstract:
                            
                            paragraph_decode = str(paragraph)
                            paragraph_list.append(paragraph_decode)
                             
                        abstract_final = [' '.join(paragraph_list)]
                        
                        # Keep the information of the abstract:
                        
                        dic_genes[gene].append(abstract_final)
                         
                    except KeyError:                        
                        continue
               
        dic_results[axis] = dic_genes
        
    return(dic_results)

def Get_Abstracts_Axes(Species, matrix, annotations, dim, associations = "_Hard"):
    
    # Paths:
    
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    empty_path   = "/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/"
    
    # Load genes:
    
    genes = list(pd.read_table(f'{network_path}Genes_{Species}_PPI', header = None, dtype={0: str})[0])
    
    # Gene associations:
    
    genes_ass = Annotate_Genes_Axes_p(Species, dim, genes, network_path)
    
    # Read the Table to translate the gene names:
    
    Table_Ent2Gen = Ensembl_query_p(Species)
    
    # Do the query in PubMed:
    
    result = Query_Abstracts_Pubmed(genes_ass, Table_Ent2Gen)
    
    # Save the result:
    
    save = json.dumps(result)
    f    = open(f'{empty_path}Empty_Axes_Abstracts_{Species}_{matrix}_{annotations}_{dim}{associations}.json',"w")
    f.write(save)
    f.close()


def Perform_IDF_Axes_Abstracts(Species, matrix, annotations, dim, associations = "_Hard"):
        
    # Paths:
    
    empty_path   = "/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/"
    
    # Load the axes with the abstracts:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{Species}_{matrix}_{annotations}_{dim}{associations}.json',"r"))
    
    # load engligh dictionary:
    
    nltk.download('words')
    words = set(nltk.corpus.words.words())
    
    # Get the full dictionary from the abstracts:
    
    vocabulary = []
    
    for axis in axes_dic.keys(): 
        
        axis_it = axes_dic[axis] 
        
        for gene in axis_it.keys():
                       
            gene_axis = axis_it[gene] 
            
            for description in gene_axis:               
                try:
                    # Parse the xml:
                    
                    defin = description[0]
                    defin = np.char.replace(defin, "-", " ")
                    defin = np.char.replace(defin, ",", " ")
                    defin = np.char.replace(defin, ".", " ")
                    defin = np.char.replace(defin, ")", " ")
                    defin = np.char.replace(defin, "(", " ")
                    defin = np.char.replace(defin, "[", " ")
                    defin = np.char.replace(defin, "]", " ")
                    defin = np.char.replace(defin, "<i>", "")
                    defin = np.char.replace(defin, "</i>", "")
                    defin = np.char.replace(defin, "*", " ")
                    defin = np.char.replace(defin, "</sup>", "")
                    defin = np.char.replace(defin, "/", "")
                    defin = np.char.replace(defin, ":", " ")
                    defin = np.char.replace(defin, "<sup>", " ")
                    defin = np.char.replace(defin, "<", " ")
                    defin = np.char.replace(defin, ">", " ")
                    defin = np.char.replace(defin, "=", " ")
                    defin = np.char.replace(defin, '"', " ")
                    defin = np.char.replace(defin, '±', " ")
                    defin = np.char.replace(defin, '≥', " ")

                    vocabulary.append(str(defin))                    
                except KeyError:
                    continue
                    
    # Preprocess the vocabulary:
        
    vocabulary = [i.split() for i in vocabulary]
    vocabulary = [item for sublist in vocabulary for item in sublist]
    vocabulary = list(set(vocabulary))  
    vocabulary = [w for w in vocabulary if len(w) > 1] 
    vocabulary = [word for word in vocabulary if word.lower() not in nltk.corpus.stopwords.words('english')]  
    vocabulary = [word for word in vocabulary if not word.isdigit()]
    vocabulary = [word for word in vocabulary if word.lower() in words]
    vocabulary = list(set(vocabulary)) 
    
    # Get the corpus (each axis have its own corpus of words):
    
    corpus = []
    
    for axis in axes_dic.keys():        
        print(axis)
        axis_it   = axes_dic[axis] 
        corpus_it = []              
        for gene in axis_it.keys():           
            gene_axis = axis_it[gene]                       
            for description in gene_axis:        
                try:
                    # Parse the xml:
                    
                    defin = description[0]
                    defin = np.char.replace(defin, "-", " ")
                    defin = np.char.replace(defin, ",", " ")
                    defin = np.char.replace(defin, ".", " ")
                    defin = np.char.replace(defin, ")", " ")
                    defin = np.char.replace(defin, "(", " ")
                    defin = np.char.replace(defin, "[", " ")
                    defin = np.char.replace(defin, "]", " ")
                    defin = np.char.replace(defin, "<i>", "")
                    defin = np.char.replace(defin, "</i>", "")
                    defin = np.char.replace(defin, "*", " ")
                    defin = np.char.replace(defin, "</sup>", "")
                    defin = np.char.replace(defin, "/", "")
                    defin = np.char.replace(defin, ":", " ")
                    defin = np.char.replace(defin, "<sup>", " ")
                    defin = np.char.replace(defin, "<", " ")
                    defin = np.char.replace(defin, ">", " ")
                    defin = np.char.replace(defin, "=", " ")
                    defin = np.char.replace(defin, '"', " ")
                    defin = np.char.replace(defin, '±', " ")
                    defin = np.char.replace(defin, '≥', " ")

                    corpus_it.append(str(defin))                    
                except KeyError:
                    continue 
        
        # Preprocess the corpus:
               
        corpus_it = ' '.join(word for word in corpus_it)
        corpus_it = corpus_it.split()
        corpus_it = [w for w in corpus_it if len(w) > 1]
        corpus_it = [word for word in corpus_it if word.lower() not in nltk.corpus.stopwords.words('english')]
        corpus_it = [word for word in corpus_it if not word.isdigit()]
        corpus_it = [word for word in corpus_it if word.lower() in words]
            
        corpus.append(corpus_it)  
        
    # Save Corpus and Context to put in Marenostrum:
    
    with open(f'{empty_path}Corpus_{Species}_{dim}', "w") as fp:
        json.dump(corpus, fp)
    
    with open(f'{empty_path}Vocabulary_{Species}_{dim}', "w") as fp:
        json.dump(vocabulary, fp) 
        
    # Compute the TF-IDF:
    
    Final = pd.DataFrame(0, columns=list(axes_dic.keys()), index = list(vocabulary))
    
    for axis in range(len(axes_dic.keys())): 
        print(axis)
        sentence = corpus[axis]       
        if len(sentence) > 0:
            for word in vocabulary: 
                Final.loc[word, Final.columns[axis]] = tfidf(word, sentence, corpus)
        else:
            continue  
    
    # Save the TF-IDF:
    
    Final.to_csv(f'{empty_path}TF_IDF_Empty_{Species}_{matrix}_{annotations}_{dim}_{associations}.csv')
    
 
def Ensembl_query_Genes_Defint(Species):
    
    '''
    This fucntions checks if the file of Entrez2Gene exists and if not it generates it.
    '''   
    # Path:
    
    path = '/media/sergio/sershiosdisk/Axes_Species/BioMart/' 
       
    # Pandas with the transformation parameter:
    
    if os.path.exists(f'{path}Biomart_Defin_Gene_Human.txt') == False:
        
        # Connect to the server:
 
        server   = BiomartServer( "http://useast.ensembl.org/biomart")
    
        # Select the database:
    
        ensmbl   = server.datasets['hsapiens_gene_ensembl']
    
        # Query attributes:
    
        description_genes = ensmbl.search({'attributes': ['entrezgene_id', 'description']})
            
        with open(f'{path}Biomart_Defin_Gene_Human.txt', 'a') as the_file:
                    
            # Write the name of the columns:
                    
            the_file.write(f'Entrez\t')
            the_file.write(f'Description\n')   
                    
            # Start writting the file:
                    
            for line in description_genes.iter_lines():                        
                line = line.decode('utf-8')
                line = line.split("\t")  
                try:
                    the_file.write(f'{line[0]}\t')
                    line_1 = line[1]
                    line_1 = re.sub("[\(\[].*?[\)\]]", "", line_1)
                    the_file.write(f'{line_1}\n') 
                except IndexError:
                    the_file.write(f'{line[0]}\t')
                    the_file.write('\n')                     
        the_file.close()
        
        # Load the table:
        
        Table = pd.read_csv(f'{path}Biomart_Defin_Gene_Human.txt', sep = '\t', dtype={0: str})
        Table = Table.dropna()
        Table = Table.reset_index(drop = True)
        
        return(Table)
    
    else: 
        
        Table = pd.read_csv(f'{path}Biomart_Defin_Gene_Human.txt', sep = '\t', dtype={0: str})
        Table = Table.dropna()
        Table = Table.reset_index(drop = True)
        
        return(Table)


def Generate_TF_IDF_Genes(Species, matrix, annotations, dim, associations = "_Hard", Table = "biomart"):
    
    # Paths:
    
    empty_path   = "/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/"
    network_path = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    
    # Load stop words NCBI:
    
    list_stop = pd.read_csv("/media/sergio/sershiosdisk/Axes_Species/BioMart/stopwords_gene.txt", header = None)[0].to_list()
    
    # Load the axes with the abstracts:
    
    axes_dic = json.load(open(f'{empty_path}Empty_Axes_Abstracts_{Species}_{matrix}_{annotations}_{dim}{associations}.json',"r"))
    
    # Load the Table with definitions:
    
    Table_biomart = Ensembl_query_Genes_Defint(Species)   
    Table_ncbi    = pd.read_csv("/media/sergio/sershiosdisk/Axes_Species/BioMart/Homo_sapiens.gene_info", sep = "\t")
    Table_ncbi    = Table_ncbi[Table_ncbi["#tax_id"] == 9606]
    Table_ncbi    = Table_ncbi[["GeneID", "description"]]
    
    # Associate the genes to the axes:
      
    genes     = list(pd.read_table(f'{network_path}Genes_{Species}_PPI', header = None, dtype={0: str})[0])    
    genes_ass = Annotate_Genes_Axes_p(Species, dim, genes, network_path)
        
    # Get the full dictionary from the abstracts:
    
    vocabulary = []
    
    for axis in genes_ass.keys(): 
        
        print(axis)
        
        axis_it = genes_ass[axis] 
        
        for gene in axis_it:
            
            if Table == "biomart":           
                description_list = Table_biomart[Table_biomart.Entrez == gene]["Description"].to_list()
            else:
                description_list = Table_ncbi[Table_ncbi.GeneID == int(gene)]["description"].to_list()
                           
            for description in description_list:
        
                try:
                    # Parse the text:
                    
                    defin = description
                    defin = np.char.replace(defin, "-", " ")
                    defin = np.char.replace(defin, ",", " ")
                    defin = np.char.replace(defin, ".", " ")
                    defin = np.char.replace(defin, ")", " ")
                    defin = np.char.replace(defin, "(", " ")
                    defin = np.char.replace(defin, "[", " ")
                    defin = np.char.replace(defin, "]", " ")
                    defin = np.char.replace(defin, "*", " ")
                    defin = np.char.replace(defin, "/", "")
                    defin = np.char.replace(defin, ":", " ")
                    defin = np.char.replace(defin, "<", " ")
                    defin = np.char.replace(defin, ">", " ")
                    defin = np.char.replace(defin, "=", " ")
                    defin = np.char.replace(defin, '"', " ")
                    defin = np.char.replace(defin, '±', " ")
                    defin = np.char.replace(defin, '≥', " ")

                    vocabulary.append(str(defin))                    
                except KeyError:
                    print("BB")
                    continue
                
    # Preprocess the vocabulary:
        
    vocabulary = [i.split() for i in vocabulary]
    vocabulary = [item for sublist in vocabulary for item in sublist]
    vocabulary = list(set(vocabulary))  
    vocabulary = [w for w in vocabulary if len(w) > 2] 
    vocabulary = [word for word in vocabulary if word.lower() not in nltk.corpus.stopwords.words('english')]
    vocabulary = [word for word in vocabulary if word.lower() not in list_stop] 
    vocabulary = [word for word in vocabulary if not word.isdigit()]
    vocabulary = [word for word in vocabulary if not word.isupper()]
    vocabulary = list(set(vocabulary))  
    
    # Corpus:
    
    corpus = []
    
    for axis in genes_ass.keys():   
        axis_it   = genes_ass[axis] 
        corpus_it = []    
        print(axis)
        for gene in axis_it: 
            if Table == "biomart":           
                description_list = Table_biomart[Table_biomart.Entrez == gene]["Description"].to_list()
            else:
                description_list = Table_ncbi[Table_ncbi.GeneID == int(gene)]["description"].to_list()

            for description in description_list: 
                     
                try:
                    # Parse the text:
                    
                    defin = description
                    defin = np.char.replace(defin, "-", " ")
                    defin = np.char.replace(defin, ",", " ")
                    defin = np.char.replace(defin, ".", " ")
                    defin = np.char.replace(defin, ")", " ")
                    defin = np.char.replace(defin, "(", " ")
                    defin = np.char.replace(defin, "[", " ")
                    defin = np.char.replace(defin, "]", " ")
                    defin = np.char.replace(defin, "*", " ")
                    defin = np.char.replace(defin, "/", "")
                    defin = np.char.replace(defin, ":", " ")
                    defin = np.char.replace(defin, "<", " ")
                    defin = np.char.replace(defin, ">", " ")
                    defin = np.char.replace(defin, "=", " ")
                    defin = np.char.replace(defin, '"', " ")
                    defin = np.char.replace(defin, '±', " ")
                    defin = np.char.replace(defin, '≥', " ")
                    corpus_it.append(str(defin))                    
                except KeyError:
                    continue
    
        # Preprocess the corpus:
               
        corpus_it = ' '.join(word for word in corpus_it)
        corpus_it = corpus_it.split()
        corpus_it = [w for w in corpus_it if len(w) > 2]
        corpus_it = [word for word in corpus_it if word.lower() not in nltk.corpus.stopwords.words('english')]
        corpus_it = [word for word in corpus_it if word.lower() not in list_stop]
        corpus_it = [word for word in corpus_it if not word.isdigit()]
        corpus_it = [word for word in corpus_it if not word.isupper()]
            
        corpus.append(corpus_it) 


    # Save the corpus and the vocabulary (in NCBI we need to use the supercomputer):
    
    if Table != "biomart":
        
        print("Super computer is needed")
    
        with open(f'{empty_path}Corpus_{Species}_{dim}_{Table}', "w") as fp:
            json.dump(corpus, fp)
    
        with open(f'{empty_path}Vocabulary_{Species}_{dim}_{Table}', "w") as fp:
            json.dump(vocabulary, fp) 
    else:
                         
        # Compute the TF-IDF:
        
        Final = pd.DataFrame(0, columns=list(axes_dic.keys()), index = list(vocabulary))
        
        for axis in range(len(axes_dic.keys())): 
            print(axis)
            sentence = corpus[axis]       
            if len(sentence) > 0:
                for word in sentence: 
                    Final.loc[word, Final.columns[axis]] = tfidf(word, sentence, corpus)
            else:
                continue  
    
        # Save TF-IDF:
        
        Final.to_csv(f'{empty_path}Empty_TF_IDF_{Species}_MART.csv')
        
def Print_No_Annotated_Axes(species, matrix, annotations, dim, associations = "_Hard"):
    
    # Paths:

    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Load the axes associations:
    
    Axes = json.load(open(f'{Axes_Annot_Path}Associations_{species}_{matrix}_{annotations}_{dim}{associations}.json'))
    
    # Axes empty:
    
    empty_list = [key for key in Axes.keys() if not Axes[key]]
    
    print(empty_list)
    
def Give_Example_Axes_ENSEMBL(species, axis):
    
    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/"  
    
    # Load meaning of the axes:
    
    meaning = pd.read_csv(f'{Path_Meaning}Empty_TF_IDF_{species}_MART.csv', index_col = 0)
    
    # Choose the axis:
    
    meaning_axis = meaning[axis]
    meaning_axis = meaning_axis[meaning_axis > 0.0]
    
    print(meaning_axis)    
    
def Generate_Suppl_Table_Empty_Axes(species, dim, matrix, annotations, associations):
    
    # Paths:

    Path_Meaning    = "/media/sergio/sershiosdisk/Axes_Species/Empty_Axes/" 
    network_path    = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Load the axes associations:
    
    Axes = json.load(open(f'{Axes_Annot_Path}Associations_{species}_{matrix}_{annotations}_{dim}{associations}.json'))
    
    # Axes empty:
    
    empty_list = [key for key in Axes.keys() if not Axes[key]]
    
    # Load meaning of the axes:
    
    meaning = pd.read_csv(f'{Path_Meaning}Empty_TF_IDF_{species}_GENES2.csv', index_col = 0)
    
    # Associate genes to the axes:
    
    genes     = list(pd.read_table(f'{network_path}Genes_{species}_PPI', header = None, dtype={0: str})[0])    
    Axes      = Annotate_Genes_Axes_p(species, dim, genes, network_path)    
    
    # Load 
    
    # Start writting:

    with open(f'{Path_Meaning}Table_Empty_Axes_AxNotations_{species}_{dim}_GENES2.txt', 'a') as the_file:
        
        # write the name of the columns:
        
        the_file.write(f'Axis\t')
        the_file.write(f'& Terms\t')
        the_file.write(f'& \#Genes\t')      
        the_file.write(f'& \Empty\n') 
                       
        for d in meaning.columns:
            
            meaning_d = meaning[d]
            meaning_d = meaning_d[meaning_d > 0.0]
            meaning_d = meaning_d.sort_values(ascending = False)
            
            the_file.write(f'{d}\t')
            the_file.write(f'& ')
            
            for word in meaning_d.index:               
                the_file.write(f'{word}, ')
            the_file.write(f'\t')
            
            # Add the number of genes:
            
            the_file.write(f'& {len(Axes[int(d)])}\t')
            
            # Check if the axnotation existed:
            
            if d in empty_list:               
                the_file.write(f'& Yes \\')
                the_file.write(f'\\')
            else:
                the_file.write(f'& No \\') 
                the_file.write(f'\\')
            the_file.write(f'\n')
    the_file.close()    


def KMedoids_format(matrix, labels):
    
    # Calculate cosine distance:
    
    distance = pairwise_distances(matrix, metric="cosine")
    
    # Number of clusters (thum-rule):
    
    n_clusters        = round(math.sqrt(len(matrix)/2))
    
    # Compute the kmedoids using a precomputed distance matrix:
    
    clustering   = KMedoids(n_clusters=n_clusters, metric='precomputed',random_state=0).fit(distance)
    
    # Get the clusters:
    
    clusters_lab = clustering.labels_
    
    # Adapt it to the correct format:
    
    clusters = [[] for i in range(n_clusters)]
    n = len(clusters_lab)
    
    for i in range(n):
        clusters[clusters_lab[i]].append(labels[i])
    
    # Return:
    
    return clusters  


def Save_Enrichments(Species, dimensions, matrix = "PPMI", Orthonormal = True, annotation = "BP_Back"):
    
    # Paths:
    
    Path_Annotation  = "/media/sergio/sershiosdisk/Axes_Species/Annotations/"
    Path_Data        = "/media/sergio/sershiosdisk/Axes_Species/Data/"
    Deep_path        = "/media/sergio/sershiosdisk/Axes_Species/Deepwalk/"
    
    # Save Path:
    
    save = "/media/sergio/sershiosdisk/Axes_Species/Comparison_Methods/"
    
    # Generate the dictionary:
    
    # Start the computations:
        
    for org in Species:
        
        # Load the annotations:
        
        BP = json.load(open(f'{Path_Annotation}_BP_Back_{org}.json'))
        
        # Load genes:
               
        genes = list(pd.read_table(f'{Path_Data}Genes_{org}_PPI', header = None, dtype={0: str})[0])
        
        # Do the results dictionary:
        
        list_keys = []  
        for i in dimensions:      
            list_keys.append(f'{i}')
        Final = dict.fromkeys(list_keys)
        
        for dim in dimensions: 
            
            print(dim)
            
            # Load G matrices:

            if matrix == "Deepwalk":
                G_Matrix_sp = np.load(f'{Deep_path}Deepwalk_Coordinates_{org}_{dim}.npy', allow_pickle=True)
                name = "Deepwalk"
            else:
                if Orthonormal == True:
                    Path_Data_NMTF = Path_Data
                    P = np.load(f'{Path_Data_NMTF}P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                    U = np.load(f'{Path_Data_NMTF}U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)     
                    name = "PPMI"
                else:
                    Path_Data_NMTF = Path_Data
                    P = np.load(f'{Path_Data_NMTF}No_Orth_P_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)
                    U = np.load(f'{Path_Data_NMTF}No_Orth_U_Matrix_{dim}_PPI_{org}_{matrix}.npy', allow_pickle=True)   
                    name = "No_Orth_PPMI"
                
                G_Matrix_sp = P.dot(U)
            
            # Do the clustring:
            
            G_Cluster  = KMedoids_format(G_Matrix_sp, genes)
            
            # Do Enrichments of the clusters:
           
            Enrichments  = enrichment_analysis.enrichment_analysis(G_Cluster, BP, dim, save, 0.05, annotation, f'{org}', "PPMI", "terms")
            
            # Append the results to the dictionary:
                       
            Final[f'{dim}'] = list(Enrichments)
          
        # Saver the results:
    
        json_id = json.dumps(Final)
        f       = open(f'{save}GO_Enrichments_{org}_{name}.json',"w")
        f.write(json_id)

        # close file
        f.close()

def Give_Generic_Comparison_Methods(Species, embeddings, dimensions):
    
    # Paths:
    
    save            = "/media/sergio/sershiosdisk/Axes_Species/Comparison_Methods/"
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # write the columns:
    
    with open(save + "Comaprison_Generic.txt", 'a') as the_file:

        the_file.write("# Embedding"            + "\t") 
        the_file.write("# Times_More"           + "\t") 
        the_file.write("# Semantic_Axes"        + "\t") 
        the_file.write("# Semantic_Enrichments" + "\t") 
        the_file.write("# p-value"              + "\t")  
        the_file.write("# Jaccard_Index"        + "\t") 
        the_file.write("# Level_unique_Axes"    + "\t")
        the_file.write("# Level_unique_Axes"    + "\t")
        the_file.write("# Level_unique_Inter"   + "\n")
                       
        # Start the computations:
    
        for emb in embeddings:
            
            # Compute:
            
            ax_SS, ax_ER, pvalue    = Give_Semantic_Similatity(Species, emb, dimensions, save, Axes_Annot_Path)
            fold, ji                = Give_Amount_Info(Species, emb, dimensions, save, Axes_Annot_Path)
            lvl_ax, lvl_ER, lvl_int = give_levels(Species, emb, dimensions, save, Axes_Annot_Path)
            
            the_file.write(f'{emb}\t')
            the_file.write(f'{fold}\t')
            the_file.write(f'{ax_SS}\t')
            the_file.write(f'{ax_ER}\t')
            the_file.write(f'{pvalue}\t')
            the_file.write(f'{ji}\t')
            the_file.write(f'{lvl_ax}\t')
            the_file.write(f'{lvl_ER}\t')
            the_file.write(f'{lvl_int}\n')
            
    the_file.close()             
                     
def Calculater_Intra_Inter_SS(X, G):
    
    intra_distance = []

    # Intra cluster semantic similarity:
                    
    for cluster in X:
                        
        # Get the intra cluster distance:
                        
        GO_cluster_1 = cluster 
        
        cosas_ss = []
        
        for i in range(len(GO_cluster_1)):
            for j in range(i+1, len(GO_cluster_1)):
                try:
                    distance = similarity.lin(G, GO_cluster_1[i], GO_cluster_1[j]) 
                    cosas_ss.append(distance)

                except Exception as PGSSLookupError:
                        continue  
                    
        intra_distance.append(np.nanmean(cosas_ss))
     
    return(np.nanmean(intra_distance))            
            
    
def Give_Semantic_Similatity(Species, emb, dimensions, save, Axes_Annot_Path):
    
    G = graph.from_resource("go-basic")
    similarity.precalc_lower_bounds(G)
    
    final_semantic_ax =[]
    final_semantic_er =[]
    
    for org in Species:
        
        mean_dim_ax = []
        mean_dim_er = []
        
        for dim in dimensions:
            
            print(dim)
            
            # load the information:
            
            # Enrichments:
            
            f        = open (f'{save}GO_Enrichments_{org}_{emb}.json', "r")
            enrich   = json.loads(f.read())
            f.close()           
            enrich   = enrich[f'{dim}']
            enrich   = [x for x in enrich if x]
            
            # Axes:
            
            axis = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{emb}_BP_Back_{dim}_Hard.json'))           
            clusters =[]    
            for i in axis.keys():
                clusters.append(axis[i])   
            axis = [x for x in clusters if x]
            
            SS_enr = Calculater_Intra_Inter_SS(enrich,G)
            SS_ax  = Calculater_Intra_Inter_SS(axis,G)
            
            mean_dim_ax.append(SS_ax)
            mean_dim_er.append(SS_enr)
            
        final_semantic_ax.append(np.mean(mean_dim_ax))
        final_semantic_er.append(np.mean(mean_dim_er))
    
    # Significance:
    
    p_value_cancer  = mannwhitneyu(final_semantic_ax,  final_semantic_er,   alternative = "greater").pvalue
     
     # Show results:
     
    print(f'{emb} - SS_axes:{np.mean(final_semantic_ax)} | SS_enrich:{np.mean(final_semantic_er)} | p-value:{round(p_value_cancer, 3)}')
     
    return(np.mean(final_semantic_ax), np.mean(final_semantic_er),round(p_value_cancer, 3))      

def Give_Amount_Info(Species, emb, dimensions, save, Axes_Annot_Path):

    final_semantic_fold = []
    final_semantic_JI   = []
    
    for org in Species:
        
        mean_dim_fold = []
        mean_dim_JI   = []
        
        for dim in dimensions:
            
            # load the information:
            
            # Enrichments:
            
            f        = open (f'{save}GO_Enrichments_{org}_{emb}.json', "r")
            enrich   = json.loads(f.read())
            f.close()           
            enrich   = enrich[f'{dim}']
            enrich   = [x for x in enrich if x]
            
            # Axes:
            
            axis = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{emb}_BP_Back_{dim}_Hard.json'))           
            clusters =[]    
            for i in axis.keys():
                clusters.append(axis[i])   
            axis = [x for x in clusters if x]
            
            # Calculate the Fold:
            
            flat_axis   = set([item for sublist in axis for item in sublist])
            flat_enrich = set([item for sublist in enrich for item in sublist])
            
            fold = len(flat_axis)/len(flat_enrich)
            
            mean_dim_fold.append(fold)
            
            # Jaccard Index:
            
            ji = Jaccar_index(flat_axis, flat_enrich)
            mean_dim_JI.append(ji)
        
        final_semantic_fold.append(round(np.mean(mean_dim_fold), 3))
        final_semantic_JI.append(round(np.mean(mean_dim_JI), 3))

    return(np.mean(final_semantic_fold), np.mean(final_semantic_JI))        
            
def give_levels(Species, emb, dimensions, save, Axes_Annot_Path):
    
    levels_path      = "/media/sergio/sershiosdisk/Human/All_Annotation/"  
    Levels           = pd.read_csv(f'{levels_path}GO_Terms_Level_Matrix_BP_Back_Propagation.csv',
                        index_col = 0)
    Levels.index     = Levels.GO
    
    final_level_axes  = []
    final_level_enri  = []
    final_level_inter = []
    
    for org in Species:
        
        mean_dim_axes  = []
        mean_dim_erch  = []
        mean_dim_inter = []
        
        for dim in dimensions:
            
            # load the information:
            
            # Enrichments:
            
            f        = open (f'{save}GO_Enrichments_{org}_{emb}.json', "r")
            enrich   = json.loads(f.read())
            f.close()           
            enrich   = enrich[f'{dim}']
            enrich   = [x for x in enrich if x]
            
            # Axes:
            
            axis = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{emb}_BP_Back_{dim}_Hard.json'))           
            clusters =[]    
            for i in axis.keys():
                clusters.append(axis[i])   
            axis = [x for x in clusters if x]
            
            flat_axis   = set([item for sublist in axis for item in sublist])
            flat_enrich = set([item for sublist in enrich for item in sublist])
            
            # Sets:
            
            only_axes  = list(set(flat_axis)   - set(flat_enrich))
            only_enri  = list(set(flat_enrich) - set(flat_axis)) 
            onli_inter = list(set(flat_axis).intersection(set(flat_enrich)))
            
            # Get levels:
            
            lvl_ax  = Levels.loc[Levels.GO.isin(only_axes)].Level
            lvl_er  = Levels.loc[Levels.GO.isin(only_enri)].Level
            lvl_int = Levels.loc[Levels.GO.isin(onli_inter)].Level
            
            mean_dim_axes.append(np.mean(lvl_ax))
            mean_dim_erch.append(np.mean(lvl_er))
            mean_dim_inter.append(np.mean(lvl_int))         

        final_level_axes.append(round(np.mean(mean_dim_axes),3))
        final_level_enri.append(round(np.mean(mean_dim_erch),3))
        final_level_inter.append(round(np.mean(mean_dim_inter),3))        

    return(np.nanmean(final_level_axes), np.nanmean(final_level_enri), np.nanmean(final_level_inter))  



def Individual_Analysis(Species, embeddings, dimensions):
    
    # Paths:
    
    save            = "/media/sergio/sershiosdisk/Axes_Species/Comparison_Methods/"
    
    # write the columns:
    
    with open(save + "Comaprison_Specific.txt", 'a') as the_file:

        the_file.write("# Embedding"               + "\t")
        the_file.write("# Rand"                    + "\t") 
        the_file.write("# SS_Axes"                 + "\t") 
        the_file.write("# SS_Enrich"               + "\t")                       
        the_file.write("# Fold_Interaction"        + "\t")
        the_file.write("# Fold_Interaction_Total"  + "\n")
                     
        # Start the computations:
    
        for emb in embeddings:
            
            # Compute:
            
            auroc               = give_RAND(Species, dimensions, emb, save)
            fold_int, fold_glob = give_interactions_avg(Species, dimensions, emb, save)
            axis_SS, enrich_SS  = Give_SS_Intersection(Species, dimensions, emb, save)
            
            # write:
            
            the_file.write(f'{emb}\t')
            the_file.write(f'{auroc}\t')
            the_file.write(f'{axis_SS}\t')
            the_file.write(f'{enrich_SS}\t')
            the_file.write(f'{fold_int}\t')   
            the_file.write(f'{fold_glob}\n')    
                          
    the_file.close()             
            
def Give_SS_Intersection(Species, dimensions, emb, save):
    
    # Load Semantic to speed up
    
    Path_Semantic  = "/media/sergio/sershiosdisk/Axes_Species/Semantic_Similarity/"
    Semantic       = pd.read_csv(f'{Path_Semantic}Semantic_Human.csv', index_col = 0)
    
    # Load the Semantic to speed up:
    
    final_semantic_ss_axes  = []
    final_semantic_ss_enric = []
    
    for org in Species:
            
        ss_enrich = []
        ss_axes = []

        for dim in dimensions:
            
            print(dim)
                       
            # Load the genes:
            
            Enrichments = pd.read_csv(f'{save}Matrix_Enrichments_{emb}_{org}_{dim}.csv', index_col = 0)
            Axes        = pd.read_csv(f'{save}Matrix_Axes_{emb}_{org}_{dim}.csv',        index_col = 0)
            
            # Take only the intersection:
            
            GO_analisis = list(set(list(Axes.index)).intersection(set(list(Enrichments.index))))
            
            Enrichments = Enrichments.loc[GO_analisis,GO_analisis]
            Axes        = Axes.loc[GO_analisis,GO_analisis] 
                       
            GO_in_Matrix = list(set(Enrichments.index).intersection(set(Semantic.index)))
            
            Enrichments = np.array(Enrichments.loc[GO_in_Matrix, GO_in_Matrix])
            Axes        = np.array(Axes.loc[GO_in_Matrix       , GO_in_Matrix]) 
            Semantic_it = np.array(Semantic.loc[GO_in_Matrix,GO_in_Matrix])
            
            indices_Enr,  columns_Enr   = np.nonzero(Enrichments)
            indices_Axes, columns_Axes = np.nonzero(Axes)
            
            result_Ax  = Semantic_it[indices_Axes, columns_Axes]
            result_Enr = Semantic_it[indices_Enr, columns_Enr]
            
            ss_enrich.append(np.mean(result_Enr))
            ss_axes.append(np.mean(result_Ax))
        
        final_semantic_ss_axes.append(np.mean(ss_axes))
        final_semantic_ss_enric.append(np.mean(ss_enrich))

           
    return(round(np.nanmean(final_semantic_ss_axes), 3), round(np.nanmean(final_semantic_ss_enric), 3))


            
def give_RAND(Species, dimensions, emb, save):

    final_semantic_auroc =[]
    
    for org in Species:
        
        mean_dim_auroc = []
        
        for dim in dimensions:
                       
            # Load the genes:
            
            Enrichments = pd.read_csv(f'{save}Matrix_Enrichments_{emb}_{org}_{dim}.csv', index_col = 0)
            Axes        = pd.read_csv(f'{save}Matrix_Axes_{emb}_{org}_{dim}.csv',        index_col = 0)
            
            # Take only the intersection:
            
            GO_analisis = list(set(list(Axes.index)).intersection(set(list(Enrichments.index))))
            
            Enrichments = Enrichments.loc[GO_analisis,GO_analisis]
            Axes        = Axes.loc[GO_analisis,GO_analisis] 
            
            labels_true = Enrichments.values.ravel()
            labels_pred = Axes.values.ravel()

            # compute the Rand index
            
            rand_index = adjusted_rand_score(labels_true, labels_pred)
            
            mean_dim_auroc.append(rand_index)

        final_semantic_auroc.append(np.mean(mean_dim_auroc))
            
    return(round(np.mean(rand_index), 3)) 

            
def give_interactions_avg(Species, dimensions, emb, save):

   final_int        = []
   final_int_fold   = []
    
   for org in Species:
        
        mean_int          = []
        mean_int_fold     = []
        
        for dim in dimensions:
                       
            # Load the genes:
            
            Enrichments = pd.read_csv(f'{save}Matrix_Enrichments_{emb}_{org}_{dim}.csv', index_col = 0)
            Axes        = pd.read_csv(f'{save}Matrix_Axes_{emb}_{org}_{dim}.csv',        index_col = 0)
            
            # Global interactions
            
            mean_int_fold.append(round(Enrichments.sum().sum() / Axes.sum().sum(), 3))
            
            # Take only the intersection:
            
            GO_analisis = list(set(list(Axes.index)).intersection(set(list(Enrichments.index))))
            
            Enrichments = Enrichments.loc[GO_analisis,GO_analisis]
            Axes        = Axes.loc[GO_analisis,GO_analisis] 
            
            mean_int.append(round(Enrichments.sum().sum() / Axes.sum().sum(), 3))
            
            
        final_int.append(np.mean(mean_int))
        final_int_fold.append(np.mean(mean_int_fold))
                
   return(round(np.mean(final_int), 3), round(np.mean(final_int_fold), 3)) 


def Comparison_FMM(Species, embeddings, dimensions):
    
    # Paths:
    
    save            = "/media/sergio/sershiosdisk/Axes_Species/Comparison_Methods/"
    
    # write the columns:
    
    with open(save + "Comaprison_with_FMM.txt", 'a') as the_file:

        the_file.write("# Embedding"               + "\t")
        the_file.write("# AUROC Axes FMM"          + "\t") 
        the_file.write("# AUROC Enrich FMM"        + "\n") 

        # Start the computations:
    
        for emb in embeddings:
            
            # Compute:
            
            auroc_axes, auroc_enri = give_AUROC_FMM(Species, dimensions, emb, save)
            
            # write:
            
            the_file.write(f'{emb}\t')
            the_file.write(f'{auroc_axes}\t')
            the_file.write(f'{auroc_enri}\n')
                           
    the_file.close()    
            
def give_AUROC_FMM(Species, dimensions, emb, save):

    final_auroc_axes   = []
    final_auroc_enrich = []
    
    for org in Species:
        
        mean_auroc_axes = []
        mean_auroc_enri = []
        
        for dim in dimensions:
            
            print(f'{org}-{dim}-{emb}')
                       
            # Load the genes:
            
            Enrichments = pd.read_csv(f'{save}Matrix_Enrichments_{emb}_{org}_{dim}.csv', index_col = 0)
            Axes        = pd.read_csv(f'{save}Matrix_Axes_{emb}_{org}_{dim}.csv',        index_col = 0)
            FMM         = pd.read_csv(f'{save}FMM_{org}_{emb}_{dim}.csv',                index_col = 0) 
            FMM         = 1 - FMM
            
            # Compare axes with FMM:
            
            FMM_compare = FMM.loc[Axes.index, Axes.index]
            
            labels_true = Axes.values.ravel()
            labels_pred = FMM_compare.values.ravel()    
            
            fpr_axes,  tpr_axes,  thresholds_axes  = metrics.roc_curve(labels_true,  labels_pred,  pos_label=1)
            
            mean_auroc_axes.append(metrics.auc(fpr_axes, tpr_axes))
            
            # Compare enrich with FMM:
            
            FMM_compare = FMM.loc[Enrichments.index, Enrichments.index]
            
            labels_true = Enrichments.values.ravel()
            labels_pred = FMM_compare.values.ravel()    
            
            fpr_enri,  tpr_enri,  thresholds_enri  = metrics.roc_curve(labels_true,  labels_pred,  pos_label=1)
            
            mean_auroc_enri.append(metrics.auc(fpr_enri, tpr_enri))
            
        # save the info:

        final_auroc_axes.append(np.mean(mean_auroc_axes))
        final_auroc_enrich.append(np.mean(mean_auroc_enri))     
            
    return(round(np.mean(final_auroc_axes), 3), round(np.mean(final_auroc_enrich), 3)) 
        

def Prepare_Axes_SS(Species, dim_op, matrix = "PPMI", associations = "_Hard", annotations = "BP_Back"):
    
    # Paths:
    
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    save_path       = "/media/sergio/sershiosdisk/Axes_Species/Functional_Interactions/"
    
    # Prepare the Gene ontology to compute the Lin's SS:
    
    G = graph.from_resource("go-basic")
    similarity.precalc_lower_bounds(G)
    
    for org, dim in zip(Species,dim_op):
        
        # Load the axes associations:
        
        Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_{matrix}_{annotations}_{dim}{associations}.json'))
        Axes = {k: v for k, v in Axes.items() if v} 
        
        # Start the dictionary for the results:
        
        keys = []
        
        for dm in Axes.keys():
            keys.append(f'{dm}')
            
        Result = dict(([(key,0) for key in keys])) 
        
        # Compute the mean intra SS of each axis:
    
        for cluster in list(set(Axes.keys())):
            
            GO_cluster_1 = Axes[cluster]
            intra_dist   = []
            
            if len(GO_cluster_1) > 1:
            
                for i in range(len(GO_cluster_1)):
                    for j in range(i+1, len(GO_cluster_1)):
                            try:
                                distance = similarity.lin(G, GO_cluster_1[i], GO_cluster_1[j]) 
                                intra_dist.append(distance)
                
                            except Exception as PGSSLookupError:
                                continue 
                
                Result[f'{cluster}'] = np.nanmean(intra_dist)          
        
        # Filter empty axes (with no associated GO BP terms):
        
        Result = {k: v for k, v in Result.items() if v != 0} 
        Result= {k: v for k, v in Result.items() if math.isnan(v) == False}

        # Prepare the DataFrame to save the results:

        Result_dataFrame         = pd.DataFrame()
        Result_dataFrame["Axes"] = Result.keys()                         # Axes name
        Result_dataFrame["SS"]   = Result.values()                       # intra SS
        Result_dataFrame["GO"]   = [len(Axes[i]) for i in Result.keys()] # Number of GO terms  

        # Save the dataframe:
        
        Result_dataFrame.to_csv(f'{save_path}Matrix_SS_{org}.csv')

        
def Plot_Distribution_SS_axes(Species):            
        
    # Paths:
    
    save_path       = "/media/sergio/sershiosdisk/Axes_Species/Functional_Interactions/"  

    for org in Species:
        
        # Load the matrix with the SS:
        
        Matrix_SS = pd.read_csv(f'{save_path}Matrix_SS_{org}.csv', index_col = 0)
        
        # Take the percentile:
        
        SS_threshold = np.percentile(Matrix_SS.SS, 5)
        
        # Do the plot:
        
        plt.style.use("seaborn-whitegrid")
        
        fig, axes = plt.subplots(figsize=(10,5))
             
        plt.hist(Matrix_SS.SS, bins = 50)
        axes.set_ylabel("Axes", fontsize=20, fontweight='bold') 
        axes.set_xlabel("SS", fontsize=20, fontweight='bold') 
        axes.spines['right'].set_visible(False)
        axes.spines['top'].set_visible(False)
        axes.spines['left'].set_linewidth(4)
        axes.spines['bottom'].set_linewidth(4)
        axes.spines['left'].set_color("grey")
        axes.spines['bottom'].set_color("grey") 
        plt.axvline(x=SS_threshold, color = "red")
        
        plt.savefig(f'{save_path}Distribution_SS_{org}.png', format="png", dpi=600,
                    bbox_inches='tight')
                
def clust(GO_min, G):
    
    matrix = pd.DataFrame(0, columns = GO_min, index = GO_min)
    
    for i in GO_min:
        for j in GO_min:
            matrix.loc[i,j] =similarity.lin(G, i, j)
    
    return(matrix)
                               
           
def Plot_Heatmaps_SS_axes(Species, dim_op):  

    # Paths:      
        
    save_path       = "/media/sergio/sershiosdisk/Axes_Species/Functional_Interactions/" 
    Axes_Annot_Path = "/media/sergio/sershiosdisk/Axes_Species/Axes_Annotations/"
    
    # Prepare the ontology:

    G = graph.from_resource("go-basic")
    similarity.precalc_lower_bounds(G)
    
    # Start the plots:

    for org, dim in zip(Species,dim_op):
        
        # Load the matrix with the SS:
        
        Matrix_SS = pd.read_csv(f'{save_path}Matrix_SS_{org}.csv', index_col = 0)
        
        # Load the Axes associations:
        
        Axes = json.load(open(f'{Axes_Annot_Path}Associations_{org}_PPMI_BP_Back_{dim}_Hard.json'))
        
        # Take the percentile:
        
        SS_threshold = np.percentile(Matrix_SS.SS, 5) 
        
        # Choose the Axes that are in that percentile:
        
        Axes_perc = list(Matrix_SS[Matrix_SS.SS <= SS_threshold]["Axes"])
        
        Matrix_SS_lin = Matrix_SS[Matrix_SS.SS <= SS_threshold]["SS"]
        
        print(Matrix_SS_lin.mean())
        
        
        # Start the plots:
        
        plt.style.use("seaborn-whitegrid")
        plt.rcParams.update({'font.size': 25})
        plt.rc('font', weight='bold')  
            
        for axis in Axes_perc:
            
            GO_min       = Axes[str(axis)]
            matrix_clust = clust(GO_min, G)
            
            linkage    = hc.linkage(sp.distance.squareform(1 -matrix_clust),method='average')
            cg         = sns.clustermap(round(matrix_clust, 2), cmap="coolwarm", row_linkage=linkage, col_linkage=linkage, 
                            tree_kws=dict(linewidths=4, colors=(0.2, 0.2, 0.4)),
                             vmin=0, vmax=1, linewidths=3, annot = True, xticklabels=False)   
            cg.cax.remove()
            cg.ax_row_dendrogram.set_visible(False) #suppress row dendrogram
            cg.ax_col_dendrogram.set_visible(False) 
            
            
            plt.setp(cg.ax_heatmap.get_yticklabels(), rotation=0, size = 25)  # For y axis
            plt.setp(cg.ax_heatmap.get_xticklabels(), rotation=90) # For x axis
            
            # Save:
            
            path_it = f'{save_path}Heatmap_{org}/'
            
            fig = plt.gcf()
            fig.savefig(f'{path_it}Axis_{axis}.png', dpi=600, format="png", bbox_inches='tight')
            

            
            
            

                    
        
        
        











