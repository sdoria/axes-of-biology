'''
This Script contains all the functions needed to perform the taxon study in human
'''

# Packages:

from goatools import obo_parser
import pandas as pd
import numpy as np
import json


# Functions:

def Create_whole_Taxons(GO_path, obo_path):
    
    '''
    This functions generates a dictionary with all the GO terms (NCBI) with the corresponding taxons
    
    Input:
            - GO_path     : String, path tho the gene2go (NCBI) file.
            - obo_path    : String, path tho the obo file.
    '''

    # Load the files:
    
    GO_db  = pd.read_table(GO_path)
    go_dag = obo_parser.GODag(obo_path)

    # Filter the file:
    
    GO_db = GO_db[["#tax_id","GeneID", "GO_ID", "Evidence", "Category"]]
    GO_db["GeneID"] = GO_db["GeneID"].astype(str) 
    
    # Filter them by evidence and category (BP in this case):
    
    GO_db_filtered    = GO_db[GO_db['Evidence'].isin(['EXP', 'IDA', 'IPI', 'IMP', 'IGI', 'IEP'])]
    GO_db_filtered_BP = GO_db_filtered[GO_db_filtered['Category'].isin(['Process'])]
    
    # Get the dic
    
    taxon_dictionary = dict(GO_db_filtered_BP.groupby('GO_ID')['#tax_id'].apply(set))
    taxon_dictionary = {key:list(values) for key,values in taxon_dictionary.items()}
    
    # Back Propagation: #
    
    # Prepare the GO terms:
    
    GO_list   = taxon_dictionary.keys()
    common_GO = set(GO_list).intersection(set(go_dag.keys()))
    
    # Inizializate the final dictionary:
    
    dict_back = {}
    
    # Start the back propagation:
    
    # First loop:
    
    for GO in common_GO:
        
        dict_back.setdefault(GO,[]) 
        
        parents = pd.DataFrame([(parent_id) for parent_id in go_dag[GO].get_all_parents()
                               if go_dag[parent_id].namespace=='biological_process'])
        
        # Add parents to the dictionary same taxons as sons:
        
        for key in parents[0]:
            dict_back.setdefault(key,[]) 
            
    # Second loop:
    
    for GO in common_GO:
        
        dict_back[GO].extend(taxon_dictionary[GO]) 
        
        # Parents:
        
        parents = pd.DataFrame([(parent_id) for parent_id in go_dag[GO].get_all_parents()
                               if go_dag[parent_id].namespace=='biological_process'])
        
        # Add parents to the dictionary same taxons as sons:
        
        for key in parents[0]:
            dict_back[key].extend(taxon_dictionary[GO])
            
    # Create the
     
    dict_back = {key:list(set(values)) for key,values in dict_back.items()}    
    
    # Save the dictionary:
    
    with open("/media/sergio/sershiosdisk/Human/Axes/" + 'GO_Taxons_Whole_Annotations.json', 'w') as fp:
        json.dump(dict_back, fp) 
    
    
def Create_P_value_Dist(Taxons_Dic, k, file_path , network = "PPI", matrix = "PPMI"):
    
    '''
    This function gives back a distribution with the p-values and corrected p-values for GO terms that have a concret number of
    taxons.
    
    Inputs:
            - Taxons_Dic : dictionary, keys are GO terms, values are the taxons (generated from Create_whole_Taxons function).
            - k          : int, number of dimensions.   
            - file_path  : String, path to the folder with the matrices of p-values and corrected p-values.
            - network    : String, network that is gonna be abalized (protein-protein interaction, PPI, by default).
            - matrix     : String, matrix used for the decomposition (Adj : adjancency, PPMI by default).
    '''
    
    # Load the p-values matrices:
    
    P_values           = pd.read_csv(file_path + "P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    Corrected_P_values = pd.read_csv(file_path+ "Adjusted_P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    
    # Subset the Taxons_Dic:
    
    GO_human = P_values.columns
    Taxons_Dic_Filt = {key : values for key,values in Taxons_Dic.items() if key in GO_human}
    
    # Get the number of taxons per each:
    
    Distribution        = [len(item) for item in Taxons_Dic_Filt.values()]
    Taxons_Dic_Filt_num = {}
    i = 0
    
    for key in Taxons_Dic_Filt.keys():
        Taxons_Dic_Filt_num[key] = Distribution[i]
        i +=1
        
    # Create a list with the dictionary:
    
    Taxons_db = pd.DataFrame.from_dict(Taxons_Dic_Filt_num, orient='index')
    
    ## Distributions ##
        
    # For normal p-values distribution:
    
    Result_Normal_P_values = {}
    
    for taxons in set(Taxons_db[0]):
        
        # GO terms with this specificity:
        
        GO_filt = Taxons_db[Taxons_db[0] == taxons]
        GO_filt = list(GO_filt.index)
        
        # Get the p-values of this GO_filt:
        
        P_values_filt = P_values[GO_filt]
        
        # Fit the Dataframe:
        
        Result_Normal_P_values[taxons] = [item for sublist in P_values_filt.values for item in sublist]
        
    # For corrected P-values:
        
    Result_Corrected_P_values = {}
    
    for taxons in set(Taxons_db[0]):
        
        # GO terms with this specificity:
        
        GO_filt = Taxons_db[Taxons_db[0] == taxons]
        GO_filt = list(GO_filt.index)
        
        # Get the p-values of this GO_filt:
        
        P_values_filt = Corrected_P_values[GO_filt]
        
        # Fit the Dataframe:
        
        Result_Corrected_P_values[taxons] = [item for sublist in Corrected_P_values.values for item in sublist]   
    
    # Save Both DataFrames:
    
    with open(file_path + "Taxon_P_vale_Normal_Distribution_"+ str(network) + "_" + str(matrix) + "_" + 
                                   str(k) + ".json", 'w') as fp:
        json.dump(Result_Normal_P_values, fp)
    with open(file_path + "Taxon_P_vale_Corrected_Distribution_"+ str(network) + "_" + str(matrix) + "_" + 
                                   str(k) + ".json", 'w') as fp:
        json.dump(Result_Normal_P_values, fp)
    


def Count_GO_Terms(Taxons_Dic, k, file_path , network = "PPI", matrix = "PPMI", filt = 1):        
 
    '''
    This function gives you back a count of how many terms per each taxon you have associated before and after the correction:
        
    Inputs:
                - Taxons_Dic : dictionary, keys are GO terms, values are the taxons (generated from Create_whole_Taxons function).
                - k          : int, number of dimensions.   
                - file_path  : String, path to the folder with the matrices of p-values and corrected p-values.
                - network    : String, network that is gonna be abalized (protein-protein interaction, PPI, by default).
                - matrix     : String, matrix used for the decomposition (Adj : adjancency, PPMI by default).
                - filt       : int, threshold to keep the GO terms enriched for a Jaccard Index analyses.
    '''       
 
    # Load the p-values matrices:
    
    P_values           = pd.read_csv(file_path + "P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    Corrected_P_values = pd.read_csv(file_path+ "Adjusted_P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    
    # Subset the Taxons_Dic:
    
    GO_human = P_values.columns
    Taxons_Dic_Filt = {key : values for key,values in Taxons_Dic.items() if key in GO_human}
    
    # Get the number of taxons per each:
    
    Distribution        = [len(item) for item in Taxons_Dic_Filt.values()]
    Taxons_Dic_Filt_num = {}
    i = 0
    
    for key in Taxons_Dic_Filt.keys():
        Taxons_Dic_Filt_num[key] = Distribution[i]
        i +=1
        
    # Create a list with the dictionary:
    
    Taxons_db = pd.DataFrame.from_dict(Taxons_Dic_Filt_num, orient='index')

    ## Counts ##
        
    # For normal p-values distribution:
    
    Result_Normal_P_values             = pd.DataFrame()
    Result_Normal_P_values_Percentages = pd.DataFrame()
    
    for taxons in set(Taxons_db[0]):
        
        # Reestar Counter:
        
        Counter = 0
        
        # GO terms with this specificity:
        
        GO_filt = Taxons_db[Taxons_db[0] == taxons]
        GO_filt = list(GO_filt.index)
        
        # Get the p-values of this GO_filt:
        
        P_values_filt = P_values[GO_filt]
        
        # Fit the Dataframe:
        
        for column in P_values_filt.columns:
            if sum(P_values_filt[column] <= 0.05) >=1:
                Counter +=1
        
        # Keep the information:
                
        Result_Normal_P_values[taxons]              = [Counter]
        Result_Normal_P_values_Percentages[taxons]  = [Counter/len(GO_filt)]

    # For Corrected p-values distribution:
    
    Result_Corrected_P_values             = pd.DataFrame()
    Result_Corrected_P_values_Percentages = pd.DataFrame()
    
    # To save the GO terms
    
    GO_terms = []
    
    for taxons in set(Taxons_db[0]):
        
        # Reestar Counter:
        
        Counter = 0
        
        # GO terms with this specificity:
        
        GO_filt = Taxons_db[Taxons_db[0] == taxons]
        GO_filt = list(GO_filt.index)
        
        # Get the p-values of this GO_filt:
        
        P_values_filt = Corrected_P_values[GO_filt]
        
        # Fit the Dataframe:
        
        # Get a list with the GO terms that are chosen by the filt argument.
        
        for column in P_values_filt.columns:
            if sum(P_values_filt[column] <= 0.05) >=1:
                if taxons <= filt:
                    GO_terms.append(column)
                Counter +=1
        
        # Keep the information:
                
        Result_Corrected_P_values[taxons] = [Counter]
        Result_Corrected_P_values_Percentages[taxons]  = [Counter/len(GO_filt)]
        
    # Put all together:
    
    Final_Result = Result_Normal_P_values.append(Result_Normal_P_values_Percentages)
    Final_Result = Final_Result.append(Result_Corrected_P_values)
    Final_Result = Final_Result.append(Result_Corrected_P_values_Percentages)
    
    Final_Result.index = ["Normal", "Perc_Normal", "Corrected", "Perc_Corrected"]
    
    # Save the information:
    
    return(Final_Result)
    
    '''
    Final_Result.to_csv(file_path + "Statistics_Taxons_Comparison"+ str(network) + "_" + str(matrix) + "_" + 
                                   str(k) + ".csv")
    '''
        
def Get_Jaccard(GO_terms1, GO_terms2):
    
        '''
        
        This functions gives the Jaccard index between two GO terms set.
        
        Input:
            
                - GO_terms1: list, GO terms with taxon 1 enriched.
                - GO_terms2: list, GO terms with taxon 1 enriched.
            
        '''
        
        # Get the set:
        
        GO_terms1 = set(GO_terms1)
        GO_terms2 = set(GO_terms2)
        
        # Calculate Jaccard index:
        
        intersection = len(GO_terms1.intersection(GO_terms2))
        union = (len(GO_terms1) + len(GO_terms2)) - intersection
        
        # Return the results:
        
        return float(intersection) / union

def Get_Level_GO_Terms(Matrix_Gene_GO_BP, annotation = "BP_Back_Propagation", Specie = "Human"): 
    
    '''
    This function produces a DataFrame with the GO terms and its corresponding level in the ontology:
    
    Inputs:
            - Matrix_Gene_GO_BP : Pandas DataFrame, gene/GO assotiation binary matrix.
            - annotation        : String, annotation that is gonna be used (BP_Back_Propagation by default)
            - Specie            : String, specie that is under study (Human by default)
    '''
    
    # Prepare the Goatools:
    
    GO_file = "/media/sergio/sershiosdisk/Axes_Species/Annotations/go-basic.obo"
    go           = obo_parser.GODag(GO_file)
    
    # Get the list of the GO terms:
    
    GO_terms = Matrix_Gene_GO_BP.columns
   
    # Get the levels of these GO terms form the obo file:
    
    levels_list = []
    GO_list     = []
    for GO in GO_terms:
        try:
            levels_list.append(go[GO].level)
            GO_list.append(GO)
        except KeyError:
            continue
        
    # Produce the DataFrame with the information:
    
    Result = pd.DataFrame({ "GO" : GO_list, "Level" :levels_list})
    
    return(Result)
    
    # Save the Result in the corresponding folder:
    
    if Specie == "Human":
    
        Result.to_csv("/media/sergio/sershiosdisk/Human/All_Annotation/GO_Terms_Level_Matrix_" +  str(annotation) + ".csv", 
                      header = True, index=True)
    else: 
        Result.to_csv("/home/sergio/Project_2/Data/All Annotation/GO_Terms_Level_Matrix_" +  str(annotation) + ".csv", 
                      header = True, index=True)
        
def Define_Leaf_Annotations_V2(Matrix_Gene_GO_BP):
    
    '''
    This function produces cuts the ontology to keep only those GO terms that doesn´t have childs or that the
    childs are not described in human.
    
    Inputs:
            - Matrix_Gene_GO_BP : Pandas DataFrame, gene/GO assotiation binary matrix.
            
    '''
    
    # Prepare the Goatools:
    
    GO_file = "/home/sergio/workspace_Eclipse/Lucky_GOGO_Extra/Results/Python_Srcipts/go-basic.obo"
    go           = obo_parser.GODag(GO_file) 
    
    # Get the list of the GO terms:
    
    GO_terms = Matrix_Gene_GO_BP.columns
    
    # Get the GO terms:
    
    GO_list = []
    
    for GO in GO_terms:
        try:
            # If does not have a child is directly specific:
            if len(go[GO].children) == 0:
                GO_list.append(GO)
            # If it has we have to check if the child is in our annotation:
            else:
                # Get the childs:
                child = list(go[GO].get_all_children())
                # Check the childs
                if sum(Matrix_Gene_GO_BP.columns.isin(child)) == 0:
                    GO_list.append(GO)
        except KeyError:
            continue
        
    # Give Back the list:
    
    return(pd.DataFrame(0, index=np.arange(1), columns=GO_list))
        
def Define_Leaf_Annotations(Matrix_Gene_GO_BP):
    
    '''
    This function produces cuts the ontology to keep only those GO terms that doesn't have childs.
    
    Inputs:
            - Matrix_Gene_GO_BP : Pandas DataFrame, gene/GO assotiation binary matrix.
            
    '''
    
    # Prepare the Goatools:
    
    GO_file = "/home/sergio/workspace_Eclipse/Lucky_GOGO_Extra/Results/Python_Srcipts/go-basic.obo"
    go           = obo_parser.GODag(GO_file) 
    
    # Get the list of the GO terms:
    
    GO_terms = Matrix_Gene_GO_BP.columns
    
    # Get the GO terms:

    GO_list = []
    
    for GO in GO_terms:
        try:
            if len(go[GO].children) == 0:
                GO_list.append(GO)
        except KeyError:
            continue
        
    # Give Back the list:
    
    return(pd.DataFrame(0, index=np.arange(1), columns=GO_list))
    
    
def Count_Levels(k, Specific, General, file_path, annotation = "BP_Back_Propagation", network = "PPI", matrix = "PPMI", Specie = "Human"):

    '''
    This function return the percentage of specific and general GO terms (based on its levels of the ontology), that the embedding
    is capturing.
    
    Input:
          - k          : int, number of dimensions. 
          - Specific   : int, level threshold that the user consider as specific.
          - General    : int, level threshold that the user consider as general.
          - annotation : String, annotation that is gonna be used (BP_Back_Propagation by default).
          - file_path  : String, path to the folder with the matrices of corrected p-values.
          - network    : String, network that is gonna be abalized (protein-protein interaction, PPI, by default).
          - matrix     : String, matrix used for the decomposition (Adj : adjancency, PPMI by default).
          - Specie     : String, specie that is under study (Human by default)

    '''
    
    # Load the GO/Level DataFrame:
    
    if Specie == "Human":
    
        GO_Levels = pd.read_csv("/media/sergio/sershiosdisk/Human/All_Annotation/GO_Terms_Level_Matrix_" +  str(annotation) + ".csv",
                            index_col = 0, dtype={0: str})
    else:
        GO_Levels = pd.read_csv("/home/sergio/Project_2/Data/All Annotation/GO_Terms_Level_Matrix_"  +  str(annotation) + ".csv",
                            index_col = 0, dtype={0: str})
        
    
    # Load the corrected P-values:
    
    Corrected_P_values = pd.read_csv(file_path+ "Adjusted_P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    
    # Totatal number of GO terms that are Specific and General:
    
    Specific_Total = len(GO_Levels[GO_Levels.Level >= Specific])
    General_Total  = len(GO_Levels[GO_Levels.Level <= General])
    Other_Total    = len(GO_Levels[(GO_Levels.Level < Specific) & (GO_Levels.Level > General)])
    
    # Calculate the percentage that are captured for the corresponding number of dimensions:
    
    Specific_Count = 0
    Generic_Count  = 0
    Other_Count    = 0
    
    # For GO terms:
    
    GO = []
    
    for column in Corrected_P_values.columns: 
        
            # If the GO terms is significant in at least one dimension:
            
            if sum(Corrected_P_values[column] <= 0.05) >=1:
                
                # Get its level:
                
                try:
                    Level_iter = int(GO_Levels[GO_Levels.GO == column].Level)
                    
                    # Count the total number of these GO terms that are specific or general:
                    
                    if Level_iter >= Specific:
                        Specific_Count +=1
                        # Save the GO term
                        GO.append(column)
                    elif Level_iter <= General:
                        Generic_Count  += 1
                    else:
                        Other_Count += 1
                except TypeError:
                    print(str(column) + " don't match")
                    continue
    
    # Calculate the percentage:
    
    Specific_perc = Specific_Count/Specific_Total
    Generic_perc  = Generic_Count/General_Total
    Other_perc    = Other_Count/Other_Total
    
    # Give back the result:
    
    print( "% of Specific is: "  + str(Specific_perc) + "\n" +
           "% of Generic  is: " + str(Generic_perc) + "\n"  +
           "% of Other is: " + str(Other_perc))
    
    # Return the list of GO terms:
    
    return(GO)
    
    
def Count_Leaf(k, Leafs, GO_Levels, file_path, annotation = "BP_Back_Propagation", network = "PPI", matrix = "PPMI", Specie = "Human", level = 3):
    
    '''
    This function count the percentage of specific terms considering as specific those that are described as leaf annotations:
        
    Input:
          - k          : int, number of dimensions. 
          - Leafs      : DataFrame, with the GO/gene matrix of the Leaf annotations.
          - GO_Levels  : DataFrame, levels of the GO terms obtained after using the Count_Levels function.
          - annotation : String, annotation that is gonna be used (BP_Back_Propagation by default).
          - file_path  : String, path to the folder with the matrices of corrected p-values.
          - network    : String, network that is gonna be abalized (protein-protein interaction, PPI, by default).
          - matrix     : String, matrix used for the decomposition (Adj : adjancency, PPMI by default).
          - Specie     : String, specie that is under study (Human by default)
          - level      : int, level in which the user consider that the GO terms are general.

    '''
    
    # Load the corresponding Matrices:
    
    if Specie == "Human":
        # P_values:
        
        p_values = pd.read_csv(file_path+ "Adjusted_P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
    else:
        # P_values:
        
        p_values = pd.read_csv(file_path+ "Adjusted_P_value_"+ str(network) + "_" + str(matrix) + "_" + str(k) + ".csv",
                                     index_col = 0, dtype={0: str})
        
    # Prepare the Leafs:
    
    Leaf_GO_list  = Leafs.columns[Leafs.columns.isin(p_values.columns)]
    Leaf_GO_Total = len(Leaf_GO_list)
    
    # Choose the Generic Annotations (those that are equal or less to a level and they are not in the Leaf list):
    
    Generic_list = p_values.columns[~(p_values.columns).isin(Leaf_GO_list)] # 1st, not a leaf.
    Generic_list = GO_Levels[(GO_Levels.GO.isin(Generic_list))] # 2nd, specific level.
    Generic_list = Generic_list[Generic_list.Level <= level].GO
    Generic_Total = len(Generic_list)
    
    # Other:
    
    Other_Total = abs((Generic_Total + Leaf_GO_Total) + len(p_values.columns))
    
    # Prepare the variables for the study:
    
    Leaf_Count    = 0
    Generic_Count = 0
    Other_Count   = 0
    
    # For getting the GO terms:
    
    GO = []
    
    # Count the GO terms:
    
    for column in p_values.columns: 
    # If the GO terms is significant in at least one dimension:
            if sum(p_values[column] <= 0.05) >=1:
                # If the GO term is a Leaf GO term then count 1:
                if sum(column == Leaf_GO_list)>=1:
                    Leaf_Count += 1
                    GO.append(column)
                # If the GO terms is a Generic Term then count 1:
                elif sum(column == Generic_list)>=1:
                    Generic_Count += 1
                else:
                    Other_Count += 1
    
    # Calculate the percentages:
    
    Leaf_Percentage    = Leaf_Count/Leaf_GO_Total
    Generic_Percentage = Generic_Count/Generic_Total
    Other_Percentage   = Other_Count/Other_Total
    
    # Return the information:
    
    return(Leaf_Percentage, Generic_Percentage)
    
    print( "% of Specific is: "  + str(Leaf_Percentage) + "\n" +
           "% of Generic  is: "  + str(Generic_Percentage) + "\n" +
           "% of Other is: "     + str(Other_Percentage))
    
    # Return the GO terms:
    
    return(GO)
    
    
def Mean_Leaf_Taxon(Taxons_Dic, Leaf_Annotation, GO_Levels, file_path, Specie = "Human", level_filt = 5): 
    
    # Save a different variable:
    
    Leafs = Leaf_Annotation.copy()
    
    # Load the corresponding Matrices (for this experiment does not matter the dimensions, we pick one random):
    
    if Specie == "Human":
        # P_values:
        p_values = pd.read_csv("/media/sergio/sershiosdisk/Human/Axes/P_Values/60000/Adjusted_P_value_PPI_Adj_48.csv",
                                     index_col = 0, dtype={0: str})
    else:
        # P_values:
        p_values = pd.read_csv("/home/sergio/Project_2/Axes_Study/All_Annotation/6000/Adjusted_P_value_PPI_Adj_81.csv",
                                     index_col = 0, dtype={0: str})
    
    # Prepare variables:
    
    level = []
    Taxon = []
    
    # Filter Leaf Annotations:
    
    Leaf_Annotation_filt = Leaf_Annotation.columns[Leaf_Annotation.columns.isin(Taxons_Dic.keys())]
    
    # Calculate the Taxon File:
    
    for go in Leaf_Annotation_filt:
        
        try:
            level.append(int(GO_Levels[GO_Levels["GO"] == go].Level))
            Taxon.append(len(Taxons_Dic[go]))
        except KeyError:
            continue
    
    # Calculate the mean:
    
    mean_Taxons_Leaf = np.array(Taxon).mean()
    
    # Calculate the mean of the Generics:
    
    # Prepare the Leafs:
    
    Leaf_GO_list  = Leafs.columns[Leafs.columns.isin(p_values.columns)]
    
    # Choose the Generic Annotations (those that are equal or less to a level and they are not in the Leaf list):
    
    Generic_list = p_values.columns[~(p_values.columns).isin(Leaf_GO_list)] # 1st, not a leaf.
    Generic_list = GO_Levels[(GO_Levels.GO.isin(Generic_list))] # 2nd, specific level.
    Generic_list = Generic_list[Generic_list.Level <= level_filt].GO
    
    Generic_GO_Filt = Generic_list[Generic_list.isin(Taxons_Dic.keys())]
    
    # Get the info for the generic ones:
    
    level_general = []
    Taxon_general = []
    
    for go in Generic_GO_Filt:
        
        try:
            level_general.append(int(GO_Levels[GO_Levels["GO"] == go].Level))
            Taxon_general.append(len(Taxons_Dic[go]))
        except KeyError:
            continue
    
    # Calculate the mean of the taxons for the generic:
    
    mean_Taxons_Generic = np.array(Taxon_general).mean()
    
    # Return the results:
    
    print("The mean for Taxons in Leaf is: "   + str(mean_Taxons_Leaf) + "\n" 
          "The mean for Taxons in Generic is: "+ str(mean_Taxons_Generic))
    
    
          

               
                    
           


    
    
    
    
    
    
    
    
    
    
    

                
          
    
    
    
    


  

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    